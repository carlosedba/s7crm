#!/usr/bin/env node
const Promise = require('bluebird')
const fs = Promise.promisifyAll(require('fs'))
const path = require('path')
const { nanoid } = require('nanoid')
const mime = require('mime-types')
const update = require('immutability-helper')

const DATA_DIR_EMPRESAS = path.resolve(__dirname, 'data/empresas')
const DATA_DIR_PROFISSIONAIS = path.resolve(__dirname, 'data/profissionais')
const DATA_DIR_PROJETOS = path.resolve(__dirname, 'data/projetos')

const PUBLIC_TEMP = path.resolve(__dirname, 'public/temp')

console.log(DATA_DIR_EMPRESAS)
console.log(DATA_DIR_PROFISSIONAIS)
console.log(DATA_DIR_PROJETOS)
console.log(PUBLIC_TEMP)

main()

async function main() {
  const dirContent = await readPath('./data/empresas/123').catch(console.error)

  console.log(dirContent)
}

async function getFileStats(path) {
  const stat = await fs.statAsync(path).catch(console.error)

  return stat
}

async function readPath(dir) {
  let dirContent = []

  const files = await fs.readdirAsync(dir, {}).catch(console.error)

  for (const file of files) {
    let filepath = path.resolve(dir, file)

    let fileMeta = {
      id: nanoid(),
      name: file,
      isDirectory: false,
      path: filepath
    }

    let fileStats = await getFileStats(filepath)

    if (fileStats) {
      if (fileStats.isDirectory()) {
        fileMeta = { ...fileMeta, isDirectory: true }
      } else {
        fileMeta = {
          ...fileMeta,
          mimeType: mime.lookup(file),
          extension: path.extname(file),
          ino: fileStats.ino,
          size: fileStats.size,
          createdAt: fileStats.atimeMs,
          lastTimeAccessed: fileStats.atimeMs,
          lastTimeModified: fileStats.mtimeMs,
        }
      }

      dirContent.push(fileMeta)
    }
  }

  return dirContent
}
