import Ajv from 'ajv'

import AreaSchema from '@/schema/AreaSchema'
import BeneficioSchema from '@/schema/BeneficioSchema'
import CargoSchema from '@/schema/CargoSchema'
import CategoriaProjetoSchema from '@/schema/CategoriaProjetoSchema'
import ConselhoAdministracaoSchema from '@/schema/ConselhoAdministracaoSchema'
import CursoSchema from '@/schema/CursoSchema'
import EmpresaSchema from '@/schema/EmpresaSchema'
import FerramentaSchema from '@/schema/FerramentaSchema'
import FormacaoSchema from '@/schema/FormacaoSchema'
import FormatoSchema from '@/schema/FormatoSchema'
import GestaoSchema from '@/schema/GestaoSchema'
import InstituicaoSchema from '@/schema/InstituicaoSchema'
import MoedaSchema from '@/schema/MoedaSchema'
import PorteSchema from '@/schema/PorteSchema'
import ProfissionalSchema from '@/schema/ProfissionalSchema'
import ProjetoS7BrandSchema from '@/schema/ProjetoS7BrandSchema'
import ProjetoS7SearchMapeamentoMercadoSchema from '@/schema/ProjetoS7SearchMapeamentoMercadoSchema'
import ProjetoS7SearchPesquisaSalarialSchema from '@/schema/ProjetoS7SearchPesquisaSalarialSchema'
import ProjetoS7SearchRecrutamentoSelecaoSchema from '@/schema/ProjetoS7SearchRecrutamentoSelecaoSchema'
import ProjetoS7UpSchema from '@/schema/ProjetoS7UpSchema'
import SegmentoSchema from '@/schema/SegmentoSchema'
import SetorSchema from '@/schema/SetorSchema'
import StatusSchema from '@/schema/StatusSchema'

import FormHelper from '@/FormHelper'

const schemas = {
  S7Area: AreaSchema,
  S7Beneficio: BeneficioSchema,
  S7Cargo: CargoSchema,
  S7CategoriaProjeto: CategoriaProjetoSchema,
  S7ConselhoAdministracao: ConselhoAdministracaoSchema,
  S7Curso: CursoSchema,
  S7Empresa: EmpresaSchema,
  S7Ferramenta: FerramentaSchema,
  S7Formacao: FormacaoSchema,
  S7Formato: FormatoSchema,
  S7Gestao: GestaoSchema,
  S7Instituicao: InstituicaoSchema,
  S7Moeda: MoedaSchema,
  S7Porte: PorteSchema,
  S7Profissional: ProfissionalSchema,
  ProjetoS7Brand: ProjetoS7BrandSchema,
  ProjetoS7SearchMapeamentoMercado: ProjetoS7SearchMapeamentoMercadoSchema,
  ProjetoS7SearchPesquisaSalarial: ProjetoS7SearchPesquisaSalarialSchema,
  ProjetoS7SearchRecrutamentoSelecao: ProjetoS7SearchRecrutamentoSelecaoSchema,
  ProjetoS7Up: ProjetoS7UpSchema,
  S7Segmento: SegmentoSchema,
  S7Setor: SetorSchema,
  S7Status: StatusSchema,
}

export default {
  getSchema(modelName) {
    if (schemas[modelName]) return schemas[modelName]

    return null
  },

  validate(formProps) {
    return new Promise((resolve, reject) => {
      const form = FormHelper.getById(formProps.id)

      if (form) {
        let schema

        if (formProps.validator) {
          schema = this.getSchema(formProps.validator)
        } else {
          schema = this.getSchema(formProps.model)
        }

        console.log(formProps, schema)

        if (schema) {
          const ajv = new Ajv({
            allErrors: true
          })
          const ajvValidator = ajv.compile(schema)
          const data = {...form.raw, ...form.computed}
          const result = ajvValidator(data)

          if (result) resolve()
          else reject(ajvValidator.errors)
        } else {
          reject(new Error('Model schema not found.'))
        }
      }

      resolve()
      //reject(new Error('Form not found.'))
    })
  }
}