export default function NetQueuePayload(id, params, data) {
  this.id = id
  this.params = params
  this.data = data
  this.status = 'idle'
  this.timestamp = Date.now()
}

