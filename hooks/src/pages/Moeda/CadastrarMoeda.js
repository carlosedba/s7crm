import React, { useState, useEffect } from 'react'
import { Link, useHistory } from 'react-router-dom'

import Form from '@/components/Form'
import TextInput from '@/components/Input/TextInput'

import MoedaRepository from '@/repository/MoedaRepository'

import FormHelper from '@/FormHelper'
import FormValidator from '@/FormValidator'

export default function CadastrarMoeda(props) {
  const [formId, setFormId] = useState('')
  const [formProps, setFormProps] = useState({})

  const history = useHistory()

  useEffect(() => {
    const formId = FormHelper.new()

    setFormId(formId)

    setFormProps({
      id: formId,
      model: MoedaRepository.model,
      action: 'create'
    })
  }, [])

  async function handleSubmit(event) {
    FormValidator.validate(formProps)
      .then(() => {
        FormHelper.submit(formProps, true)
          .then(() => {
            alert('Moeda cadastrada com sucesso!')
            history.push('/moedas')
          })
          .catch((err) => {
            console.error(err)
            alert('Um erro ocorreu ao salvar os dados.')
          })
      })
      .catch((err) => {
        console.error(err)
        alert('Preencha todos campos obrigatórios.')
      })
  }

  return (
    <div className="page">
      <div className="page-wrapper">
        <div className="page-header">
          <div className="page-titles">
            <Link to="/moedas" className="page-tie">Voltar</Link>
            <p className="page-title">Cadastrar Moeda</p>
            <p className="page-subtitle"></p>
          </div>
        </div>
        <div className="page-content">
          <div className="page-content-wrapper page-content-form-wrapper">
            <Form {...formProps}>
              <div className="inputs input-style-2">
                <div className="multi-input">
                  <TextInput formId={formId} classes="w1" label="Símbolo" attribute="simbolo" required={true}/>
                  <TextInput formId={formId} classes="w3" label="Nome" attribute="nome" required={true}/>
                </div>
              </div>
            </Form>

            <button className="btn btn-six btn-orange" onClick={handleSubmit}>Salvar</button>
          </div>
        </div>
      </div>
    </div>
  )
}