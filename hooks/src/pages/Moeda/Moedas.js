import React, { useState, useEffect } from 'react'
import { Link, useHistory } from 'react-router-dom'

import DataTable from '@/components/DataTable'
import ModalConfirmacaoDeletar from '@/components/Modal/ModalConfirmacaoDeletar'

import IcPlusSign from '@/assets/icons/ic_plus_sign.svg'
import IcPencil from '@/assets/icons/ic_pencil.svg'
import IcTrashcan from '@/assets/icons/ic_trashcan.svg'

import MoedaRepository from '@/repository/MoedaRepository'

import ModalHelper from '@/ModalHelper'

export default function Moedas(props) {
  const [moedas, setMoedas] = useState([])

  const history = useHistory()

  useEffect(() => {
    fetchMoedas()
  }, [])

  const columns = [
    {
      Header: 'Símbolo',
      accessor: 'simbolo',
      width: 100
    },
    {
      Header: 'Nome',
      accessor: 'nome',
      width: 200
    },
    {
      Header: ' ',
      width: 100,
      Cell: ({row}) => (
        <div className="data-actions no-margin">
          <button className="svg data-action" onClick={(event) => handleEdit(event, row.original)}>
            <IcPencil/>
          </button>
          <button className="svg data-action" onClick={(event) => handleDelete(event, row.original)}>
            <IcTrashcan/>
          </button>
        </div>
      )
    }
  ]

  function fetchMoedas(cursor) {
    let queryParams = {}

    if (cursor) queryParams = {...queryParams, cursor}

    MoedaRepository.find(queryParams)
      .then((data) => {
        setMoedas(data)
      })
  }

  function handleEdit(event, row) {
    history.push(`/moedas/${row._id}/editar`)
  }

  function handleDelete(event, row) {
    ModalHelper.open({
      name: 'ModalConfirmacaoDeletar',
      params: {
        model: MoedaRepository.model,
        objectId: row._id
      }
    })
  }

  return (
    <div className="page">
      <div className="page-wrapper">
        <div className="page-header-lr">
          <div className="page-header-left">
            <div className="page-titles">
              <p className="page-title">Moedas</p>
              <p className="page-text"></p>
            </div>
          </div>
          <div className="page-header-right">
            <Link to="/moedas/cadastrar" className="btn btn-five btn-orange">
              <div className="svg btn-icon"><IcPlusSign/></div>
              Cadastrar Moeda
            </Link>
          </div>
        </div>
        <div className="page-content">
          <DataTable
            columns={columns}
            data={moedas}
          />
        </div>

        <ModalConfirmacaoDeletar/>
      </div>
    </div>
  )
}