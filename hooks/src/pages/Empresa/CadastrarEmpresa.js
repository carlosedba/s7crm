import React, { useState, useEffect } from 'react'
import { useSelector, useDispatch } from 'react-redux'
import { Link, useHistory } from 'react-router-dom'

import Form from '@/components/Form'
import TextInput from '@/components/Input/TextInput'
import CurrencyInput from '@/components/Input/CurrencyInput'
import SelectInput from '@/components/Input/SelectInput'
import FormSection from '@/components/FormSection'
import ArrayAttributeFormSection from '@/components/FormSection/ArrayAttributeFormSection'
import ObjectArrayAttributeFormSection from '@/components/FormSection/ObjectArrayAttributeFormSection'
import ModalConAdministrativoCreate from '@/components/Modal/ModalConAdministrativoCreate'
import ModalConAdministrativoUpdate from '@/components/Modal/ModalConAdministrativoUpdate'
import ModalPriContatosCreate from '@/components/Modal/ModalPriContatosCreate'
import ModalPriContatosUpdate from '@/components/Modal/ModalPriContatosUpdate'
import ModalRegionalCreate from '@/components/Modal/ModalRegionalCreate'
import ModalRegionalUpdate from '@/components/Modal/ModalRegionalUpdate'
import TextAttributeFormSection from '@/components/FormSection/TextAttributeFormSection'
import ModalDescricaoEmpresaCreate from '@/components/Modal/ModalDescricaoEmpresaCreate'

import IcPlusSign from '@/assets/icons/ic_plus_sign.svg'

import EmpresaRepository from '@/repository/EmpresaRepository'
import SegmentoRepository from '@/repository/SegmentoRepository'
import SetorRepository from '@/repository/SetorRepository'
import PorteRepository from '@/repository/PorteRepository'
import PaisRepository from '@/repository/PaisRepository'
import FormatoRepository from '@/repository/FormatoRepository'
import GestaoRepository from '@/repository/GestaoRepository'
import ConselhoAdministracaoRepository from '@/repository/ConselhoAdministracaoRepository'

import { preencherEnderecoPorCep } from '@/utils/FormUtils'

import * as textMasks from '@/textMasks'
import FormHelper from '@/FormHelper'
import FormValidator from '@/FormValidator'

export default function CadastrarEmpresa(props) {
  const [formId, setFormId] = useState('')
  const [formProps, setFormProps] = useState({})

  const [segmentos, setSegmentos] = useState([])
  const [setores, setSetores] = useState([])
  const [portes, setPortes] = useState([])
  const [paises, setPaises] = useState([])
  const [formatos, setFormatos] = useState([])
  const [gestao, setGestao] = useState([])
  const [conselhoAdministracao, setConselhoAdministracao] = useState([])

  const dispatch = useDispatch()

  const history = useHistory()

  useEffect(() => {
    const formId = FormHelper.new()

    setFormId(formId)

    setFormProps({
      id: formId,
      model: EmpresaRepository.model,
      action: 'create'
    })

    fetchSegmentos()
    fetchPortes()
    fetchPaises()
    fetchFormatos()
    fetchGestao()
    fetchConselhoAdministracao()
  }, [])

  function fetchSegmentos() {
    SegmentoRepository.find()
      .then((data) => {
        const segmentoOptions = data.map((segmento) => ({ value: segmento._id, label: segmento.nome }))

        setSegmentos(segmentoOptions)
      })
  }

  function fetchPortes() {
    PorteRepository.find()
      .then((data) => {
        const porteOptions = data.map((porte) => ({ value: porte._id, label: porte.nome }))

        setPortes(porteOptions)
      })
  }

  function fetchPaises() {
    PaisRepository.find()
      .then((data) => {
        const paisOptions = data.map((pais) => ({ value: pais._id, label: pais.nome }))

        setPaises(paisOptions)
      })
  }

  function fetchFormatos() {
    FormatoRepository.find()
      .then((data) => {
        const formatoOptions = data.map((formato) => ({ value: formato._id, label: formato.nome }))

        setFormatos(formatoOptions)
      })
  }

  function fetchGestao() {
    GestaoRepository.find()
      .then((data) => {
        const gestaoOptions = data.map((gestao) => ({ value: gestao._id, label: gestao.nome }))

        setGestao(gestaoOptions)
      })
  }

  function fetchConselhoAdministracao() {
    ConselhoAdministracaoRepository.find()
      .then((data) => {
        const conselhoAdministracaoOptions = data.map((conselhoAdministracao) => ({ value: conselhoAdministracao._id, label: conselhoAdministracao.nome }))

        setConselhoAdministracao(conselhoAdministracaoOptions)
      })
  }

  function handleTextInputChange(event) {
    if (event.value) {
      if (event.attribute === 'cep') preencherEnderecoPorCep(dispatch, formId, event.value)
    }
  }

  function handleSegmentoChange(event) {
    SetorRepository.find({ segmento: event.value })
      .then((data) => {
        const setorOptions = data.map((setor) => ({ value: setor._id, label: setor.nome }))

        setSetores(setorOptions)
      })
  }

  async function handleSubmit(event) {
    FormValidator.validate(formProps)
      .then(() => {
        FormHelper.submit(formProps, true)
          .then(() => {
            alert('Empresa cadastrada com sucesso!')
            history.push('/empresas')
          })
          .catch((err) => {
            console.error(err)
            alert('Um erro ocorreu ao salvar os dados.')
          })
      })
      .catch((err) => {
        console.error(err)
        alert('Preencha todos campos obrigatórios.')
      })
  }

  return (
    <div className="page">
      <div className="page-wrapper">
        <div className="page-header">
          <div className="page-titles">
            <Link to="/empresas" className="page-tie">Voltar</Link>
            <p className="page-title">Cadastrar Empresa</p>
            <p className="page-subtitle">Informe os dados em seus respectivos campos:</p>
          </div>
        </div>
        <div className="page-content">
          <div className="page-content-wrapper page-content-form-wrapper">
            <Form {...formProps}>
              <div className="inputs input-style-2">
                <div className="multi-input">
                  <SelectInput
                    formId={formId}
                    size="w2-q2"
                    label="Segmento"
                    attribute="segmento"
                    required={true}
                    options={segmentos}
                    onChange={handleSegmentoChange}
                  />
                  <SelectInput
                    formId={formId}
                    size="w3"
                    label="Setor"
                    attribute="setor"
                    required={true}
                    options={setores}
                  />
                </div>
                <div className="multi-input">
                  <TextInput formId={formId} classes="w3" label="Nome fantasia" attribute="nomeFantasia" required={true}/>
                  <TextInput formId={formId} classes="w3" label="Razão Social" attribute="razaoSocial" required={true}/>
                  <TextInput formId={formId} classes="w2" label="CNPJ" attribute="cnpj" mask="cnpj" required={true}/>
                </div>
                <div className="multi-input">
                  <TextInput formId={formId} classes="w1-q1" label="CEP" attribute="cep" mask="cep" required={true} onChange={handleTextInputChange}/>
                  <TextInput formId={formId} classes="w4" label="Rua" attribute="rua" required={true}/>
                  <TextInput formId={formId} classes="w1" label="Número" attribute="numero" required={true}/>
                  <TextInput formId={formId} classes="w1" label="Complemento" attribute="complemento"/>
                </div>
                <div className="multi-input">
                  <TextInput formId={formId} classes="w2" label="Bairro" attribute="bairro" required={true}/>
                  <TextInput formId={formId} classes="w2-q1" label="Cidade" attribute="cidade" required={true}/>
                  <TextInput formId={formId} classes="w2" label="Estado" attribute="estado" required={true}/>
                  <TextInput formId={formId} classes="w1-q2" label="País" attribute="pais" required={true}/>
                </div>
                <div className="multi-input">
                  <TextInput formId={formId} classes="w2" label="Telefone" attribute="telefone" mask="telefone" required={true}/>
                  <TextInput formId={formId} classes="w3-q2" label="Site" attribute="website"/>
                </div>
                <div className="multi-input">
                  <CurrencyInput formId={formId} classes="w2-q2" label="Faturamento" attribute="faturamento" mask={textMasks.BRL} required={true}/>
                  <TextInput formId={formId} classes="w2-q1" label="Ebitda" attribute="ebitda" required={true}/>
                  <SelectInput
                    formId={formId}
                    size="w1-q2"
                    label="Porte"
                    attribute="porte"
                    required={true}
                    options={portes}
                  />
                  <SelectInput
                    formId={formId}
                    size="w1-q2"
                    label="Origem"
                    attribute="origem"
                    options={paises}
                    required={true}
                  />
                </div>
                <div className="multi-input">
                  <SelectInput
                    formId={formId}
                    size="w4"
                    label="Formato"
                    attribute="formato"
                    isMulti={true}
                    required={true}
                    options={formatos}
                  />
                  <SelectInput
                    formId={formId}
                    size="w2"
                    label="Gestão"
                    attribute="gestao"
                    required={true}
                    options={gestao}
                  />
                </div>
                <div className="multi-input">
                  <SelectInput
                    formId={formId}
                    size="w4"
                    label="Conselho de Administração"
                    attribute="conselhoAdministracao"
                    required={true}
                    options={conselhoAdministracao}
                  />
                  <TextInput formId={formId} classes="w1" label="Head count" attribute="headCount" required={true}/>
                </div>
              </div>
            </Form>

            <TextAttributeFormSection
              title="Descrição"
              message="Editar descrição"
              icon={IcPlusSign}
              onCreateModalName="ModalDescricaoEmpresaCreate">
              <ModalDescricaoEmpresaCreate parentFormId={formId} attribute="descricao"/>
            </TextAttributeFormSection>

            <FormSection
              model="S7Conselheiro"
              action="create"
              title="Conselho Administrativo"
              message="Adicionar pessoa"
              icon={IcPlusSign}
              attributeToDisplay="profissional"
              onCreateModalName="ModalConAdministrativoCreate"
              onUpdateModalName="ModalConAdministrativoUpdate">
              <ModalConAdministrativoCreate dependsOn={[{ id: formId, key: 'empresa' }]}/>
              <ModalConAdministrativoUpdate dependsOn={[{ id: formId, key: 'empresa' }]}/>
            </FormSection>

            <FormSection
              model="S7PrincipalContato"
              action="create"
              title="Principais contatos"
              message="Adicionar contato"
              icon={IcPlusSign}
              attributeToDisplay="profissional"
              onCreateModalName="ModalPriContatosCreate"
              onUpdateModalName="ModalPriContatosUpdate">
              <ModalPriContatosCreate dependsOn={[{ id: formId, key: 'empresa' }]}/>
              <ModalPriContatosUpdate dependsOn={[{ id: formId, key: 'empresa' }]}/>
            </FormSection>

            <ObjectArrayAttributeFormSection
              formId={formId}
              attribute="regionais"
              title="Regionais"
              message="Adicionar regional"
              icon={IcPlusSign}
              attributeToDisplay="nome"
              onCreateModalName="ModalRegionalCreate"
              onUpdateModalName="ModalRegionalUpdate">
              <ModalRegionalCreate parentFormId={formId} attribute="regionais"/>
              <ModalRegionalUpdate parentFormId={formId} attribute="regionais"/>
            </ObjectArrayAttributeFormSection>

            <button className="btn btn-six btn-orange" onClick={handleSubmit}>Salvar</button>
          </div>
        </div>
      </div>
    </div>
  )
}

