import React, {useState, useEffect} from 'react'
import {useSelector, useDispatch} from 'react-redux'
import {Link, useParams, useHistory} from 'react-router-dom'
import {motion} from 'framer-motion'
import _ from 'lodash'

import FadeVariants from '@/animation/FadeVariants'

import SingleDataWidget from '@/components/SingleDataWidget'
import MultiDataWidget from '@/components/MultiDataWidget'
import ModalPriContatosView from '@/components/Modal/ModalPriContatosView'
import ModalPriContatosCreate from '@/components/Modal/ModalPriContatosCreate'
import ModalPriContatosUpdate from '@/components/Modal/ModalPriContatosUpdate'
import ModalConAdministrativoCreate from '@/components/Modal/ModalConAdministrativoCreate'
import ModalConAdministrativoUpdate from '@/components/Modal/ModalConAdministrativoUpdate'
import ModalRegionalView from '@/components/Modal/ModalRegionalView'
import ModalConfirmacaoDeletar from '@/components/Modal/ModalConfirmacaoDeletar'
import ModalConfirmacaoDeletarArquivo from '@/components/Modal/ModalConfirmacaoDeletarArquivo'
import PopUpContainer from '@/components/PopUp/PopUpContainer'
import FilesPopUp from '@/components/PopUp/FilesPopUp'
import LoadingOverlay from '@/components/LoadingOverlay'

import useWait from '@/hooks/useWait'

import EmpresaRepository from '@/repository/EmpresaRepository'
import PrincipalContatoRepository from '@/repository/PrincipalContatoRepository'
import ConselheiroRepository from '@/repository/ConselheiroRepository'

import ModalHelper from '@/ModalHelper'

export default function Empresa(props) {
  const [ready, setReady] = useState(false)

  const [empresa, setEmpresa] = useState([])

  const params = useParams()
  const history = useHistory()
  const waitComplete = useWait(500)

  useEffect(() => {
    fetchEmpresa()
  }, [])

  function fetchEmpresa() {
    EmpresaRepository.findById(params.id)
      .then((data) => {
        setEmpresa(data)

        setReady(true)
      })
  }

  function clickPrincipalContato(row) {
    history.push(`/profissionais/${row.profissional._id}`)
  }

  function createPrincipalContato() {
    ModalHelper.open({
      name: 'ModalPriContatosCreate'
    })
  }

  function updatePrincipalContato(row) {
    ModalHelper.open({
      name: 'ModalPriContatosUpdate',
      data: row
    })
  }

  function deletePrincipalContato(row) {
    ModalHelper.open({
      name: 'ModalConfirmacaoDeletar',
      params: {
        model: PrincipalContatoRepository.model,
        objectId: row._id
      }
    })
  }

  function clickConselheiro(row) {
    history.push(`/profissionais/${row.profissional._id}`)
  }

  function createConselheiro() {
    ModalHelper.open({
      name: 'ModalConAdministrativoCreate'
    })
  }

  function updateConselheiro(row) {
    ModalHelper.open({
      name: 'ModalConAdministrativoUpdate',
      data: row
    })
  }

  function deleteConselheiro(row) {
    ModalHelper.open({
      name: 'ModalConfirmacaoDeletar',
      params: {
        model: PrincipalContatoRepository.model,
        objectId: row._id
      }
    })
  }

  return (
    <motion.div className="page" initial="exit" animate="enter" exit="exit" variants={FadeVariants}>
      <div className="page-wrapper">
        <div className="page-header-lr">
          <div className="page-header-left">
            <div className="page-titles">
              <a className="page-tie --clickable" onClick={() => (history.goBack())}>Voltar</a>
              <p className="page-title">{_.get(empresa, 'nomeFantasia')}</p>
              <p className="page-text">
                {_.get(empresa, 'descricao')}
              </p>
            </div>
          </div>
          <div className="page-header-right">
            <Link to={`/empresas/${params.id}/editar`} className="btn btn-five btn-orange">
              Editar
            </Link>
            <Link to={`/empresas/${params.id}/editar`} className="btn btn-five btn-orange">
              Exportar
            </Link>
          </div>
        </div>
        <div className="page-content">

          <div className="data-widget-grid">

            <div className="data-widget-grid-row">
              <div className="data-widget-grid-col sp1">
                <SingleDataWidget
                  title="Informações básicas"
                  fields={[
                    { label: 'Segmento:', attribute: 'segmento.nome' },
                    { label: 'Setor:', attribute: 'setor.nome' },
                    { label: 'Nome Fantasia:', attribute: 'nomeFantasia' },
                    { label: 'Razão Social:', attribute: 'razaoSocial' },
                    { label: 'CNPJ:', attribute: 'cnpj' },
                    { label: 'Telefone:', attribute: 'telefone' },
                    { label: 'Faturamento:', attribute: 'faturamento.value' },
                    { label: 'Ebitda:', attribute: 'ebitda' },
                    { label: 'Porte:', attribute: 'porte.nome' },
                    { label: 'Origem:', attribute: 'origem.nome' },
                    {
                      label: 'Formato:',
                      attribute: (row) => {
                        if (row.formato) {
                          return row.formato.map((formato) => {
                            return formato.nome
                          }).join(', ')
                        }
                      }
                    },
                    { label: 'Gestão:', attribute: 'gestao.nome' },
                    { label: 'Conselho de Administração:', attribute: 'conselhoAdministracao.nome' },
                    { label: 'Head count:', attribute: 'headCount' },
                  ]}
                  initialData={empresa}
                />

                <SingleDataWidget
                  title="Endereço"
                  fields={[
                    { label: 'Rua:', attribute: 'rua' },
                    { label: 'Número:', attribute: 'numero' },
                    { label: 'Complemento:', attribute: 'complemento' },
                    { label: 'Bairro:', attribute: 'bairro' },
                    { label: 'Cidade:', attribute: 'cidade' },
                    { label: 'Estado:', attribute: 'estado' },
                    { label: 'País:', attribute: 'pais' },
                  ]}
                  initialData={empresa}
                />
              </div>
              <div className="data-widget-grid-col">
                <MultiDataWidget
                  size="full"
                  title="Principais contatos"
                  columns={[
                    { label: 'Nome', attribute: 'profissional.nome', width: '150px' },
                    { label: 'E-mail corporativo', attribute: 'profissional.emailCorporativo', width: '200px' },
                    { label: 'Celular', attribute: 'profissional.celular1', width: '130px' },
                  ]}
                  dataSource={{
                    repository: PrincipalContatoRepository,
                    params: {
                      empresa: _.get(empresa, '_id')
                    }
                  }}
                  clickHandler={clickPrincipalContato}
                  createHandler={createPrincipalContato}
                  updateHandler={updatePrincipalContato}
                  deleteHandler={deletePrincipalContato}
                  ready={ready}
                />
                <MultiDataWidget
                  size="full"
                  title="Conselho Administrativo"
                  columns={[
                    { label: 'Nome', attribute: 'profissional.nome', width: '150px' },
                    { label: 'E-mail corporativo', attribute: 'profissional.emailCorporativo', width: '200px' },
                    { label: 'Celular', attribute: 'profissional.celular1', width: '130px' },
                  ]}
                  dataSource={{
                    repository: ConselheiroRepository,
                    params: {
                      empresa: _.get(empresa, '_id')
                    }
                  }}
                  clickHandler={clickConselheiro}
                  createHandler={createConselheiro}
                  updateHandler={updateConselheiro}
                  deleteHandler={deleteConselheiro}
                  ready={ready}
                />
                <MultiDataWidget
                  size="full"
                  title="Regionais"
                  columns={[
                    { label: 'Nome', attribute: 'nome', width: '150px' },
                    { label: 'Endereço', attribute: (row) => {
                      return `${row.rua}, ${row.numero}, ${(row.complemento) ? row.complemento + ', ' : ''}${row.bairro} - ${row.cidade}, ${row.estado}`
                    }},
                  ]}
                  initialData={_.get(empresa, 'regionais')}
                />
              </div>
            </div>

          </div>

        </div>

        <ModalConfirmacaoDeletar/>
        <ModalConfirmacaoDeletarArquivo/>

        <ModalConAdministrativoCreate process={true} formComputed={{ empresa: _.get(empresa, '_id') }}/>
        <ModalConAdministrativoUpdate action="update" process={true}/>

        <ModalPriContatosCreate process={true} formComputed={{ empresa: _.get(empresa, '_id') }}/>
        <ModalPriContatosUpdate action="update" process={true}/>

        <ModalRegionalView/>

        <PopUpContainer>
          <FilesPopUp path={_.get(empresa, '_id')}/>
        </PopUpContainer>

        <LoadingOverlay visible={!(waitComplete && ready)}/>
      </div>
    </motion.div>
  )
}