import React, { useState, useEffect } from 'react'
import { Link, useHistory } from 'react-router-dom'
import { motion } from 'framer-motion'
import update from 'immutability-helper'

import FadeVariants from '@/animation/FadeVariants'

import IcPlusSign from '@/assets/icons/ic_plus_sign.svg'
import IcFilter from '@/assets/icons/ic_filter.svg'

import CustomTable from '@/components/CustomTable'
import ModalConfirmacaoDeletar from '@/components/Modal/ModalConfirmacaoDeletar'
import ModalFiltrosEmpresas from '@/components/Modal/ModalFiltrosEmpresas'

import EmpresaRepository from '@/repository/EmpresaRepository'

import ModalHelper from '@/ModalHelper'

export default function Empresas(props) {
  const [params, setParams] = useState({})

  const history = useHistory()

  function cleanFilters(filters) {
    let obj = filters

    for (let key of Object.keys(obj)) {
      if (obj.hasOwnProperty(key)) {
        let filter = obj[key]

        if (filter && filter._id) {
          obj[key] = filter._id
        }
      }
    }

    return obj
  }

  function handleFiltersApply(filters) {
    setParams(update(params, {
      $set: cleanFilters(filters)
    }))
  }

  function handleFiltrosClick(event) {
    event.preventDefault()

    ModalHelper.open({ name: 'ModalFiltrosEmpresas' })
  }

  function handleRowClick(row) {
    history.push(`/empresas/${row._id}`)
  }

  return (
    <motion.div className="page" initial="exit" animate="enter" exit="exit" variants={FadeVariants}>
      <div className="page-wrapper">
        <div className="page-header-lr">
          <div className="page-header-left">
            <div className="page-titles">
              <p className="page-title">Empresas</p>
              <p className="page-text"></p>
            </div>
          </div>
          <div className="page-header-right">
            <button className="btn btn-five btn-orange" onClick={handleFiltrosClick}>
              <div className="svg btn-icon"><IcFilter/></div>
              Filtros
            </button>
            <Link to="/empresas/cadastrar" className="btn btn-five btn-orange">
              <div className="svg btn-icon"><IcPlusSign/></div>
              Cadastrar Empresa
            </Link>
          </div>
        </div>
        <div className="page-content">
          <CustomTable
            leftColumns={[
              { label: 'Nome', attribute: 'nomeFantasia', width: '400px' },
              { label: 'CNPJ', attribute: 'cnpj', width: '150px' },
              { label: 'Cidade', attribute: 'cidade', width: '100px' },
              { label: 'Estado', attribute: 'estado', width: '100px' },
              {
                label: 'Site',
                attribute: (row) => {
                  return (<a href={row.website} target="_blank">{row.website}</a>)
                },
                width: '225px'
              },
            ]}
            rightColumns={[]}
            dataSource={{
              repository: EmpresaRepository,
              params: params
            }}
            clickHandler={handleRowClick}
          />
        </div>

        <ModalConfirmacaoDeletar/>
        <ModalFiltrosEmpresas onSubmit={handleFiltersApply}/>
      </div>
    </motion.div>
  )
}