import React, { useState, useEffect } from 'react'
import { useSelector, useDispatch } from 'react-redux'
import {Link, useHistory, useParams} from 'react-router-dom'

import Form from '@/components/Form'
import TextInput from '@/components/Input/TextInput'
import CurrencyInput from '@/components/Input/CurrencyInput'
import SelectInput from '@/components/Input/SelectInput'
import ObjectArrayAttributeFormSection from '@/components/FormSection/ObjectArrayAttributeFormSection'
import ModalRegionalCreate from '@/components/Modal/ModalRegionalCreate'
import ModalRegionalUpdate from '@/components/Modal/ModalRegionalUpdate'
import TextAttributeFormSection from '@/components/FormSection/TextAttributeFormSection'
import ModalDescricaoEmpresaCreate from '@/components/Modal/ModalDescricaoEmpresaCreate'

import IcPlusSign from '@/assets/icons/ic_plus_sign.svg'

import { preencherEnderecoPorCep } from '@/utils/FormUtils'

import PaisRepository from '@/repository/PaisRepository'
import EmpresaRepository from '@/repository/EmpresaRepository'
import SegmentoRepository from '@/repository/SegmentoRepository'
import SetorRepository from '@/repository/SetorRepository'
import PorteRepository from '@/repository/PorteRepository'
import FormatoRepository from '@/repository/FormatoRepository'
import GestaoRepository from '@/repository/GestaoRepository'
import ConselhoAdministracaoRepository from '@/repository/ConselhoAdministracaoRepository'

import * as textMasks from '@/textMasks'
import FormHelper from '@/FormHelper'
import FormValidator from '@/FormValidator'

export default function EditarEmpresa(props) {
  const [formId, setFormId] = useState(null)
  const [formProps, setFormProps] = useState({})

  const [empresa, setEmpresa] = useState({})
  const [segmentos, setSegmentos] = useState([])
  const [setores, setSetores] = useState([])
  const [portes, setPortes] = useState([])
  const [paises, setPaises] = useState([])
  const [formatos, setFormatos] = useState([])
  const [gestao, setGestao] = useState([])
  const [conselhoAdministracao, setConselhoAdministracao] = useState([])

  const dispatch = useDispatch()
  const params = useParams()
  const history = useHistory()

  useEffect(() => {
    const formId = FormHelper.new()

    FormHelper.setObjectId(formId, params.id)

    setFormId(formId)

    setFormProps({
      id: formId,
      model: EmpresaRepository.model,
      action: 'update'
    })

    fetchEmpresa(formId)
  }, [])

  function fetchEmpresa(formId) {
    EmpresaRepository.findById(params.id)
      .then(async (data) => {
        await fetchSegmentos()
        await fetchSetores(data.segmento._id)
        await fetchPortes()
        await fetchPaises()
        await fetchFormatos()
        await fetchGestao()
        await fetchConselhoAdministracao()

        setEmpresa(data)
        setDescricao(formId, data.descricao)
        setConselheiros(formId, data.conselheiros)
        setRegionais(formId, data.regionais)
      })
  }

  function fetchSegmentos() {
    return new Promise((resolve, reject) => {
      SegmentoRepository.find()
        .then((data) => {
          const options = data.map((segmento) => ({ value: segmento._id, label: segmento.nome }))

          setSegmentos(options)
        })
        .then(resolve)
    })
  }

  function fetchSetores(segmento) {
    return new Promise((resolve, reject) => {
      SetorRepository.find({ segmento: segmento })
        .then((data) => {
          const options = data.map((setor) => ({ value: setor._id, label: setor.nome }))

          setSetores(options)
        })
        .then(resolve)
    })
  }

  function fetchPortes() {
    return new Promise((resolve, reject) => {
      PorteRepository.find()
        .then((data) => {
          const porteOptions = data.map((porte) => ({ value: porte._id, label: porte.nome }))

          setPortes(porteOptions)
        })
        .then(resolve)
    })
  }

  function fetchPaises() {
    return new Promise((resolve, reject) => {
      PaisRepository.find()
        .then((data) => {
          const options = data.map((pais) => ({ value: pais._id, label: pais.nome }))

          setPaises(options)
        })
        .then(resolve)
    })
  }

  function fetchFormatos() {
    return new Promise((resolve, reject) => {
      FormatoRepository.find()
        .then((data) => {
          const formatoOptions = data.map((formato) => ({ value: formato._id, label: formato.nome }))

          setFormatos(formatoOptions)
        })
        .then(resolve)
    })
  }

  function fetchGestao() {
    return new Promise((resolve, reject) => {
      GestaoRepository.find()
        .then((data) => {
          const gestaoOptions = data.map((gestao) => ({ value: gestao._id, label: gestao.nome }))

          setGestao(gestaoOptions)
        })
        .then(resolve)
    })
  }

  function fetchConselhoAdministracao() {
    return new Promise((resolve, reject) => {
      ConselhoAdministracaoRepository.find()
        .then((data) => {
          const conselhoAdministracaoOptions = data.map((conselhoAdministracao) => ({ value: conselhoAdministracao._id, label: conselhoAdministracao.nome }))

          setConselhoAdministracao(conselhoAdministracaoOptions)
        })
        .then(resolve)
    })
  }

  function setConselheiros(formId, conselheiros) {
    FormHelper.setComputedAttribute(formId, 'conselheiros', {
      $set: conselheiros
    })
  }

  function setDescricao(formId, descricao) {
    FormHelper.setRawAttribute(formId, 'descricao', {
      $set: descricao
    })
  }

  function setRegionais(formId, regionais) {
    FormHelper.setRawAttribute(formId, 'regionais', {
      $set: regionais
    })
  }

  function handleSegmentoChange(event) {
    console.log(event)
    SetorRepository.find({ segmento: event.value })
      .then((data) => {
        const options = data.map((setor) => ({ value: setor._id, label: setor.nome }))

        setSetores(options)
      })
  }

  function handleTextInputChange(event) {
    if (event.value) {
      if (event.attribute === 'cep') preencherEnderecoPorCep(dispatch, formId, event.value)
    }
  }

  async function handleSubmit(event) {
    FormValidator.validate(formProps)
      .then(() => {
        FormHelper.submit(formProps, true)
          .then(() => {
            alert('Empresa atualizada com sucesso!')
            history.push(`/empresas/${params.id}`)
          })
          .catch((err) => {
            console.error(err)
            alert('Um erro ocorreu ao salvar os dados.')
          })
      })
      .catch((err) => {
        console.error(err)
        alert('Preencha todos campos obrigatórios.')
      })
  }

  return (
    <div className="page">
      <div className="page-wrapper">
        <div className="page-header">
          <div className="page-titles">
            <Link to={`/empresas/${params.id}`} className="page-tie">Voltar</Link>
            <p className="page-title">Editar Empresa</p>
            <p className="page-subtitle">Informe os dados em seus respectivos campos:</p>
          </div>
        </div>
        <div className="page-content">
          <div className="page-content-wrapper page-content-form-wrapper">
            <Form {...formProps}>
              <div className="inputs input-style-2">
                <div className="multi-input">
                  <SelectInput
                    formId={formId}
                    size="w2-q2"
                    label="Segmento"
                    attribute="segmento"
                    required={true}
                    options={segmentos}
                    initialValue={empresa.segmento}
                    onChange={handleSegmentoChange}
                  />
                  <SelectInput
                    formId={formId}
                    size="w3"
                    label="Setor"
                    attribute="setor"
                    required={true}
                    options={setores}
                    initialValue={empresa.setor}
                  />
                </div>
                <div className="multi-input">
                  <TextInput formId={formId} classes="w3" label="Nome fantasia" attribute="nomeFantasia" initialValue={empresa.nomeFantasia}/>
                  <TextInput formId={formId} classes="w3" label="Razão Social" attribute="razaoSocial" initialValue={empresa.razaoSocial}/>
                  <TextInput formId={formId} classes="w2" label="CNPJ" attribute="cnpj" mask="cnpj" initialValue={empresa.cnpj}/>
                </div>
                <div className="multi-input">
                  <TextInput formId={formId} classes="w1-q1" label="CEP" attribute="cep" mask="cep" initialValue={empresa.cep} onChange={handleTextInputChange}/>
                  <TextInput formId={formId} classes="w4" label="Rua" attribute="rua" initialValue={empresa.rua}/>
                  <TextInput formId={formId} classes="w1" label="Número" attribute="numero" initialValue={empresa.numero}/>
                  <TextInput formId={formId} classes="w1" label="Complemento" attribute="complemento" initialValue={empresa.complemento}/>
                </div>
                <div className="multi-input">
                  <TextInput formId={formId} classes="w2" label="Bairro" attribute="bairro" initialValue={empresa.bairro}/>
                  <TextInput formId={formId} classes="w2-q1" label="Cidade" attribute="cidade" collection="cidades" initialValue={empresa.cidade}/>
                  <TextInput formId={formId} classes="w2" label="Estado" attribute="estado" collection="estados" initialValue={empresa.estado}/>
                  <TextInput formId={formId} classes="w1-q2" label="País" attribute="pais" collection="paises" initialValue={empresa.pais}/>
                </div>
                <div className="multi-input">
                  <TextInput formId={formId} classes="w2" label="Telefone" attribute="telefone" mask="telefone" initialValue={empresa.telefone}/>
                  <TextInput formId={formId} classes="w3-q2" label="Site" attribute="website" initialValue={empresa.website}/>
                </div>
                <div className="multi-input">
                  <CurrencyInput formId={formId} classes="w2-q2" label="Faturamento" attribute="faturamento" mask={textMasks.BRL} initialValue={empresa.faturamento}/>
                  <TextInput formId={formId} classes="w2-q1" label="Ebitda" attribute="ebitda" initialValue={empresa.ebitda}/>
                  <SelectInput
                    formId={formId}
                    size="w1-q2"
                    label="Porte"
                    attribute="porte"
                    options={portes}
                    initialValue={empresa.porte}
                  />
                  <SelectInput
                    formId={formId}
                    size="w1-q2"
                    label="Origem"
                    attribute="origem"
                    options={paises}
                    initialValue={empresa.origem}
                  />
                </div>
                <div className="multi-input">
                  <SelectInput
                    formId={formId}
                    size="w4"
                    label="Formato"
                    attribute="formato"
                    isMulti={true}
                    options={formatos}
                    initialValue={empresa.formato}
                  />
                  <SelectInput
                    formId={formId}
                    size="w2"
                    label="Gestão"
                    attribute="gestao"
                    options={gestao}
                    initialValue={empresa.gestao}
                  />
                </div>
                <div className="multi-input">
                  <SelectInput
                    formId={formId}
                    size="w4"
                    label="Conselho de Administração"
                    attribute="conselhoAdministracao"
                    options={conselhoAdministracao}
                    initialValue={empresa.conselhoAdministracao}
                  />
                  <TextInput formId={formId} classes="w1" label="Head count" attribute="headCount" initialValue={empresa.headCount}/>
                </div>
              </div>
            </Form>

            <TextAttributeFormSection
              title="Descrição"
              message="Editar descrição"
              icon={IcPlusSign}
              onCreateModalName="ModalDescricaoEmpresaCreate">
              <ModalDescricaoEmpresaCreate parentFormId={formId} attribute="descricao"/>
            </TextAttributeFormSection>

            <ObjectArrayAttributeFormSection
              formId={formId}
              attribute="regionais"
              title="Regionais"
              message="Adicionar regional"
              icon={IcPlusSign}
              attributeToDisplay="nome"
              onCreateModalName="ModalRegionalCreate"
              onUpdateModalName="ModalRegionalUpdate">
              <ModalRegionalCreate parentFormId={formId} attribute="regionais"/>
              <ModalRegionalUpdate parentFormId={formId} attribute="regionais"/>
            </ObjectArrayAttributeFormSection>

            <button className="btn btn-six btn-orange" onClick={handleSubmit}>Salvar</button>
          </div>
        </div>
      </div>
    </div>
  )
}

