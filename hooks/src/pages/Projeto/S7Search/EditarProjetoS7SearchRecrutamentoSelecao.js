import React, { useState, useEffect } from 'react'
import { useSelector, useDispatch } from 'react-redux'
import {Link, useHistory, useParams} from 'react-router-dom'

import Form from '@/components/Form'
import TextInput from '@/components/Input/TextInput'
import SuggestionInput from '@/components/Input/SuggestionInput'
import CurrencyInput from '@/components/Input/CurrencyInput'
import DateInput from '@/components/Input/DateInput'
import SelectInput from '@/components/Input/SelectInput'
import FormSection from '@/components/FormSection'
import TextAttributeFormSection from '@/components/FormSection/TextAttributeFormSection'

import ModalObjetivo from '@/components/Modal/ModalObjetivo'
import ModalEmpresaAlvoCreate from '@/components/Modal/ModalEmpresaAlvoCreate'
import ModalEmpresaAlvoUpdate from '@/components/Modal/ModalEmpresaAlvoUpdate'

import IcPlusSign from '@/assets/icons/ic_plus_sign.svg'

import ProjetoRepository from '@/repository/ProjetoRepository'
import CategoriaProjetoRepository from '@/repository/CategoriaProjetoRepository'

import * as textMasks from '@/textMasks'
import FormHelper from '@/FormHelper'
import FormValidator from '@/FormValidator'

import * as Forms from '@/actions/Form'
import ModalMissaoCargo from '@/components/Modal/ModalMissaoCargo'
import ModalResponsabilidadesCargo from '@/components/Modal/ModalResponsabilidadesCargo'
import CargoRepository from '@/repository/CargoRepository'
import AreaRepository from '@/repository/AreaRepository'
import UsuarioRepository from '@/repository/UsuarioRepository'
import EmpresaRepository from '@/repository/EmpresaRepository'
import ProfissionalRepository from '@/repository/ProfissionalRepository'

export default function EditarProjetoS7SearchMapeamentoMercado(props) {
  const [formId, setFormId] = useState(null)
  const [formProps, setFormProps] = useState({})

  const [projeto, setProjeto] = useState({})
  const [cargos, setCargos] = useState([])
  const [areas, setAreas] = useState([])
  const [dataFechamento, setDataFechamento] = useState(false)

  const dispatch = useDispatch()
  const history = useHistory()
  const params = useParams()

  useEffect(() => {
    const formId = FormHelper.new()

    FormHelper.setObjectId(formId, params.id)

    setFormId(formId)

    setFormProps({
      id: formId,
      model: ProjetoRepository.model,
      action: 'update'
    })

    fetchProjeto(formId)
  }, [])

  function fetchProjeto(formId) {
    ProjetoRepository.findById(params.id)
      .then(async (data) => {
        await fetchCargos()
        await fetchAreas()

        setProjeto(data)
        setMissaoCargo(formId, data.missaoCargo)
        setResponsabildiadesCargo(formId, data.responsabilidadesCargo)
      })
  }

  function fetchCargos() {
    return new Promise((resolve, reject) => {
      CargoRepository.find()
        .then((data) => {
          const cargoOptions = data.map((cargo) => ({ value: cargo._id, label: cargo.nome }))

          setCargos(cargoOptions)
        })
        .then(resolve)
    })
  }

  function fetchAreas() {
    return new Promise((resolve, reject) => {
      AreaRepository.find()
        .then((data) => {
          const areaOptions = data.map((area) => ({ value: area._id, label: area.nome }))

          setAreas(areaOptions)
        })
        .then(resolve)
    })
  }

  function setMissaoCargo(formId, missaoCargo) {
    dispatch(Forms.updateFormRawAttribute(formId, 'missaoCargo', {
      $set: missaoCargo
    }))
  }

  function setResponsabildiadesCargo(formId, responsabilidadesCargo) {
    dispatch(Forms.updateFormRawAttribute(formId, 'responsabilidadesCargo', {
      $set: responsabilidadesCargo
    }))
  }

  function handleStatusChange(event) {
    if (['Lost', 'Finalizado', 'Stand-by', 'On hold'].includes(event.value)) {
      setDataFechamento(true)
    } else {
      setDataFechamento(false)
    }
  }

  async function handleSubmit(event) {
    FormValidator.validate({ id: formId, validator: 'ProjetoS7SearchRecrutamentoSelecao' })
      .then(() => {
        FormHelper.submit(formProps, true)
          .then(() => {
            alert('Projeto atualizado com sucesso!')
            history.push(`/projetos/s7-search/recrutamento-selecao/${params.id}`)
          })
          .catch((err) => {
            console.error(err)
            alert('Um erro ocorreu ao salvar os dados.')
          })
      })
      .catch((err) => {
        console.error(err)
        alert('Preencha todos campos obrigatórios.')
      })
  }

  return (
    <div className="page control-page">
      <div className="page-wrapper">
        <div className="page-header">
          <div className="page-titles">
            <a className="page-tie --clickable" onClick={() => (history.goBack())}>Voltar</a>
            <p className="page-title">Editar Projeto</p>
            <p className="page-subtitle">S7 Search - Mapeamento de Mercado</p>
          </div>
        </div>
        <div className="page-content">
          <div className="page-content-wrapper page-content-form-wrapper">
            <Form {...formProps}>
              <div className="inputs input-style-2">
                <SuggestionInput
                  formId={formId}
                  classes="w3"
                  label="Consultor responsável"
                  attribute="consultorResponsavel"
                  dataSource={{
                    repository: UsuarioRepository,
                    attributes: ['nome', 'sobrenome']
                  }}
                  required={true}
                  initialValue={projeto.consultorResponsavel}
                />
                <div className="multi-input">
                  <SuggestionInput
                    formId={formId}
                    classes="w3"
                    label="Cliente"
                    attribute="cliente"
                    dataSource={{
                      repository: EmpresaRepository,
                      attributes: ['nomeFantasia']
                    }}
                    required={true}
                    initialValue={projeto.cliente}
                  />
                  <SuggestionInput
                    formId={formId}
                    classes="w3"
                    label="Contato no cliente"
                    attribute="contatoCliente"
                    dataSource={{
                      repository: ProfissionalRepository,
                      attributes: ['nome', 'sobrenome']
                    }}
                    required={true}
                    initialValue={projeto.contatoCliente}
                  />
                </div>
                <div className="multi-input">
                  <SelectInput
                    formId={formId}
                    size="w1-q3"
                    label="Status"
                    attribute="status"
                    required={true}
                    options={[
                      { value: 'Open', label: 'Open' },
                      { value: 'Ativo 1', label: 'Ativo 1' },
                      { value: 'Ativo 2', label: 'Ativo 2' },
                      { value: 'Ativo 3', label: 'Ativo 3' },
                      { value: 'Lost', label: 'Lost' },
                      { value: 'Finalizado', label: 'Finalizado' },
                      { value: 'Stand-by', label: 'Stand-by' },
                      { value: 'On hold', label: 'On hold' },
                    ]}
                    initialValue={projeto.status}
                    onChange={handleStatusChange}
                  />
                  <SelectInput
                    formId={formId}
                    size="w2"
                    label="Tipo"
                    attribute="tipo"
                    required={true}
                    options={[
                      { value: 'C-Level', label: 'C-Level' },
                      { value: 'Middle Management', label: 'Middle Management' },
                      { value: 'Specialist', label: 'Specialist' },
                    ]}
                    initialValue={projeto.tipo}
                  />
                </div>
                <div className="multi-input">
                  <DateInput formId={formId} classes="w1-q3" label="Data de abertura" attribute="dataAbertura" required={true} initialValue={projeto.dataAbertura}/>
                  {(dataFechamento) ? (<DateInput formId={formId} classes="w1-q3" label="Data de fechamento" attribute="dataFechamento" initialValue={projeto.dataFechamento}/>) : null}
                </div>
                <div className="multi-input">
                  <SelectInput
                    formId={formId}
                    size="w2-q2"
                    label="Cargo"
                    attribute="cargo"
                    required={true}
                    options={cargos}
                    initialValue={projeto.cargo}
                  />
                  <SelectInput
                    formId={formId}
                    size="w2-q2"
                    label="Área"
                    attribute="area"
                    required={true}
                    options={areas}
                    initialValue={projeto.area}
                  />
                </div>
                <div className="multi-input">
                  <CurrencyInput formId={formId} classes="w2-q2" label="Salário proposto" attribute="salarioProposto" mask={textMasks.BRL} required={true} initialValue={projeto.salarioProposto}/>
                  <CurrencyInput formId={formId} classes="w2-q2" label="Salário final" attribute="salarioFinal" mask={textMasks.BRL} initialValue={projeto.salarioFinal}/>
                  <CurrencyInput formId={formId} classes="w2-q2" label="Honorário" attribute="honorario" mask={textMasks.BRL} required={true} initialValue={projeto.honorario}/>
                </div>
                <div className="multi-input">
                  <TextInput formId={formId} classes="w1-q1" label="Equipe liderada" attribute="equipeLiderada" required={true} initialValue={projeto.equipeLiderada}/>
                  <SelectInput
                    formId={formId}
                    size="w4-q2"
                    label="Nível equipe"
                    attribute="nivelEquipe"
                    isMulti={true}
                    required={true}
                    options={[
                      { value: 'Diretor', label: 'Diretor' },
                      { value: 'Gerente Corporativo', label: 'Gerente Corporativo' },
                      { value: 'Gerente', label: 'Gerente' },
                      { value: 'Coordenador', label: 'Coordenador' },
                      { value: 'Supervisor', label: 'Supervisor' },
                      { value: 'Especialista', label: 'Especialista' },
                      { value: 'Analista', label: 'Analista' },
                    ]}
                    initialValue={projeto.nivelEquipe}
                  />
                </div>
              </div>
            </Form>

            <TextAttributeFormSection
              title="Missão do cargo"
              message="Editar missão do cargo"
              icon={IcPlusSign}
              onCreateModalName="ModalMissaoCargo">
              <ModalMissaoCargo parentFormId={formId} attribute="missaoCargo"/>
            </TextAttributeFormSection>

            <TextAttributeFormSection
              title="Responsabilidades do cargo"
              message="Editar responsabilidades do cargo"
              icon={IcPlusSign}
              onCreateModalName="ModalResponsabilidadesCargo">
              <ModalResponsabilidadesCargo parentFormId={formId} attribute="responsabilidadesCargo"/>
            </TextAttributeFormSection>

            <button className="btn btn-six btn-orange" onClick={handleSubmit}>Salvar</button>
          </div>
        </div>
      </div>
    </div>
  )
}

