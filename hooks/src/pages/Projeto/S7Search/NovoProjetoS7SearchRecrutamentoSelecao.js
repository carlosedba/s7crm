import React, { useState, useEffect } from 'react'
import { useSelector, useDispatch } from 'react-redux'
import { Link, useHistory } from 'react-router-dom'

import Form from '@/components/Form'
import TextInput from '@/components/Input/TextInput'
import SuggestionInput from '@/components/Input/SuggestionInput'
import CurrencyInput from '@/components/Input/CurrencyInput'
import DateInput from '@/components/Input/DateInput'
import SelectInput from '@/components/Input/SelectInput'
import FormSection from '@/components/FormSection'
import TextAttributeFormSection from '@/components/FormSection/TextAttributeFormSection'

import ModalMissaoCargo from '@/components/Modal/ModalMissaoCargo'
import ModalResponsabilidadesCargo from '@/components/Modal/ModalResponsabilidadesCargo'
import ModalProfissionalProjetoCreate from '@/components/Modal/ModalProfissionalProjetoCreate'
import ModalProfissionalProjetoUpdate from '@/components/Modal/ModalProfissionalProjetoUpdate'

import IcPlusSign from '@/assets/icons/ic_plus_sign.svg'

import ProjetoRepository from '@/repository/ProjetoRepository'
import CargoRepository from '@/repository/CargoRepository'
import AreaRepository from '@/repository/AreaRepository'

import * as textMasks from '@/textMasks'
import FormHelper from '@/FormHelper'
import FormValidator from '@/FormValidator'
import UsuarioRepository from '@/repository/UsuarioRepository'
import EmpresaRepository from '@/repository/EmpresaRepository'
import ProfissionalRepository from '@/repository/ProfissionalRepository'

export default function NovoProjetoS7SearchRecrutamentoSelecao(props) {
  const [formId, setFormId] = useState('')
  const [formProps, setFormProps] = useState({})

  const [dataFechamento, setDataFechamento] = useState(false)

  const [cargos, setCargos] = useState([])
  const [areas, setAreas] = useState([])

  const history = useHistory()

  useEffect(() => {
    const formId = FormHelper.new({
      props: {
        raw: { divisao: 'S7 Search', varianteDivisao: 'Recrutamento e Seleção' }
      }
    })

    setFormId(formId)

    setFormProps({
      id: formId,
      model: ProjetoRepository.model,
      action: 'create'
    })

    fetchCargos()
    fetchAreas()
  }, [])

  function fetchCargos() {
    CargoRepository.find()
      .then((data) => {
        const cargoOptions = data.map((cargo) => ({ value: cargo._id, label: cargo.nome }))

        setCargos(cargoOptions)
      })
  }

  function fetchAreas() {
    AreaRepository.find()
      .then((data) => {
        const areaOptions = data.map((area) => ({ value: area._id, label: area.nome }))

        setAreas(areaOptions)
      })
  }

  function handleStatusChange(event) {
    if (['Lost', 'Finalizado', 'Stand-by', 'On hold'].includes(event.value)) {
      setDataFechamento(true)
    } else {
      setDataFechamento(false)
    }
  }

  async function handleSubmit(event) {
    FormValidator.validate({ id: formId, validator: 'ProjetoS7SearchRecrutamentoSelecao' })
      .then(() => {
        FormHelper.submit(formProps, true)
          .then(() => {
            alert('Projeto cadastrado com sucesso!')
            history.push('/projetos')
          })
          .catch((err) => {
            console.error(err)
            alert('Um erro ocorreu ao salvar os dados.')
          })
      })
      .catch((err) => {
        console.error(err)
        alert('Preencha todos campos obrigatórios.')
      })
  }

  return (
    <div className="page control-page">
      <div className="page-wrapper">
        <div className="page-header">
          <div className="page-titles">
            <a className="page-tie --clickable" onClick={() => (history.goBack())}>Voltar</a>
            <p className="page-title">Novo Projeto</p>
            <p className="page-subtitle">S7 Search - Recrutamento e seleção</p>
          </div>
        </div>
        <div className="page-content">
          <div className="page-content-wrapper page-content-form-wrapper">
            <Form {...formProps}>
              <div className="inputs input-style-2">
                <SuggestionInput
                  formId={formId}
                  classes="w3"
                  label="Consultor responsável"
                  attribute="consultorResponsavel"
                  dataSource={{
                    repository: UsuarioRepository,
                    attributes: ['nome', 'sobrenome']
                  }}
                  required={true}
                />
                <div className="multi-input">
                  <SuggestionInput
                    formId={formId}
                    classes="w3"
                    label="Cliente"
                    attribute="cliente"
                    dataSource={{
                      repository: EmpresaRepository,
                      attributes: ['nomeFantasia']
                    }}
                    required={true}
                  />
                  <SuggestionInput
                    formId={formId}
                    classes="w3"
                    label="Contato no cliente"
                    attribute="contatoCliente"
                    dataSource={{
                      repository: ProfissionalRepository,
                      attributes: ['nome', 'sobrenome']
                    }}
                    required={true}
                  />
                </div>
                <div className="multi-input">
                  <SelectInput
                    formId={formId}
                    size="w1-q3"
                    label="Status"
                    attribute="status"
                    required={true}
                    options={[
                      { value: 'Open', label: 'Open' },
                      { value: 'Ativo 1', label: 'Ativo 1' },
                      { value: 'Ativo 2', label: 'Ativo 2' },
                      { value: 'Ativo 3', label: 'Ativo 3' },
                      { value: 'Lost', label: 'Lost' },
                      { value: 'Finalizado', label: 'Finalizado' },
                      { value: 'Stand-by', label: 'Stand-by' },
                      { value: 'On hold', label: 'On hold' },
                    ]}
                    onChange={handleStatusChange}
                  />
                  <SelectInput
                    formId={formId}
                    size="w2"
                    label="Tipo"
                    attribute="tipo"
                    required={true}
                    options={[
                      { value: 'C-Level', label: 'C-Level' },
                      { value: 'Middle Management', label: 'Middle Management' },
                      { value: 'Specialist', label: 'Specialist' },
                    ]}
                  />
                </div>
                <div className="multi-input">
                  <DateInput formId={formId} classes="w1-q3" label="Data de abertura" attribute="dataAbertura" required={true}/>
                  {(dataFechamento) ? (<DateInput formId={formId} classes="w1-q3" label="Data de fechamento" attribute="dataFechamento"/>) : null}
                </div>
                <div className="multi-input">
                  <SelectInput
                    formId={formId}
                    size="w2-q2"
                    label="Cargo"
                    attribute="cargo"
                    required={true}
                    options={cargos}
                  />
                  <SelectInput
                    formId={formId}
                    size="w2-q2"
                    label="Área"
                    attribute="area"
                    required={true}
                    options={areas}
                  />
                </div>
                <div className="multi-input">
                  <CurrencyInput formId={formId} classes="w2-q2" label="Salário proposto" attribute="salarioProposto" mask={textMasks.BRL} required={true}/>
                  <CurrencyInput formId={formId} classes="w2-q2" label="Salário final" attribute="salarioFinal" mask={textMasks.BRL}/>
                  <CurrencyInput formId={formId} classes="w2-q2" label="Honorário" attribute="honorario" mask={textMasks.BRL} required={true}/>
                </div>
                <div className="multi-input">
                  <TextInput formId={formId} classes="w1-q1" label="Equipe liderada" attribute="equipeLiderada" required={true}/>
                  <SelectInput
                    formId={formId}
                    size="w4-q2"
                    label="Nível equipe"
                    attribute="nivelEquipe"
                    isMulti={true}
                    required={true}
                    options={[
                      { value: 'Diretor', label: 'Diretor' },
                      { value: 'Gerente Corporativo', label: 'Gerente Corporativo' },
                      { value: 'Gerente', label: 'Gerente' },
                      { value: 'Coordenador', label: 'Coordenador' },
                      { value: 'Supervisor', label: 'Supervisor' },
                      { value: 'Especialista', label: 'Especialista' },
                      { value: 'Analista', label: 'Analista' },
                    ]}
                  />
                </div>
              </div>
            </Form>

            <TextAttributeFormSection
              title="Missão do cargo"
              message="Editar missão do cargo"
              icon={IcPlusSign}
              onCreateModalName="ModalMissaoCargo">
              <ModalMissaoCargo parentFormId={formId} attribute="missaoCargo"/>
            </TextAttributeFormSection>

            <TextAttributeFormSection
              title="Responsabilidades do cargo"
              message="Editar responsabilidades do cargo"
              icon={IcPlusSign}
              onCreateModalName="ModalResponsabilidadesCargo">
              <ModalResponsabilidadesCargo parentFormId={formId} attribute="responsabilidadesCargo"/>
            </TextAttributeFormSection>

            <FormSection
              model="S7ProfissionalProjeto"
              action="create"
              title="Profissionais"
              message="Adicionar profissional"
              icon={IcPlusSign}
              attributeToDisplay="profissional"
              onCreateModalName="ModalProfissionalProjetoCreate"
              onUpdateModalName="ModalProfissionalProjetoUpdate">
              <ModalProfissionalProjetoCreate dependsOn={[{ id: formId, key: 'projeto' }]}/>
              <ModalProfissionalProjetoUpdate dependsOn={[{ id: formId, key: 'projeto' }]}/>
            </FormSection>

            <button className="btn btn-six btn-orange" onClick={handleSubmit}>Salvar</button>
          </div>
        </div>
      </div>
    </div>
  )
}