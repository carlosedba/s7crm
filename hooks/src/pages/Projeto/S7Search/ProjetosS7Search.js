import React, { useState, useEffect } from 'react'
import { useSelector, useDispatch } from 'react-redux'
import {Link, useHistory} from 'react-router-dom'
import {motion} from 'framer-motion'
import update from 'immutability-helper'
import moment from 'moment'

import FadeVariants from '@/animation/FadeVariants'

import IcPlusSign from '@/assets/icons/ic_plus_sign.svg'
import IcFilter from '@/assets/icons/ic_filter.svg'

import CustomTable from '@/components/CustomTable'
import ModalDivisaoProjetoS7Search from '@/components/Modal/ModalDivisaoProjetoS7Search'
import ModalFiltrosProjetosS7Search from '@/components/Modal/ModalFiltrosProjetosS7Search'

import ProjetoRepository from '@/repository/ProjetoRepository'

import ModalHelper from '@/ModalHelper'

export default function ProjetosS7Search(props) {
  const defaultParams = { divisao: 'S7 Search' }

  const [params, setParams] = useState(defaultParams)

  const history = useHistory()

  function cleanFilters(filters) {
    let obj = filters

    for (let key of Object.keys(obj)) {
      if (obj.hasOwnProperty(key)) {
        let filter = obj[key]

        if (filter && filter._id) {
          obj[key] = filter._id
        }
      }
    }

    return obj
  }

  function handleFiltersApply(filters) {
    setParams(update(params, {
      $set: {
        ...defaultParams,
        ...cleanFilters(filters)
      }
    }))
  }

  function handleNovoProjetoClick(event) {
    event.preventDefault()

    ModalHelper.open({ name: 'ModalDivisaoProjetoS7Search' })
  }

  function handleFiltrosClick(event) {
    event.preventDefault()

    ModalHelper.open({ name: 'ModalFiltrosProjetosS7Search' })
  }

  function handleS7SearchRowClick(row) {
    const varianteDivisao = row.varianteDivisao
    let path = ''

    switch (varianteDivisao) {
      case 'Recrutamento e Seleção':
        path = 'recrutamento-selecao'
        break

      case 'Mapeamento de Mercado':
        path = 'mapeamento-mercado'
        break

      case 'Pesquisa Salarial':
        path = 'pesquisa-salarial'
        break

      case 'Pesquisa de Benefícios':
        path = 'pesquisa-beneficios'
        break
    }

    history.push(`/projetos/s7-search/${path}/${row._id}`)
  }

  return (
    <motion.div className="page" initial="exit" animate="enter" exit="exit" variants={FadeVariants}>
      <div className="page-wrapper">
        <div className="page-header-lr">
          <div className="page-header-left">
            <div className="page-titles">
              <a className="page-tie --clickable" onClick={() => (history.goBack())}>Voltar</a>
              <p className="page-title">Projetos S7 Search</p>
            </div>
          </div>
          <div className="page-header-right">
            <button className="btn btn-five btn-orange" onClick={handleFiltrosClick}>
              <div className="svg btn-icon"><IcFilter/></div>
              Filtros
            </button>
            <button className="btn btn-five btn-orange" onClick={handleNovoProjetoClick}>
              <div className="svg btn-icon"><IcPlusSign/></div>
              Novo Projeto
            </button>
          </div>
        </div>
        <div className="page-content">
          
          <CustomTable
            leftColumns={[
              { label: 'Consultor responsável', attribute: 'consultorResponsavel.nome', width: '125px' },
              { label: 'Cliente', attribute: 'cliente.nomeFantasia', width: '275px' },
              { label: 'Categoria', attribute: 'varianteDivisao', width: '150px' },
              { label: 'Título', attribute: 'titulo', width: '300px' },
              { label: 'Cargo', attribute: 'cargo.nome', width: '100px' },
              { label: 'Área', attribute: 'area.nome', width: '175px' },
            ]}
            rightColumns={[
              { label: 'Status', attribute: 'status', width: '65px' },
              {
                label: 'Data de abertura',
                attribute: 'dataAbertura',
                width: '125px',
                transformer: (value) => {
                  return moment(value).format('L')
                }
              },
            ]}
            dataSource={{
              repository: ProjetoRepository,
              params: params
            }}
            clickHandler={handleS7SearchRowClick}
          />

        </div>
      </div>

      <ModalDivisaoProjetoS7Search/>
      <ModalFiltrosProjetosS7Search onSubmit={handleFiltersApply}/>
    </motion.div>
  )
}