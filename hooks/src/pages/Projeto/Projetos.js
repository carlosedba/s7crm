import React, { useState, useEffect } from 'react'
import { useSelector, useDispatch } from 'react-redux'
import {Link, useHistory} from 'react-router-dom'
import moment from 'moment'

import RecentProjectsTable from '@/components/RecentProjectsTable'
import ModalDivisaoProjeto from '@/components/Modal/ModalDivisaoProjeto'

import ProjetoRepository from '@/repository/ProjetoRepository'

import IcPlusSign from '@/assets/icons/ic_plus_sign.svg'

import ModalHelper from '@/ModalHelper'
import {motion} from 'framer-motion'
import FadeVariants from '@/animation/FadeVariants'

export default function Projetos(props) {
  const history = useHistory()

  function handleNovoProjetoClick(event) {
    event.preventDefault()

    ModalHelper.open({ name: 'ModalDivisaoProjeto' })
  }

  function handleS7UpRowClick(row) {
    history.push(`/projetos/s7-up/${row._id}`)
  }

  function handleS7BrandRowClick(row) {
    history.push(`/projetos/s7-brand/${row._id}`)
  }

  function handleS7SearchRowClick(row) {
    const varianteDivisao = row.varianteDivisao
    let path = ''

    switch (varianteDivisao) {
      case 'Recrutamento e Seleção':
        path = 'recrutamento-selecao'
        break

      case 'Mapeamento de Mercado':
        path = 'mapeamento-mercado'
        break

      case 'Pesquisa Salarial':
        path = 'pesquisa-salarial'
        break

      case 'Pesquisa de Benefícios':
        path = 'pesquisa-beneficios'
        break
    }

    history.push(`/projetos/s7-search/${path}/${row._id}`)
  }

  return (
    <motion.div className="page" initial="exit" animate="enter" exit="exit" variants={FadeVariants}>
      <div className="page-wrapper">
        <div className="page-header-lr">
          <div className="page-header-left">
            <div className="page-titles">
              <p className="page-title">Projetos</p>
              <p className="page-text"></p>
            </div>
          </div>
          <div className="page-header-right">
            <button className="btn btn-five btn-orange" onClick={handleNovoProjetoClick}>
              <div className="svg btn-icon"><IcPlusSign/></div>
              Novo Projeto
            </button>
          </div>
        </div>
        <div className="page-content">
          <div className="recent-projects">
            <RecentProjectsTable
              title="S7 Up"
              nextViewPath="/projetos/s7-up"
              leftColumns={[
                { label: 'Consultor responsável', attribute: 'consultorResponsavel.nome', width: '125px' },
                { label: 'Cliente', attribute: 'cliente.nomeFantasia', width: '350px' },
                { label: 'Categoria', attribute: 'categoria.nome', width: '200px' },
              ]}
              rightColumns={[
                { label: 'Status', attribute: 'status', width: '65px' },
                {
                  label: 'Data de abertura',
                  attribute: 'dataAbertura',
                  width: '125px',
                  transformer: (value) => {
                    return moment(value).format('L')
                  }
                },
              ]}
              dataSource={{
                repository: ProjetoRepository,
                params: {
                  divisao: 'S7 Up'
                }
              }}
              clickHandler={handleS7UpRowClick}
            />

            <RecentProjectsTable
              title="S7 Brand"
              nextViewPath="/projetos/s7-brand"
              leftColumns={[
                { label: 'Consultor responsável', attribute: 'consultorResponsavel.nome', width: '125px' },
                { label: 'Cliente', attribute: 'cliente.nomeFantasia', width: '350px' },
                { label: 'Categoria', attribute: 'categoria.nome', width: '200px' },
              ]}
              rightColumns={[
                { label: 'Status', attribute: 'status', width: '65px' },
                {
                  label: 'Data de abertura',
                  attribute: 'dataAbertura',
                  width: '125px',
                  transformer: (value) => {
                    return moment(value).format('L')
                  }
                },
              ]}
              dataSource={{
                repository: ProjetoRepository,
                params: {
                  divisao: 'S7 Brand'
                }
              }}
              clickHandler={handleS7BrandRowClick}
            />

            <RecentProjectsTable
              title="S7 Search"
              nextViewPath="/projetos/s7-search"
              leftColumns={[
                { label: 'Consultor responsável', attribute: 'consultorResponsavel.nome', width: '125px' },
                { label: 'Cliente', attribute: 'cliente.nomeFantasia', width: '275px' },
                { label: 'Categoria', attribute: 'varianteDivisao', width: '150px' },
                { label: 'Título', attribute: 'titulo', width: '300px' },
                { label: 'Cargo', attribute: 'cargo.nome', width: '100px' },
                { label: 'Área', attribute: 'area.nome', width: '175px' },
              ]}
              rightColumns={[
                { label: 'Status', attribute: 'status', width: '65px' },
                {
                  label: 'Data de abertura',
                  attribute: 'dataAbertura',
                  width: '125px',
                  transformer: (value) => {
                    return moment(value).format('L')
                  }
                },
              ]}
              dataSource={{
                repository: ProjetoRepository,
                params: {
                  divisao: 'S7 Search'
                }
              }}
              clickHandler={handleS7SearchRowClick}
            />
          </div>
        </div>
      </div>

      <ModalDivisaoProjeto/>
    </motion.div>
  )
}