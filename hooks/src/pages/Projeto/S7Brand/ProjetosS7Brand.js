import React, { useState, useEffect } from 'react'
import { useSelector, useDispatch } from 'react-redux'
import {Link, useHistory} from 'react-router-dom'
import {motion} from 'framer-motion'
import update from 'immutability-helper'
import moment from 'moment'

import FadeVariants from '@/animation/FadeVariants'

import IcPlusSign from '@/assets/icons/ic_plus_sign.svg'
import IcFilter from '@/assets/icons/ic_filter.svg'

import CustomTable from '@/components/CustomTable'
import ModalFiltrosProjetosS7Brand from '@/components/Modal/ModalFiltrosProjetosS7Brand'

import ProjetoRepository from '@/repository/ProjetoRepository'

import ModalHelper from '@/ModalHelper'

export default function ProjetosS7Brand(props) {
  const defaultParams = { divisao: 'S7 Brand' }

  const [params, setParams] = useState(defaultParams)

  const history = useHistory()

  function cleanFilters(filters) {
    let obj = filters

    for (let key of Object.keys(obj)) {
      if (obj.hasOwnProperty(key)) {
        let filter = obj[key]

        if (filter && filter._id) {
          obj[key] = filter._id
        }
      }
    }

    return obj
  }

  function handleFiltersApply(filters) {
    setParams(update(params, {
      $set: {
        ...defaultParams,
        ...cleanFilters(filters)
      }
    }))
  }

  function handleFiltrosClick(event) {
    event.preventDefault()

    ModalHelper.open({ name: 'ModalFiltrosProjetosS7Brand' })
  }

  function handleS7BrandRowClick(row) {
    history.push(`/projetos/s7-brand/${row._id}`)
  }

  return (
    <motion.div className="page" initial="exit" animate="enter" exit="exit" variants={FadeVariants}>
      <div className="page-wrapper">
        <div className="page-header-lr">
          <div className="page-header-left">
            <div className="page-titles">
              <a className="page-tie --clickable" onClick={() => (history.goBack())}>Voltar</a>
              <p className="page-title">Projetos S7 Brand</p>
            </div>
          </div>
          <div className="page-header-right">
            <button className="btn btn-five btn-orange" onClick={handleFiltrosClick}>
              <div className="svg btn-icon"><IcFilter/></div>
              Filtros
            </button>
            <Link className="btn btn-five btn-orange" to="/projetos/s7-brand/novo">
              <div className="svg btn-icon"><IcPlusSign/></div>
              Novo Projeto
            </Link>
          </div>
        </div>
        <div className="page-content">
          <CustomTable
            leftColumns={[
              { label: 'Consultor responsável', attribute: 'consultorResponsavel.nome', width: '125px' },
              { label: 'Cliente', attribute: 'cliente.nomeFantasia', width: '350px' },
              { label: 'Categoria', attribute: 'categoria.nome', width: '300px' },
            ]}
            rightColumns={[
              { label: 'Status', attribute: 'status', width: '65px' },
              {
                label: 'Data de abertura',
                attribute: 'dataAbertura',
                width: '125px',
                transformer: (value) => {
                  return moment(value).format('L')
                }
              },
            ]}
            dataSource={{
              repository: ProjetoRepository,
              params: params
            }}
            clickHandler={handleS7BrandRowClick}
          />
        </div>
      </div>

      <ModalFiltrosProjetosS7Brand onSubmit={handleFiltersApply}/>
    </motion.div>
  )
}