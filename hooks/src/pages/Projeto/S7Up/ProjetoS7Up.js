import React, {useState, useEffect} from 'react'
import { Link, useParams, useHistory } from 'react-router-dom'
import {motion} from 'framer-motion'
import _ from 'lodash'
import moment from 'moment'

import FadeVariants from '@/animation/FadeVariants'

import SingleDataWidget from '@/components/SingleDataWidget'
import MultiDataWidget from '@/components/MultiDataWidget'
import ModalConfirmacaoDeletar from '@/components/Modal/ModalConfirmacaoDeletar'
import ModalConfirmacaoDeletarArquivo from '@/components/Modal/ModalConfirmacaoDeletarArquivo'
import ModalEmpresaAlvoCreate from '@/components/Modal/ModalEmpresaAlvoCreate'
import ModalEmpresaAlvoUpdate from '@/components/Modal/ModalEmpresaAlvoUpdate'
import PopUpContainer from '@/components/PopUp/PopUpContainer'
import FilesPopUp from '@/components/PopUp/FilesPopUp'
import LoadingOverlay from '@/components/LoadingOverlay'

import useWait from '@/hooks/useWait'

import ProjetoRepository from '@/repository/ProjetoRepository'
import EmpresaAlvoRepository from '@/repository/EmpresaAlvoRepository'

import ModalHelper from '@/ModalHelper'

export default function ProjetoS7Up(props) {
  const [ready, setReady] = useState(false)

  const [projeto, setProjeto] = useState({})

  const params = useParams()
  const history = useHistory()
  const waitComplete = useWait(500)

  useEffect(() => {
    fetchProjeto()
  }, [])

  function fetchProjeto() {
    ProjetoRepository.findById(params.id)
      .then((data) => {
        setProjeto(data)

        setReady(true)
      })
  }

  function clickEmpresaAlvo(row) {
    history.push(`/empresas/${row.empresa._id}`)
  }

  function createEmpresaAlvo() {
    ModalHelper.open({
      name: 'ModalEmpresaAlvoCreate'
    })
  }

  function updateEmpresaAlvo(row) {
    ModalHelper.open({
      name: 'ModalEmpresaAlvoUpdate',
      data: row
    })
  }

  function deleteEmpresaAlvo(row) {
    ModalHelper.open({
      name: 'ModalConfirmacaoDeletar',
      params: {
        model: EmpresaAlvoRepository.model,
        objectId: row._id
      }
    })
  }

  function handleDelete(event) {
    ModalHelper.open({
      name: 'ModalConfirmacaoDeletar',
      params: {
        model: ProjetoRepository.model,
        objectId: projeto._id
      }
    })
  }

  return (
    <motion.div className="page" initial="exit" animate="enter" exit="exit" variants={FadeVariants}>
      <div className="page-wrapper">
        <div className="page-header-lr">
          <div className="page-header-left">
            <div className="page-titles">
              <a className="page-tie --clickable" onClick={() => (history.goBack())}>Voltar</a>
              <p className="page-title">{_.get(projeto, 'categoria.nome')}</p>
              <p className="page-text">
                {_.get(projeto, 'cliente.nomeFantasia')}
              </p>
            </div>
          </div>
          <div className="page-header-right">
            <Link to={`/projetos/s7-up/${params.id}/editar`} className="btn btn-five btn-orange">
              Editar
            </Link>
            <button className="btn btn-five btn-orange" onClick={handleDelete}>
              Deletar
            </button>
          </div>
        </div>
        <div className="page-content">

          <div className="data-widget-grid">

            <div className="data-widget-grid-row">
              <div className="data-widget-grid-col sp1">

                <div className="data-widget data-widget-one full">
                  <div className="data-widget-header">
                    <span className="data-widget-title">Objetivo</span>
                  </div>
                  <div className="data-widget-content">
                    <div className="data-widget-text">
                      <p>{projeto.objetivo}</p>
                    </div>
                  </div>
                </div>

                <SingleDataWidget
                  title="Geral"
                  fields={[
                    { label: 'Consultor responsável:', attribute: 'consultorResponsavel.firstName' },
                    { label: 'Status:', attribute: 'status' },
                    {
                      label: 'Data de abertura:',
                      attribute: 'dataAbertura',
                      transformer: (value) => {
                        return moment(value).format('L')
                      }
                    },
                    {
                      label: 'Data de fechamento:',
                      attribute: 'dataFechamento',
                      transformer: (value) => {
                        return moment(value).format('L')
                      }
                    },
                    {
                      label: 'Honorário:',
                      attribute: (data) => {
                        return _.get(data, 'honorario.currency') + ' ' + _.get(data, 'honorario.value')
                      }
                    },
                  ]}
                  initialData={projeto}
                />

              </div>
              <div className="data-widget-grid-col">
                <MultiDataWidget
                  size="full"
                  title="Empresas alvo"
                  columns={[
                    { label: 'Nome fantasia', attribute: 'empresa.nomeFantasia', width: '350px' },
                    { label: 'Segmento', attribute: 'empresa.segmento.nome', width: '150px' },
                    { label: 'Setor', attribute: 'empresa.setor.nome', width: '150px' },
                  ]}
                  dataSource={{
                    repository: EmpresaAlvoRepository,
                    params: {
                      projeto: _.get(projeto, '_id')
                    }
                  }}
                  clickHandler={clickEmpresaAlvo}
                  createHandler={createEmpresaAlvo}
                  updateHandler={updateEmpresaAlvo}
                  deleteHandler={deleteEmpresaAlvo}
                  ready={ready}
                />
              </div>
            </div>
          </div>

        </div>

        <ModalConfirmacaoDeletar/>
        <ModalConfirmacaoDeletarArquivo/>

        <ModalEmpresaAlvoCreate process={true} formComputed={{ projeto: _.get(projeto, '_id') }}/>
        <ModalEmpresaAlvoUpdate action="update" process={true} formComputed={{ projeto: _.get(projeto, '_id') }}/>

        <PopUpContainer>
          <FilesPopUp path={_.get(projeto, '_id')}/>
        </PopUpContainer>

        <LoadingOverlay visible={!(waitComplete && ready)}/>
      </div>
    </motion.div>
  )
}

