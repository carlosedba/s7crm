import React, { useState, useEffect } from 'react'
import { useSelector, useDispatch } from 'react-redux'
import { Link, useHistory } from 'react-router-dom'

import Form from '@/components/Form'
import TextInput from '@/components/Input/TextInput'
import SuggestionInput from '@/components/Input/SuggestionInput'
import CurrencyInput from '@/components/Input/CurrencyInput'
import DateInput from '@/components/Input/DateInput'
import SelectInput from '@/components/Input/SelectInput'
import FormSection from '@/components/FormSection'
import TextAttributeFormSection from '@/components/FormSection/TextAttributeFormSection'

import ModalObjetivo from '@/components/Modal/ModalObjetivo'
import ModalEmpresaAlvoCreate from '@/components/Modal/ModalEmpresaAlvoCreate'
import ModalEmpresaAlvoUpdate from '@/components/Modal/ModalEmpresaAlvoUpdate'

import IcPlusSign from '@/assets/icons/ic_plus_sign.svg'

import * as textMasks from '@/textMasks'
import FormHelper from '@/FormHelper'
import FormValidator from '@/FormValidator'

import ProjetoRepository from '@/repository/ProjetoRepository'
import CategoriaProjetoRepository from '@/repository/CategoriaProjetoRepository'
import EmpresaRepository from '@/repository/EmpresaRepository'
import ProfissionalRepository from '@/repository/ProfissionalRepository'
import UsuarioRepository from '@/repository/UsuarioRepository'

export default function NovoProjetoS7Up(props) {
  const [formId, setFormId] = useState('')
  const [formProps, setFormProps] = useState({})

  const [dataFechamento, setDataFechamento] = useState(false)
  const [categoriasProjeto, setCategoriasProjeto] = useState([])

  const history = useHistory()

  useEffect(() => {
    const formId = FormHelper.new({
      props: {
        raw: { divisao: 'S7 Up' }
      }
    })

    setFormId(formId)

    setFormProps({
      id: formId,
      model: ProjetoRepository.model,
      action: 'create'
    })

    fetchCategoriasProjeto()
  }, [])

  function fetchCategoriasProjeto() {
    CategoriaProjetoRepository.find({ divisao: 'S7 Up' })
      .then((data) => {
        const categoriasProjetoOptions = data.map((categoria) => ({ value: categoria._id, label: categoria.nome }))

        setCategoriasProjeto(categoriasProjetoOptions)
      })
  }

  function handleStatusChange(event) {
    if (['Lost', 'Finalizado', 'Stand-by', 'On hold'].includes(event.value)) {
      setDataFechamento(true)
    } else {
      setDataFechamento(false)
    }
  }

  async function handleSubmit(event) {
    FormValidator.validate({ id: formId, validator: 'ProjetoS7Up' })
      .then(() => {
        FormHelper.submit(formProps, true)
          .then(() => {
            alert('Projeto cadastrado com sucesso!')
            history.push('/projetos')
          })
          .catch((err) => {
            console.log(err)
            alert('Um erro ocorreu ao salvar os dados.')
          })
      })
      .catch((err) => {
        console.log(err)
        alert('Preencha todos campos obrigatórios.')
      })
  }

  return (
    <div className="page control-page">
      <div className="page-wrapper">
        <div className="page-header">
          <div className="page-titles">
            <a className="page-tie --clickable" onClick={() => (history.goBack())}>Voltar</a>
            <p className="page-title">Novo Projeto</p>
            <p className="page-subtitle">S7 Up</p>
          </div>
        </div>
        <div className="page-content">
          <div className="page-content-wrapper page-content-form-wrapper">
            <Form {...formProps}>
              <div className="inputs input-style-2">
                <SuggestionInput
                  formId={formId}
                  classes="w3"
                  label="Consultor responsável"
                  attribute="consultorResponsavel"
                  dataSource={{
                    repository: UsuarioRepository,
                    attributes: ['nome', 'sobrenome']
                  }}
                  required={true}
                />
                <div className="multi-input">
                  <SuggestionInput
                    formId={formId}
                    classes="w3"
                    label="Cliente"
                    attribute="cliente"
                    dataSource={{
                      repository: EmpresaRepository,
                      attributes: ['nomeFantasia']
                    }}
                    required={true}
                  />
                  <SuggestionInput
                    formId={formId}
                    classes="w3"
                    label="Contato no cliente"
                    attribute="contatoCliente"
                    dataSource={{
                      repository: ProfissionalRepository,
                      attributes: ['nome', 'sobrenome']
                    }}
                    required={true}
                  />
                </div>
                <div className="multi-input">
                  <SelectInput
                    formId={formId}
                    size="w1-q3"
                    label="Status"
                    attribute="status"
                    options={[
                      { value: 'Open', label: 'Open' },
                      { value: 'Ativo 1', label: 'Ativo 1' },
                      { value: 'Ativo 2', label: 'Ativo 2' },
                      { value: 'Ativo 3', label: 'Ativo 3' },
                      { value: 'Lost', label: 'Lost' },
                      { value: 'Finalizado', label: 'Finalizado' },
                      { value: 'Stand-by', label: 'Stand-by' },
                      { value: 'On hold', label: 'On hold' },
                    ]}
                    onChange={handleStatusChange}
                    required={true}
                  />
                  <SelectInput
                    formId={formId}
                    size="w3"
                    label="Categoria"
                    attribute="categoria"
                    options={categoriasProjeto}
                    required={true}
                  />
                </div>
                <div className="multi-input">
                  <DateInput formId={formId} classes="w1-q3" label="Data de abertura" attribute="dataAbertura" required={true}/>
                  {(dataFechamento) ? (<DateInput formId={formId} classes="w1-q3" label="Data de fechamento" attribute="dataFechamento"/>) : null}
                </div>
                <div className="multi-input">
                  <CurrencyInput formId={formId} classes="w2-q2" label="Honorário" attribute="honorario" mask={textMasks.BRL} required={true}/>
                </div>
              </div>
            </Form>

            <TextAttributeFormSection
              title="Objetivo"
              message="Editar objetivo"
              icon={IcPlusSign}
              onCreateModalName="ModalObjetivo">
              <ModalObjetivo parentFormId={formId} attribute="objetivo"/>
            </TextAttributeFormSection>

            <FormSection
              model="S7EmpresaAlvo"
              action="create"
              title="Empresas alvo"
              message="Editar empresas alvo"
              icon={IcPlusSign}
              attributeToDisplay="empresa"
              onCreateModalName="ModalEmpresaAlvoCreate"
              onUpdateModalName="ModalEmpresaAlvoUpdate">
              <ModalEmpresaAlvoCreate dependsOn={[{ id: formId, key: 'projeto' }]}/>
              <ModalEmpresaAlvoUpdate dependsOn={[{ id: formId, key: 'projeto' }]}/>
            </FormSection>

            <button className="btn btn-six btn-orange" onClick={handleSubmit}>Salvar</button>
          </div>
        </div>
      </div>
    </div>
  )
}

