import React, { useState, useEffect } from 'react'
import { Link, useHistory, useParams } from 'react-router-dom'

import Form from '@/components/Form'
import TextInput from '@/components/Input/TextInput'

import CategoriaProjetoRepository from '@/repository/CategoriaProjetoRepository'

import FormHelper from '@/FormHelper'
import FormValidator from '@/FormValidator'
import SelectInput from '@/components/Input/SelectInput'

export default function EditarCategoriaProjeto(props) {
  const [formId, setFormId] = useState(null)
  const [formProps, setFormProps] = useState({})

  const [categoriaProjeto, setCategoriaProjeto] = useState([])

  const params = useParams()
  const history = useHistory()

  useEffect(() => {
    const formId = FormHelper.new()

    FormHelper.setObjectId(formId, params.id)

    setFormId(formId)

    setFormProps({
      id: formId,
      model: CategoriaProjetoRepository.model,
      action: 'update'
    })

    fetchCategoriaProjeto(formId)
  }, [])

  function fetchCategoriaProjeto(formId) {
    CategoriaProjetoRepository.findById(params.id)
      .then(async (data) => {
        setCategoriaProjeto(data)
      })
  }

  async function handleSubmit(event) {
    FormValidator.validate(formProps)
      .then(() => {
        FormHelper.submit(formProps, true)
          .then(() => {
            alert('Categoria atualizada com sucesso!')
            history.push('/categorias-projeto')
          })
          .catch((err) => {
            console.error(err)
            alert('Um erro ocorreu ao salvar os dados.')
          })
      })
      .catch((err) => {
        console.error(err)
        alert('Preencha todos campos obrigatórios.')
      })
  }

  return (
    <div className="page">
      <div className="page-wrapper">
        <div className="page-header">
          <div className="page-titles">
            <Link to="/categorias-projeto" className="page-tie">Voltar</Link>
            <p className="page-title">Editar Categoria de Projeto</p>
            <p className="page-subtitle"></p>
          </div>
        </div>
        <div className="page-content">
          <div className="page-content-wrapper page-content-form-wrapper">
            <Form {...formProps}>
              <div className="inputs input-style-2">
                <SelectInput
                  formId={formId}
                  size="w1-q3"
                  label="Divisão"
                  attribute="divisao"
                  required={true}
                  options={[
                    { value: 'S7 Brand', label: 'S7 Brand' },
                    { value: 'S7 Search', label: 'S7 Search' },
                    { value: 'S7 Up', label: 'S7 Up' },
                  ]}
                  initialValue={categoriaProjeto.divisao}
                />
                <TextInput formId={formId} classes="w4" label="Nome" attribute="nome" initialValue={categoriaProjeto.nome}/>
              </div>
            </Form>

            <button className="btn btn-six btn-orange" onClick={handleSubmit}>Salvar</button>
          </div>
        </div>
      </div>
    </div>
  )
}