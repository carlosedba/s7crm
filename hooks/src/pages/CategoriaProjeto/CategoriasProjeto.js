import React, { useState, useEffect } from 'react'
import { Link, useHistory } from 'react-router-dom'

import DataTable from '@/components/DataTable'
import ModalConfirmacaoDeletar from '@/components/Modal/ModalConfirmacaoDeletar'

import IcPlusSign from '@/assets/icons/ic_plus_sign.svg'
import IcPencil from '@/assets/icons/ic_pencil.svg'
import IcTrashcan from '@/assets/icons/ic_trashcan.svg'

import CategoriaProjetoRepository from '@/repository/CategoriaProjetoRepository'

import ModalHelper from '@/ModalHelper'

export default function CategoriasProjeto(props) {
  const [categoriasProjeto, setCategoriasProjeto] = useState([])

  const history = useHistory()

  useEffect(() => {
    fetchCategoriasProjeto()
  }, [])

  const columns = [
    {
      Header: 'Divisão',
      accessor: 'divisao',
      width: 100
    },
    {
      Header: 'Nome',
      accessor: 'nome',
      width: 300
    },
    {
      Header: ' ',
      width: 100,
      Cell: ({row}) => (
        <div className="data-actions no-margin">
          <button className="svg data-action" onClick={(event) => handleEdit(event, row.original)}>
            <IcPencil/>
          </button>
          <button className="svg data-action" onClick={(event) => handleDelete(event, row.original)}>
            <IcTrashcan/>
          </button>
        </div>
      )
    }
  ]

  function fetchCategoriasProjeto(cursor) {
    let queryParams = {}

    if (cursor) queryParams = {...queryParams, cursor}

    CategoriaProjetoRepository.find(queryParams)
      .then((data) => {
        setCategoriasProjeto(data)
      })
  }

  function handleEdit(event, row) {
    history.push(`/categorias-projeto/${row._id}/editar`)
  }

  function handleDelete(event, row) {
    ModalHelper.open({
      name: 'ModalConfirmacaoDeletar',
      params: {
        model: CategoriaProjetoRepository.model,
        objectId: row._id
      }
    })
  }

  return (
    <div className="page">
      <div className="page-wrapper">
        <div className="page-header-lr">
          <div className="page-header-left">
            <div className="page-titles">
              <p className="page-title">Categorias de Projeto</p>
              <p className="page-text"></p>
            </div>
          </div>
          <div className="page-header-right">
            <Link to="/categorias-projeto/cadastrar" className="btn btn-five btn-orange">
              <div className="svg btn-icon"><IcPlusSign/></div>
              Cadastrar Categoria
            </Link>
          </div>
        </div>
        <div className="page-content">
          <DataTable
            columns={columns}
            data={categoriasProjeto}
          />
        </div>

        <ModalConfirmacaoDeletar/>
      </div>
    </div>
  )
}