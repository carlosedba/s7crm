import React, { useState, useEffect } from 'react'
import { Link, useHistory } from 'react-router-dom'

import Form from '@/components/Form'
import SelectInput from '@/components/Input/SelectInput'
import TextInput from '@/components/Input/TextInput'

import CategoriaProjetoRepository from '@/repository/CategoriaProjetoRepository'

import FormHelper from '@/FormHelper'
import FormValidator from '@/FormValidator'

export default function CadastrarCategoriaProjeto(props) {
  const [formId, setFormId] = useState('')
  const [formProps, setFormProps] = useState({})

  const history = useHistory()

  useEffect(() => {
    const formId = FormHelper.new()

    setFormId(formId)

    setFormProps({
      id: formId,
      model: CategoriaProjetoRepository.model,
      action: 'create'
    })
  }, [])

  async function handleSubmit(event) {
    FormValidator.validate(formProps)
      .then(() => {
        FormHelper.submit(formProps, true)
          .then(() => {
            alert('Categoria cadastrada com sucesso!')
            history.push('/categorias-projeto')
          })
          .catch((err) => {
            console.error(err)
            alert('Um erro ocorreu ao salvar os dados.')
          })
      })
      .catch((err) => {
        console.error(err)
        alert('Preencha todos campos obrigatórios.')
      })
  }

  return (
    <div className="page">
      <div className="page-wrapper">
        <div className="page-header">
          <div className="page-titles">
            <Link to="/categorias-projeto" className="page-tie">Voltar</Link>
            <p className="page-title">Cadastrar Categoria de Projeto</p>
            <p className="page-subtitle"></p>
          </div>
        </div>
        <div className="page-content">
          <div className="page-content-wrapper page-content-form-wrapper">
            <Form {...formProps}>
              <div className="inputs input-style-2">
                <SelectInput
                  formId={formId}
                  size="w1-q3"
                  label="Divisão"
                  attribute="divisao"
                  required={true}
                  options={[
                    { value: 'S7 Brand', label: 'S7 Brand' },
                    { value: 'S7 Search', label: 'S7 Search' },
                    { value: 'S7 Up', label: 'S7 Up' },
                  ]}
                />
                <TextInput formId={formId} classes="w4" label="Nome" attribute="nome" required={true}/>
              </div>
            </Form>

            <button className="btn btn-six btn-orange" onClick={handleSubmit}>Salvar</button>
          </div>
        </div>
      </div>
    </div>
  )
}