import React, { useState, useEffect } from 'react'
import { Link, useHistory } from 'react-router-dom'

import Form from '@/components/Form'
import TextInput from '@/components/Input/TextInput'

import BeneficioRepository from '@/repository/BeneficioRepository'

import FormHelper from '@/FormHelper'
import FormValidator from '@/FormValidator'

export default function CadastrarBeneficio(props) {
  const [formId, setFormId] = useState('')
  const [formProps, setFormProps] = useState({})

  const history = useHistory()

  useEffect(() => {
    const formId = FormHelper.new()

    setFormId(formId)

    setFormProps({
      id: formId,
      model: BeneficioRepository.model,
      action: 'create'
    })
  }, [])

  async function handleSubmit(event) {
    FormValidator.validate(formProps)
      .then(() => {
        FormHelper.submit(formProps, true)
          .then(() => {
            alert('Benefício cadastrado com sucesso!')
            history.push('/beneficios')
          })
          .catch((err) => {
            console.error(err)
            alert('Um erro ocorreu ao salvar os dados.')
          })
      })
      .catch((err) => {
        console.error(err)
        alert('Preencha todos campos obrigatórios.')
      })
  }

  return (
    <div className="page">
      <div className="page-wrapper">
        <div className="page-header">
          <div className="page-titles">
            <Link to="/beneficios" className="page-tie">Voltar</Link>
            <p className="page-title">Cadastrar Benefício</p>
            <p className="page-subtitle"></p>
          </div>
        </div>
        <div className="page-content">
          <div className="page-content-wrapper page-content-form-wrapper">
            <Form {...formProps}>
              <div className="inputs input-style-2">
                <TextInput formId={formId} classes="w5" label="Nome" attribute="nome" required={true}/>
              </div>
            </Form>

            <button className="btn btn-six btn-orange" onClick={handleSubmit}>Salvar</button>
          </div>
        </div>
      </div>
    </div>
  )
}