import React, { useState, useEffect } from 'react'
import { Link, useHistory } from 'react-router-dom'

import DataTable from '@/components/DataTable'
import ModalConfirmacaoDeletar from '@/components/Modal/ModalConfirmacaoDeletar'

import IcPlusSign from '@/assets/icons/ic_plus_sign.svg'
import IcPencil from '@/assets/icons/ic_pencil.svg'
import IcTrashcan from '@/assets/icons/ic_trashcan.svg'

import StatusRepository from '@/repository/StatusRepository'

import ModalHelper from '@/ModalHelper'

export default function Status(props) {
  const [status, setStatus] = useState([])

  const history = useHistory()

  useEffect(() => {
    fetchStatus()
  }, [])

  const columns = [
    {
      Header: 'Nome',
      accessor: 'nome',
      width: 600
    },
    {
      Header: ' ',
      width: 100,
      Cell: ({row}) => (
        <div className="data-actions no-margin">
          <button className="svg data-action" onClick={(event) => handleEdit(event, row.original)}>
            <IcPencil/>
          </button>
          <button className="svg data-action" onClick={(event) => handleDelete(event, row.original)}>
            <IcTrashcan/>
          </button>
        </div>
      )
    }
  ]

  function fetchStatus(cursor) {
    let queryParams = {}

    if (cursor) queryParams = {...queryParams, cursor}

    StatusRepository.find(queryParams)
      .then((data) => {
        setStatus(data)
      })
  }

  function handleEdit(event, row) {
    history.push(`/status/${row._id}/editar`)
  }

  function handleDelete(event, row) {
    ModalHelper.open({
      name: 'ModalConfirmacaoDeletar',
      params: {
        model: StatusRepository.model,
        objectId: row._id
      }
    })
  }

  return (
    <div className="page">
      <div className="page-wrapper">
        <div className="page-header-lr">
          <div className="page-header-left">
            <div className="page-titles">
              <p className="page-title">Status</p>
              <p className="page-text"></p>
            </div>
          </div>
          <div className="page-header-right">
            <Link to="/status/cadastrar" className="btn btn-five btn-orange">
              <div className="svg btn-icon"><IcPlusSign/></div>
              Cadastrar Status
            </Link>
          </div>
        </div>
        <div className="page-content">
          <DataTable
            columns={columns}
            data={status}
          />
        </div>

        <ModalConfirmacaoDeletar/>
      </div>
    </div>
  )
}