import React, { useState, useEffect } from 'react'
import { Link, useHistory } from 'react-router-dom'

import Form from '@/components/Form'
import TextInput from '@/components/Input/TextInput'

import SetorRepository from '@/repository/SetorRepository'

import FormHelper from '@/FormHelper'
import FormValidator from '@/FormValidator'
import SegmentoRepository from '@/repository/SegmentoRepository'
import SelectInput from '@/components/Input/SelectInput'

export default function CadastrarSetor(props) {
  const [formId, setFormId] = useState('')
  const [formProps, setFormProps] = useState({})

  const [segmentos, setSegmentos] = useState([])

  const history = useHistory()

  useEffect(() => {
    const formId = FormHelper.new()

    setFormId(formId)

    setFormProps({
      id: formId,
      model: SetorRepository.model,
      action: 'create'
    })

    fetchSegmentos()
  }, [])

  function fetchSegmentos() {
    SegmentoRepository.find()
      .then((data) => {
        const segmentoOptions = data.map((segmento) => ({ value: segmento._id, label: segmento.nome }))

        setSegmentos(segmentoOptions)
      })
  }

  async function handleSubmit(event) {
    FormValidator.validate(formProps)
      .then(() => {
        FormHelper.submit(formProps, true)
          .then(() => {
            alert('Setor cadastrado com sucesso!')
            history.push('/setores')
          })
          .catch((err) => {
            console.error(err)
            alert('Um erro ocorreu ao salvar os dados.')
          })
      })
      .catch((err) => {
        console.error(err)
        alert('Preencha todos campos obrigatórios.')
      })
  }

  return (
    <div className="page">
      <div className="page-wrapper">
        <div className="page-header">
          <div className="page-titles">
            <Link to="/setores" className="page-tie">Voltar</Link>
            <p className="page-title">Cadastrar Setor</p>
            <p className="page-subtitle"></p>
          </div>
        </div>
        <div className="page-content">
          <div className="page-content-wrapper page-content-form-wrapper">
            <Form {...formProps}>
              <div className="inputs input-style-2">
                <SelectInput
                  formId={formId}
                  size="w2-q2"
                  label="Segmento"
                  attribute="segmento"
                  required={true}
                  options={segmentos}
                />
                <TextInput formId={formId} classes="w4" label="Nome" attribute="nome" required={true}/>
              </div>
            </Form>

            <button className="btn btn-six btn-orange" onClick={handleSubmit}>Salvar</button>
          </div>
        </div>
      </div>
    </div>
  )
}