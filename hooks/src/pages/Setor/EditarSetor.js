import React, { useState, useEffect } from 'react'
import { Link, useHistory, useParams } from 'react-router-dom'

import Form from '@/components/Form'
import TextInput from '@/components/Input/TextInput'
import SelectInput from '@/components/Input/SelectInput'

import SetorRepository from '@/repository/SetorRepository'
import SegmentoRepository from '@/repository/SegmentoRepository'

import FormHelper from '@/FormHelper'
import FormValidator from '@/FormValidator'

export default function EditarSetor(props) {
  const [formId, setFormId] = useState(null)
  const [formProps, setFormProps] = useState({})

  const [setor, setSetor] = useState([])
  const [segmentos, setSegmentos] = useState([])

  const params = useParams()
  const history = useHistory()

  useEffect(() => {
    const formId = FormHelper.new()

    FormHelper.setObjectId(formId, params.id)

    setFormId(formId)

    setFormProps({
      id: formId,
      model: SetorRepository.model,
      action: 'update'
    })

    fetchSetor(formId)
  }, [])

  function fetchSetor(formId) {
    SetorRepository.findById(params.id)
      .then(async (data) => {
        await fetchSegmentos()

        setSetor(data)
      })
  }

  function fetchSegmentos() {
    return new Promise((resolve, reject) => {
      SegmentoRepository.find()
        .then((data) => {
          const options = data.map((segmento) => ({ value: segmento._id, label: segmento.nome }))

          setSegmentos(options)

          resolve()
        })
    })
  }

  async function handleSubmit(event) {
    FormValidator.validate(formProps)
      .then(() => {
        FormHelper.submit(formProps, true)
          .then(() => {
            alert('Setor atualizado com sucesso!')
            history.push('/setores')
          })
          .catch((err) => {
            console.error(err)
            alert('Um erro ocorreu ao salvar os dados.')
          })
      })
      .catch((err) => {
        console.error(err)
        alert('Preencha todos campos obrigatórios.')
      })
  }

  return (
    <div className="page">
      <div className="page-wrapper">
        <div className="page-header">
          <div className="page-titles">
            <Link to="/setores" className="page-tie">Voltar</Link>
            <p className="page-title">Editar Setor</p>
            <p className="page-subtitle"></p>
          </div>
        </div>
        <div className="page-content">
          <div className="page-content-wrapper page-content-form-wrapper">
            <Form {...formProps}>
              <div className="inputs input-style-2">
                <SelectInput
                  formId={formId}
                  size="w2-q2"
                  label="Segmento"
                  attribute="segmento"
                  required={true}
                  options={segmentos}
                  initialValue={setor.segmento}
                />
                <TextInput formId={formId} classes="w4" label="Nome" attribute="nome" initialValue={setor.nome}/>
              </div>
            </Form>

            <button className="btn btn-six btn-orange" onClick={handleSubmit}>Salvar</button>
          </div>
        </div>
      </div>
    </div>
  )
}