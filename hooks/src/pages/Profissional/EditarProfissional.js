import React, { useState, useEffect } from 'react'
import { useSelector, useDispatch } from 'react-redux'
import { Link, useHistory, useParams } from 'react-router-dom'

import Form from '@/components/Form'
import TextInput from '@/components/Input/TextInput'
import TextDateInput from '@/components/Input/TextDateInput'
import SelectInput from '@/components/Input/SelectInput'
import ArrayAttributeFormSection from '@/components/FormSection/ArrayAttributeFormSection'
import ObjectArrayAttributeFormSection from '@/components/FormSection/ObjectArrayAttributeFormSection'
import ModalFilhosCreate from '@/components/Modal/ModalFilhosCreate'
import ModalFilhosUpdate from '@/components/Modal/ModalFilhosUpdate'
import ModalIdiomaCreate from '@/components/Modal/ModalIdiomaCreate'
import ModalIdiomaUpdate from '@/components/Modal/ModalIdiomaUpdate'
import TextAttributeFormSection from '@/components/FormSection/TextAttributeFormSection'
import ModalAbordagem from '@/components/Modal/ModalAbordagem'
import ModalComentarios from '@/components/Modal/ModalComentarios'

import IcPlusSign from '@/assets/icons/ic_plus_sign.svg'

import PaisRepository from '@/repository/PaisRepository'
import EstadoRepository from '@/repository/EstadoRepository'
import CidadeRepository from '@/repository/CidadeRepository'
import ProfissionalRepository from '@/repository/ProfissionalRepository'

import * as textMasks from '@/textMasks'
import FormHelper from '@/FormHelper'
import FormValidator from '@/FormValidator'

export default function EditarProfissional(props) {
  const [formId, setFormId] = useState(null)
  const [formProps, setFormProps] = useState({})

  const [profissional, setProfissional] = useState({})
  const [naturalidades, setNaturalidades] = useState([])
  const [estadoOrigemList, setEstadoOrigemList] = useState([])
  const [cidadeOrigemList, setCidadeOrigemList] = useState([])
  const [paisAtualList, setPaisAtualList] = useState([])
  const [estadoAtualList, setEstadoAtualList] = useState([])
  const [cidadeAtualList, setCidadeAtualList] = useState([])

  const params = useParams()
  const history = useHistory()

  useEffect(() => {
    const formId = FormHelper.new()

    FormHelper.setObjectId(formId, params.id)

    setFormId(formId)

    setFormProps({
      id: formId,
      model: ProfissionalRepository.model,
      action: 'update'
    })

    fetchPaises()
    fetchProfissional()
  }, [])

  function fetchProfissional() {
    ProfissionalRepository.findById(params.id)
      .then(async (data) => {
        await fetchEstadoOrigemList(data.naturalidade)
        await fetchCidadeOrigemList(data.estadoOrigem)
        await fetchEstadoAtualList(data.paisAtual)
        await fetchCidadeAtualList(data.estadoAtual)

        setProfissional(data)
        setAbordagem(data.abordagem)
        setComentarios(data.comentarios)
        setFilhos(data.filhos)
        setIdiomas(data.idiomas)
      })
  }

  function fetchEstadoOrigemList(pais) {
    return new Promise((resolve, reject) => {
      EstadoRepository.find({ pais: pais._id })
        .then((data) => {
          const estadoOptions = data.map((estado) => ({ value: estado._id, label: estado.nome }))

          setEstadoOrigemList(estadoOptions)
        })
        .then(resolve)
    })
  }

  function fetchCidadeOrigemList(estado) {
    return new Promise((resolve, reject) => {
      CidadeRepository.find({ estado: estado._id })
        .then((data) => {
          const cidadeOptions = data.map((cidade) => ({ value: cidade._id, label: cidade.nome }))

          setCidadeOrigemList(cidadeOptions)
        })
        .then(resolve)
    })
  }

  function fetchEstadoAtualList(pais) {
    return new Promise((resolve, reject) => {
      EstadoRepository.find({ pais: pais._id })
        .then((data) => {
          const estadoOptions = data.map((estado) => ({ value: estado._id, label: estado.nome }))

          setEstadoAtualList(estadoOptions)
        })
        .then(resolve)
    })
  }

  function fetchCidadeAtualList(estado) {
    return new Promise((resolve, reject) => {
      CidadeRepository.find({ estado: estado._id })
        .then((data) => {
          const cidadeOptions = data.map((cidade) => ({ value: cidade._id, label: cidade.nome }))

          setCidadeAtualList(cidadeOptions)
        })
        .then(resolve)
    })
  }

  function setAbordagem(abordagem) {
    FormHelper.setRawAttribute(formId, 'abordagem',{
      $set: abordagem
    })
  }

  function setComentarios(comentarios) {
    FormHelper.setRawAttribute(formId, 'comentarios',{
      $set: comentarios
    })
  }

  function setFilhos(filhos) {
    FormHelper.setRawAttribute(formId, 'filhos',{
      $set: filhos
    })
  }

  function setIdiomas(idiomas) {
    FormHelper.setRawAttribute(formId, 'idiomas',{
      $set: idiomas
    })

    FormHelper.setComputedAttribute(formId, 'idiomas',{
      $set: idiomas
    })
  }

  function fetchPaises() {
    PaisRepository.find()
      .then((data) => {
        const naturalidadeOptions = data.map((pais) => ({ value: pais._id, label: pais.gentilico }))
        const paisAtualOptions = data.map((pais) => ({ value: pais._id, label: pais.nome }))

        setNaturalidades(naturalidadeOptions)
        setPaisAtualList(paisAtualOptions)
      })
  }

  function handleNaturalidadeChange(event) {
    EstadoRepository.find({ pais: event.value })
      .then((data) => {
        const estadoOrigemOptions = data.map((estado) => ({ value: estado._id, label: estado.nome }))

        setEstadoOrigemList(estadoOrigemOptions)
      })
  }

  function handleEstadoOrigemChange(event) {
    CidadeRepository.find({ estado: event.value })
      .then((data) => {
        const cidadeOrigemOptions = data.map((cidade) => ({ value: cidade._id, label: cidade.nome }))

        setCidadeOrigemList(cidadeOrigemOptions)
      })
  }

  function handlePaisAtualChange(event) {
    EstadoRepository.find({ pais: event.value })
      .then((data) => {
        const estadoAutalOptions = data.map((estado) => ({ value: estado._id, label: estado.nome }))

        setEstadoAtualList(estadoAutalOptions)
      })
  }

  function handleEstadoAtualChange(event) {
    console.log(event)
    CidadeRepository.find({ estado: event.value })
      .then((data) => {
        const cidadeAtualOptions = data.map((cidade) => ({ value: cidade._id, label: cidade.nome }))

        setCidadeAtualList(cidadeAtualOptions)
      })
  }

  async function handleSubmit(event) {
    FormValidator.validate(formProps)
      .then(() => {
        FormHelper.submit(formProps, true)
          .then(() => {
            alert('Profissional atualizado com sucesso!')
            history.push(`/profissionais/${params.id}`)
          })
          .catch((err) => {
            console.error(err)
            alert('Um erro ocorreu ao salvar os dados.')
          })
      })
      .catch((err) => {
        console.error(err)
        alert('Preencha todos campos obrigatórios.')
      })
  }

  return (
    <div className="page">
      <div className="page-wrapper">
        <div className="page-header">
          <div className="page-titles">
            <Link to={`/profissionais/${params.id}`} className="page-tie">Voltar</Link>
            <p className="page-title">Editar Profissional</p>
            <p className="page-subtitle">Informe os dados em seus respectivos campos:</p>
            <p className="page-text"></p>
          </div>
        </div>
        <div className="page-content">
          <div className="page-content-wrapper page-content-form-wrapper">
            <Form {...formProps}>
              <div className="inputs input-style-2">
                <div className="multi-input">
                  <TextInput formId={formId} classes="w2" label="Nome" attribute="nome" required={true} initialValue={profissional.nome}/>
                  <TextInput formId={formId} classes="w2-q2" label="Sobrenome" attribute="sobrenome" required={true} initialValue={profissional.sobrenome}/>
                  <SelectInput
                    formId={formId}
                    size="w1-q3"
                    label="Sexo"
                    attribute="sexo"
                    required={true}
                    options={[
                      { value: 'Masculino', label: 'Masculino' },
                      { value: 'Feminino', label: 'Feminino' },
                      { value: 'Outro', label: 'Outro' },
                    ]}
                    initialValue={profissional.sexo}
                  />
                  <SelectInput
                    formId={formId}
                    size="w1-q2"
                    label="Deficiente"
                    attribute="deficiente"
                    required={true}
                    options={[
                      { value: 'Sim', label: 'Sim' },
                      { value: 'Não', label: 'Não' },
                    ]}
                    initialValue={profissional.deficiente}
                  />
                </div>
                <div className="multi-input">
                  <TextInput formId={formId} classes="w1-q2" label="RG" attribute="rg" required={true} mask={textMasks.RG} initialValue={profissional.rg}/>
                  <TextInput formId={formId} classes="w1-q3" label="CPF" attribute="cpf" required={true} mask="cpf" initialValue={profissional.cpf}/>
                  <TextInput formId={formId} classes="w1-q3" label="Telefone" attribute="telefone" mask="telefone" initialValue={profissional.telefone}/>
                  <TextInput formId={formId} classes="w1-q3" label="Celular 1" attribute="celular1" mask="celular" required={true} initialValue={profissional.celular1}/>
                  <TextInput formId={formId} classes="w1-q3" label="Celular 2" attribute="celular2" mask="celular" initialValue={profissional.celular2}/>
                </div>
                <div className="multi-input">
                  <TextInput formId={formId} classes="w06" label="Idade" attribute="idade" initialValue={profissional.idade}/>
                  <TextDateInput formId={formId} classes="w2" label="Data de nascimento" attribute="dataNascimento" required={true} mask={textMasks.DATE} initialValue={profissional.dataNascimento}/>
                  <SelectInput
                    formId={formId}
                    size="w2"
                    label="Estado civil"
                    attribute="estadoCivil"
                    required={true}
                    options={[
                      { value: 'Solteiro (a)', label: 'Solteiro (a)' },
                      { value: 'Casado (a)', label: 'Casado (a)' },
                      { value: 'Divorciado (a)', label: 'Divorciado (a)' },
                      { value: 'Viúvo (a)', label: 'Viúvo (a)' },
                      { value: 'União estável', label: 'União estável' },
                    ]}
                    initialValue={profissional.estadoCivil}
                  />
                  <TextInput formId={formId} classes="w2-q2" label="Emprego do cônjuge" attribute="empregoConjuge" initialValue={profissional.empregoConjuge}/>
                </div>
                <div className="multi-input">
                  <TextInput formId={formId} classes="w3-q1" label="E-mail 1" attribute="email1" required={true} initialValue={profissional.email1}/>
                  <TextInput formId={formId} classes="w3-q1" label="E-mail 2" attribute="email2" initialValue={profissional.email2}/>
                </div>
                <div className="multi-input">
                  <TextInput formId={formId} classes="w3-q1" label="E-mail comercial" attribute="emailComercial" initialValue={profissional.emailComercial}/>
                  <TextInput formId={formId} classes="w2" label="Skype" attribute="skype" initialValue={profissional.skype}/>
                  <TextInput formId={formId} classes="w2" label="LinkedIn" attribute="linkedin" intialValue={profissional.linkedin}/>
                </div>
                <div className="multi-input">
                  <SelectInput
                    formId={formId}
                    size="w2"
                    label="Naturalidade"
                    attribute="naturalidade"
                    required={true}
                    options={naturalidades}
                    initialValue={profissional.naturalidade}
                    onChange={handleNaturalidadeChange}
                  />
                  <SelectInput
                    formId={formId}
                    size="w2-q1"
                    label="Estado de origem"
                    attribute="estadoOrigem"
                    required={true}
                    options={estadoOrigemList}
                    initialValue={profissional.estadoOrigem}
                    onChange={handleEstadoOrigemChange}
                  />
                  <SelectInput
                    formId={formId}
                    size="w2"
                    label="Cidade de origem"
                    attribute="cidadeOrigem"
                    required={true}
                    options={cidadeOrigemList}
                    initialValue={profissional.cidadeOrigem}
                  />
                </div>
                <div className="multi-input">
                  <SelectInput
                    formId={formId}
                    size="w2"
                    label="País atual"
                    attribute="paisAtual"
                    required={true}
                    options={paisAtualList}
                    initialValue={profissional.paisAtual}
                    onChange={handlePaisAtualChange}
                  />
                  <SelectInput
                    formId={formId}
                    size="w2-q1"
                    label="Estado atual"
                    attribute="estadoAtual"
                    required={true}
                    options={estadoAtualList}
                    initialValue={profissional.estadoAtual}
                    onChange={handleEstadoAtualChange}
                  />
                  <SelectInput
                    formId={formId}
                    size="w2"
                    label="Cidade atual"
                    attribute="cidadeAtual"
                    required={true}
                    options={cidadeAtualList}
                    initialValue={profissional.cidadeAtual}
                  />
                </div>
              </div>
            </Form>

            <TextAttributeFormSection
              title="Abordagem"
              message="Editar abordagem"
              icon={IcPlusSign}
              onCreateModalName="ModalAbordagem">
              <ModalAbordagem parentFormId={formId} attribute="abordagem"/>
            </TextAttributeFormSection>

            <TextAttributeFormSection
              title="Comentários"
              message="Editar comentários"
              icon={IcPlusSign}
              onCreateModalName="ModalComentarios">
              <ModalComentarios parentFormId={formId} attribute="comentarios"/>
            </TextAttributeFormSection>

            <ArrayAttributeFormSection
              formId={formId}
              attribute="filhos"
              title="Filhos"
              message="Adicionar filho"
              icon={IcPlusSign}
              onCreateModalName="ModalFilhosCreate"
              onUpdateModalName="ModalFilhosUpdate">
              <ModalFilhosCreate parentFormId={formId} attribute="filhos"/>
              <ModalFilhosUpdate parentFormId={formId} attribute="filhos"/>
            </ArrayAttributeFormSection>

            <ObjectArrayAttributeFormSection
              formId={formId}
              attribute="idiomas"
              title="Idiomas"
              message="Adicionar idioma"
              icon={IcPlusSign}
              attributeIsSelect={true}
              attributeToDisplay="idioma"
              onCreateModalName="ModalIdiomaCreate"
              onUpdateModalName="ModalIdiomaUpdate">
              <ModalIdiomaCreate parentFormId={formId} attribute="idiomas"/>
              <ModalIdiomaUpdate parentFormId={formId} attribute="idiomas"/>
            </ObjectArrayAttributeFormSection>

            <button className="btn btn-six btn-orange" onClick={handleSubmit}>Salvar</button>
          </div>
        </div>
      </div>
    </div>
  )
}

