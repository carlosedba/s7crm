import React, { useState, useEffect } from 'react'
import { Link, useHistory } from 'react-router-dom'
import { motion } from 'framer-motion'
import update from 'immutability-helper'

import FadeVariants from '@/animation/FadeVariants'

import CustomTable from '@/components/CustomTable'
import ModalFiltrosProfissionais from '@/components/Modal/ModalFiltrosProfissionais'

import IcPlusSign from '@/assets/icons/ic_plus_sign.svg'
import IcFilter from '@/assets/icons/ic_filter.svg'

import ProfissionalRepository from '@/repository/ProfissionalRepository'

import ModalHelper from '@/ModalHelper'

export default function Profissionais(props) {
  const defaultParams = { empregoAtual: true }

  const [params, setParams] = useState(defaultParams)

  const history = useHistory()

  function cleanFilters(filters) {
    let obj = filters

    for (let key of Object.keys(obj)) {
      if (obj.hasOwnProperty(key)) {
        let filter = obj[key]

        if (filter && filter._id) {
          obj[key] = filter._id
        }
      }
    }

    return obj
  }

  function handleFiltersApply(filters) {
    setParams(update(params, {
      $set: {
        ...defaultParams,
        ...cleanFilters(filters)
      }
    }))
  }

  function handleFiltrosClick(event) {
    event.preventDefault()

    ModalHelper.open({ name: 'ModalFiltrosProfissionais' })
  }

  function handleRowClick(row) {
    history.push(`/profissionais/${row._id}`)
  }

  return (
    <motion.div className="page" initial="exit" animate="enter" exit="exit" variants={FadeVariants}>
      <div className="page-wrapper">
        <div className="page-header-lr">
          <div className="page-header-left">
            <div className="page-titles">
              <p className="page-title">Profissionais</p>
              <p className="page-text"></p>
            </div>
          </div>
          <div className="page-header-right">
            <button className="btn btn-five btn-orange" onClick={handleFiltrosClick}>
              <div className="svg btn-icon"><IcFilter/></div>
              Filtros
            </button>
            <Link to="/profissionais/cadastrar" className="btn btn-five btn-orange">
              <div className="svg btn-icon"><IcPlusSign/></div>
              Cadastrar Profissional
            </Link>
          </div>
        </div>
        <div className="page-content">
          <CustomTable
            leftColumns={[
              {
                label: 'Nome',
                attribute: (row) => {
                  return row.nome + ' ' + row.sobrenome
                },
                width: '250px'
              },
              { label: 'Empresa', attribute: 'empregoAtual.empresa.nomeFantasia', width: '200px' },
              { label: 'Cargo', attribute: 'empregoAtual.cargo.nome', width: '125px' },
              { label: 'Área', attribute: 'empregoAtual.area.nome', width: '175px' },
              { label: 'Salário', attribute: 'empregoAtual.salario', width: '75px' },
              { label: 'Cidade atual', attribute: 'cidadeAtual.nome', width: '200px' },
            ]}
            rightColumns={[]}
            dataSource={{
              repository: ProfissionalRepository,
              params: params
            }}
            clickHandler={handleRowClick}
          />
        </div>

        <ModalFiltrosProfissionais onSubmit={handleFiltersApply}/>
      </div>
    </motion.div>
  )
}