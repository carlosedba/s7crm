import React, {useState, useEffect} from 'react'
import {useSelector, useDispatch} from 'react-redux'
import {Link, useParams, useHistory} from 'react-router-dom'
import { motion } from 'framer-motion'
import moment from 'moment'
import _ from 'lodash'

import FadeVariants from '@/animation/FadeVariants'

import SingleDataWidget from '@/components/SingleDataWidget'
import MultiDataWidget from '@/components/MultiDataWidget'
import ModalFormacaoView from '@/components/Modal/ModalFormacaoView'
import ModalFormacaoCreate from '@/components/Modal/ModalFormacaoCreate'
import ModalFormacaoUpdate from '@/components/Modal/ModalFormacaoUpdate'
import ModalExpProfissionalView from '@/components/Modal/ModalExpProfissionalView'
import ModalExpProfissionalCreate from '@/components/Modal/ModalExpProfissionalCreate'
import ModalExpProfissionalUpdate from '@/components/Modal/ModalExpProfissionalUpdate'
import ModalExpProfissionalSalario from '@/components/Modal/ModalExpProfissionalSalario'
import ModalConfirmacaoDeletar from '@/components/Modal/ModalConfirmacaoDeletar'
import ModalConfirmacaoDeletarArquivo from '@/components/Modal/ModalConfirmacaoDeletarArquivo'
import PopUpContainer from '@/components/PopUp/PopUpContainer'
import FilesPopUp from '@/components/PopUp/FilesPopUp'
import LoadingOverlay from '@/components/LoadingOverlay'

import IcMoney from '@/assets/icons/ic_money.svg'

import ProfissionalRepository from '@/repository/ProfissionalRepository'
import ExperienciaProfissionalRepository from '@/repository/ExperienciaProfissionalRepository'
import FormacaoRepository from '@/repository/FormacaoRepository'

import useWait from '@/hooks/useWait'

import ModalHelper from '@/ModalHelper'

export default function Profissional(props) {
  const [ready, setReady] = useState(false)

  const [profissional, setProfissional] = useState(null)
  const [filhos, setFilhos] = useState([])

  const params = useParams()
  const history = useHistory()
  const waitComplete = useWait(500)

  useEffect(() => {
    fetchProfissional()
  }, [])

  function fetchProfissional() {
    ProfissionalRepository.findById(params.id)
      .then(async (data) => {
        setProfissional(data)
        setFilhos(toObjectArray(data.filhos))

        setReady(true)
      })
  }

  function toObjectArray(arr) {
    return arr.map((item, i) => ({ value: item }))
  }

  function viewFormacao(row) {
    ModalHelper.open({
      name: 'ModalFormacaoView',
      data: row
    })
  }

  function createFormacao() {
    ModalHelper.open({
      name: 'ModalFormacaoCreate'
    })
  }

  function updateFormacao(row) {
    ModalHelper.open({
      name: 'ModalFormacaoUpdate',
      data: row
    })
  }

  function deleteFormacao(row) {
    ModalHelper.open({
      name: 'ModalConfirmacaoDeletar',
      params: {
        model: FormacaoRepository.model,
        objectId: row._id
      }
    })
  }

  function viewExperienciaProfissional(row) {
    ModalHelper.open({
      name: 'ModalExpProfissionalView',
      data: row
    })
  }

  function createExperienciaProfissional() {
    ModalHelper.open({
      name: 'ModalExpProfissionalCreate'
    })
  }

  function updateExperienciaProfissional(row) {
    ModalHelper.open({
      name: 'ModalExpProfissionalUpdate',
      data: row
    })
  }

  function deleteExperienciaProfissional(row) {
    ModalHelper.open({
      name: 'ModalConfirmacaoDeletar',
      params: {
        model: ExperienciaProfissionalRepository.model,
        objectId: row._id
      }
    })
  }

  function salarioExperienciaProfissional(event, row) {
    event.stopPropagation()

    ModalHelper.open({
      name: 'ModalExpProfissionalSalario',
      data: row
    })
  }

  return (
    <motion.div className="page" initial="exit" animate="enter" exit="exit" variants={FadeVariants}>
      <div className="page-wrapper">
        <div className="page-header-lr">
          <div className="page-header-left">
            <div className="page-titles">
              <a className="page-tie --clickable" onClick={() => (history.goBack())}>Voltar</a>
              <p className="page-title">{_.get(profissional, 'nome') + ' ' + _.get(profissional, 'sobrenome')}</p>
              <p className="page-text">
                <a href={_.get(profissional, 'email1')} target="_blank">{_.get(profissional, 'email1')}</a>
              </p>
            </div>
          </div>
          <div className="page-header-right">
            <Link to={`/profissionais/${params.id}/editar`} className="btn btn-five btn-orange">
              Editar
            </Link>
          </div>
        </div>
        <div className="page-content">
          <div className="data-widget-grid">
            <div className="data-widget-grid-row">
              <div className="data-widget-grid-col sp3">

                <div className="data-widget data-widget-one full">
                  <div className="data-widget-header">
                    <span className="data-widget-title">Abordagem</span>
                  </div>
                  <div className="data-widget-content">
                    <div className="data-widget-text">
                      <p>{_.get(profissional, 'abordagem')}</p>
                    </div>
                  </div>
                </div>
                <div className="data-widget-grid-row">
                  <SingleDataWidget
                    title="Informações básicas"
                    fields={[
                      { label: 'Deficiente:', attribute: 'deficiente' },
                      { label: 'RG:', attribute: 'rg' },
                      { label: 'CPF:', attribute: 'cpf' },
                      { label: 'Telefone:', attribute: 'telefone' },
                      { label: 'Celular 1:', attribute: 'celular1' },
                      { label: 'Celular 2:', attribute: 'celular2' },
                      { label: 'Idade:', attribute: 'idade' },
                      {
                        label: 'Data de nascimento:',
                        attribute: 'dataNascimento',
                        transformer: (value) => {
                           return moment(value).format('L')
                        }
                      },
                      { label: 'Estado civil:', attribute: 'estadoCivil' },
                      { label: 'Emprego do cônjuge:', attribute: 'empregoConjuge' },
                      { label: 'Email 1:', attribute: 'email1' },
                      { label: 'Email 2:', attribute: 'email2' },
                      { label: 'Email Comercial:', attribute: 'emailComercial' },
                      { label: 'Skype:', attribute: 'skype' },
                      { label: 'LinkedIn:', attribute: 'linkedin' },
                      { label: 'Naturalidade:', attribute: 'naturalidade.nome' },
                      { label: 'Estado de origem:', attribute: 'estadoOrigem.nome' },
                      { label: 'Cidade de origem:', attribute: 'cidadeOrigem.nome' },
                      { label: 'País atual:', attribute: 'paisAtual.nome' },
                      { label: 'Estado atual:', attribute: 'estadoAtual.nome' },
                      { label: 'Cidade atual:', attribute: 'cidadeAtual.nome' },
                    ]}
                    initialData={profissional}
                  />
                </div>
              </div>
              <div className="data-widget-grid-col sp2">
                <div className="data-widget-grid-row">
                  <MultiDataWidget
                    size="full"
                    title="Formações"
                    columns={[
                      { label: 'Curso', attribute: 'curso.nome', width: '200px' },
                      { label: 'Instituição', attribute: 'instituicao.nome', width: '325px' },
                      { label: 'Ano de conclusão', attribute: 'anoConclusao', width: '125px' },
                    ]}
                    dataSource={{
                      repository: FormacaoRepository,
                      params: {
                        profissional: _.get(profissional, '_id')
                      }
                    }}
                    viewHandler={viewFormacao}
                    createHandler={createFormacao}
                    updateHandler={updateFormacao}
                    deleteHandler={deleteFormacao}
                    ready={ready}
                  />
                </div>
                <div className="data-widget-grid-row">
                  <MultiDataWidget
                    size="full"
                    title="Experiências Profissionais"
                    columns={[
                      { label: 'Cargo', attribute: 'cargo.nome', width: '150px' },
                      { label: 'Área', attribute: 'area.nome', width: '150px' },
                      { label: 'Emprego atual', attribute: 'empregoAtual', width: '100px' },
                    ]}
                    dataSource={{
                      repository: ExperienciaProfissionalRepository,
                      params: {
                        profissional: _.get(profissional, '_id')
                      }
                    }}
                    viewHandler={viewExperienciaProfissional}
                    createHandler={createExperienciaProfissional}
                    updateHandler={updateExperienciaProfissional}
                    deleteHandler={deleteExperienciaProfissional}
                    extraActions={[
                      { icon: IcMoney, handler: salarioExperienciaProfissional }
                    ]}
                    ready={ready}
                  />
                </div>
                <div className="data-widget-grid-row">
                  <div className="data-widget-grid-col">
                    <MultiDataWidget
                      size="full"
                      title="Idiomas"
                      columns={[
                        { label: 'Idioma', attribute: 'idioma.label', width: '150px' },
                        { label: 'Nível', attribute: 'nivel.label' },
                      ]}
                      initialData={_.get(profissional, 'idiomas')}
                    />
                  </div>
                  <div className="data-widget-grid-col">
                    <MultiDataWidget
                      size="full"
                      title="Filhos"
                      columns={[
                        { label: 'Idades', attribute: 'value', width: '150px' },
                      ]}
                      initialData={filhos}
                    />
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>

        <ModalConfirmacaoDeletar/>
        <ModalConfirmacaoDeletarArquivo/>

        <ModalFormacaoView/>
        <ModalFormacaoCreate process={true} formComputed={{ profissional: _.get(profissional, '_id') }}/>
        <ModalFormacaoUpdate action="update" process={true}/>

        <ModalExpProfissionalView/>
        <ModalExpProfissionalCreate process={true} formComputed={{ profissional: _.get(profissional, '_id') }}/>
        <ModalExpProfissionalUpdate action="update" process={true}/>
        <ModalExpProfissionalSalario action="update" process={true}/>
      </div>

      <PopUpContainer>
        <FilesPopUp path={_.get(profissional, '_id')}/>
      </PopUpContainer>

      <LoadingOverlay visible={!(waitComplete && ready)}/>
    </motion.div>
  )
}

