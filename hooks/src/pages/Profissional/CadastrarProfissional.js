import React, { useState, useEffect } from 'react'
import { useSelector, useDispatch } from 'react-redux'
import { Link, useHistory } from 'react-router-dom'
import moment from 'moment'

import Form from '@/components/Form'
import TextInput from '@/components/Input/TextInput'
import TextDateInput from '@/components/Input/TextDateInput'
import SelectInput from '@/components/Input/SelectInput'
import FormSection from '@/components/FormSection'
import TextAttributeFormSection from '@/components/FormSection/TextAttributeFormSection'
import ArrayAttributeFormSection from '@/components/FormSection/ArrayAttributeFormSection'
import ObjectArrayAttributeFormSection from '@/components/FormSection/ObjectArrayAttributeFormSection'
import ModalFilhosCreate from '@/components/Modal/ModalFilhosCreate'
import ModalFilhosUpdate from '@/components/Modal/ModalFilhosUpdate'
import ModalIdiomaCreate from '@/components/Modal/ModalIdiomaCreate'
import ModalIdiomaUpdate from '@/components/Modal/ModalIdiomaUpdate'
import ModalExpProfissionalCreate from '@/components/Modal/ModalExpProfissionalCreate'
import ModalExpProfissionalUpdate from '@/components/Modal/ModalExpProfissionalUpdate'
import ModalExpProfissionalSalario from '@/components/Modal/ModalExpProfissionalSalario'
import ModalFormacaoCreate from '@/components/Modal/ModalFormacaoCreate'
import ModalFormacaoUpdate from '@/components/Modal/ModalFormacaoUpdate'
import ModalAbordagem from '@/components/Modal/ModalAbordagem'
import ModalComentarios from '@/components/Modal/ModalComentarios'

import IcPlusSign from '@/assets/icons/ic_plus_sign.svg'
import IcMoney from '@/assets/icons/ic_money.svg'

import PaisRepository from '@/repository/PaisRepository'
import EstadoRepository from '@/repository/EstadoRepository'
import CidadeRepository from '@/repository/CidadeRepository'
import ProfissionalRepository from '@/repository/ProfissionalRepository'

import * as textMasks from '@/textMasks'
import FormHelper from '@/FormHelper'
import FormValidator from '@/FormValidator'
import ModalHelper from '@/ModalHelper'

export default function CadastrarProfissional(props) {
  const [formId, setFormId] = useState('')
  const [formProps, setFormProps] = useState({})

  const [naturalidades, setNaturalidades] = useState([])
  const [estadoOrigemList, setEstadoOrigemList] = useState([])
  const [cidadeOrigemList, setCidadeOrigemList] = useState([])
  const [paisAtualList, setPaisAtualList] = useState([])
  const [estadoAtualList, setEstadoAtualList] = useState([])
  const [cidadeAtualList, setCidadeAtualList] = useState([])

  const history = useHistory()

  useEffect(() => {
    const formId = FormHelper.new()

    setFormId(formId)

    setFormProps({
      id: formId,
      model: ProfissionalRepository.model,
      action: 'create'
    })

    fetchPaises()
  }, [])

  function fetchPaises() {
    PaisRepository.find()
      .then((data) => {
        const naturalidadeOptions = data.map((pais) => ({ value: pais._id, label: pais.gentilico }))
        const paisAtualOptions = data.map((pais) => ({ value: pais._id, label: pais.nome }))

        setNaturalidades(naturalidadeOptions)
        setPaisAtualList(paisAtualOptions)
      })
  }

  function handleIdadeChange(event) {
    let age = parseInt(event.value)
    let date = moment().subtract(age, 'years')
    let format = 'L'

    FormHelper.setRawAttribute(formId, 'dataNascimento', {
      $set: date.format(format)
    })

    FormHelper.setComputedAttribute(formId, 'dataNascimento', {
      $set: {
        type: 'date',
        format: format,
        value: date.toDate()
      }
    })
  }

  function handleNaturalidadeChange(event) {
    EstadoRepository.find({ pais: event.value })
      .then((data) => {
        const estadoOrigemOptions = data.map((estado) => ({ value: estado._id, label: estado.nome }))

        setEstadoOrigemList(estadoOrigemOptions)
      })
  }

  function handleEstadoOrigemChange(event) {
    CidadeRepository.find({ estado: event.value })
      .then((data) => {
        const cidadeOrigemOptions = data.map((cidade) => ({ value: cidade._id, label: cidade.nome }))

        setCidadeOrigemList(cidadeOrigemOptions)
      })
  }

  function handlePaisAtualChange(event) {
    EstadoRepository.find({ pais: event.value })
      .then((data) => {
        const estadoAutalOptions = data.map((estado) => ({ value: estado._id, label: estado.nome }))

        setEstadoAtualList(estadoAutalOptions)
      })
  }

  function handleEstadoAtualChange(event) {
    CidadeRepository.find({ estado: event.value })
      .then((data) => {
        const cidadeAtualOptions = data.map((cidade) => ({ value: cidade._id, label: cidade.nome }))

        setCidadeAtualList(cidadeAtualOptions)
      })
  }

  function salarioExperienciaProfissional(event, payload) {
    event.stopPropagation()

    ModalHelper.open({
      name: 'ModalExpProfissionalSalario',
      params: {
        formId: payload.id
      }
    })
  }

  async function handleSubmit(event) {
    FormValidator.validate(formProps)
      .then(() => {
        FormHelper.submit(formProps, true)
          .then(() => {
            alert('Profissional cadastrado com sucesso!')
            history.push('/profissionais')
          })
          .catch((err) => {
            console.error(err)
            alert('Um erro ocorreu ao salvar os dados.')
          })
      })
      .catch((err) => {
        console.error(err)
        alert('Preencha todos campos obrigatórios.')
      })
  }

  return (
    <div className="page">
      <div className="page-wrapper">
        <div className="page-header">
          <div className="page-titles">
            <Link to="/profissionais" className="page-tie">Profissionais</Link>
            <p className="page-title">Cadastrar Profissional</p>
            <p className="page-subtitle">Informe os dados em seus respectivos campos:</p>
            <p className="page-text"></p>
          </div>
        </div>
        <div className="page-content">
          <div className="page-content-wrapper page-content-form-wrapper">
            <Form {...formProps}>
              <div className="inputs input-style-2">
                <div className="multi-input">
                  <TextInput formId={formId} classes="w2" label="Nome" attribute="nome" required={true}/>
                  <TextInput formId={formId} classes="w2-q2" label="Sobrenome" attribute="sobrenome" required={true}/>
                  <SelectInput
                    formId={formId}
                    size="w1-q3"
                    label="Sexo"
                    attribute="sexo"
                    required={true}
                    options={[
                      { value: 'Masculino', label: 'Masculino' },
                      { value: 'Feminino', label: 'Feminino' },
                      { value: 'Outro', label: 'Outro' },
                    ]}
                  />
                  <SelectInput
                    formId={formId}
                    size="w1-q2"
                    label="Deficiente"
                    attribute="deficiente"
                    required={true}
                    options={[
                      { value: 'Sim', label: 'Sim' },
                      { value: 'Não', label: 'Não' },
                    ]}
                  />
                </div>
                <div className="multi-input">
                  <TextInput formId={formId} classes="w1-q2" label="RG" attribute="rg" mask={textMasks.RG}/>
                  <TextInput formId={formId} classes="w1-q3" label="CPF" attribute="cpf" mask="cpf"/>
                  <TextInput formId={formId} classes="w1-q3" label="Telefone" attribute="telefone" mask="telefone"/>
                  <TextInput formId={formId} classes="w1-q3" label="Celular 1" attribute="celular1" mask="celular" required={true}/>
                  <TextInput formId={formId} classes="w1-q3" label="Celular 2" attribute="celular2" mask="celular"/>
                </div>
                <div className="multi-input">
                  <TextInput formId={formId} classes="w06" label="Idade" attribute="idade" onChange={handleIdadeChange}/>
                  <TextDateInput formId={formId} classes="w2" label="Data de nascimento" attribute="dataNascimento" format="L"/>
                  <SelectInput
                    formId={formId}
                    size="w2"
                    label="Estado civil"
                    attribute="estadoCivil"
                    options={[
                      { value: 'Solteiro (a)', label: 'Solteiro (a)' },
                      { value: 'Casado (a)', label: 'Casado (a)' },
                      { value: 'Divorciado (a)', label: 'Divorciado (a)' },
                      { value: 'Viúvo (a)', label: 'Viúvo (a)' },
                      { value: 'União estável', label: 'União estável' },
                    ]}
                  />
                  <TextInput formId={formId} classes="w2-q2" label="Emprego do cônjuge" attribute="empregoConjuge"/>
                </div>
                <div className="multi-input">
                  <TextInput formId={formId} classes="w3-q1" label="E-mail 1" attribute="email1" required={true}/>
                  <TextInput formId={formId} classes="w3-q1" label="E-mail 2" attribute="email2"/>
                </div>
                <div className="multi-input">
                  <TextInput formId={formId} classes="w3-q1" label="E-mail comercial" attribute="emailComercial"/>
                  <TextInput formId={formId} classes="w2" label="Skype" attribute="skype"/>
                  <TextInput formId={formId} classes="w2" label="LinkedIn" attribute="linkedin"/>
                </div>
                <div className="multi-input">
                  <SelectInput formId={formId} size="w2" label="Naturalidade" attribute="naturalidade" options={naturalidades} onChange={handleNaturalidadeChange}/>
                  <SelectInput formId={formId} size="w2-q1" label="Estado de origem" attribute="estadoOrigem" options={estadoOrigemList} onChange={handleEstadoOrigemChange}/>
                  <SelectInput formId={formId} size="w2" label="Cidade de origem" attribute="cidadeOrigem" options={cidadeOrigemList}/>
                </div>
                <div className="multi-input">
                  <SelectInput
                    formId={formId}
                    size="w2"
                    label="País atual"
                    attribute="paisAtual"
                    required={true}
                    options={paisAtualList}
                    onChange={handlePaisAtualChange}
                  />
                  <SelectInput
                    formId={formId}
                    size="w2-q1"
                    label="Estado atual"
                    attribute="estadoAtual"
                    required={true}
                    options={estadoAtualList}
                    onChange={handleEstadoAtualChange}
                  />
                  <SelectInput
                    formId={formId}
                    size="w2"
                    label="Cidade atual"
                    attribute="cidadeAtual"
                    required={true}
                    options={cidadeAtualList}
                  />
                </div>
              </div>
            </Form>

            <TextAttributeFormSection
              title="Abordagem"
              message="Editar abordagem"
              icon={IcPlusSign}
              onCreateModalName="ModalAbordagem">
              <ModalAbordagem parentFormId={formId} attribute="abordagem"/>
            </TextAttributeFormSection>

            <TextAttributeFormSection
              title="Comentários"
              message="Editar comentários"
              icon={IcPlusSign}
              onCreateModalName="ModalComentarios">
              <ModalComentarios parentFormId={formId} attribute="comentarios"/>
            </TextAttributeFormSection>

            <ArrayAttributeFormSection
              formId={formId}
              attribute="filhos"
              title="Filhos"
              message="Adicionar filho"
              icon={IcPlusSign}
              onCreateModalName="ModalFilhosCreate"
              onUpdateModalName="ModalFilhosUpdate">
              <ModalFilhosCreate parentFormId={formId} attribute="filhos"/>
              <ModalFilhosUpdate parentFormId={formId} attribute="filhos"/>
            </ArrayAttributeFormSection>

            <ObjectArrayAttributeFormSection
              formId={formId}
              attribute="idiomas"
              title="Idiomas"
              message="Adicionar idioma"
              icon={IcPlusSign}
              attributeIsSelect={true}
              attributeToDisplay="idioma"
              onCreateModalName="ModalIdiomaCreate"
              onUpdateModalName="ModalIdiomaUpdate">
              <ModalIdiomaCreate parentFormId={formId} attribute="idiomas"/>
              <ModalIdiomaUpdate parentFormId={formId} attribute="idiomas"/>
            </ObjectArrayAttributeFormSection>

            <FormSection
              model="S7ExperienciaProfissional"
              action="create"
              title="Experiências profissionais"
              message="Adicionar experiência"
              icon={IcPlusSign}
              attributeIsSelect={true}
              attributeToDisplay="cargo"
              onCreateModalName="ModalExpProfissionalCreate"
              onUpdateModalName="ModalExpProfissionalUpdate"
              extraActions={[
                { icon: IcMoney, handler: salarioExperienciaProfissional }
              ]}>
              <ModalExpProfissionalCreate dependsOn={[{ id: formId, key: 'profissional' }]}/>
              <ModalExpProfissionalUpdate dependsOn={[{ id: formId, key: 'profissional' }]}/>
              <ModalExpProfissionalSalario dependsOn={[{ id: formId, key: 'profissional' }]}/>
            </FormSection>

            <FormSection
              model="S7Formacao"
              action="create"
              title="Formações"
              message="Adicionar formação"
              icon={IcPlusSign}
              attributeToDisplay="curso"
              onCreateModalName="ModalFormacaoCreate"
              onUpdateModalName="ModalFormacaoUpdate">
              <ModalFormacaoCreate dependsOn={[{ id: formId, key: 'profissional' }]}/>
              <ModalFormacaoUpdate dependsOn={[{ id: formId, key: 'profissional' }]}/>
            </FormSection>

            <button className="btn btn-six btn-orange" onClick={handleSubmit}>Salvar</button>
          </div>
        </div>
      </div>
    </div>
  )
}

