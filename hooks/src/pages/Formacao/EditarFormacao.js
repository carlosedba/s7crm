import React, { useState, useEffect } from 'react'
import { useSelector, useDispatch } from 'react-redux'
import {Link, useHistory, useParams} from 'react-router-dom'

import { addModal } from '@/actions/Modal'
import * as Forms from '@/actions/Form'

import Form from '@/components/Form'
import TextInput from '@/components/Input/TextInput'
import SuggestionInput from '@/components/Input/SuggestionInput'
import CurrencyInput from '@/components/Input/CurrencyInput'
import SelectInput from '@/components/Input/SelectInput'
import FormSection from '@/components/FormSection'
import ArrayAttributeFormSection from '@/components/FormSection/ArrayAttributeFormSection'
import ObjectArrayAttributeFormSection from '@/components/FormSection/ObjectArrayAttributeFormSection'

import IdGenerator from '@/utils/IdGenerator'

import * as textMasks from '@/textMasks'
import FormHelper from '@/FormHelper'
import FormValidator from '@/FormValidator'
import PaisRepository from '@/repository/PaisRepository'
import EstadoRepository from '@/repository/EstadoRepository'
import CidadeRepository from '@/repository/CidadeRepository'
import ModalConAdministrativoCreate from '@/components/Modal/ModalConAdministrativoCreate'
import ModalPriContatosCreate from '@/components/Modal/ModalPriContatosCreate'
import FormacaoRepository from '@/repository/FormacaoRepository'

const formId = IdGenerator.default()

export default function EditarFormacao(props) {
  const params = useParams()

  const [loading, setLoading] = useState(true)
  const [formacao, setFormacao] = useState([])

  const forms = useSelector(state => state.Forms)

  const dispatch = useDispatch()

  const history = useHistory()

  const formProps = {
    id: formId,
    model: 'S7Formacao',
    action: 'update',
    dependsOn: null,
    proccess: true
  }

  useEffect(() => {
    dispatch(Forms.newForm(formId))

    fetchFormacao()
  }, [])

  function fetchFormacao() {
    FormacaoRepository.findById(params.idFormacao)
      .then(async (data) => {
        setFormacao(data)
        setObjectId(data._id)

        setLoading(false)
      })
  }

  function setObjectId(id) {
    dispatch(Forms.updateFormComputedAttribute(formId, '_id', {
      $set: id
    }))
  }

  async function handleSubmit(event) {
    FormValidator.validate(formProps)
      .then(() => {
        FormHelper.submit(formProps)
          .then(() => {
            alert('Formação atualizado com sucesso!')
            history.push(`/profissionais/${params.idProfissional}`)
          })
          .catch((err) => {
            alert('Um erro ocorreu ao salvar os dados.')
          })
      })
      .catch((err) => {
        console.error(err)
        alert('Preencha todos campos obrigatórios.')
      })
  }

  return (
    <div className="page">
      <div className="page-wrapper">
        <div className="page-header">
          <div className="page-titles">
            <Link to={`/profissionais/${params.idProfissional}`} className="page-tie">Voltar</Link>
            <p className="page-title">Editar Formação</p>
            <p className="page-subtitle">Informe os dados em seus respectivos campos:</p>
            <p className="page-text"></p>
          </div>
        </div>
        <div className="page-content">
          <div className="page-content-wrapper page-content-form-wrapper">
            <Form {...formProps}>
              <div className="inputs input-style-2">
                <div className="multi-input">
                  <SuggestionInput
                    formId={formId}
                    classes="w4"
                    label="Instituição"
                    attribute="instituicao"
                    required={true}
                    collection="instituicoes"
                    attributeToQuery="nome"
                    initialValue={formacao.instituicao}/>
                  <SuggestionInput
                    formId={formId}
                    classes="w3"
                    label="Curso"
                    attribute="curso"
                    required={true}
                    collection="cursos"
                    attributeToQuery="nome"
                    initialValue={formacao.curso}/>
                </div>
                <div className="multi-input">
                  <SelectInput
                    formId={formId}
                    size="w2-q2"
                    label="Tipo"
                    attribute="tipo"
                    required={true}
                    options={[
                      { value: 'Técnico', label: 'Técnico' },
                      { value: 'Graduação', label: 'Graduação' },
                      { value: 'Pós-Gradução', label: 'Pós-Gradução' },
                      { value: 'Especialização', label: 'Especialização' },
                      { value: 'MBA', label: 'MBA' },
                      { value: 'Mestrado', label: 'Mestrado' },
                      { value: 'Doutorado', label: 'Doutorado' },
                    ]}
                    initialValue={formacao.tipo}/>
                  <TextInput
                    formId={formId}
                    classes="w1-q2"
                    label="Ano de conclusão"
                    required={true}
                    attribute="anoConclusao"
                    initialValue={formacao.anoConclusao}/>
                </div>
              </div>
            </Form>

            <button className="btn btn-six btn-orange" onClick={handleSubmit}>Salvar</button>
          </div>
        </div>
      </div>
    </div>
  )
}

