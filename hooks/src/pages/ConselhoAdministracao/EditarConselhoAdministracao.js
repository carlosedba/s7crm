import React, { useState, useEffect } from 'react'
import { Link, useHistory, useParams } from 'react-router-dom'

import Form from '@/components/Form'
import TextInput from '@/components/Input/TextInput'

import ConselhoAdministracaoRepository from '@/repository/ConselhoAdministracaoRepository'

import FormHelper from '@/FormHelper'
import FormValidator from '@/FormValidator'

export default function EditarConselhoAdministracao(props) {
  const [formId, setFormId] = useState(null)
  const [formProps, setFormProps] = useState({})

  const [conselhoAdministracao, setConselhoAdministracao] = useState([])

  const params = useParams()
  const history = useHistory()

  useEffect(() => {
    const formId = FormHelper.new()

    FormHelper.setObjectId(formId, params.id)

    setFormId(formId)

    setFormProps({
      id: formId,
      model: ConselhoAdministracaoRepository.model,
      action: 'update'
    })

    fetchConselhoAdministracao(formId)
  }, [])

  function fetchConselhoAdministracao(formId) {
    ConselhoAdministracaoRepository.findById(params.id)
      .then(async (data) => {
        setConselhoAdministracao(data)
      })
  }

  async function handleSubmit(event) {
    FormValidator.validate(formProps)
      .then(() => {
        FormHelper.submit(formProps, true)
          .then(() => {
            alert('Item atualizado com sucesso!')
            history.push('/conselho-dministracao')
          })
          .catch((err) => {
            console.error(err)
            alert('Um erro ocorreu ao salvar os dados.')
          })
      })
      .catch((err) => {
        console.error(err)
        alert('Preencha todos campos obrigatórios.')
      })
  }

  return (
    <div className="page">
      <div className="page-wrapper">
        <div className="page-header">
          <div className="page-titles">
            <Link to="/conselho-dministracao" className="page-tie">Voltar</Link>
            <p className="page-title">Editar Item</p>
            <p className="page-subtitle"></p>
          </div>
        </div>
        <div className="page-content">
          <div className="page-content-wrapper page-content-form-wrapper">
            <Form {...formProps}>
              <div className="inputs input-style-2">
                <TextInput formId={formId} classes="w5" label="Nome" attribute="nome" initialValue={conselhoAdministracao.nome}/>
              </div>
            </Form>

            <button className="btn btn-six btn-orange" onClick={handleSubmit}>Salvar</button>
          </div>
        </div>
      </div>
    </div>
  )
}