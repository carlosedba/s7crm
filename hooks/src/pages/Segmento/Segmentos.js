import React, { useState, useEffect } from 'react'
import { Link, useHistory } from 'react-router-dom'

import DataTable from '@/components/DataTable'
import ModalConfirmacaoDeletar from '@/components/Modal/ModalConfirmacaoDeletar'

import IcPlusSign from '@/assets/icons/ic_plus_sign.svg'
import IcPencil from '@/assets/icons/ic_pencil.svg'
import IcTrashcan from '@/assets/icons/ic_trashcan.svg'

import SegmentoRepository from '@/repository/SegmentoRepository'

import ModalHelper from '@/ModalHelper'

export default function Segmentos(props) {
  const [segmentos, setSegmentos] = useState([])

  const history = useHistory()

  useEffect(() => {
    fetchSegmentos()
  }, [])

  const columns = [
    {
      Header: 'Nome',
      accessor: 'nome',
      width: 600
    },
    {
      Header: ' ',
      width: 100,
      Cell: ({row}) => (
        <div className="data-actions no-margin">
          <button className="svg data-action" onClick={(event) => handleEdit(event, row.original)}>
            <IcPencil/>
          </button>
          <button className="svg data-action" onClick={(event) => handleDelete(event, row.original)}>
            <IcTrashcan/>
          </button>
        </div>
      )
    }
  ]

  function fetchSegmentos(segmentor) {
    let queryParams = {}

    if (segmentor) queryParams = {...queryParams, segmentor}

    SegmentoRepository.find(queryParams)
      .then((data) => {
        setSegmentos(data)
      })
  }

  function handleEdit(event, row) {
    history.push(`/segmentos/${row._id}/editar`)
  }

  function handleDelete(event, row) {
    ModalHelper.open({
      name: 'ModalConfirmacaoDeletar',
      params: {
        model: SegmentoRepository.model,
        objectId: row._id
      }
    })
  }

  return (
    <div className="page">
      <div className="page-wrapper">
        <div className="page-header-lr">
          <div className="page-header-left">
            <div className="page-titles">
              <p className="page-title">Segmentos</p>
              <p className="page-text"></p>
            </div>
          </div>
          <div className="page-header-right">
            <Link to="/segmentos/cadastrar" className="btn btn-five btn-orange">
              <div className="svg btn-icon"><IcPlusSign/></div>
              Cadastrar Segmento
            </Link>
          </div>
        </div>
        <div className="page-content">
          <DataTable
            columns={columns}
            data={segmentos}
          />
        </div>

        <ModalConfirmacaoDeletar/>
      </div>
    </div>
  )
}