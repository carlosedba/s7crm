import React, { useState, useEffect } from 'react'
import { Link, useHistory } from 'react-router-dom'

import Form from '@/components/Form'
import TextInput from '@/components/Input/TextInput'
import SelectInput from '@/components/Input/SelectInput'

import InstituicaoRepository from '@/repository/InstituicaoRepository'
import EstadoRepository from '@/repository/EstadoRepository'

import FormHelper from '@/FormHelper'
import FormValidator from '@/FormValidator'

export default function CadastrarInstituicao(props) {
  const [formId, setFormId] = useState('')
  const [formProps, setFormProps] = useState({})

  const [estados, setEstados] = useState([])

  const history = useHistory()

  useEffect(() => {
    const formId = FormHelper.new()

    setFormId(formId)

    setFormProps({
      id: formId,
      model: InstituicaoRepository.model,
      action: 'create'
    })

    fetchEstados()
  }, [])

  function fetchEstados() {
    EstadoRepository.find()
      .then((data) => {
        const estadoOptions = data.map((estado) => ({ value: estado._id, label: estado.nome }))

        setEstados(estadoOptions)
      })
  }

  async function handleSubmit(event) {
    FormValidator.validate(formProps)
      .then(() => {
        FormHelper.submit(formProps, true)
          .then(() => {
            alert('Instituição cadastrada com sucesso!')
            history.push('/instituicoes')
          })
          .catch((err) => {
            console.error(err)
            alert('Um erro ocorreu ao salvar os dados.')
          })
      })
      .catch((err) => {
        console.error(err)
        alert('Preencha todos campos obrigatórios.')
      })
  }

  return (
    <div className="page">
      <div className="page-wrapper">
        <div className="page-header">
          <div className="page-titles">
            <Link to="/instituicoes" className="page-tie">Voltar</Link>
            <p className="page-title">Cadastrar Instituição</p>
            <p className="page-subtitle"></p>
          </div>
        </div>
        <div className="page-content">
          <div className="page-content-wrapper page-content-form-wrapper">
            <Form {...formProps}>
              <div className="inputs input-style-2">
                <SelectInput
                  formId={formId}
                  size="w2-q1"
                  label="Estado"
                  attribute="estado"
                  required={true}
                  options={estados}
                />
                <div className="multi-input">
                  <TextInput formId={formId} classes="w1-q2" label="Sigla" attribute="sigla" required={true}/>
                  <TextInput formId={formId} classes="w4-q2" label="Nome" attribute="nome" required={true}/>
                </div>
              </div>
            </Form>

            <button className="btn btn-six btn-orange" onClick={handleSubmit}>Salvar</button>
          </div>
        </div>
      </div>
    </div>
  )
}