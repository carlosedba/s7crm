import React, { useState, useEffect } from 'react'
import { Link, useHistory } from 'react-router-dom'

import DataTable from '@/components/DataTable'
import ModalConfirmacaoDeletar from '@/components/Modal/ModalConfirmacaoDeletar'

import IcPlusSign from '@/assets/icons/ic_plus_sign.svg'
import IcPencil from '@/assets/icons/ic_pencil.svg'
import IcTrashcan from '@/assets/icons/ic_trashcan.svg'

import InstituicaoRepository from '@/repository/InstituicaoRepository'

import ModalHelper from '@/ModalHelper'

export default function Instituicoes(props) {
  const [instituicoes, setInstituicoes] = useState([])

  const history = useHistory()

  useEffect(() => {
    fetchInstituicoes()
  }, [])

  const columns = [
    {
      Header: 'Estado',
      accessor: 'estado.nome',
      width: 125
    },
    {
      Header: 'Sigla',
      accessor: 'sigla',
      width: 135
    },
    {
      Header: 'Nome',
      accessor: 'nome',
      width: 400
    },
    {
      Header: ' ',
      width: 100,
      Cell: ({row}) => (
        <div className="data-actions no-margin">
          <button className="svg data-action" onClick={(event) => handleEdit(event, row.original)}>
            <IcPencil/>
          </button>
          <button className="svg data-action" onClick={(event) => handleDelete(event, row.original)}>
            <IcTrashcan/>
          </button>
        </div>
      )
    }
  ]

  function fetchInstituicoes(cursor) {
    let queryParams = {}

    if (cursor) queryParams = {...queryParams, cursor}

    InstituicaoRepository.find(queryParams)
      .then((data) => {
        setInstituicoes(data)
      })
  }

  function handleEdit(event, row) {
    history.push(`/instituicoes/${row._id}/editar`)
  }

  function handleDelete(event, row) {
    ModalHelper.open({
      name: 'ModalConfirmacaoDeletar',
      params: {
        model: InstituicaoRepository.model,
        objectId: row._id
      }
    })
  }

  return (
    <div className="page">
      <div className="page-wrapper">
        <div className="page-header-lr">
          <div className="page-header-left">
            <div className="page-titles">
              <p className="page-title">Instituições</p>
              <p className="page-text"></p>
            </div>
          </div>
          <div className="page-header-right">
            <Link to="/instituicoes/cadastrar" className="btn btn-five btn-orange">
              <div className="svg btn-icon"><IcPlusSign/></div>
              Cadastrar Instituição
            </Link>
          </div>
        </div>
        <div className="page-content">
          <DataTable
            columns={columns}
            data={instituicoes}
          />
        </div>

        <ModalConfirmacaoDeletar/>
      </div>
    </div>
  )
}