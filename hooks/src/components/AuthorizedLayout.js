import React, { useState, useEffect } from 'react'
import { useSelector, useDispatch } from 'react-redux'
import {
  Switch,
  Route
} from 'react-router-dom'
import { AnimatePresence } from 'framer-motion'

import Navbar from '@/components/Navbar'
import ModalConfig from '@/components/Modal/ModalConfig'

import Dashboard from '@/pages/Dashboard'
import Dev from '@/pages/Dev'
import Logout from '@/pages/Logout'

import Empresas from '@/pages/Empresa/Empresas'
import Empresa from '@/pages/Empresa/Empresa'
import CadastrarEmpresa from '@/pages/Empresa/CadastrarEmpresa'
import EditarEmpresa from '@/pages/Empresa/EditarEmpresa'

import Profissionais from '@/pages/Profissional/Profissionais'
import Profissional from '@/pages/Profissional/Profissional'
import CadastrarProfissional from '@/pages/Profissional/CadastrarProfissional'
import EditarProfissional from '@/pages/Profissional/EditarProfissional'
import EditarFormacao from '@/pages/Formacao/EditarFormacao'

import Projetos from '@/pages/Projeto/Projetos'

import ProjetosS7Up from '@/pages/Projeto/S7Up/ProjetosS7Up'
import NovoProjetoS7Up from '@/pages/Projeto/S7Up/NovoProjetoS7Up'
import ProjetoS7Up from '@/pages/Projeto/S7Up/ProjetoS7Up'
import EditarProjetoS7Up from '@/pages/Projeto/S7Up/EditarProjetoS7Up'

import ProjetosS7Brand from '@/pages/Projeto/S7Brand/ProjetosS7Brand'
import NovoProjetoS7Brand from '@/pages/Projeto/S7Brand/NovoProjetoS7Brand'
import ProjetoS7Brand from '@/pages/Projeto/S7Brand/ProjetoS7Brand'
import EditarProjetoS7Brand from '@/pages/Projeto/S7Brand/EditarProjetoS7Brand'

import NovoProjetoS7SearchRecrutamentoSelecao from '@/pages/Projeto/S7Search/NovoProjetoS7SearchRecrutamentoSelecao'
import ProjetoS7SearchRecrutamentoSelecao from '@/pages/Projeto/S7Search/ProjetoS7SearchRecrutamentoSelecao'
import EditarProjetoS7SearchRecrutamentoSelecao from '@/pages/Projeto/S7Search/EditarProjetoS7SearchRecrutamentoSelecao'

import NovoProjetoS7SearchMapeamentoMercado from '@/pages/Projeto/S7Search/NovoProjetoS7SearchMapeamentoMercado'
import ProjetoS7SearchMapeamentoMercado from '@/pages/Projeto/S7Search/ProjetoS7SearchMapeamentoMercado'
import EditarProjetoS7SearchMapeamentoMercado from '@/pages/Projeto/S7Search/EditarProjetoS7SearchMapeamentoMercado'

import NovoProjetoS7SearchPesquisaSalarial from '@/pages/Projeto/S7Search/NovoProjetoS7SearchPesquisaSalarial'
import ProjetoS7SearchPesquisaSalarial from '@/pages/Projeto/S7Search/ProjetoS7SearchPesquisaSalarial'
import EditarProjetoS7SearchPesquisaSalarial from '@/pages/Projeto/S7Search/EditarProjetoS7SearchPesquisaSalarial'

import ProjetosS7Search from '@/pages/Projeto/S7Search/ProjetosS7Search'

import Areas from '@/pages/Area/Areas'
import EditarArea from '@/pages/Area/EditarArea'
import CadastrarArea from '@/pages/Area/CadastrarArea'

import Beneficios from '@/pages/Beneficio/Beneficios'
import EditarBeneficio from '@/pages/Beneficio/EditarBeneficio'
import CadastrarBeneficio from '@/pages/Beneficio/CadastrarBeneficio'

import Cargos from '@/pages/Cargo/Cargos'
import EditarCargo from '@/pages/Cargo/EditarCargo'
import CadastrarCargo from '@/pages/Cargo/CadastrarCargo'

import CategoriasProjeto from '@/pages/CategoriaProjeto/CategoriasProjeto'
import EditarCategoriaProjeto from '@/pages/CategoriaProjeto/EditarCategoriaProjeto'
import CadastrarCategoriaProjeto from '@/pages/CategoriaProjeto/CadastrarCategoriaProjeto'

import ConselhoAdministracao from '@/pages/ConselhoAdministracao/ConselhoAdministracao'
import EditarConselhoAdministracao from '@/pages/ConselhoAdministracao/EditarConselhoAdministracao'
import CadastrarConselhoAdministracao from '@/pages/ConselhoAdministracao/CadastrarConselhoAdministracao'

import Cursos from '@/pages/Curso/Cursos'
import EditarCurso from '@/pages/Curso/EditarCurso'
import CadastrarCurso from '@/pages/Curso/CadastrarCurso'

import Ferramentas from '@/pages/Ferramenta/Ferramentas'
import EditarFerramenta from '@/pages/Ferramenta/EditarFerramenta'
import CadastrarFerramenta from '@/pages/Ferramenta/CadastrarFerramenta'

import Formatos from '@/pages/Formato/Formatos'
import EditarFormato from '@/pages/Formato/EditarFormato'
import CadastrarFormato from '@/pages/Formato/CadastrarFormato'

import Gestao from '@/pages/Gestao/Gestao'
import EditarGestao from '@/pages/Gestao/EditarGestao'
import CadastrarGestao from '@/pages/Gestao/CadastrarGestao'

import Instituicoes from '@/pages/Instituicao/Instituicoes'
import EditarInstituicao from '@/pages/Instituicao/EditarInstituicao'
import CadastrarInstituicao from '@/pages/Instituicao/CadastrarInstituicao'

import Moedas from '@/pages/Moeda/Moedas'
import EditarMoeda from '@/pages/Moeda/EditarMoeda'
import CadastrarMoeda from '@/pages/Moeda/CadastrarMoeda'

import Portes from '@/pages/Porte/Portes'
import EditarPorte from '@/pages/Porte/EditarPorte'
import CadastrarPorte from '@/pages/Porte/CadastrarPorte'

import Segmentos from '@/pages/Segmento/Segmentos'
import EditarSegmento from '@/pages/Segmento/EditarSegmento'
import CadastrarSegmento from '@/pages/Segmento/CadastrarSegmento'

import Setores from '@/pages/Setor/Setores'
import EditarSetor from '@/pages/Setor/EditarSetor'
import CadastrarSetor from '@/pages/Setor/CadastrarSetor'

export default function AuthorizedLayout() {
  return (
    <>
      <Navbar/>

      <AnimatePresence>
        <Switch>
          <Route path="/logout" component={Logout}/>
          <Route path="/dev" component={Dev}/>

          <Route path="/areas/cadastrar" component={CadastrarArea}/>
          <Route path="/areas/:id/editar" component={EditarArea}/>
          <Route path="/areas" component={Areas}/>

          <Route path="/beneficios/cadastrar" component={CadastrarBeneficio}/>
          <Route path="/beneficios/:id/editar" component={EditarBeneficio}/>
          <Route path="/beneficios" component={Beneficios}/>

          <Route path="/cargos/cadastrar" component={CadastrarCargo}/>
          <Route path="/cargos/:id/editar" component={EditarCargo}/>
          <Route path="/cargos" component={Cargos}/>

          <Route path="/categorias-projeto/cadastrar" component={CadastrarCategoriaProjeto}/>
          <Route path="/categorias-projeto/:id/editar" component={EditarCategoriaProjeto}/>
          <Route path="/categorias-projeto" component={CategoriasProjeto}/>

          <Route path="/conselho-administracao/cadastrar" component={CadastrarConselhoAdministracao}/>
          <Route path="/conselho-administracao/:id/editar" component={EditarConselhoAdministracao}/>
          <Route path="/conselho-administracao" component={ConselhoAdministracao}/>

          <Route path="/cursos/cadastrar" component={CadastrarCurso}/>
          <Route path="/cursos/:id/editar" component={EditarCurso}/>
          <Route path="/cursos" component={Cursos}/>

          <Route path="/ferramentas/cadastrar" component={CadastrarFerramenta}/>
          <Route path="/ferramentas/:id/editar" component={EditarFerramenta}/>
          <Route path="/ferramentas" component={Ferramentas}/>

          <Route path="/formatos/cadastrar" component={CadastrarFormato}/>
          <Route path="/formatos/:id/editar" component={EditarFormato}/>
          <Route path="/formatos" component={Formatos}/>

          <Route path="/gestao/cadastrar" component={CadastrarGestao}/>
          <Route path="/gestao/:id/editar" component={EditarGestao}/>
          <Route path="/gestao" component={Gestao}/>

          <Route path="/instituicoes/cadastrar" component={CadastrarInstituicao}/>
          <Route path="/instituicoes/:id/editar" component={EditarInstituicao}/>
          <Route path="/instituicoes" component={Instituicoes}/>

          <Route path="/moedas/cadastrar" component={CadastrarMoeda}/>
          <Route path="/moedas/:id/editar" component={EditarMoeda}/>
          <Route path="/moedas" component={Moedas}/>

          <Route path="/portes/cadastrar" component={CadastrarPorte}/>
          <Route path="/portes/:id/editar" component={EditarPorte}/>
          <Route path="/portes" component={Portes}/>

          <Route path="/segmentos/cadastrar" component={CadastrarSegmento}/>
          <Route path="/segmentos/:id/editar" component={EditarSegmento}/>
          <Route path="/segmentos" component={Segmentos}/>

          <Route path="/setores/cadastrar" component={CadastrarSetor}/>
          <Route path="/setores/:id/editar" component={EditarSetor}/>
          <Route path="/setores" component={Setores}/>

          <Route path="/projetos/s7-up/novo" component={NovoProjetoS7Up}/>
          <Route path="/projetos/s7-up/:id/editar" component={EditarProjetoS7Up}/>
          <Route path="/projetos/s7-up/:id" component={ProjetoS7Up}/>
          <Route path="/projetos/s7-up" component={ProjetosS7Up}/>

          <Route path="/projetos/s7-brand/novo" component={NovoProjetoS7Brand}/>
          <Route path="/projetos/s7-brand/:id/editar" component={EditarProjetoS7Brand}/>
          <Route path="/projetos/s7-brand/:id" component={ProjetoS7Brand}/>
          <Route path="/projetos/s7-brand" component={ProjetosS7Brand}/>

          <Route path="/projetos/s7-search/recrutamento-selecao/novo" component={NovoProjetoS7SearchRecrutamentoSelecao}/>
          <Route path="/projetos/s7-search/recrutamento-selecao/:id/editar" component={EditarProjetoS7SearchRecrutamentoSelecao}/>
          <Route path="/projetos/s7-search/recrutamento-selecao/:id" component={ProjetoS7SearchRecrutamentoSelecao}/>

          <Route path="/projetos/s7-search/mapeamento-mercado/novo" component={NovoProjetoS7SearchMapeamentoMercado}/>
          <Route path="/projetos/s7-search/mapeamento-mercado/:id/editar" component={EditarProjetoS7SearchMapeamentoMercado}/>
          <Route path="/projetos/s7-search/mapeamento-mercado/:id" component={ProjetoS7SearchMapeamentoMercado}/>

          <Route path="/projetos/s7-search/pesquisa-salarial/novo" component={NovoProjetoS7SearchPesquisaSalarial}/>
          <Route path="/projetos/s7-search/pesquisa-salarial/:id/editar" component={EditarProjetoS7SearchPesquisaSalarial}/>
          <Route path="/projetos/s7-search/pesquisa-salarial/:id" component={ProjetoS7SearchPesquisaSalarial}/>

          <Route path="/projetos/s7-search" component={ProjetosS7Search}/>

          <Route path="/projetos" component={Projetos}/>

          <Route path="/profissionais/cadastrar" component={CadastrarProfissional}/>
          <Route path="/profissionais/:id/editar" component={EditarProfissional}/>
          <Route path="/profissionais/:idProfissional/formacoes/:idFormacao" component={EditarFormacao}/>
          <Route path="/profissionais/:id" component={Profissional}/>
          <Route path="/profissionais" component={Profissionais}/>

          <Route path="/empresas/cadastrar" component={CadastrarEmpresa}/>
          <Route path="/empresas/:id/editar" component={EditarEmpresa}/>
          <Route path="/empresas/:id" component={Empresa}/>
          <Route path="/empresas" component={Empresas}/>

          <Route path="/" component={Projetos}/>
        </Switch>
      </AnimatePresence>

      <ModalConfig/>
    </>
  )
}