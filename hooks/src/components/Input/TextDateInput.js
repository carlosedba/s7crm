import React, { useState, useEffect } from 'react'
import { useSelector, useDispatch } from 'react-redux'
import { Link } from 'react-router-dom'
import update from 'immutability-helper'
import classNames from 'classnames'
import { utilsBr } from 'js-brasil'
import MaskedInput from 'react-text-mask'
import moment from 'moment'

import * as Forms from '@/actions/Form'

export default function TextDateInput(props) {
  const formId = props.formId
  const attribute = props.attribute
  const initialValue =  props.initialValue
  const format = props.format || 'L'
  const label = props.label
  const classes = props.classes || ''
  const required = props.required || false
  const submitted = props.submitted || false
  const onChange = props.onChange
  const onFocus = props.onFocus

  const forms = useSelector(state => state.Forms)

  const [inputValue, setInputValue] = useState('')

  const dispatch = useDispatch()

  const formats = {
    'L': [/\d/, /\d/, '/', /\d/, /\d/, '/', /\d/, /\d/, /\d/, /\d/]
  }

  let textMask, value

  useEffect(() => {
    console.log('initialValue', initialValue)
    setInitialValue()
  }, [formId, initialValue])

  function initFormAttribute() {
    if (forms[formId]) {
      if (forms[formId]['raw'][attribute] === undefined) {
        dispatch(Forms.updateFormRawAttribute(formId, attribute, {
          $set: ''
        }))
      }
    }
  }

  function updateRenderedValue() {
    if (forms[formId]) {
      if (forms[formId]['raw'][attribute] !== undefined) {
        value = forms[formId]['raw'][attribute]
      }
    }
  }

  function setInitialValue() {
    if (formId && initialValue && (!value || value === '')) {
      let initValue = initialValue
      let date = moment(initValue)

      if (date.isValid()) {

        dispatch(Forms.updateFormRawAttribute(formId, attribute, {
          $set: date.format(format)
        }))

        dispatch(Forms.updateFormComputedAttribute(formId, attribute, {
          $set: {
            type: 'date',
            format: format,
            value: date.toDate()
          }
        }))
      } else {
        if (typeof initialValue === 'number') initValue = initValue.toString()

        dispatch(Forms.updateFormRawAttribute(formId, attribute, {
          $set: initValue
        }))
      }
    }
  }

  function handleInputChange(event) {
    const inputText = event.target.value

    if (inputText !== undefined && inputText !== null) {
      dispatch(Forms.updateFormRawAttribute(formId, attribute, {
        $set: inputText
      }))

      let date = moment(inputText, format).toDate()

      dispatch(Forms.updateFormComputedAttribute(formId, attribute, {
        $set: {
          type: 'date',
          format: format,
          value: date
        }
      }))

      if (onChange) onChange({
        attribute: attribute,
        value: inputText
      })
    }
  }

  function handleInputFocus(event) {
    if (onFocus) onFocus({
      attribute: attribute,
      value: value
    })
  }

  function renderMaskedInput(value) {
    let mask = formats[0]

    switch (format) {
      case 'L':
        mask = formats[format]
        break
    }

    return (<MaskedInput mask={mask} value={value || ''} onChange={handleInputChange} onFocus={handleInputFocus}/>)
  }

  initFormAttribute()
  updateRenderedValue()

  return (
    <div className={classNames('input', classes)}>
      <label>{label}:{(required) ? (<span className="orange"> *</span>) : ''}</label>
      <div className="input-container">
        {renderMaskedInput(value)}
      </div>
    </div>
  )
}