import React, { useState, useEffect } from 'react'
import { useSelector, useDispatch } from 'react-redux'
import classNames from 'classnames'
import { utilsBr } from 'js-brasil'
import MaskedInput from 'react-text-mask'

import * as Forms from '@/actions/Form'

export default function TextInput(props) {
  const formId = props.formId
  const attribute = props.attribute
  const initialValue =  props.initialValue
  const mask = props.mask
  const label = props.label
  const classes = props.classes || ''
  const required = props.required || false
  const onChange = props.onChange
  const onFocus = props.onFocus

  const forms = useSelector(state => state.Forms)

  const dispatch = useDispatch()

  let value

  useEffect(() => {
    setInitialValue()
  }, [formId, initialValue])

  function initFormAttribute() {
    if (formId && forms[formId]) {
      if (forms[formId]['raw'][attribute] === undefined) {
        dispatch(Forms.updateFormRawAttribute(formId, attribute, {
          $set: ''
        }))
      }
    }
  }

  function updateRenderedValue() {
    if (formId && forms[formId]) {
      if (forms[formId]['raw'][attribute] !== undefined) {
        value = forms[formId]['raw'][attribute]
      }
    }
  }

  function setInitialValue() {
    if (initialValue) {
      let _initialValue = initialValue

      if (typeof _initialValue === 'number') _initialValue = _initialValue.toString()

      if (formId) {
        dispatch(Forms.updateFormRawAttribute(formId, attribute, {
          $set: _initialValue
        }))
      } else {
        value = _initialValue
      }
    }
  }

  function handleInputChange(event) {
    const inputText = event.target.value

    if (inputText !== undefined && inputText !== null) {

      if (formId) {
        dispatch(Forms.updateFormRawAttribute(formId, attribute, {
          $set: inputText
        }))
      }

      if (onChange) onChange({
        attribute: attribute,
        value: inputText
      })
    }
  }

  function handleInputFocus(event) {
    if (onFocus) onFocus({
      attribute: attribute,
      value: value
    })
  }

  function renderMaskedInput(value) {
    let textMask

    if (typeof mask === "string") {
      textMask = utilsBr.MASKS[mask].textMask

      return (<MaskedInput mask={textMask} value={value || ''} onChange={handleInputChange} onFocus={handleInputFocus}/>)
    } else {
      return (<MaskedInput mask={mask} value={value || ''} onChange={handleInputChange} onFocus={handleInputFocus}/>)
    }
  }

  initFormAttribute()
  updateRenderedValue()

  return (
    <div className={classNames('input', classes)}>
      <label>{label}:{(required) ? (<span className="orange"> *</span>) : ''}</label>
      <div className="input-container">
        {(mask)
          ? renderMaskedInput(value)
          : (<input type="text" value={value || ''} onChange={handleInputChange} onFocus={handleInputFocus}/>)
        }
      </div>
    </div>
  )
}

