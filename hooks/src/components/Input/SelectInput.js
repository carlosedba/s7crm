import React, {useEffect, useState} from 'react'
import {useDispatch, useSelector} from 'react-redux'
import classNames from 'classnames'
import ReactSelect from 'react-select'

import * as Forms from '@/actions/Form'

export default function SelectInput(props) {
  const formId = props.formId
  const attribute = props.attribute
  const initialValue = props.initialValue
  const options = props.options
  const size = props.size
  const label = props.label
  const placeholder = props.placeholder || 'Selecione...'
  const classes = props.classes || ''
  const required = props.required || false
  const styles = props.styles
  const labelKey = props.labelKey || 'nome'
  const valueKey = props.valueKey || '_id'
  const isClearable = props.isClearable || false
  const onChange = props.onChange

  const [inputValue, setInputValue] = useState(null)

  const forms = useSelector(state => state.Forms)

  const dispatch = useDispatch()

  let reactSelectOptions, customStyles

  useEffect(() => {
    setInitialValue()
  }, [formId, initialValue])

  function initFormAttribute() {
    if (forms[formId]) {
      if (forms[formId]['raw'][attribute] === undefined) {
        dispatch(Forms.updateFormRawAttribute(formId, attribute, {
          $set: ''
        }))
      }
    }
  }

  function isOption(obj) {
    if (obj.label && obj.value) return true
    return false
  }

  function isOptionsArray(arr) {
    for (const item of arr) {
      if (!isOption(item)) return false
      return true
    }
  }

  function isStringArray(arr) {
    for (const item of arr) {
      if (typeof item !== 'string') return false
      return true
    }
  }

  function isObjectArray(arr) {
    for (const item of arr) {
      if (typeof item === 'object') return true
      return false
    }
  }

  function setInitialValue() {
    if (initialValue) {
      if (typeof initialValue === 'object' && isOption(initialValue)) {
        setInputValue(initialValue)

        if (formId) {
          dispatch(Forms.updateFormRawAttribute(formId, attribute, {
            $set: initialValue
          }))
        }
      }

      if (typeof initialValue === 'object' && initialValue._id) {
        let option = getOptionByValue(initialValue._id)

        if (option) {
          setInputValue(option)

          if (formId) {
            dispatch(Forms.updateFormRawAttribute(formId, attribute, {
              $set: option
            }))
          }
        }
      }

      if (typeof initialValue === 'string') {
        let option = getOptionByValue(initialValue)

        if (option) {
          setInputValue(option)

          if (formId) {
            dispatch(Forms.updateFormRawAttribute(formId, attribute, {
              $set: option
            }))
          }
        }
      }

      if (Array.isArray(initialValue)) {
        if (isOptionsArray(initialValue)) {
          setInputValue(initialValue)

          if (formId) {
            dispatch(Forms.updateFormRawAttribute(formId, attribute, {
              $set: initialValue
            }))
          }
        } else if (isStringArray(initialValue)) {
          let options = generateOptionsFromStringArray(initialValue)

          setInputValue(options)

          if (formId) {
            dispatch(Forms.updateFormRawAttribute(formId, attribute, {
              $set: options
            }))
          }
        } else if (isObjectArray(initialValue)) {
          let options = generateOptionsFromObjectArray(initialValue)

          setInputValue(options)

          if (formId) {
            dispatch(Forms.updateFormRawAttribute(formId, attribute, {
              $set: options
            }))
          }
        }
      }
    }
  }

  function getOptionByValue(value) {
    for (let option of options) {
      if (option.value === value) return option
    }

    return null
  }

  function generateOptionsFromStringArray(strArr) {
    const arr = []

    for (let str of strArr) {
      arr.push({ value: str, label: str })
    }

    return arr
  }

  function generateOptionsFromObjectArray(objArr) {
    const arr = []

    for (let item of objArr) {
      arr.push({ value: item[valueKey], label: item[labelKey] })
    }

    return arr
  }

  function getSize() {
    switch (size) {
      case 'w1':
        return 100

      case 'w1-q1':
        return 125

      case 'w1-q2':
        return 150

      case 'w1-q3':
        return 175


      case 'w2':
        return 200

      case 'w2-q1':
        return 225

      case 'w2-q2':
        return 250

      case 'w2-q3':
        return 275


      case 'w3':
        return 300

      case 'w3-q1':
        return 325

      case 'w3-q2':
        return 350

      case 'w3-q3':
        return 375


      case 'w4':
        return 400

      case 'w4-q1':
        return 425

      case 'w4-q2':
        return 450

      case 'w4-q3':
        return 475

      default:
        return 150
    }
  }

  function handleChange(selectedOption) {
    if (selectedOption) {
      setInputValue(selectedOption)

      if (formId) {
        dispatch(Forms.updateFormRawAttribute(formId, attribute, {
          $set: selectedOption
        }))
      }

      if (onChange) onChange({
        attribute: attribute,
        label: selectedOption.label,
        value: selectedOption.value,
      })
    }

    if (selectedOption === null) {
      setInputValue(null)

      if (formId) {
        dispatch(Forms.updateFormRawAttribute(formId, attribute, {
          $set: ''
        }))
      }

      if (onChange) onChange({
        attribute: attribute,
        label: null,
        value: null,
      })
    }
  }

  initFormAttribute()

  if (typeof options[0] === 'string') {
    reactSelectOptions = generateOptionsFromStringArray(options)
  } else {
    reactSelectOptions = options
  }

  let controlStyles = {
    borderRadius: '6px',
    borderColor: 'rgb(229, 229, 234)',
    backgroundColor: '#fff',
  }

  if (size) {
    customStyles = Object.assign({}, {
      control: (provided, state) => ({
        ...provided,
        ...controlStyles,
        width: getSize(),
      })
    }, styles) 
  } else {
    customStyles = {
      control: (provided, state) => ({
        ...provided,
        ...controlStyles,
        width: getSize(),
      })
    }
  }

  return (
    <div className={classNames('input', classes)}>
      {(label) ? (
        <label>{label}:{(required) ? (<span className="orange"> *</span>) : ''}</label>
      ) : null}

      <ReactSelect
        {...props}
        styles={customStyles}
        placeholder={placeholder}
        value={inputValue}
        isClearable={isClearable}
        onChange={handleChange}
        options={reactSelectOptions}/>
    </div>
  )
}

