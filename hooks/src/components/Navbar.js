import React, { useState, useEffect } from 'react'
import { useSelector, useDispatch } from 'react-redux'
import { Link, useLocation } from 'react-router-dom'
import update from 'immutability-helper'
import classNames from 'classnames'
import _ from 'lodash'

import IcDashBoard from '@/assets/icons/ic_dashboard.svg'
import IcProjects from '@/assets/icons/ic_projects.svg'
import IcCompany from '@/assets/icons/ic_company.svg'
import IcUsers from '@/assets/icons/ic_users.svg'

import user_placeholder from '@/assets/img/user_placeholder.png'

import { addModal, openModal } from '@/actions/Modal'
import ModalHelper from '@/ModalHelper'

export default function Navbar(props) {
  const user = useSelector(state => state.Auth.user)

  const location = useLocation()

  const dispatch = useDispatch()

  useEffect(() => {

  }, [])
  
  function navbarMenuAction(name, fn, Icon) {
    return (
      <li className={classNames('navbar-menu-item')} onClick={fn}>
        <a>
          {(Icon) ? (
            <div className="navbar-menu-icon">
              <Icon/>
            </div>
          ) : null}
          {name}
        </a>
      </li>
    )
  }

  function navbarMenuLink(name, url, Icon) {
    return (
      <li className={classNames('navbar-menu-item', {
        active: isCurrentUrl(url)
      })}>
        <Link to={url}>
          {(Icon) ? (
            <div className="navbar-menu-icon">
              <Icon/>
            </div>
          ) : null}
          {name}
        </Link>
      </li>
    )
  }

  function isCurrentUrl(url) {
    const pathname = location.pathname

    if (pathname === url) {
      return true
    }

    if (url.length > 1) {
      url = url.replace('/', '\/')

      const regex = new RegExp('^' + url)

      return pathname.match(regex)
    }
  }

  function handleConfigClick() {
    ModalHelper.open({ name: 'ModalConfig' })
  }

  return (
    <div className="navbar-wrapper">
      <nav className="navbar">
        <div className="navbar-left">
          <Link className="navbar-logo" to="/projetos">
            <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 787.3 1024"><linearGradient id="a" gradientUnits="userSpaceOnUse" x1="-49.656" y1="463.707" x2="927.737" y2="463.707" gradientTransform="rotate(-96.131 414.732 512.377)"><stop offset="0" stopColor="#c53d27"/><stop offset=".045" stopColor="#ca4725" stopOpacity=".967"/><stop offset=".143" stopColor="#d15723" stopOpacity=".895"/><stop offset=".254" stopColor="#d66021" stopOpacity=".814"/><stop offset=".409" stopColor="#d76321" stopOpacity=".7"/><stop offset=".732" stopColor="#d86521" stopOpacity=".864"/><stop offset=".848" stopColor="#db6c20" stopOpacity=".923"/><stop offset=".931" stopColor="#e0771e" stopOpacity=".965"/><stop offset=".997" stopColor="#e8881b" stopOpacity=".999"/><stop offset="1" stopColor="#e8891b"/></linearGradient><path fill="url(#a)" d="M-.1 1024L0 518.2 381.4 0l405.9 518.7v342.2L415.6 649z"/><linearGradient id="b" gradientUnits="userSpaceOnUse" x1="23.182" y1="468.904" x2="864.249" y2="468.904" gradientTransform="rotate(-96.131 414.732 512.377)"><stop offset="0" stopColor="#c53d27"/><stop offset=".045" stopColor="#ca4725" stopOpacity=".967"/><stop offset=".143" stopColor="#d15723" stopOpacity=".895"/><stop offset=".254" stopColor="#d66021" stopOpacity=".814"/><stop offset=".409" stopColor="#d76321" stopOpacity=".7"/><stop offset=".732" stopColor="#d86521" stopOpacity=".864"/><stop offset=".848" stopColor="#db6c20" stopOpacity=".923"/><stop offset=".931" stopColor="#e0771e" stopOpacity=".965"/><stop offset=".997" stopColor="#e8881b" stopOpacity=".999"/><stop offset="1" stopColor="#e8891b"/></linearGradient><path opacity=".622" fill="url(#b)" d="M39.1 946.5V530.6L381.4 63.9l370 466.2v258.7L416 604.1z"/><linearGradient id="c" gradientUnits="userSpaceOnUse" x1="67.701" y1="471.293" x2="862.609" y2="471.293" gradientTransform="rotate(-96.131 414.732 512.377)"><stop offset="0" stopColor="#c53d27"/><stop offset=".045" stopColor="#ca4725" stopOpacity=".967"/><stop offset=".143" stopColor="#d15723" stopOpacity=".895"/><stop offset=".254" stopColor="#d66021" stopOpacity=".814"/><stop offset=".409" stopColor="#d76321" stopOpacity=".7"/><stop offset=".732" stopColor="#d86521" stopOpacity=".864"/><stop offset=".848" stopColor="#db6c20" stopOpacity=".923"/><stop offset=".931" stopColor="#e0771e" stopOpacity=".965"/><stop offset=".997" stopColor="#e8881b" stopOpacity=".999"/><stop offset="1" stopColor="#e8891b"/></linearGradient><path opacity=".498" fill="url(#c)" d="M39.1 901.8l-1.5-369.2L381.4 65.5l370 464.4-.1 224.8L416 565.9z"/></svg>
          </Link>
          <div className="navbar-separator"/>
          <ul className="navbar-menu">
            {navbarMenuLink('Projetos', '/projetos', IcProjects)}
            {navbarMenuLink('Empresas', '/empresas', IcCompany)}
            {navbarMenuLink('Profissionais', '/profissionais', IcUsers)}
            {navbarMenuAction('Configurações', handleConfigClick)}
          </ul>
        </div>
        <div className="navbar-right">
          <div className="navbar-user">
            <div className="navbar-user-info">
              <span className="navbar-user-name">{_.get(user, 'nome')}</span>
              <Link className="navbar-user-link" to="/logout">Sair</Link>
            </div>
            <div className="navbar-user-picture" style={{ backgroundImage: `url(${user_placeholder})` }}></div>
          </div>
        </div>
      </nav>
    </div>
  )
}

