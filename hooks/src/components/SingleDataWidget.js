import React, { useState, useEffect } from 'react'
import { useSelector, useDispatch } from 'react-redux'
import { Link, useLocation } from 'react-router-dom'
import update from 'immutability-helper'
import classNames from 'classnames'
import _ from 'lodash'

export default function SingleDataWidget(props) {
  const title = props.title
  const fields = props.fields
  const initialData = props.initialData
  const dataSource = props.dataSource

  const [data, setData] = useState({})

  useEffect(() => {
    if (dataSource) fetchInitialData()
  }, [dataSource])

  useEffect(() => {
    if (initialData) setData(initialData)
  }, [initialData])

  async function fetchInitialData() {
    let data = await dataSource().catch(console.error)

    if (data) {
      setData(data)
    }
  }

  function renderFields() {
    return fields.map((field, i) => {
      let value = ''

      if (typeof field.attribute === 'function') {
        value = field.attribute(data)
      } else {
        value = _.get(data, field.attribute)

        if (field.transformer) {
          value = field.transformer(value)
        }
      }

      if (value) {
        return (
          <div key={i} className={classNames('data-field', {
            [field.customClass]: field.customClass
          })}>
            <span className="data-field-name">{field.label}</span>
            <span className="data-field-value">{value}</span>
          </div>
        )
      }
    })
  }

  return (
    <div className="data-widget data-widget-one full">
      <div className="data-widget-header">
        <span className="data-widget-title">{title}</span>
      </div>
      <div className="data-widget-content">
        <div className="data-fields data-fields-one">
          {renderFields()}
        </div>
      </div>
    </div>
  )
}
