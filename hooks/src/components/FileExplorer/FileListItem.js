import React, { useState, useEffect } from 'react'
import { useSelector, useDispatch } from 'react-redux'
import classNames from 'classnames'
import moment from 'moment'

import IcFolder from '@/assets/icons/ic_folder.svg'
import IcFile2 from '@/assets/icons/ic_file_2.svg'
import IcTrashcan from '@/assets/icons/ic_trashcan.svg'
import FileSystemService from '@/services/FileSystemService'
import ModalHelper from '@/ModalHelper'

export default function FileListItem(props) {
  const id = props.id
  const name = props.name
  const mimeType = props.mimeType
  const isDirectory = props.isDirectory
  const path = props.path
  const extension = props.extension
  const ino = props.ino
  const size = props.size
  const createdAt = props.createdAt
  const lastTimeAccessed = props.lastTimeAccessed
  const lastTimeModified = props.lastTimeModified
  const onDelete = props.onDelete

  const contextMenu = useSelector(state => state.ContextMenu)

  const dispatch = useDispatch()

  useEffect(() => {}, [])

  function getIcon() {
    if (isDirectory) {
      return (<IcFolder/>)
    }

    return (<IcFile2/>)
  }

  function getFileSize() {
    const sizeInKb = (size / Math.pow(1024, 1)).toPrecision(3)
    const sizeInMb = (size / Math.pow(1024, 2)).toPrecision(3)
    const sizeInGb = (size / Math.pow(1024, 3)).toPrecision(3)

    if (sizeInKb < 1000) {
      return sizeInKb + 'kb'
    }

    if (sizeInGb >= 1) {
      return sizeInGb + 'gb'
    }

    return sizeInMb + 'mb'
  }

  function isActive() {
    if (contextMenu.focusedElement === id) {
      return true
    }

    false
  }

  function onClick(event) {
    event.stopPropagation()

    if (!contextMenu.open) {
      if (isDirectory) {
        //
      }
    }

    FileSystemService.retrieveFile(path, name).catch(console.error)
  }

  function handleDeleteClick(event) {
    event.stopPropagation()

    ModalHelper.open({
      name: 'ModalConfirmacaoDeletarArquivo',
      handlers: {
        onDelete: () => {
          FileSystemService.deleteFile(path)
            .then(() => {
              if (onDelete) onDelete()
            })
            .catch(console.error)
        }
      }
    })
  }

  return (
    <div className={classNames('file', {
      active: isActive()
    })} onClick={onClick}>
      <div className="svg file-icon hidden">{getIcon()}</div>
      <div className="file-info">
        <div className="file-header">
          <span className="file-name">{name}</span>
        </div>
        <div className="file-description">
          {(!isDirectory) ? (
            <>
            <span className="file-field">
              {getFileSize()}
              {' - '}
              {moment(lastTimeModified).format('ddd, DD/MM/YY [às] HH:mm')}
            </span>
            </>
          ) : null}
        </div>
      </div>
      <div className="file-actions">
        <button className="svg file-action" onClick={handleDeleteClick}>
          <IcTrashcan/>
        </button>
      </div>
    </div>
  )
}

