import React, { useState, useEffect, useCallback } from 'react'
import { useSelector, useDispatch } from 'react-redux'
import path from 'path'
import { ContextMenu, MenuItem, ContextMenuTrigger } from 'react-contextmenu'

import * as ContextMenuActions from '@/actions/ContextMenu'

import FileListItem from '@/components/FileExplorer/FileListItem'

import FileSystemService from '@/services/FileSystemService'

export default function FileList(props) {
  const root = props.root
  const dirContent = props.dirContent || []
  const setDirContent = props.setDirContent

  useEffect(() => {
    getDirContent(root)
  }, [root])

  const dispatch = useDispatch()

  async function getDirContent(path) {
    const dirContent = await FileSystemService.readDir(path)

    setDirContent(dirContent)
  }

  function handleContextMenuOpen(event) {
    const focusedElement = event.detail.data

    dispatch(ContextMenuActions.open(focusedElement.id))
  }

  function handleContextMenuClose(event) {
    setTimeout(() => {
      dispatch(ContextMenuActions.close())
    }, 250)
  }

  function handleDownloadClick(event, focusedElement) {
    //
  }

  function handleMenuItemClick(event, focusedElement) {
    handleContextMenuOpen(focusedElement)
  }

  function handleItemDelete() {
    getDirContent(root)
  }

  function renderFileListItems() {
    return dirContent.map((item, i) => {
      if (item.isDirectory) {
        return (
          <ContextMenuTrigger key={i} id="list-item-menu-2" collect={(props) => { return item }}>
            <FileListItem key={i} {...item} onDelete={handleItemDelete}/>
          </ContextMenuTrigger>
        )
      } else {
        return (
          <ContextMenuTrigger key={i} id="list-item-menu" collect={(props) => { return item }}>
            <FileListItem {...item} onDelete={handleItemDelete}/>
          </ContextMenuTrigger>
        )
      }
    })
  }

  return (
    <div className="file-list">
      {(dirContent.length === 0) ? (
        <>
          <div className="placeholder-message">
            <div className="svg icon"></div>
            Clique ou arraste um arquivo aqui para fazer o upload.
          </div>
        </>
      ) : null}

      {renderFileListItems()}

      <ContextMenu id="list-item-menu" onShow={handleContextMenuOpen} onHide={handleContextMenuClose}>
        <MenuItem onClick={handleDownloadClick}>
          Baixar
        </MenuItem>
        <MenuItem className="disabled" onClick={handleMenuItemClick}>
          Salvar no OneDrive
        </MenuItem>
        <MenuItem className="disabled" onClick={handleMenuItemClick}>
          Compartilhar
        </MenuItem>
        <MenuItem divider />
        <MenuItem className="disabled" onClick={handleMenuItemClick}>
          Copiar
        </MenuItem>
        <MenuItem className="disabled" onClick={handleMenuItemClick}>
          Duplicar
        </MenuItem>
        <MenuItem className="disabled" onClick={handleMenuItemClick}>
          Renomear
        </MenuItem>
        <MenuItem className="disabled" onClick={handleMenuItemClick}>
          Deletar
        </MenuItem>
        <MenuItem divider />
        <MenuItem className="disabled" onClick={handleMenuItemClick}>
          Informações
        </MenuItem>
      </ContextMenu>

      <ContextMenu id="list-item-menu-2" onShow={handleContextMenuOpen} onHide={handleContextMenuClose}>
        <MenuItem className="disabled" onClick={handleMenuItemClick}>
          Compartilhar
        </MenuItem>
        <MenuItem divider />
        <MenuItem className="disabled" onClick={handleMenuItemClick}>
          Copiar
        </MenuItem>
        <MenuItem className="disabled" onClick={handleMenuItemClick}>
          Duplicar
        </MenuItem>
        <MenuItem className="disabled" onClick={handleMenuItemClick}>
          Renomear
        </MenuItem>
        <MenuItem className="disabled" onClick={handleMenuItemClick}>
          Deletar
        </MenuItem>
      </ContextMenu>
    </div>
  )
}

