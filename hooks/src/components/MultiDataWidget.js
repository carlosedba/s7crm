import React, { useState, useEffect } from 'react'
import { useSelector, useDispatch } from 'react-redux'
import classNames from 'classnames'
import _ from 'lodash'

import IcPlusSign from '@/assets/icons/ic_plus_sign.svg'
import IcPencil from '@/assets/icons/ic_pencil.svg'
import IcTrashcan from '@/assets/icons/ic_trashcan.svg'

export default function MultiDataWidget(props) {
  const size = props.size
  const title = props.title
  const columns = props.columns
  const initialData = props.initialData
  const dataSource = props.dataSource
  const clickHandler = props.clickHandler
  const viewHandler = props.viewHandler
  const createHandler = props.createHandler
  const updateHandler = props.updateHandler
  const deleteHandler = props.deleteHandler
  const extraActions = props.extraActions || []
  const ready = props.ready || false
  const refresh = props.refresh || false

  const [data, setData] = useState([])
  const [cursor, setCursor] = useState(null)

  const requests = useSelector(state => state.Network.requests)

  useEffect(() => {
    if (initialData) setData(initialData)
  }, [initialData])

  useEffect(() => {
    if (ready) fetchInitialData()
  }, [ready])

  useEffect(() => {
    //if (refresh) fetchInitialData()
  }, [refresh])

  useEffect(() => {
    if (dataSource) {
      for (let  i = 0; i < requests.length; i++) {
        const request = requests[i]

        if (request.params && request.params.model === dataSource.repository.model && request.status === 'success') {
          fetchInitialData()
        }
      }
    }
  }, [requests])

  function getValidParams() {
    let params = {}

    let paramKeys = Object.keys(dataSource.params)

    for (let i = 0; i < paramKeys.length; i++) {
      let paramKey = paramKeys[i]
      let paramValue = dataSource.params[paramKey]

      if (paramValue) {
        params[paramKey] = paramValue
      } else {
        return false
      }
    }

    return params
  }

  async function fetchInitialData() {
    let repository = dataSource.repository
    let params = getValidParams()

    if (dataSource.params && !params) return

    let data = await repository.find(params).catch(console.error)

    if (data) {
      setData(data)
    }
  }

  function callCreateHandler() {
    if (createHandler) createHandler()
  }

  function callUpdateHandler(event, row) {
    event.stopPropagation()
    if (updateHandler) updateHandler(row)
  }

  function callDeleteHandler(event, row) {
    event.stopPropagation()
    if (deleteHandler) deleteHandler(row)
  }

  function handleRowClick(event, row) {
    if (clickHandler) clickHandler(row)
    if (viewHandler) viewHandler(row)
  }

  function renderColumns() {
    return columns.map((col, i) => {
      return (
        <div key={i} className="data-widget-col" style={{ width: col.width }}>
          {col.label}
        </div>
      )
    })
  }

  function renderExtraActions(row) {
    if (extraActions.length) {
      return extraActions.map((action, i) => {
        const Icon = action.icon

        return (
          <button key={i} className="svg data-action" onClick={(event) => action.handler(event, row)}>
            <Icon/>
          </button>
        )
      })
    }
  }

  function renderRows() {
    if (data) {
      return data.map((row, i) => {
        return (
          <div key={i} className={classNames('data-widget-row', {
            '--clickable': viewHandler || clickHandler
          })} onClick={(event) => handleRowClick(event, row)}>
            {renderCell(row)}

            <div className="data-actions no-margin">
              {renderExtraActions(row)}

              {(updateHandler) ? (
                <button className="svg data-action" onClick={(event) => callUpdateHandler(event, row)}>
                  <IcPencil/>
                </button>
              ) : null}

              {(deleteHandler) ? (
                <button className="svg data-action" onClick={(event) => callDeleteHandler(event, row)}>
                  <IcTrashcan/>
                </button>
              ) : null}
            </div>
          </div>
        )
      })
    }
  }

  function renderCell(row) {
    return columns.map((col, i) => {
      let value = ''

      if (typeof col.attribute === 'function') {
        value = col.attribute(row)
      } else {
        value = _.get(row, col.attribute)

        if (col.transformer) {
          value = col.transformer(value)
        }
      }

      return (
        <div key={i} className={classNames('data-widget-cell-wrapper', {
          [col.customClass]: col.customClass
        })} style={{ width: col.width }}>
          <div className="data-widget-cell">
            {value}
          </div>
        </div>
      )
    })
  }

  return (
    <div className={classNames('data-widget', 'data-widget-two', {
      [size]: size
    })}>
      <div className="data-widget-head">

        <div className="data-widget-header">
          <div className="data-widget-header-left">
            <span className="data-widget-title">{title}</span>
          </div>
          <div className="data-widget-header-right">
            {(createHandler) ? (
              <button className="btn btn-seven btn-orange" onClick={callCreateHandler}>
                Adicionar
                <div className="svg btn-icon"><IcPlusSign/></div>
              </button>
            ) : null}
          </div>
        </div>


        <div className="data-widget-row">
          {renderColumns()}
        </div>
      </div>

      <div className="data-widget-content">
        {(data.length === 0) ? (
          <div className="data-widget-empty">
            <span>Nada para mostrar</span>
          </div>
        ) : null}

        {renderRows()}
      </div>
    </div>
  )
}