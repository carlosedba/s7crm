import React, { useState, useEffect } from 'react'
import { useSelector, useDispatch } from 'react-redux'
import { Link, useLocation } from 'react-router-dom'
import update from 'immutability-helper'
import { motion } from 'framer-motion'
import classNames from 'classnames'
import _ from 'lodash'

export default function ProjectsTable(props) {
  const title = props.title
  const nextViewPath = props.nextViewPath
  const leftColumns = props.leftColumns || props.columns || []
  const rightColumns = props.rightColumns || []
  const initialData = props.initialData
  const dataSource = props.dataSource
  const clickHandler = props.clickHandler

  const [data, setData] = useState([])
  const [loading, setLoading] = useState(false)
  const [tableHeight, setTableHeight] = useState('0px')

  useEffect(() => {
    if (dataSource) fetchInitialData()
  }, [dataSource])

  useEffect(() => {
    if (initialData) setData(initialData)
  }, [initialData])

  function calculateHeight(data) {
    let y = data.length * 72

    return y
  }

  async function fetchInitialData() {
    setLoading(true)

    let repository = dataSource.repository
    let params = dataSource.params

    let data = await repository.find(params).catch(console.error)

    if (data) {
      let tableHeight = calculateHeight(data.data)
      setTableHeight(tableHeight)

      setData(data.data)
      setLoading(false)
    }
  }

  function handleRowClick(event, row) {
    if (clickHandler) clickHandler(row)
  }

  function renderRows() {
    if (data) {
      return data.map((row, i) => {
        return (
          <motion.div
            key={i}
            className="ct-row"
            initial="disappear"
            animate="appear"
            exit="disappear"
            variants={{
              appear: {
                x: 0,
                y: 0,
                opacity: 1,
                transition: {
                  delay: i * 0.13
                },
              },
              disappear: {
                x: -15,
                y: -15,
                opacity: 0,
              },
              disappear2: {
                opacity: 0,
              },
            }}
            onClick={(event) => handleRowClick(event, row)}
          >
            <div className="left">
              {renderCells(row, leftColumns)}
            </div>
            <div className="right">
              {renderCells(row, rightColumns)}
            </div>
          </motion.div>
        )
      })
    }
  }

  function renderCells(row, columns) {
    return columns.map((col, i) => {
      let value = ''

      if (typeof col.attribute === 'function') {
        value = col.attribute(row)
      } else {
        value = _.get(row, col.attribute)

        if (col.transformer) {
          value = col.transformer(value)
        }
      }

      let style = {}

      if (col.maxWidth) style.maxWidth = col.maxWidth
      if (col.width) style.width = col.width

      if (value) {
        return (
          <div key={i} className={classNames('ct-cell', {
            [col.customClass]: col.customClass
          })} style={style}>
            <p className="ct-cell-hat">{col.label}</p>
            <p className="ct-cell-title">{value}</p>
          </div>
        )
      }
    })
  }

  return (
    <div className="widget style-1">
      <div className="w-header">
        <div className="left">
          <div className="w-titles">
            <p className="w-title">{title}</p>
            <p className="w-subtitle">Projetos recentes</p>
          </div>
        </div>
        <div className="center">
        </div>
        <div className="right">
          <Link className="btn btn-five btn-orange" to={nextViewPath}>
            Ver mais
          </Link>
        </div>
      </div>

      <div className="custom-table style-1 --no-border">
        <motion.div
          className="ct-table"
          animate={{ height: tableHeight }}
          transition={{ stiffness: 35 }}
        >
          {renderRows()}
        </motion.div>

      </div>
    </div>
  )
}