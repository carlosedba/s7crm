import React, { useState, useEffect } from 'react'
import { useSelector, useDispatch } from 'react-redux'
import { Link, useLocation } from 'react-router-dom'
import { motion } from 'framer-motion'
import update from 'immutability-helper'
import classNames from 'classnames'
import _ from 'lodash'

import useWait from '@/hooks/useWait'

export default function CustomTable(props) {
  const leftColumns = props.leftColumns || props.columns || []
  const rightColumns = props.rightColumns || []
  const initialData = props.initialData
  const dataSource = props.dataSource
  const clickHandler = props.clickHandler

  const [tableHeight, setTableHeight] = useState('0px')
  const [loading, setLoading] = useState(false)
  const [firstLoadingComplete, setFirstLoadingComplete] = useState(false)
  const [data, setData] = useState([])
  const [cursors, setCursors] = useState({})
  const [cursor, setCursor] = useState({})

  const waitComplete = useWait(1)

  useEffect(() => {
    if (dataSource) fetchData(cursor)
  }, [dataSource])

  useEffect(() => {
    if (initialData) setData(initialData)
  }, [initialData])

  useEffect(() => {
    console.log('cursor', cursor)
    if (dataSource) fetchData(cursor)
  }, [cursor])

  function calculateHeight(data) {
    let y = data.length * 72

    return y
  }

  function getValidParams() {
    let params = {}

    let paramKeys = Object.keys(dataSource.params)

    for (let i = 0; i < paramKeys.length; i++) {
      let paramKey = paramKeys[i]
      let paramValue = dataSource.params[paramKey]

      if (paramValue) {
        params[paramKey] = paramValue
      } else {
        return false
      }
    }

    return params
  }

  async function fetchInitialData() {
    let repository = dataSource.repository
    let params = dataSource.params
    //let params = getValidParams()

    //if (dataSource.params && !params) return

    let data = await repository.find(params).catch(console.error)

    if (data) {
      setData(data.data)
      setCursors(data.cursors)
    }
  }

  async function fetchData({ before, after }) {
    console.log('fetchData')

    setLoading(true)
    setFirstLoadingComplete(true)

    let repository = dataSource.repository
    let params = dataSource.params

    if (before) params = {
      ...params,
      before
    }

    if (after) params = {
      ...params,
      after
    }

    let data = await repository.find(params).catch(console.error)

    if (data) {
      let tableHeight = calculateHeight(data.data)
      setTableHeight(tableHeight)

      setData(data.data)
      setCursors(data.cursors)
      setLoading(false)
    }
  }

  function handleRowClick(event, row) {
    if (clickHandler) clickHandler(row)
  }

  function renderRows() {
    if (data) {
      return data.map((row, i) => {
        return (
          <motion.div
            key={i}
            className="ct-row"
            initial="disappear"
            animate="appear"
            exit="disappear2"
            variants={{
              appear: {
                x: 0,
                y: 0,
                opacity: 1,
                transition: {
                  delay: i * 0.13
                },
              },
              disappear: {
                x: -15,
                y: -15,
                opacity: 0,
              },
              disappear2: {
                opacity: 0,
              },
            }}
            onClick={(event) => handleRowClick(event, row)}
          >
            <div className="left">
              {renderCells(row, leftColumns)}
            </div>
            <div className="right">
              {renderCells(row, rightColumns)}
            </div>
          </motion.div>
        )
      })
    }
  }

  function renderCells(row, columns) {
    return columns.map((col, i) => {
      let value = ''

      if (typeof col.attribute === 'function') {
        value = col.attribute(row)
      } else {
        value = _.get(row, col.attribute)

        if (col.transformer) {
          value = col.transformer(value)
        }
      }

      let style = {}

      if (col.maxWidth) style.maxWidth = col.maxWidth
      if (col.width) style.width = col.width

      if (value) {
        return (
          <div key={i} className={classNames('ct-cell', {
            [col.customClass]: col.customClass
          })} style={style}>
            <p className="ct-cell-hat">{col.label}</p>
            <p className="ct-cell-title">{value}</p>
          </div>
        )
      }
    })
  }

  function handlePrevPage(event) {
    setCursor({ before: cursors.before })
  }

  function handleNextPage(event) {
    setCursor({ after: cursors.after })
  }

  console.log(loading, waitComplete)

  return (
    <div
      className="custom-table style-1"
    >
      <motion.div
        className={classNames('ct-table', {
          'is-filled': !loading && waitComplete
        })}
        animate={{ height: tableHeight }}
        transition={{ stiffness: 35 }}
      >
        {(!loading && waitComplete) ? (renderRows()) : null}
      </motion.div>

      {(firstLoadingComplete && waitComplete) ? (
        <div className="ct-actions">
          <button className="btn btn-orange btn-five" onClick={handlePrevPage} disabled={(cursors && !cursors.before)}>Página anterior</button>
          <button className="btn btn-orange btn-five" onClick={handleNextPage} disabled={(cursors && !cursors.after)}>Próxima página</button>
        </div>
      ) : null}

      <motion.div
        className="ct-loading"
        animate={(loading || !waitComplete) ? 'visible' : 'hidden'}
        variants={{
          visible: {
            opacity: 1,
            display: 'flex'
          },
          hidden: {
            opacity: 0,
            transitionEnd: {
              display: 'none',
            },
          },
        }}
        transition={{
          duration: 0.5
        }}
      >
        <motion.div
          className="spinner-container"
          animate={(loading || !waitComplete) ? 'visible' : 'hidden'}
          variants={{
            visible: { scale: 1 },
            hidden: { scale: 0 },
          }}
          transition={{ duration: 0.25 }}
        >
          <div className="spinner"></div>
        </motion.div>
      </motion.div>
    </div>
  )
}