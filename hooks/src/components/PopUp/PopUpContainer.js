import React, { useState, useEffect } from 'react'

export default function PopUpContainer(props) {
  return (
    <div className="popup-container">
      <div className="popup-wrapper">
        {props.children}
      </div>
    </div>
  )
}