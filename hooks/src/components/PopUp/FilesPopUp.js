import React, {useState, useEffect, useCallback, useRef} from 'react'
import { Link } from 'react-router-dom'
import { useSelector, useDispatch } from 'react-redux'
import classNames from 'classnames'
import { useDropzone } from 'react-dropzone'
import { Power4, TweenMax } from 'gsap'

import FileList from '@/components/FileExplorer/FileList'

import FileSystemService from '@/services/FileSystemService'

export default function FilesPopUp(props) {
  const path = props.path

  const element = useRef(null)

  const [isOpen, setOpen] = useState(false)
  const [dirContent, setDirContent] = useState([])

  const onDrop = useCallback(acceptedFiles => {
    sendFile(acceptedFiles)
  }, [path])

  const { getRootProps, getInputProps, isDragActive } = useDropzone({onDrop})

  useEffect(() => {
    getDirContent(path)
  }, [path])

  async function getDirContent(path) {
    const dirContent = await FileSystemService.readDir(path)

    setDirContent(dirContent)
  }

  function sendFile(acceptedFiles) {
    FileSystemService.writeFiles(acceptedFiles, path)
      .then(() => {
        getDirContent(path)
      })
      .catch(console.error)
  }

  function open() {
    TweenMax.to(element.current, 0.6, {
      y: '0',
      ease: Power4.easeOut
    })

    return setOpen(true)
  }

  function close() {
    TweenMax.to(element.current, 0.6, {
      y: '250px',
      ease: Power4.easeOut
    })

    return setOpen(false)
  }

  function handleHeaderClick(event) {
    if (isOpen) close()
    else open()
  }

  return (
    <div className={classNames('popup', 'files-popup')} ref={element}>
      <div className="popup-header" onClick={handleHeaderClick}>
        <div className="popup-header-left">
          <span className="popup-title">Arquivos</span>
        </div>
        <div className="popup-header-right"></div>
      </div>
      <div className="popup-content" {...getRootProps()}>
        <input {...getInputProps()} />
        {(dirContent.length === 0) ? (
          <>
            <div className="placeholder-message">
              <div className="svg icon"></div>
              Clique ou arraste um arquivo aqui para fazer o upload.
            </div>
          </>
        ) : (
          <FileList root={path} dirContent={dirContent} setDirContent={setDirContent}/>
        )}
      </div>
    </div>
  )
}