import React, { useState, useEffect } from 'react'

export default function Accordion(props) {
  return (
    <div className="accordion">
      {props.children}
    </div>
  )
}