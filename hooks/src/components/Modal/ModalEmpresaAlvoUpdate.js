import React, { useState, useEffect } from 'react'
import { useSelector, useDispatch } from 'react-redux'

import Modal from '@/components/Modal'
import Form from '@/components/Form'
import SuggestionInput from '@/components/Input/SuggestionInput'

import IcClose from '@/assets/icons/ic_close.svg'

import FormHelper from '@/FormHelper'
import ModalHelper from '@/ModalHelper'

export default function ModalEmpresaAlvoUpdate(props) {
  const action = props.action || 'create'
  const process = props.process || false
  const dependsOn = props.dependsOn

  const [formId, setFormId] = useState(null)
  const [formProps, setFormProps] = useState(null)

  const modalName = 'ModalEmpresaAlvoUpdate'

  function onAfterOpen() {
    let formId = ModalHelper.getParam(modalName, 'formId')

    if (!formId) formId = FormHelper.new()

    setFormId(formId)

    setFormProps({
      id: formId,
      model: 'S7EmpresaAlvo',
      action: action,
      dependsOn: dependsOn,
      process: process
    })

    if (action === 'update') {
      let objectId = ModalHelper.getRawValue(modalName, formId, '_id')

      FormHelper.setObjectId(formId, objectId)
    }
  }

  function onRequestClose() {
    let formId = ModalHelper.getParam(modalName, 'formId')

    if (!formId) FormHelper.remove(formId)

    setFormId(null)
  }

  function handleBtnCloseClick(event) {
    ModalHelper.close(modalName)
  }

  function handleSubmit(event) {
    ModalHelper.close(modalName)
  }

  return (
    <Modal name={modalName} onAfterOpen={onAfterOpen} onRequestClose={onRequestClose}>
      <div className="modal">
        <div className="modal-header">
          <div className="modal-header-left">
            <div className="modal-titles">
              <span className="modal-hat">Empresas alvo</span>
              <span className="modal-title">Editar empresa</span>
            </div>
          </div>
          <div className="modal-header-right">
            <button className="btn btn-close" onClick={handleBtnCloseClick}>
              <div className="svg icon">
                <IcClose/>
              </div>
            </button>
          </div>
        </div>
        <div className="modal-content">
          <Form {...formProps} dependsOn={dependsOn} update={true} onSubmit={handleSubmit}>
            <div className="inputs input-style-2">
              <SuggestionInput formId={formId} classes="w3" label="Nome da Empresa" attribute="empresa" collection="empresas" attributeToQuery="nomeFantasia" initialValue={ModalHelper.getComputedValue(modalName, formId, 'empresa')} required={true}/>

              <button type="submit" className="btn btn-six btn-bright-blue">Salvar</button>
            </div>
          </Form>
        </div>
      </div>
    </Modal>
  )
}