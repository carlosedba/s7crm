import React, { useState, useEffect } from 'react'
import { useSelector, useDispatch } from 'react-redux'
import { Link } from 'react-router-dom'

import Modal from '@/components/Modal'

import IcClose from '@/assets/icons/ic_close.svg'

import ModalHelper from '@/ModalHelper'

export default function ModalConfig(props) {
  const modalName = 'ModalConfig'

  function handleBtnCloseClick(event) {
    ModalHelper.close(modalName)
  }

  function handleExit(event) {
    ModalHelper.close(modalName)
  }

  return (
    <Modal name={modalName}>
      <div className="modal">
        <div className="modal-header">
          <div className="modal-header-left">
            <div className="modal-titles">
              <span className="modal-title">Configurações</span>
            </div>
          </div>
          <div className="modal-header-right">
            <button className="btn btn-close" onClick={handleBtnCloseClick}>
              <div className="svg icon">
                <IcClose/>
              </div>
            </button>
          </div>
        </div>
        <div className="modal-content">

          <div className="config-main-panel">
            <ul className="config-menu">
              <li className="">Geral</li>
              <li className="">Conta</li>
              <li className="active">Dados</li>
              <li>Suporte</li>
            </ul>
            <div className="config-panel-container">
              <div className="config-panel">
                <div className="config-panel-section">
                  <span className="config-panel-section-title">Dados de referência</span>

                  <div className="list list-one">
                    <div className="list-item">
                      <div className="list-item-left">
                        <span className="list-item-name">Áreas</span>
                      </div>
                      <div className="list-item-right">
                        <Link className="btn btn-seven btn-outlined-orange" to="/areas" onClick={handleExit}>Gerenciar</Link>
                      </div>
                    </div>

                    <div className="list-item">
                      <div className="list-item-left">
                        <span className="list-item-name">Benefícios</span>
                      </div>
                      <div className="list-item-right">
                        <Link className="btn btn-seven btn-outlined-orange" to="/beneficios" onClick={handleExit}>Gerenciar</Link>
                      </div>
                    </div>

                    <div className="list-item">
                      <div className="list-item-left">
                        <span className="list-item-name">Cargos</span>
                      </div>
                      <div className="list-item-right">
                        <Link className="btn btn-seven btn-outlined-orange" to="/cargos" onClick={handleExit}>Gerenciar</Link>
                      </div>
                    </div>

                    <div className="list-item">
                      <div className="list-item-left">
                        <span className="list-item-name">Categorias de Projetos</span>
                      </div>
                      <div className="list-item-right">
                        <Link className="btn btn-seven btn-outlined-orange" to="/categorias-projeto" onClick={handleExit}>Gerenciar</Link>
                      </div>
                    </div>

                    <div className="list-item">
                      <div className="list-item-left">
                        <span className="list-item-name">Conselho de Administração</span>
                      </div>
                      <div className="list-item-right">
                        <Link className="btn btn-seven btn-outlined-orange" to="/conselho-administracao" onClick={handleExit}>Gerenciar</Link>
                      </div>
                    </div>

                    <div className="list-item">
                      <div className="list-item-left">
                        <span className="list-item-name">Cursos</span>
                      </div>
                      <div className="list-item-right">
                        <Link className="btn btn-seven btn-outlined-orange" to="/cursos" onClick={handleExit}>Gerenciar</Link>
                      </div>
                    </div>

                    <div className="list-item">
                      <div className="list-item-left">
                        <span className="list-item-name">Ferramentas</span>
                      </div>
                      <div className="list-item-right">
                        <Link className="btn btn-seven btn-outlined-orange" to="/ferramentas" onClick={handleExit}>Gerenciar</Link>
                      </div>
                    </div>

                    <div className="list-item">
                      <div className="list-item-left">
                        <span className="list-item-name">Formatos</span>
                      </div>
                      <div className="list-item-right">
                        <Link className="btn btn-seven btn-outlined-orange" to="/formatos" onClick={handleExit}>Gerenciar</Link>
                      </div>
                    </div>

                    <div className="list-item">
                      <div className="list-item-left">
                        <span className="list-item-name">Gestão</span>
                      </div>
                      <div className="list-item-right">
                        <Link className="btn btn-seven btn-outlined-orange" to="/gestao" onClick={handleExit}>Gerenciar</Link>
                      </div>
                    </div>

                    <div className="list-item">
                      <div className="list-item-left">
                        <span className="list-item-name">Instituições</span>
                      </div>
                      <div className="list-item-right">
                        <Link className="btn btn-seven btn-outlined-orange" to="/instituicoes" onClick={handleExit}>Gerenciar</Link>
                      </div>
                    </div>

                    <div className="list-item">
                      <div className="list-item-left">
                        <span className="list-item-name">Moedas</span>
                      </div>
                      <div className="list-item-right">
                        <Link className="btn btn-seven btn-outlined-orange" to="/moedas" onClick={handleExit}>Gerenciar</Link>
                      </div>
                    </div>

                    <div className="list-item">
                      <div className="list-item-left">
                        <span className="list-item-name">Portes</span>
                      </div>
                      <div className="list-item-right">
                        <Link className="btn btn-seven btn-outlined-orange" to="/portes" onClick={handleExit}>Gerenciar</Link>
                      </div>
                    </div>

                    <div className="list-item">
                      <div className="list-item-left">
                        <span className="list-item-name">Segmentos</span>
                      </div>
                      <div className="list-item-right">
                        <Link className="btn btn-seven btn-outlined-orange" to="/segmentos" onClick={handleExit}>Gerenciar</Link>
                      </div>
                    </div>

                    <div className="list-item">
                      <div className="list-item-left">
                        <span className="list-item-name">Setores</span>
                      </div>
                      <div className="list-item-right">
                        <Link className="btn btn-seven btn-outlined-orange" to="/setores" onClick={handleExit}>Gerenciar</Link>
                      </div>
                    </div>

                  </div>
                </div>

              </div>
            </div>
          </div>

        </div>
      </div>
    </Modal>
  )
}

