import React, { useState, useEffect } from 'react'
import { useSelector, useDispatch } from 'react-redux'

import Modal from '@/components/Modal'
import SelectInput from '@/components/Input/SelectInput'
import ObjectArrayAttributeForm from '@/components/Form/ObjectArrayAttributeForm'

import IcClose from '@/assets/icons/ic_close.svg'

import IdiomaRepository from '@/repository/IdiomaRepository'

import FormHelper from '@/FormHelper'
import ModalHelper from '@/ModalHelper'
import ObjectArrayAttributeFormHelper from '@/ObjectArrayAttributeFormHelper'

export default function ModalIdiomaUpdate(props) {
  const parentFormId = props.parentFormId
  const attribute = props.attribute

  const [formId, setFormId] = useState(null)

  const [idiomas, setIdiomas] = useState([])

  const modalName = 'ModalIdiomaUpdate'

  useEffect(() => {
    fetchIdiomas()
  }, [])

  function onAfterOpen() {
    let formId = FormHelper.new()

    setFormId(formId)
  }

  function onRequestClose() {
    setFormId(null)

    FormHelper.remove(formId)
  }

  function getRawValue(subAttribute) {
    let index = ModalHelper.getParam(modalName, 'index')

    return ObjectArrayAttributeFormHelper.getRawValue(parentFormId, attribute, index, subAttribute)
  }

  function getComputedValue(subAttribute) {
    let index = ModalHelper.getParam(modalName, 'index')

    return ObjectArrayAttributeFormHelper.getComputedValue(parentFormId, attribute, index, subAttribute)
  }

  function fetchIdiomas() {
    IdiomaRepository.find()
      .then((data) => {
        const idiomaOptions = data.map((idioma) => ({ value: idioma._id, label: idioma.nome }))

        setIdiomas(idiomaOptions)
      })
  }

  function handleBtnCloseClick(event) {
    ModalHelper.close(modalName)
  }

  function handleSubmit(event) {
    ModalHelper.close(modalName)
  }

  return (
    <Modal name={modalName} onAfterOpen={onAfterOpen} onRequestClose={onRequestClose}>
      <div className="modal">
        <div className="modal-header">
          <div className="modal-header-left">
            <div className="modal-titles">
              <span className="modal-hat">Idioma</span>
              <span className="modal-title">Editar idioma</span>
            </div>
          </div>
          <div className="modal-header-right">
            <button className="btn btn-close" onClick={handleBtnCloseClick}>
              <div className="svg icon">
                <IcClose/>
              </div>
            </button>
          </div>
        </div>
        <div className="modal-content">
          <ObjectArrayAttributeForm formId={formId} parentFormId={parentFormId} attribute={attribute} mode="update" modalName={modalName} onSubmit={handleSubmit}>
            <div className="inputs input-style-2">
              <SelectInput
                formId={formId}
                size="w2-q2"
                label="Idioma"
                attribute="idioma"
                required={true}
                initialValue={getRawValue('idioma')}
                isSearchable={true}
                options={idiomas}
              />
              <SelectInput
                formId={formId}
                size="w2"
                label="Nível"
                attribute="nivel"
                required={true}
                initialValue={getRawValue('nivel')}
                options={[
                  { value: 'Básico', label: 'Básico' },
                  { value: 'Intermediário', label: 'Intermediário' },
                  { value: 'Avançado', label: 'Avançado' },
                  { value: 'Fluente', label: 'Fluente' },
                  { value: 'Nativo', label: 'Nativo' }
                ]}
              />

              <button type="submit" className="btn btn-six btn-bright-blue">Salvar</button>
            </div>
          </ObjectArrayAttributeForm>
        </div>
      </div>
    </Modal>
  )
}