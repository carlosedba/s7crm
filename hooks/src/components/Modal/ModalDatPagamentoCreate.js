import React, { useState, useEffect } from 'react'
import { useSelector, useDispatch } from 'react-redux'
import { Link } from 'react-router-dom'
import update from 'immutability-helper'

import { closeModal } from '@/actions/Modal'
import { updateFormRaw, updateFormComputed } from '@/actions/Form'

import Modal from '@/components/Modal'
import ObjectArrayAttributeForm from '@/components/Form/ObjectArrayAttributeForm'
import TextInput from '@/components/Input/TextInput'

import IcClose from '@/assets/icons/ic_close.svg'

export default function ModalDatPagamentoCreate(props) {
  const modal = useSelector(state => state.Modal)
  const forms = useSelector(state => state.Forms)

  const name = 'ModalDatPagamentoCreate'
  const title = 'Datas de pagamento'

  const formSource = {
    model: 'DataPagamento',
    action: 'create',
  }

  const formTarget = {
    model: 'S7Projeto',
    action: 'create',
    attribute: 'datas_pagamento'
  }

  const dispatch = useDispatch()

  function onRequestClose() {
    //dispatch(updateFormRaw(formSource.model, formSource.action, { $set: {} }))
    //dispatch(updateFormComputed(formSource.model, formSource.action, { $set: {} }))
  }

  function handleBtnCloseClick(event) {
    dispatch(closeModal(name))
  }

  function handleSubmit(event) {
    dispatch(closeModal(name))
  }

  return (
    <Modal name={name} contentLabel={title} onRequestClose={onRequestClose}>
      <div className="modal">
        <div className="modal-header">
          <div className="modal-header-left">
            <div className="modal-titles">
              <span className="modal-hat">{title}</span>
              <span className="modal-title">Adicionar data</span>
            </div>
          </div>
          <div className="modal-header-right">
            <button className="btn btn-close" onClick={handleBtnCloseClick}>
              <div className="svg icon">
                <IcClose/>
              </div>
            </button>
          </div>
        </div>
        <div className="modal-content">
          <ObjectArrayAttributeForm source={formSource} target={formTarget} onSubmit={handleSubmit}>
            <div className="inputs">
              <TextInput {...formSource} classes="w3" label="Data" attribute="data"/>
              <button type="submit" className="btn btn-three">Salvar</button>
            </div>
          </ObjectArrayAttributeForm>
        </div>
      </div>
    </Modal>
  )
}

