import React, { useState, useEffect } from 'react'
import { useSelector, useDispatch } from 'react-redux'

import Modal from '@/components/Modal'
import ArrayAttributeForm from '@/components/Form/ArrayAttributeForm'
import TextInput from '@/components/Input/TextInput'

import IcClose from '@/assets/icons/ic_close.svg'

import ModalHelper from '@/ModalHelper'
import FormHelper from '@/FormHelper'
import ArrayAttributeFormHelper from '@/ArrayAttributeFormHelper'

export default function ModalFilhosUpdate(props) {
  const parentFormId = props.parentFormId
  const attribute = props.attribute

  const [formId, setFormId] = useState(null)

  const modalName = 'ModalFilhosUpdate'

  function onAfterOpen() {
    let formId = FormHelper.new()

    setFormId(formId)
  }

  function onRequestClose() {
    setFormId(null)

    FormHelper.remove(formId)
  }

  function getRawValue() {
    let index = ModalHelper.getParam(modalName, 'index')

    return ArrayAttributeFormHelper.getRawValue(parentFormId, attribute, index)
  }

  function getComputedValue() {
    let index = ModalHelper.getParam(modalName, 'index')

    return ArrayAttributeFormHelper.getComputedValue(parentFormId, attribute, index)
  }

  function handleBtnCloseClick(event) {
    ModalHelper.close(modalName)
  }

  function handleSubmit(event) {
    ModalHelper.close(modalName)
  }

  return (
    <Modal name={modalName} onAfterOpen={onAfterOpen} onRequestClose={onRequestClose}>
      <div className="modal">
        <div className="modal-header">
          <div className="modal-header-left">
            <div className="modal-titles">
              <span className="modal-hat">Filhos</span>
              <span className="modal-title">Editar filho</span>
            </div>
          </div>
          <div className="modal-header-right">
            <button className="btn btn-close" onClick={handleBtnCloseClick}>
              <div className="svg icon">
                <IcClose/>
              </div>
            </button>
          </div>
        </div>
        <div className="modal-content">
          <ArrayAttributeForm formId={formId} parentFormId={parentFormId} attribute={attribute} mode="update" modalName={modalName} onSubmit={handleSubmit}>
            <div className="inputs input-style-2">
              <TextInput formId={formId} attribute={attribute} classes="w3" label="Nome" required={true} initialValue={getRawValue()}/>

              <button type="submit" className="btn btn-six btn-bright-blue">Salvar</button>
            </div>
          </ArrayAttributeForm>
        </div>
      </div>
    </Modal>
  )
}