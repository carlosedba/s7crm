import React, { useState, useEffect } from 'react'
import { useSelector, useDispatch } from 'react-redux'
import { Link } from 'react-router-dom'
import update from 'immutability-helper'

import Form from '@/components/Form'
import Modal from '@/components/Modal'
import TextAreaInput from '@/components/Input/TextAreaInput'

import IcClose from '@/assets/icons/ic_close.svg'

import FormHelper from '@/FormHelper'
import ModalHelper from '@/ModalHelper'

const formId = FormHelper.new()

export default function ModalDescricaoCreate(props) {
  const parentFormId = props.parentFormId

  const modalName = 'ModalDescricaoCreate'
  const attribute = 'descricao'

  const formProps = {
    id: formId,
    model: 'S7Projeto',
    action: 'create',
    dependsOn: null,
    proccess: true
  }

  function handleBtnCloseClick(event) {
    ModalHelper.close(modalName)
  }

  return (
    <Modal name={modalName}>
      <div className="modal">
        <div className="modal-header">
          <div className="modal-header-left">
            <div className="modal-titles">
              <span className="modal-hat">Descrição</span>
              <span className="modal-title">Editar descrição</span>
            </div>
          </div>
          <div className="modal-header-right">
            <button className="btn btn-close" onClick={handleBtnCloseClick}>
              <div className="svg icon">
                <IcClose/>
              </div>
            </button>
          </div>
        </div>
        <div className="modal-content">
          <Form {...formProps}>
            <div className="inputs input-style-2">
              <TextAreaInput formId={formId} classes="w4-q3" attribute={attribute} required={true}/>
              <button type="submit" className="btn btn-six btn-bright-blue">Salvar</button>
            </div>
          </Form>
        </div>
      </div>
    </Modal>
  )
}

