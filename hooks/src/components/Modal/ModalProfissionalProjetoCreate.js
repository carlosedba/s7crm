import React, { useState, useEffect } from 'react'
import { useSelector, useDispatch } from 'react-redux'

import Modal from '@/components/Modal'
import Form from '@/components/Form'
import SuggestionInput from '@/components/Input/SuggestionInput'

import IcClose from '@/assets/icons/ic_close.svg'
import FormHelper from '@/FormHelper'
import ModalHelper from '@/ModalHelper'

export default function ModalProfissionalProjetoCreate(props) {
  const dependsOn = props.dependsOn
  const process = props.process || false
  const formRaw = props.formRaw || {}
  const formComputed = props.formComputed || {}

  const [formId, setFormId] = useState(null)
  const [formProps, setFormProps] = useState(null)

  const modalName = 'ModalProfissionalProjetoCreate'

  function onAfterOpen() {
    let formId = FormHelper.new({
      props: { raw: formRaw, computed: formComputed }
    })

    setFormId(formId)

    setFormProps({
      id: formId,
      model: 'S7ProfissionalProjeto',
      action: 'create',
      dependsOn: dependsOn,
      process: process
    })
  }

  function onRequestClose() {
    FormHelper.remove(formId)
  }

  function handleBtnCloseClick(event) {
    ModalHelper.close(modalName)
  }

  function handleSubmit(event) {
    ModalHelper.close(modalName)
  }

  return (
    <Modal name={modalName} onAfterOpen={onAfterOpen} onRequestClose={onRequestClose}>
      <div className="modal">
        <div className="modal-header">
          <div className="modal-header-left">
            <div className="modal-titles">
              <span className="modal-hat">Profissionais</span>
              <span className="modal-title">Adicionar profissional</span>
            </div>
          </div>
          <div className="modal-header-right">
            <button className="btn btn-close" onClick={handleBtnCloseClick}>
              <div className="svg icon">
                <IcClose/>
              </div>
            </button>
          </div>
        </div>
        <div className="modal-content">
          <Form {...formProps} onSubmit={handleSubmit}>
            <div className="inputs input-style-2">
              <SuggestionInput formId={formId} classes="w3" label="Nome do Profissional" attribute="profissional" collection="profissionais" attributeToQuery="nome" required={true}/>

              <button type="submit" className="btn btn-six btn-bright-blue">Salvar</button>
            </div>
          </Form>
        </div>
      </div>
    </Modal>
  )
}