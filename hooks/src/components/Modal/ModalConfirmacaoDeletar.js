import React, { useState, useEffect } from 'react'
import { useSelector, useDispatch } from 'react-redux'
import _ from 'lodash'

import Modal from '@/components/Modal'

import IcAlert from '@/assets/icons/ic_alert.svg'

import ModalHelper from '@/ModalHelper'
import NetworkHelper from '@/NetworkHelper'

import IdGenerator from '@/utils/IdGenerator'

export default function ModalConfirmacaoDeletar(props) {
  const [payload, setPayload] = useState(null)

  const requests = useSelector(state => state.Network.requests)

  const modalName = 'ModalConfirmacaoDeletar'

  useEffect(() => {
    for (let i = 0; i < requests.length; i++) {
      const request = requests[i]

      if (payload) {
        if (request.id === payload.id && request.status === 'success') {
          ModalHelper.close(modalName)
        }
      }
    }
  }, [requests])

  function onAfterOpen() {
    let model = ModalHelper.getParam(modalName, 'model')
    let objectId = ModalHelper.getParam(modalName, 'objectId')

    setPayload({
      id: IdGenerator.default(),
      params: {
        action: 'delete',
        model: model,
        objectId: objectId,
        process: true
      }
    })
  }

  function onRequestClose() {
    setPayload(null)
  }

  function handleCancelClick(event) {
    ModalHelper.close(modalName)
  }

  function handleDeleteClick(event) {
    let onDeleteHandler = ModalHelper.getHandler(modalName, 'onDelete')

    NetworkHelper.newRequest(payload)
  }
  
  return (
    <Modal name={modalName} onAfterOpen={onAfterOpen}>
      <div className="modal alert-modal">
        <div className="modal-header">
          <div className="svg alert-modal-icon">
            <IcAlert/>
          </div>
          <div className="modal-titles">
            <span className="modal-title">Tem certeza que deseja deletar?</span>
          </div>
        </div>
        <div className="modal-content">
          <div className="alert-modal-buttons">
            <button className="btn btn-three" onClick={handleCancelClick}>Cancelar</button>
            <button className="btn btn-three btn-red" onClick={handleDeleteClick}>Deletar</button>
          </div>
        </div>
      </div>
    </Modal>
  )
}