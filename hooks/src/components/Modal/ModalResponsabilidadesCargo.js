import React, { useState, useEffect } from 'react'
import { useSelector, useDispatch } from 'react-redux'
import { Link } from 'react-router-dom'
import update from 'immutability-helper'

import Modal from '@/components/Modal'
import TextAreaInput from '@/components/Input/TextAreaInput'

import IcClose from '@/assets/icons/ic_close.svg'

import ModalHelper from '@/ModalHelper'

export default function ModalResponsabilidadesCargo(props) {
  const parentFormId = props.parentFormId
  const attribute = props.attribute

  const modalName = 'ModalResponsabilidadesCargo'

  function handleBtnCloseClick(event) {
    ModalHelper.close(modalName)
  }

  return (
    <Modal name={modalName}>
      <div className="modal">
        <div className="modal-header">
          <div className="modal-header-left">
            <div className="modal-titles">
              <span className="modal-hat">Responsabilidades do cargo</span>
              <span className="modal-title">Editar responsabilidades do cargo</span>
            </div>
          </div>
          <div className="modal-header-right">
            <button className="btn btn-close" onClick={handleBtnCloseClick}>
              <div className="svg icon">
                <IcClose/>
              </div>
            </button>
          </div>
        </div>
        <div className="modal-content">
          <div className="inputs input-style-2">
            <TextAreaInput formId={parentFormId} classes="w5" attribute={attribute} required={true}/>

            <button type="submit" className="btn btn-six btn-bright-blue" onClick={handleBtnCloseClick}>Salvar</button>
          </div>
        </div>
      </div>
    </Modal>
  )
}