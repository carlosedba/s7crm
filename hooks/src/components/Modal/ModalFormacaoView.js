import React, { useState, useEffect } from 'react'
import { useSelector, useDispatch } from 'react-redux'
import _ from 'lodash'

import Modal from '@/components/Modal'

import IcClose from '@/assets/icons/ic_close.svg'

import ModalHelper from '@/ModalHelper'

export default function ModalFormacaoView(props) {

  const [modalData, setModalData] = useState({})

  const modalName = 'ModalFormacaoView'

  function onAfterOpen() {
    setModalData(ModalHelper.getData(modalName))
  }

  function handleBtnCloseClick(event) {
    ModalHelper.close(modalName)
  }
  
  return (
    <Modal name={modalName} onAfterOpen={onAfterOpen}>
      <div className="modal">
        <div className="modal-header">
          <div className="modal-header-left">
            <div className="modal-titles">
              <span className="modal-hat">Formações</span>
              <span className="modal-title">Formação</span>
            </div>
          </div>
          <div className="modal-header-right">
            <button className="btn btn-close" onClick={handleBtnCloseClick}>
              <div className="svg icon">
                <IcClose/>
              </div>
            </button>
          </div>
        </div>
        <div className="modal-content">
          <div className="data-fields data-fields-two">
            <div className="data-field">
              <span className="data-field-name">Instituição:</span>
              <span className="data-field-value">{_.get(modalData, 'instituicao.nome')}</span>
            </div>
            <div className="data-field">
              <span className="data-field-name">Curso:</span>
              <span className="data-field-value">{_.get(modalData, 'curso.nome')}</span>
            </div>
            <div className="data-field">
              <span className="data-field-name">Tipo:</span>
              <span className="data-field-value">{_.get(modalData, 'tipo')}</span>
            </div>
            <div className="data-field">
              <span className="data-field-name">Ano de conclusão:</span>
              <span className="data-field-value">{_.get(modalData, 'anoConclusao')}</span>
            </div>
          </div>
        </div>
      </div>
    </Modal>
  )
}

