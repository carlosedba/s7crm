import React, { useState, useEffect } from 'react'
import { useSelector, useDispatch } from 'react-redux'
import moment from 'moment'
import _ from 'lodash'

import Modal from '@/components/Modal'

import IcClose from '@/assets/icons/ic_close.svg'

import ModalHelper from '@/ModalHelper'

export default function ModalExpProfissionalView(props) {

  const [modalData, setModalData] = useState({})

  const modalName = 'ModalExpProfissionalView'

  function onAfterOpen() {
    setModalData(ModalHelper.getData(modalName))
  }

  function formatDate(str) {
    let date = moment(str)

    if (date.isValid()) {
      return date.format('MM[/]YYYY')
    }
  }

  function handleBtnCloseClick(event) {
    ModalHelper.close(modalName)
  }

  function renderBeneficios() {
    if (modalData) {
      let beneficios = modalData.beneficios

      if (beneficios) {
        return beneficios.map((beneficio) => {
          return beneficio.nome
        }).join(', ')
      }
    }
  }

  function renderFerramentas() {
    if (modalData) {
      let ferramentas = modalData.ferramentas

      if (ferramentas) {
        return ferramentas.map((ferramenta) => {
          return ferramenta.nome
        }).join(', ')
      }
    }
  }

  return (
    <Modal name={modalName} onAfterOpen={onAfterOpen}>
      <div className="modal">
        <div className="modal-header">
          <div className="modal-header-left">
            <div className="modal-titles">
              <span className="modal-hat">Experiências Profissionais</span>
              <span className="modal-title">Experiencia Profissional</span>
            </div>
          </div>
          <div className="modal-header-right">
            <button className="btn btn-close" onClick={handleBtnCloseClick}>
              <div className="svg icon">
                <IcClose/>
              </div>
            </button>
          </div>
        </div>
        <div className="modal-content">
          <div className="data-fields data-fields-two">
            <div className="data-field">
              <span className="data-field-name">Empresa:</span>
              <span className="data-field-value">{_.get(modalData, 'empresa.nomeFantasia')}</span>
            </div>
            <div className="data-multi-field">
              <div className="data-field w2-q2">
                <span className="data-field-name">Cargo:</span>
                <span className="data-field-value">{_.get(modalData, 'cargo.nome')}</span>
              </div>
              <div className="data-field w2-q2">
                <span className="data-field-name">Área:</span>
                <span className="data-field-value">{_.get(modalData, 'area.nome')}</span>
              </div>
            </div>
            <div className="data-multi-field">
              <div className="data-field w2-q2">
                <span className="data-field-name">Tipo de contratação:</span>
                <span className="data-field-value">{_.get(modalData, 'tipoContratacao')}</span>
              </div>
              <div className="data-field w2">
                <span className="data-field-name">Emprego atual:</span>
                <span className="data-field-value">{_.get(modalData, 'empregoAtual')}</span>
              </div>
            </div>
            <div className="data-multi-field">
              <div className="data-field w1-q3">
                <span className="data-field-name">Data de admissão:</span>
                <span className="data-field-value">{formatDate(_.get(modalData, 'dataAdmissao'))}</span>
              </div>
              {(_.get(modalData, 'dataDesligamento')) ? (
                <div className="data-field w1-q3">
                  <span className="data-field-name">Data de desligamento:</span>
                  <span className="data-field-value">{formatDate()}</span>
                </div>
              ) : null}
            </div>
            <div className="data-field">
              <span className="data-field-name">Benefícios:</span>
              <span className="data-field-value">{renderBeneficios()}</span>
            </div>
            <div className="data-field">
              <span className="data-field-name">Ferramentas:</span>
              <span className="data-field-value">{renderFerramentas()}</span>
            </div>
          </div>
        </div>
      </div>
    </Modal>
  )
}

