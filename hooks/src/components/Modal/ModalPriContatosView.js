import React, { useState, useEffect } from 'react'
import { useSelector, useDispatch } from 'react-redux'

import { closeModal, resetModal } from '@/actions/Modal'

import Modal from '@/components/Modal'

import IcClose from '@/assets/icons/ic_close.svg'

export default function ModalPriContatosView(props) {
  const modal = useSelector(state => state.Modal)

  const name = 'ModalPriContatosView'

  const modalProps = modal[name]

  const dispatch = useDispatch()

  function handleBtnCloseClick(event) {
    dispatch(resetModal(name))
    dispatch(closeModal(name))
  }

  return (
    <Modal name={name} contentLabel="">
      <div className="modal">
        <div className="modal-header">
          <div className="modal-header-left">
            <div className="modal-titles">
              <span className="modal-hat">Principais contatos</span>
              <span className="modal-title">{(modalProps) ? modalProps.nome + ' ' + modalProps.sobrenome : ''}</span>
            </div>
          </div>
          <div className="modal-header-right">
            <button className="btn btn-close" onClick={handleBtnCloseClick}>
              <div className="svg icon">
                <IcClose/>
              </div>
            </button>
          </div>
        </div>
        <div className="modal-content">
          <div className="data-fields data-fields-two">
            <div className="data-field">
              <span className="data-field-name">E-mail corporativo:</span>
              <span className="data-field-value">{(modalProps) ? modalProps.emailCorporativo : ''}</span>
            </div>
            <div className="data-field">
              <span className="data-field-name">Telefone comercial:</span>
              <span className="data-field-value">{(modalProps) ? modalProps.telefoneComercial : ''}</span>
            </div>
          </div>
        </div>
      </div>
    </Modal>
  )
}

