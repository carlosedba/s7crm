import React, { useState, useEffect } from 'react'
import { useSelector, useDispatch } from 'react-redux'

import Modal from '@/components/Modal'
import Form from '@/components/Form'
import SuggestionInput from '@/components/Input/SuggestionInput'

import IcClose from '@/assets/icons/ic_close.svg'

import ModalHelper from '@/ModalHelper'
import FormHelper from '@/FormHelper'
import ProfissionalRepository from '@/repository/ProfissionalRepository'

export default function ModalConAdministrativoCreate(props) {
  const dependsOn = props.dependsOn
  const process = props.process || false
  const formRaw = props.formRaw || {}
  const formComputed = props.formComputed || {}

  const [formId, setFormId] = useState(null)
  const [formProps, setFormProps] = useState(null)

  const modalName = 'ModalConAdministrativoCreate'

  function onAfterOpen() {
    let formId = FormHelper.new({
      props: { raw: formRaw, computed: formComputed }
    })

    setFormId(formId)

    setFormProps({
      id: formId,
      model: 'S7Conselheiro',
      action: 'create',
      dependsOn: dependsOn,
      process: process
    })
  }

  function onRequestClose() {
    FormHelper.remove(formId)
  }

  function handleBtnCloseClick(event) {
    ModalHelper.close(modalName)
  }

  function handleSubmit(event) {
    ModalHelper.close(modalName)
  }

  return (
    <Modal name={modalName} onAfterOpen={onAfterOpen} onRequestClose={onRequestClose}>
      <div className="modal">
        <div className="modal-header">
          <div className="modal-header-left">
            <div className="modal-titles">
              <span className="modal-hat">Conselho Administrativo</span>
              <span className="modal-title">Adicionar pessoa</span>
            </div>
          </div>
          <div className="modal-header-right">
            <button className="btn btn-close" onClick={handleBtnCloseClick}>
              <div className="svg icon">
                <IcClose/>
              </div>
            </button>
          </div>
        </div>
        <div className="modal-content">
          <Form {...formProps} onSubmit={handleSubmit}>
            <div className="inputs input-style-2">
              <SuggestionInput
                formId={formId}
                classes="w3"
                label="Nome do Profissional"
                attribute="profissional"
                dataSource={{
                  repository: ProfissionalRepository,
                  attributes: ['nome', 'sobrenome']
                }}
                required={true}
              />

              <button type="submit" className="btn btn-six btn-bright-blue">Salvar</button>
            </div>
          </Form>
        </div>
      </div>
    </Modal>
  )
}