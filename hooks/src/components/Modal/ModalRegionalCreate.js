import React, { useState, useEffect } from 'react'
import { useSelector, useDispatch } from 'react-redux'

import Modal from '@/components/Modal'
import ObjectArrayAttributeForm from '@/components/Form/ObjectArrayAttributeForm'
import TextInput from '@/components/Input/TextInput'

import IcClose from '@/assets/icons/ic_close.svg'

import { preencherEnderecoPorCep } from '@/utils/FormUtils'

import FormHelper from '@/FormHelper'
import ModalHelper from '@/ModalHelper'

export default function ModalRegionalCreate(props) {
  const parentFormId = props.parentFormId
  const attribute = props.attribute

  const [formId, setFormId] = useState(null)

  const modalName = 'ModalRegionalCreate'

  const dispatch = useDispatch()

  function onAfterOpen() {
    let formId = FormHelper.new()

    setFormId(formId)
  }

  function onRequestClose() {
    setFormId(null)

    FormHelper.remove(formId)
  }

  function handleTextInputChange(event) {
    if (event.value) {
      if (event.attribute === 'cep') preencherEnderecoPorCep(dispatch, formId, event.value)
    }
  }

  function handleBtnCloseClick(event) {
    ModalHelper.close(modalName)
  }

  function handleSubmit(event) {
    ModalHelper.close(modalName)
  }

  return (
    <Modal name={modalName} onAfterOpen={onAfterOpen} onRequestClose={onRequestClose}>
      <div className="modal">
        <div className="modal-header">
          <div className="modal-header-left">
            <div className="modal-titles">
              <span className="modal-hat">Regional</span>
              <span className="modal-title">Adicionar regional</span>
            </div>
          </div>
          <div className="modal-header-right">
            <button className="btn btn-close" onClick={handleBtnCloseClick}>
              <div className="svg icon">
                <IcClose/>
              </div>
            </button>
          </div>
        </div>
        <div className="modal-content">
          <ObjectArrayAttributeForm formId={formId} parentFormId={parentFormId} attribute={attribute} onSubmit={handleSubmit}>
            <div className="inputs input-style-2">
              <div className="multi-input">
                <TextInput formId={formId} classes="w4" label="Nome" attribute="nome" required={true}/>
                <TextInput formId={formId} classes="w1-q1" label="CEP" attribute="cep" mask="cep" required={true} onChange={handleTextInputChange}/>
              </div>
              <div className="multi-input">
                <TextInput formId={formId} classes="w4" label="Rua" attribute="rua" required={true}/>
                <TextInput formId={formId} classes="w1" label="Número" attribute="numero" required={true}/>
                <TextInput formId={formId} classes="w1" label="Complemento" attribute="complemento"/>
              </div>
              <div className="multi-input">
                <TextInput formId={formId} classes="w2" label="Bairro" attribute="bairro" required={true}/>
                <TextInput formId={formId} classes="w2-q1" label="Cidade" attribute="cidade" required={true}/>
              </div>
              <div className="multi-input">
                <TextInput formId={formId} classes="w2" label="Estado" attribute="estado" required={true}/>
                <TextInput formId={formId} classes="w1-q2" label="País" attribute="pais" required={true}/>
              </div>

              <button type="submit" className="btn btn-six btn-bright-blue">Salvar</button>
            </div>
          </ObjectArrayAttributeForm>
        </div>
      </div>
    </Modal>
  )
}