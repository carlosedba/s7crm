import React, { useState, useEffect } from 'react'
import { useSelector, useDispatch } from 'react-redux'
import { Link } from 'react-router-dom'
import update from 'immutability-helper'

import { closeModal } from '@/actions/Modal'
import { updateFormRaw, updateFormComputed } from '@/actions/Form'

import Modal from '@/components/Modal'
import Form from '@/components/Form'
import TextInput from '@/components/Input/TextInput'
import SuggestionInput from '@/components/Input/SuggestionInput'
import SelectInput from '@/components/Input/SelectInput'

import IcClose from '@/assets/icons/ic_close.svg'

export default function ModalProProjetoUpdate(props) {    
  const modal = useSelector(state => state.Modal)
  const forms = useSelector(state => state.Forms)

  const name = 'ModalProProjetoUpdate'
  const title = 'Profissionais'

  const model = 'ProfissionalProjeto'
  const action = 'create'

  const generalFormProps = { model, action }

  const dispatch = useDispatch()

  function onAfterOpen() {
    if (modal[name]) {
      formId = modal[name].id

      if (formId) {
        //NQF.mapToForm(formId)
      }
    }
  }

  function onRequestClose() {
    //dispatch(updateFormRaw(model, action, { $set: {} }))
    //dispatch(updateFormComputed(model, action, { $set: {} }))
  }

  function handleBtnCloseClick(event) {
    dispatch(closeModal(name))
  }

  function handleSubmit(event) {
    dispatch(closeModal(name))
  }

  return (
    <Modal name={name} contentLabel={title} onAfterOpen={onAfterOpen} onRequestClose={onRequestClose}>
      <div className="modal">
        <div className="modal-header">
          <div className="modal-header-left">
            <div className="modal-titles">
              <span className="modal-hat">{title}</span>
              <span className="modal-title">Editar profissional</span>
            </div>
          </div>
          <div className="modal-header-right">
            <button className="btn btn-close" onClick={handleBtnCloseClick}>
              <div className="svg icon">
                <IcClose/>
              </div>
            </button>
          </div>
        </div>
        <div className="modal-content">
          <Form {...generalFormProps} id={formId} update={true} onSubmit={handleSubmit}>
            <div className="inputs">
              <div className="multi-input">
                <SuggestionInput {...generalFormProps} classes="w3" label="Profissional" attribute="id_s7_profissional" collection="profissionais"/>
                <SuggestionInput {...generalFormProps} classes="w2-q2" label="Cargo" attribute="id_s7_cargo" collection="cargos"/>
              </div>
              <div className="multi-input">
                <TextInput {...generalFormProps} classes="w1-q1" label="Honorário" attribute="honorario"/>
                <TextInput {...generalFormProps} classes="w1-q2" label="Salário inicial" attribute="salario_inicial"/>
                <TextInput {...generalFormProps} classes="w1-q2" label="Salário final" attribute="salario_final"/>
              </div>
              <div className="multi-input"> 
                <TextInput {...generalFormProps} classes="w1" label="Comissão" attribute="comissao"/>
                <TextInput {...generalFormProps} classes="w1" label="Bonus" attribute="bonus"/>
                <SelectInput {...generalFormProps} size="w3" label="Benefícios" attribute="beneficios" isMulti={true}
                             options={[
                    { value: 'Assistência médica', label: 'Assistência médica' },
                    { value: 'Assistência odontológica', label: 'Assistência odontológica' },
                    { value: 'Auxílio farmácia', label: 'Auxílio farmácia' },
                    { value: 'Seguro de vida', label: 'Seguro de vida' },
                    { value: 'Previdência privada', label: 'Previdência privada' },
                    { value: 'Auxílio creche', label: 'Auxílio creche' },
                    { value: 'Bolsa de estudos', label: 'Bolsa de estudos' },
                    { value: 'Vale refeição', label: 'Vale refeição' },
                    { value: 'Vale almentação', label: 'Vale almentação' },
                    { value: 'Refeitório local', label: 'Refeitório local' },
                    { value: 'Cesta básica', label: 'Cesta básica' },
                    { value: 'Cartão corporativo', label: 'Cartão corporativo' },
                    { value: 'Carro', label: 'Carro' },
                    { value: 'Combustível', label: 'Combustível' },
                    { value: 'Celular', label: 'Celular' },
                    { value: 'Notebook', label: 'Notebook' },
                  ]}/>
              </div>
              <button type="submit" className="btn btn-three">Salvar</button>
            </div>
          </Form>
        </div>
      </div>
    </Modal>
  )
}

