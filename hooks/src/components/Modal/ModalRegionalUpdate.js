import React, { useState, useEffect } from 'react'
import { useSelector, useDispatch } from 'react-redux'

import Modal from '@/components/Modal'
import ObjectArrayAttributeForm from '@/components/Form/ObjectArrayAttributeForm'
import TextInput from '@/components/Input/TextInput'

import IcClose from '@/assets/icons/ic_close.svg'

import { preencherEnderecoPorCep } from '@/utils/FormUtils'

import ModalHelper from '@/ModalHelper'
import FormHelper from '@/FormHelper'
import ObjectArrayAttributeFormHelper from '@/ObjectArrayAttributeFormHelper'

export default function ModalRegionalUpdate(props) {
  const parentFormId = props.parentFormId
  const attribute = props.attribute

  const [formId, setFormId] = useState(null)

  const modalName = 'ModalRegionalUpdate'

  const dispatch = useDispatch()

  function onAfterOpen() {
    let formId = FormHelper.new()

    setFormId(formId)
  }

  function onRequestClose() {
    setFormId(null)

    FormHelper.remove(formId)
  }

  function getRawValue(subAttribute) {
    let index = ModalHelper.getParam(modalName, 'index')

    return ObjectArrayAttributeFormHelper.getRawValue(parentFormId, attribute, index, subAttribute)
  }

  function getComputedValue(subAttribute) {
    let index = ModalHelper.getParam(modalName, 'index')

    return ObjectArrayAttributeFormHelper.getComputedValue(parentFormId, attribute, index, subAttribute)
  }

  function handleTextInputChange(event) {
    if (event.value) {
      const initialCEP = getRawValue('cep')

      if (event.attribute === 'cep' && event.value !== initialCEP) preencherEnderecoPorCep(dispatch, formId, event.value)
    }
  }

  function handleBtnCloseClick(event) {
    ModalHelper.close(modalName)
  }

  function handleSubmit(event) {
    ModalHelper.close(modalName)
  }

  return (
    <Modal name={modalName} onAfterOpen={onAfterOpen} onRequestClose={onRequestClose}>
      <div className="modal">
        <div className="modal-header">
          <div className="modal-header-left">
            <div className="modal-titles">
              <span className="modal-hat">Regional</span>
              <span className="modal-title">Editar regional</span>
            </div>
          </div>
          <div className="modal-header-right">
            <button className="btn btn-close" onClick={handleBtnCloseClick}>
              <div className="svg icon">
                <IcClose/>
              </div>
            </button>
          </div>
        </div>
        <div className="modal-content">
          <ObjectArrayAttributeForm formId={formId} parentFormId={parentFormId} attribute={attribute} mode="update" modalName={modalName} onSubmit={handleSubmit}>
            <div className="inputs input-style-2">
              <div className="multi-input">
                <TextInput formId={formId} classes="w4" label="Nome" attribute="nome" required={true} initialValue={getRawValue('nome')}/>
                <TextInput formId={formId} classes="w1-q1" label="CEP" attribute="cep" mask="cep" required={true} initialValue={getRawValue('cep')} onChange={handleTextInputChange}/>
              </div>
              <div className="multi-input">
                <TextInput formId={formId} classes="w4" label="Rua" attribute="rua" required={true} initialValue={getRawValue('rua')}/>
                <TextInput formId={formId} classes="w1" label="Número" attribute="numero" required={true} initialValue={getRawValue('numero')}/>
                <TextInput formId={formId} classes="w1" label="Complemento" attribute="complemento" initialValue={getRawValue('complemento')}/>
              </div>
              <div className="multi-input">
                <TextInput formId={formId} classes="w2" label="Bairro" attribute="bairro" required={true} initialValue={getRawValue('bairro')}/>
                <TextInput formId={formId} classes="w2-q1" label="Cidade" attribute="cidade" required={true} initialValue={getRawValue('cidade')}/>
              </div>
              <div className="multi-input">
                <TextInput formId={formId} classes="w2" label="Estado" attribute="estado" required={true} initialValue={getRawValue('estado')}/>
                <TextInput formId={formId} classes="w1-q2" label="País" attribute="pais" required={true} initialValue={getRawValue('pais')}/>
              </div>

              <button type="submit" className="btn btn-six btn-bright-blue">Salvar</button>
            </div>
          </ObjectArrayAttributeForm>
        </div>
      </div>
    </Modal>
  )
}