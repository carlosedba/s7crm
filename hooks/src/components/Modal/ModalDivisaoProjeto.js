import React, { useState, useEffect } from 'react'
import { useSelector, useDispatch } from 'react-redux'
import { Link } from 'react-router-dom'

import Modal from '@/components/Modal'
import Accordion from '@/components/Accordion'
import AccordionItem from '@/components/AccordionItem'

import IcClose from '@/assets/icons/ic_close.svg'

import ModalHelper from '@/ModalHelper'

export default function ModalDivisaoProjeto(props) {
  const modalName = 'ModalDivisaoProjeto'

  function handleBtnCloseClick(event) {
    ModalHelper.close(modalName)
  }

  function handleLinkClick(event) {
    console.log('assdasds')
    ModalHelper.close(modalName)
  }

  return (
    <Modal name={modalName}>
      <div className="modal">
        <div className="modal-header">
          <div className="modal-header-left">
            <div className="modal-titles">
              <span className="modal-title">Selecione sua divisão</span>
            </div>
          </div>
          <div className="modal-header-right">
            <button className="btn btn-close" onClick={handleBtnCloseClick}>
              <div className="svg icon">
                <IcClose/>
              </div>
            </button>
          </div>
        </div>
        <div className="modal-content">

          <Accordion>
            <AccordionItem title="S7 Up" to="/projetos/s7-up/novo"></AccordionItem>
            <AccordionItem title="S7 Brand" to="/projetos/s7-brand/novo"></AccordionItem>
            <AccordionItem title="S7 Search">
              <Link to="/projetos/s7-search/recrutamento-selecao/novo" onClick={handleLinkClick}>Recrutamento e Seleção</Link>
              <Link to="/projetos/s7-search/mapeamento-mercado/novo" onClick={handleLinkClick}>Mapeamento de Mercado</Link>
              <Link to="/projetos/s7-search/pesquisa-salarial/novo" onClick={handleLinkClick}>Pesquisa Salarial</Link>
            </AccordionItem>
          </Accordion>

        </div>
      </div>
    </Modal>
  )
}