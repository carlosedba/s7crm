import React, { useState, useEffect } from 'react'
import { useSelector, useDispatch } from 'react-redux'

import Modal from '@/components/Modal'
import Form from '@/components/Form'
import TextInput from '@/components/Input/TextInput'
import SuggestionInput from '@/components/Input/SuggestionInput'
import SelectInput from '@/components/Input/SelectInput'

import IcClose from '@/assets/icons/ic_close.svg'

import InstituicaoRepository from '@/repository/InstituicaoRepository'
import CursoRepository from '@/repository/CursoRepository'

import ModalHelper from '@/ModalHelper'
import FormHelper from '@/FormHelper'

export default function ModalFormacaoCreate(props) {
  const dependsOn = props.dependsOn
  const process = props.process || false
  const formRaw = props.formRaw || {}
  const formComputed = props.formComputed || {}

  const [formId, setFormId] = useState(null)
  const [formProps, setFormProps] = useState(null)

  const modalName = 'ModalFormacaoCreate'

  function onAfterOpen() {
    let formId = FormHelper.new({
      props: { raw: formRaw, computed: formComputed }
    })

    setFormId(formId)

    setFormProps({
      id: formId,
      model: 'S7Formacao',
      action: 'create',
      dependsOn: dependsOn,
      process: process
    })
  }

  function onRequestClose() {
    FormHelper.remove(formId)
  }

  function handleBtnCloseClick(event) {
    ModalHelper.close(modalName)
  }

  function handleSubmit(event) {
    ModalHelper.close(modalName)
  }

  return (
    <Modal name={modalName} onAfterOpen={onAfterOpen} onRequestClose={onRequestClose}>
      <div className="modal">
        <div className="modal-header">
          <div className="modal-header-left">
            <div className="modal-titles">
              <span className="modal-hat">Formações</span>
              <span className="modal-title">Adicionar formação</span>
            </div>
          </div>
          <div className="modal-header-right">
            <button className="btn btn-close" onClick={handleBtnCloseClick}>
              <div className="svg icon">
                <IcClose/>
              </div>
            </button>
          </div>
        </div>
        <div className="modal-content">
          <Form {...formProps} onSubmit={handleSubmit}>
            <div className="inputs input-style-2">
              <div className="multi-input">
                <SuggestionInput
                  formId={formId}
                  classes="w3"
                  label="Instituição"
                  attribute="instituicao"
                  dataSource={{
                    repository: InstituicaoRepository,
                    attributes: ['nome']
                  }}
                  required={true}
                />
                <SuggestionInput
                  formId={formId}
                  classes="w3"
                  label="Curso"
                  attribute="curso"
                  dataSource={{
                    repository: CursoRepository,
                    attributes: ['nome']
                  }}
                  required={true}
                />
              </div>
              <div className="multi-input">
                <SelectInput formId={formId} size="w2-q2" label="Tipo" attribute="tipo" required={true}
                             options={[
                    { value: 'Técnico', label: 'Técnico' },
                    { value: 'Graduação', label: 'Graduação' },
                    { value: 'Pós-Gradução', label: 'Pós-Gradução' },
                    { value: 'Especialização', label: 'Especialização' },
                    { value: 'MBA', label: 'MBA' },
                    { value: 'Mestrado', label: 'Mestrado' },
                    { value: 'Doutorado', label: 'Doutorado' },
                  ]}/>
                <TextInput formId={formId} classes="w1-q2" label="Ano de conclusão" required={true} attribute="anoConclusao"/>
              </div>

              <button type="submit" className="btn btn-six btn-bright-blue">Salvar</button>
            </div>
          </Form>
        </div>
      </div>
    </Modal>
  )
}