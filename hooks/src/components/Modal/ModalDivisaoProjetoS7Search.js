import React, { useState, useEffect } from 'react'
import { useSelector, useDispatch } from 'react-redux'
import { Link } from 'react-router-dom'

import Modal from '@/components/Modal'

import IcClose from '@/assets/icons/ic_close.svg'

import ModalHelper from '@/ModalHelper'

export default function ModalDivisaoProjetoS7Search(props) {
  const modalName = 'ModalDivisaoProjetoS7Search'

  function handleBtnCloseClick(event) {
    ModalHelper.close(modalName)
  }

  function handleLinkClick(event) {
    console.log('assdasds')
    ModalHelper.close(modalName)
  }

  return (
    <Modal name={modalName}>
      <div className="modal">
        <div className="modal-header">
          <div className="modal-header-left">
            <div className="modal-titles">
              <span className="modal-title">Selecione a categoria</span>
            </div>
          </div>
          <div className="modal-header-right">
            <button className="btn btn-close" onClick={handleBtnCloseClick}>
              <div className="svg icon">
                <IcClose/>
              </div>
            </button>
          </div>
        </div>
        <div className="modal-content">
          <Link className="modal-link" to="/projetos/s7-search/recrutamento-selecao/novo" onClick={handleLinkClick}>Recrutamento e Seleção</Link>
          <Link className="modal-link" to="/projetos/s7-search/mapeamento-mercado/novo" onClick={handleLinkClick}>Mapeamento de Mercado</Link>
          <Link className="modal-link" to="/projetos/s7-search/pesquisa-salarial/novo" onClick={handleLinkClick}>Pesquisa Salarial</Link>
        </div>
      </div>
    </Modal>
  )
}