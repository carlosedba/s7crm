import React, { useState, useEffect } from 'react'
import update from 'immutability-helper'

import Modal from '@/components/Modal'

import IcClose from '@/assets/icons/ic_close.svg'

import ModalHelper from '@/ModalHelper'
import SuggestionInput from '@/components/Input/SuggestionInput'
import UsuarioRepository from '@/repository/UsuarioRepository'
import EmpresaRepository from '@/repository/EmpresaRepository'
import CargoRepository from '@/repository/CargoRepository'
import AreaRepository from '@/repository/AreaRepository'
import SelectInput from '@/components/Input/SelectInput'

export default function ModalFiltrosProfissionais(props) {
  const onSubmit = props.onSubmit

  const [modalData, setModalData] = useState({})

  const [filters, setFilters] = useState({})
  const [cargos, setCargos] = useState([])
  const [areas, setAreas] = useState([])

  const modalName = 'ModalFiltrosProfissionais'

  useEffect(() => {
    fetchCargos()
    fetchAreas()
  }, [])

  function onAfterOpen() {
    setModalData(ModalHelper.getData(modalName))
  }

  function fetchCargos() {
    CargoRepository.find()
      .then((data) => {
        const cargoOptions = data.map((cargo) => ({ value: cargo._id, label: cargo.nome }))

        setCargos(cargoOptions)
      })
  }

  function fetchAreas() {
    AreaRepository.find()
      .then((data) => {
        const areaOptions = data.map((area) => ({ value: area._id, label: area.nome }))

        setAreas(areaOptions)
      })
  }

  function handleSuggestionInputChange(event) {
    setFilters(update(filters, {
      $merge: {
        [event.attribute]: event.value
      }
    }))
  }

  function handleSelectInputChange(event) {
    setFilters(update(filters, {
      $merge: {
        [event.attribute]: event.value
      }
    }))
  }

  function handleSubmit(event) {
    event.preventDefault()

    if (onSubmit) onSubmit(filters)

    ModalHelper.close(modalName)
  }

  function handleClearFilters(event) {
    event.preventDefault()

    setFilters({})

    if (onSubmit) onSubmit({})

    ModalHelper.close(modalName)
  }

  function handleBtnCloseClick(event) {
    ModalHelper.close(modalName)
  }

  return (
    <Modal name={modalName} onAfterOpen={onAfterOpen}>
      <div className="modal">
        <div className="modal-header">
          <div className="modal-header-left">
            <div className="modal-titles">
              <span className="modal-hat">Empresas</span>
              <span className="modal-title">Filtros</span>
            </div>
          </div>
          <div className="modal-header-right">
            <button className="btn btn-close" onClick={handleBtnCloseClick}>
              <div className="svg icon">
                <IcClose/>
              </div>
            </button>
          </div>
        </div>
        <div className="modal-content">
          <div className="filtros">
            <form onSubmit={handleSubmit}>
              <div className="inputs input-style-2">
                <SuggestionInput
                  classes="w3"
                  label="Empresa atual"
                  attribute="empresaAtual"
                  dataSource={{
                    repository: EmpresaRepository,
                    attributes: ['nomeFantasia']
                  }}
                  initialValue={filters.empresaAtual}
                  onChange={handleSuggestionInputChange}
                />
                <div className="multi-input">
                  <SelectInput
                    size="w2-q2"
                    label="Cargo"
                    attribute="cargoAtual"
                    required={true}
                    options={cargos}
                    initialValue={filters.cargoAtual}
                    isClearable={true}
                    onChange={handleSelectInputChange}
                  />
                  <SelectInput
                    size="w2-q2"
                    label="Área"
                    attribute="areaAtual"
                    required={true}
                    options={areas}
                    initialValue={filters.areaAtual}
                    isClearable={true}
                    onChange={handleSelectInputChange}
                  />
                </div>
              </div>

              <div className="form-actions">
                <button type="submit" className="btn btn-six btn-bright-blue">Aplicar</button>
                {(Object.keys(filters).length) ? (<button className="btn btn-six btn-bright-blue" onClick={handleClearFilters}>Limpar</button>) : null}
              </div>
            </form>
          </div>
        </div>
      </div>
    </Modal>
  )
}