import React, { useState, useEffect } from 'react'
import { useSelector, useDispatch } from 'react-redux'

import { closeModal, resetModal } from '@/actions/Modal'

import Modal from '@/components/Modal'

import IcClose from '@/assets/icons/ic_close.svg'

export default function ModalRegionalView(props) {
  const modal = useSelector(state => state.Modal)

  const name = 'ModalRegionalView'

  const modalProps = modal[name]

  const dispatch = useDispatch()

  function handleBtnCloseClick(event) {
    dispatch(resetModal(name))
    dispatch(closeModal(name))
  }

  return (
    <Modal name={name} contentLabel="">
      <div className="modal">
        <div className="modal-header">
          <div className="modal-header-left">
            <div className="modal-titles">
              <span className="modal-hat">Regionais</span>
              <span className="modal-title">{(modalProps) ? modalProps.nome : ''}</span>
            </div>
          </div>
          <div className="modal-header-right">
            <button className="btn btn-close" onClick={handleBtnCloseClick}>
              <div className="svg icon">
                <IcClose/>
              </div>
            </button>
          </div>
        </div>
        <div className="modal-content">
          <div className="data-fields data-fields-two">
            <div className="data-field">
              <span className="data-field-name">Rua:</span>
              <span className="data-field-value">{(modalProps) ? modalProps.rua : ''}</span>
            </div>
            <div className="data-field">
              <span className="data-field-name">Número:</span>
              <span className="data-field-value">{(modalProps) ? modalProps.numero : ''}</span>
            </div>
            <div className="data-field">
              <span className="data-field-name">Complemento:</span>
              <span className="data-field-value">{(modalProps) ? modalProps.complemento : ''}</span>
            </div>
            <div className="data-field">
              <span className="data-field-name">Bairro:</span>
              <span className="data-field-value">{(modalProps) ? modalProps.bairro : ''}</span>
            </div>
            <div className="data-field">
              <span className="data-field-name">Cidade:</span>
              <span className="data-field-value">{(modalProps) ? modalProps.cidade : ''}</span>
            </div>
            <div className="data-field">
              <span className="data-field-name">Estado:</span>
              <span className="data-field-value">{(modalProps) ? modalProps.estado : ''}</span>
            </div>
            <div className="data-field">
              <span className="data-field-name">País:</span>
              <span className="data-field-value">{(modalProps) ? modalProps.pais : ''}</span>
            </div>
          </div>
        </div>
      </div>
    </Modal>
  )
}

