import React, { useState, useEffect } from 'react'
import { useSelector, useDispatch } from 'react-redux'
import createNumberMask from 'text-mask-addons/dist/createNumberMask'
import currencyJs from 'currency.js'

import Modal from '@/components/Modal'
import Form from '@/components/Form'
import TextInput from '@/components/Input/TextInput'
import SelectInput from '@/components/Input/SelectInput'

import IcClose from '@/assets/icons/ic_close.svg'

import MoedaRepository from '@/repository/MoedaRepository'

import FormHelper from '@/FormHelper'
import ModalHelper from '@/ModalHelper'

export default function ModalExpProfissionalSalario(props) {
  const action = props.action || 'create'
  const process = props.process || false
  const dependsOn = props.dependsOn

  const [formId, setFormId] = useState(null)
  const [formProps, setFormProps] = useState(null)

  const [moedas, setMoedas] = useState([])
  const [currency, setCurrency] = useState('')

  const forms = useSelector(state => state.Forms)
  
  const modalName = 'ModalExpProfissionalSalario'

  const masks = {
    numero: createNumberMask({
      prefix: '',
      thousandsSeparatorSymbol: '.',
      decimalSymbol: ',',
      allowDecimal: true,
      //allowLeadingZeroes: true
    })
  }

  const dispatch = useDispatch()

  function onAfterOpen() {
    let formId = ModalHelper.getParam(modalName, 'formId')

    if (!formId) formId = FormHelper.new()

    setFormId(formId)

    setFormProps({
      id: formId,
      model: 'S7ExperienciaProfissional',
      action: action,
      dependsOn: dependsOn,
      process: process
    })

    if (action === 'update') {
      let objectId = ModalHelper.getRawValue(modalName, formId, '_id')

      FormHelper.setObjectId(formId, objectId)
    }

    fetchMoedas()
  }

  function onRequestClose() {
    let formId = ModalHelper.getParam(modalName, 'formId')

    if (!formId) FormHelper.remove(formId)

    setFormId(null)
  }

  function fetchMoedas() {
    MoedaRepository.find()
      .then((data) => {
        const moedaOptions = data.map((moeda) => ({ value: moeda._id, label: moeda.simbolo }))

        setMoedas(moedaOptions)
      })
  }

  function handleTotalInputFocus(event) {
    const attribute = event.attribute

    let currentCurrency, base, multiplier, total

    currentCurrency = {
      separator: '.',
      decimal: ','
    }

    switch (attribute) {
      case 'totalSalario':
        base = forms[formId]['raw']['salario']
        multiplier = forms[formId]['raw']['quantidadeSalarios']
        break

      case 'totalComissao':
        base = forms[formId]['raw']['comissao']
        multiplier = forms[formId]['raw']['quantidadeComissao']
        break

      case 'totalBonus':
        base = forms[formId]['raw']['bonus']
        multiplier = forms[formId]['raw']['quantidadeBonus']
        break

      case 'totalPlr':
        base = forms[formId]['raw']['plr']
        multiplier = forms[formId]['raw']['quantidadePlr']
        break
    }

    //console.log(base)
    //console.log(multiplier)

    base = currencyJs(base, currentCurrency)
    multiplier = currencyJs(multiplier, currentCurrency)
    total = base.multiply(multiplier.value)

    //console.log(base)
    //console.log(multiplier)
    //console.log(total)

    FormHelper.setRawAttribute(formId, attribute, { $set: total.format() })
  }

  function handleBtnCloseClick(event) {
    ModalHelper.close(modalName)
  }

  function handleSubmit(event) {
    ModalHelper.close(modalName)
  }

  const customStyles = {
    control: (provided, state) => ({
      ...provided,
      cursor: 'pointer',
      width: '100px',
      borderRadius: '6px',
      backgroundColor: 'transparent',
      border: 'none',
    }),

    indicatorSeparator: (provided, state) => ({
      ...provided,
      display: 'none'
    }),

    dropdownIndicator: (provided, state) => ({
      ...provided,
      color: '#848484'
    }),

    placeholder: (provided, state) => ({
      ...provided,
      color: '#464646'
    }),
  }

  return (
    <Modal name={modalName} onAfterOpen={onAfterOpen} onRequestClose={onRequestClose}>
      <div className="modal">
        <div className="modal-header">
          <div className="modal-header-left">
            <div className="modal-titles">
              <span className="modal-hat">Experiência profissional</span>
              <span className="modal-title">Salário</span>
            </div>
          </div>
          <div className="modal-header-right">
            <button className="btn btn-close" onClick={handleBtnCloseClick}>
              <div className="svg icon">
                <IcClose/>
              </div>
            </button>
          </div>
        </div>
        <div className="modal-content">
          <Form {...formProps} onSubmit={handleSubmit}>
            <div className="ctable ctable-salario">
              <div className="ctable-row ctable-header">
                <div className="ctable-col w1">
                  <SelectInput
                    formId={formId}
                    placeholder="Moeda"
                    size="w1"
                    attribute="moeda"
                    value={currency}
                    options={moedas}
                    styles={customStyles}
                    initialValue={ModalHelper.getRawValue(modalName, formId, 'moeda')}
                  />
                </div>
                <div className="ctable-col center w1-q2">Remuneração</div>
                <div className="ctable-col center w1-q2">Quantidade</div>
                <div className="ctable-col center w1-q2">Total</div>
              </div>
              <div className="ctable-row center">
                <div className="ctable-col w1">Salário</div>
                <div className="ctable-col w1-q2">
                  <TextInput formId={formId} classes="ctable-input" attribute="salario" mask={masks.numero} initialValue={ModalHelper.getRawValue(modalName, formId, 'salario')}/>
                </div>
                <div className="ctable-col w1-q2">
                  <TextInput formId={formId} classes="ctable-input" attribute="quantidadeSalarios" initialValue={ModalHelper.getRawValue(modalName, formId, 'quantidadeSalarios')}/>
                </div>
                <div className="ctable-col w1-q2">
                  <TextInput formId={formId} classes="ctable-input" attribute="totalSalario" mask={masks.numero} onFocus={handleTotalInputFocus} initialValue={ModalHelper.getRawValue(modalName, formId, 'totalSalario')}/>
                </div>
              </div>
              <div className="ctable-row center">
                <div className="ctable-col w1">Comissão</div>
                <div className="ctable-col w1-q2">
                  <TextInput formId={formId} classes="ctable-input" attribute="comissao" mask={masks.numero} initialValue={ModalHelper.getRawValue(modalName, formId, 'comissao')}/>
                </div>
                <div className="ctable-col w1-q2">
                  <TextInput formId={formId} classes="ctable-input" attribute="quantidadeComissao" initialValue={ModalHelper.getRawValue(modalName, formId, 'quantidadeComissao')}/>
                </div>
                <div className="ctable-col w1-q2">
                  <TextInput formId={formId} classes="ctable-input" attribute="totalComissao" mask={masks.numero} onFocus={handleTotalInputFocus} initialValue={ModalHelper.getRawValue(modalName, formId, 'totalComissao')}/>
                </div>
              </div>
              <div className="ctable-row center">
                <div className="ctable-col w1">Bônus</div>
                <div className="ctable-col w1-q2">
                  <TextInput formId={formId} classes="ctable-input" attribute="bonus" mask={masks.numero} initialValue={ModalHelper.getRawValue(modalName, formId, 'bonus')}/>
                </div>
                <div className="ctable-col w1-q2">
                  <TextInput formId={formId} classes="ctable-input" attribute="quantidadeBonus" initialValue={ModalHelper.getRawValue(modalName, formId, 'quantidadeBonus')}/>
                </div>
                <div className="ctable-col w1-q2">
                  <TextInput formId={formId} classes="ctable-input" attribute="totalBonus" mask={masks.numero} onFocus={handleTotalInputFocus} initialValue={ModalHelper.getRawValue(modalName, formId, 'totalBonus')}/>
                </div>
              </div>
              <div className="ctable-row center">
                <div className="ctable-col w1">PLR</div>
                <div className="ctable-col w1-q2">
                  <TextInput formId={formId} classes="ctable-input" attribute="plr" mask={masks.numero} initialValue={ModalHelper.getRawValue(modalName, formId, 'plr')}/>
                </div>
                <div className="ctable-col w1-q2">
                  <TextInput formId={formId} classes="ctable-input" attribute="quantidadePlr" initialValue={ModalHelper.getRawValue(modalName, formId, 'quantidadePlr')}/>
                </div>
                <div className="ctable-col w1-q2">
                  <TextInput formId={formId} classes="ctable-input" attribute="totalPlr" mask={masks.numero} onFocus={handleTotalInputFocus} initialValue={ModalHelper.getRawValue(modalName, formId, 'totalPlr')}/>
                </div>
              </div>

              <button type="submit" className="btn btn-six btn-bright-blue">Salvar</button>
            </div>
          </Form>
        </div>
      </div>
    </Modal>
  )
}