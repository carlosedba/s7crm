import React, { useState, useEffect } from 'react'
import { useSelector, useDispatch } from 'react-redux'

import Modal from '@/components/Modal'
import Form from '@/components/Form'
import TextDateInput from '@/components/Input/TextDateInput'
import SuggestionInput from '@/components/Input/SuggestionInput'
import SelectInput from '@/components/Input/SelectInput'

import IcClose from '@/assets/icons/ic_close.svg'

import EmpresaRepository from '@/repository/EmpresaRepository'
import BeneficioRepository from '@/repository/BeneficioRepository'
import FerramentaRepository from '@/repository/FerramentaRepository'
import AreaRepository from '@/repository/AreaRepository'
import CargoRepository from '@/repository/CargoRepository'

import ModalHelper from '@/ModalHelper'
import FormHelper from '@/FormHelper'

export default function ModalExpProfissionalCreate(props) {
  const dependsOn = props.dependsOn
  const process = props.process || false
  const formRaw = props.formRaw || {}
  const formComputed = props.formComputed || {}

  const [formId, setFormId] = useState(null)
  const [formProps, setFormProps] = useState(null)

  const [cargos, setCargos] = useState([])
  const [areas, setAreas] = useState([])
  const [beneficios, setBeneficios] = useState([])
  const [ferramentas, setFerramentas] = useState([])

  const modalName = 'ModalExpProfissionalCreate'

  useEffect(() => {
    fetchCargos()
    fetchAreas()
    fetchBeneficios()
    fetchFerramentas()
  }, [])

  function onAfterOpen() {
    let formId = FormHelper.new({
      props: { raw: formRaw, computed: formComputed }
    })

    setFormId(formId)

    setFormProps({
      id: formId,
      model: 'S7ExperienciaProfissional',
      action: 'create',
      dependsOn: dependsOn,
      process: process
    })
  }

  function onRequestClose() {
    FormHelper.remove(formId)
  }

  function fetchCargos() {
    CargoRepository.find()
      .then((data) => {
        const cargoOptions = data.map((cargo) => ({ value: cargo._id, label: cargo.nome }))

        setCargos(cargoOptions)
      })
  }

  function fetchAreas() {
    AreaRepository.find()
      .then((data) => {
        const areaOptions = data.map((area) => ({ value: area._id, label: area.nome }))

        setAreas(areaOptions)
      })
  }

  function fetchBeneficios() {
    BeneficioRepository.find()
      .then((data) => {
        const beneficioOptions = data.map((beneficio) => ({ value: beneficio._id, label: beneficio.nome }))

        setBeneficios(beneficioOptions)
      })
  }

  function fetchFerramentas() {
    FerramentaRepository.find()
      .then((data) => {
        const ferramentaOptions = data.map((ferramenta) => ({ value: ferramenta._id, label: ferramenta.nome }))

        setFerramentas(ferramentaOptions)
      })
  }

  function handleBtnCloseClick(event) {
    ModalHelper.close(modalName)
  }

  function handleSubmit(event) {
    ModalHelper.close(modalName)
  }

  return (
    <Modal name={modalName} onAfterOpen={onAfterOpen} onRequestClose={onRequestClose}>
      <div className="modal">
        <div className="modal-header">
          <div className="modal-header-left">
            <div className="modal-titles">
              <span className="modal-hat">Experiências profissionais</span>
              <span className="modal-title">Adicionar experiência</span>
            </div>
          </div>
          <div className="modal-header-right">
            <button className="btn btn-close" onClick={handleBtnCloseClick}>
              <div className="svg icon">
                <IcClose/>
              </div>
            </button>
          </div>
        </div>
        <div className="modal-content">
          <Form {...formProps} onSubmit={handleSubmit}>
            <div className="inputs input-style-2">
              <SuggestionInput
                formId={formId}
                classes="w3"
                label="Empresa"
                attribute="empresa"
                dataSource={{
                  repository: EmpresaRepository,
                  attributes: ['nomeFantasia']
                }}
                required={true}
              />
              <div className="multi-input">
                <SelectInput formId={formId} size="w2-q2" label="Cargo" attribute="cargo" required={true} options={cargos}/>
                <SelectInput formId={formId} size="w2-q2" label="Área" attribute="area" required={true} options={areas}/>
              </div>
              <div className="multi-input">
                <SelectInput formId={formId} size="w2-q2" label="Tipo de contratação" attribute="tipoContratacao" required={true}
                             options={[
                    { value: 'CLT', label: 'CLT' },
                    { value: 'PJ', label: 'PJ' },
                    { value: 'Estatutário', label: 'Estatutário' },
                    { value: 'Contrato', label: 'Contrato' },
                    { value: 'Intermitente', label: 'Intermitente' },
                    { value: 'Projeto', label: 'Projeto' },
                  ]}/>
                <SelectInput formId={formId} size="w2" label="Emprego atual" attribute="empregoAtual" required={true}
                             options={[
                    { value: 'Sim', label: 'Sim' },
                    { value: 'Não', label: 'Não' },
                  ]}/>
              </div>
              <div className="multi-input">
                <TextDateInput formId={formId} classes="w1-q3" label="Data de admissão" attribute="dataAdmissao" format="L"/>
                <TextDateInput formId={formId} classes="w1-q3" label="Data de desligamento" attribute="dataDesligamento" format="L"/>
              </div>
              <SelectInput formId={formId} size="w4-q3" label="Benefícios" attribute="beneficios" isMulti={true} options={beneficios}/>
              <SelectInput formId={formId} size="w4-q3" label="Ferramentas" attribute="ferramentas" isMulti={true} options={ferramentas}/>

              <button type="submit" className="btn btn-six btn-bright-blue">Salvar</button>
            </div>
          </Form>
        </div>
      </div>
    </Modal>
  )
}