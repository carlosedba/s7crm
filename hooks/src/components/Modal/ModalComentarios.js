import React, { useState, useEffect } from 'react'
import { useSelector, useDispatch } from 'react-redux'
import { Link } from 'react-router-dom'
import update from 'immutability-helper'

import Modal from '@/components/Modal'
import TextAreaInput from '@/components/Input/TextAreaInput'

import ModalHelper from '@/ModalHelper'

import IcClose from '@/assets/icons/ic_close.svg'

export default function ModalComentarios(props) {
  const parentFormId = props.parentFormId
  const attribute = props.attribute

  const modalName = 'ModalComentarios'

  function handleBtnCloseClick(event) {
    ModalHelper.close(modalName)
  }

  return (
    <Modal name={modalName}>
      <div className="modal">
        <div className="modal-header">
          <div className="modal-header-left">
            <div className="modal-titles">
              <span className="modal-hat">Comentários</span>
              <span className="modal-title">Editar comentários</span>
            </div>
          </div>
          <div className="modal-header-right">
            <button className="btn btn-close" onClick={handleBtnCloseClick}>
              <div className="svg icon">
                <IcClose/>
              </div>
            </button>
          </div>
        </div>
        <div className="modal-content">
          <div className="inputs input-style-2">
            <TextAreaInput formId={parentFormId} classes="w4-q3" attribute={attribute} required={true}/>
            <button type="submit" className="btn btn-six btn-bright-blue" onClick={handleBtnCloseClick}>Salvar</button>
          </div>
        </div>
      </div>
    </Modal>
  )
}