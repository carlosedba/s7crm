import React, { useState, useEffect } from 'react'
import update from 'immutability-helper'

import Modal from '@/components/Modal'
import SelectInput from '@/components/Input/SelectInput'
import SuggestionInput from '@/components/Input/SuggestionInput'

import IcClose from '@/assets/icons/ic_close.svg'

import UsuarioRepository from '@/repository/UsuarioRepository'
import EmpresaRepository from '@/repository/EmpresaRepository'
import ProfissionalRepository from '@/repository/ProfissionalRepository'
import CategoriaProjetoRepository from '@/repository/CategoriaProjetoRepository'

import ModalHelper from '@/ModalHelper'

export default function ModalFiltrosProjetosS7Brand(props) {
  const onSubmit = props.onSubmit

  const [modalData, setModalData] = useState({})

  const [filters, setFilters] = useState({})
  const [categoriasProjeto, setCategoriasProjeto] = useState([])

  const modalName = 'ModalFiltrosProjetosS7Brand'

  useEffect(() => {
    fetchCategoriasProjeto()
  }, [])

  function onAfterOpen() {
    setModalData(ModalHelper.getData(modalName))
  }

  function fetchCategoriasProjeto() {
    CategoriaProjetoRepository.find({ divisao: 'S7 Brand' })
      .then((data) => {
        const categoriasProjetoOptions = data.map((categoria) => ({ value: categoria._id, label: categoria.nome }))

        setCategoriasProjeto(categoriasProjetoOptions)
      })
  }

  function handleSuggestionInputChange(event) {
    setFilters(update(filters, {
      $merge: {
        [event.attribute]: event.value
      }
    }))
  }

  function handleSelectInputChange(event) {
    setFilters(update(filters, {
      $merge: {
        [event.attribute]: event.value
      }
    }))
  }

  function handleSubmit(event) {
    event.preventDefault()

    if (onSubmit) onSubmit(filters)

    ModalHelper.close(modalName)
  }

  function handleClearFilters(event) {
    event.preventDefault()

    setFilters({})

    if (onSubmit) onSubmit({})

    ModalHelper.close(modalName)
  }

  function handleBtnCloseClick(event) {
    ModalHelper.close(modalName)
  }

  return (
    <Modal name={modalName} onAfterOpen={onAfterOpen}>
      <div className="modal">
        <div className="modal-header">
          <div className="modal-header-left">
            <div className="modal-titles">
              <span className="modal-hat">Projetos S7 Up</span>
              <span className="modal-title">Filtros</span>
            </div>
          </div>
          <div className="modal-header-right">
            <button className="btn btn-close" onClick={handleBtnCloseClick}>
              <div className="svg icon">
                <IcClose/>
              </div>
            </button>
          </div>
        </div>
        <div className="modal-content">
          <div className="filtros">
            <form onSubmit={handleSubmit}>
              <div className="inputs input-style-2">
                <SuggestionInput
                  classes="w3"
                  label="Consultor responsável"
                  attribute="consultorResponsavel"
                  dataSource={{
                    repository: UsuarioRepository,
                    attributes: ['nome', 'sobrenome']
                  }}
                  initialValue={filters.consultorResponsavel}
                  onChange={handleSuggestionInputChange}
                />
                <div className="multi-input">
                  <SuggestionInput
                    classes="w3"
                    label="Cliente"
                    attribute="cliente"
                    dataSource={{
                      repository: EmpresaRepository,
                      attributes: ['nomeFantasia']
                    }}
                    initialValue={filters.cliente}
                    onChange={handleSuggestionInputChange}
                  />
                  <SuggestionInput
                    classes="w3"
                    label="Contato no cliente"
                    attribute="contatoCliente"
                    dataSource={{
                      repository: ProfissionalRepository,
                      attributes: ['nome', 'sobrenome']
                    }}
                    initialValue={filters.contatoCliente}
                    onChange={handleSuggestionInputChange}
                  />
                </div>
                <div className="multi-input">
                  <SelectInput
                    size="w1-q2"
                    label="Status"
                    attribute="status"
                    options={[
                      { value: 'Open', label: 'Open' },
                      { value: 'Ativo 1', label: 'Ativo 1' },
                      { value: 'Ativo 2', label: 'Ativo 2' },
                      { value: 'Ativo 3', label: 'Ativo 3' },
                      { value: 'Lost', label: 'Lost' },
                      { value: 'Finalizado', label: 'Finalizado' },
                      { value: 'Stand-by', label: 'Stand-by' },
                      { value: 'On hold', label: 'On hold' },
                    ]}
                    initialValue={filters.status}
                    isClearable={true}
                    onChange={handleSelectInputChange}
                  />
                  <SelectInput
                    size="w3"
                    label="Categoria"
                    attribute="categoria"
                    options={categoriasProjeto}
                    initialValue={filters.categoria}
                    isClearable={true}
                    onChange={handleSelectInputChange}
                  />
                </div>
              </div>

              <div className="form-actions">
                <button type="submit" className="btn btn-six btn-bright-blue">Aplicar</button>
                {(Object.keys(filters).length) ? (<button className="btn btn-six btn-bright-blue" onClick={handleClearFilters}>Limpar</button>) : null}
              </div>
            </form>
          </div>
        </div>
      </div>
    </Modal>
  )
}