import React, { useState, useEffect } from 'react'
import { useSelector, useDispatch } from 'react-redux'
import _ from 'lodash'

import Modal from '@/components/Modal'

import IcAlert from '@/assets/icons/ic_alert.svg'

import ModalHelper from '@/ModalHelper'

export default function ModalConfirmacaoDeletarArquivo(props) {
  const modalName = 'ModalConfirmacaoDeletarArquivo'

  function onAfterOpen() {}

  function onRequestClose() {}

  function handleCancelClick(event) {
    ModalHelper.close(modalName)
  }

  function handleDeleteClick(event) {
    let onDeleteHandler = ModalHelper.getHandler(modalName, 'onDelete')

    onDeleteHandler()

    ModalHelper.close(modalName)
  }
  
  return (
    <Modal name={modalName} onAfterOpen={onAfterOpen}>
      <div className="modal alert-modal">
        <div className="modal-header">
          <div className="svg alert-modal-icon">
            <IcAlert/>
          </div>
          <div className="modal-titles">
            <span className="modal-title">Tem certeza que deseja deletar o arquivo?</span>
          </div>
        </div>
        <div className="modal-content">
          <div className="alert-modal-buttons">
            <button className="btn btn-three" onClick={handleCancelClick}>Cancelar</button>
            <button className="btn btn-three btn-red" onClick={handleDeleteClick}>Deletar</button>
          </div>
        </div>
      </div>
    </Modal>
  )
}