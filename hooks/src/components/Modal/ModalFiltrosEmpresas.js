import React, { useState, useEffect } from 'react'
import update from 'immutability-helper'

import Modal from '@/components/Modal'
import SelectInput from '@/components/Input/SelectInput'

import IcClose from '@/assets/icons/ic_close.svg'

import SegmentoRepository from '@/repository/SegmentoRepository'
import SetorRepository from '@/repository/SetorRepository'
import PorteRepository from '@/repository/PorteRepository'
import CidadeRepository from '@/repository/CidadeRepository'
import EstadoRepository from '@/repository/EstadoRepository'

import ModalHelper from '@/ModalHelper'

export default function ModalFiltrosEmpresas(props) {
  const onSubmit = props.onSubmit

  const [modalData, setModalData] = useState({})

  const [filters, setFilters] = useState({})

  const [segmentos, setSegmentos] = useState([])
  const [setores, setSetores] = useState([])
  const [portes, setPortes] = useState([])
  const [estados, setEstados] = useState([])
  const [cidades, setCidades] = useState([])

  const modalName = 'ModalFiltrosEmpresas'

  useEffect(() => {
    fetchSegmentos()
    fetchPortes()
    fetchEstados()
  }, [])

  function onAfterOpen() {
    setModalData(ModalHelper.getData(modalName))
  }

  function fetchSegmentos() {
    SegmentoRepository.find()
      .then((data) => {
        const segmentoOptions = data.map((segmento) => ({ value: segmento._id, label: segmento.nome }))

        setSegmentos(segmentoOptions)
      })
  }

  function fetchPortes() {
    PorteRepository.find()
      .then((data) => {
        const porteOptions = data.map((porte) => ({ value: porte._id, label: porte.nome }))

        setPortes(porteOptions)
      })
  }

  function fetchEstados() {
    EstadoRepository.find()
      .then((data) => {
        const estadoOptions = data.map((estado) => ({ value: estado._id, label: estado.nome }))

        setEstados(estadoOptions)
      })
  }

  function handleSegmentoChange(event) {
    handleSelectInputChange(event)

    SetorRepository.find({ segmento: event.value })
      .then((data) => {
        const setorOptions = data.map((setor) => ({ value: setor._id, label: setor.nome }))

        setSetores(setorOptions)
      })
  }

  function handleEstadoChange(event) {
    setFilters(update(filters, {
      $merge: {
        [event.attribute]: event.label
      }
    }))

    CidadeRepository.find({ estado: event.value })
      .then((data) => {
        const cidadeOptions = data.map((cidade) => ({ value: cidade._id, label: cidade.nome }))

        setCidades(cidadeOptions)
      })
  }

  function handleCidadeChange(event) {
    setFilters(update(filters, {
      $merge: {
        [event.attribute]: event.label
      }
    }))
  }

  function handleSuggestionInputChange(event) {
    setFilters(update(filters, {
      $merge: {
        [event.attribute]: event.value
      }
    }))
  }

  function handleSelectInputChange(event) {
    setFilters(update(filters, {
      $merge: {
        [event.attribute]: event.value
      }
    }))
  }

  function handleSubmit(event) {
    event.preventDefault()

    if (onSubmit) onSubmit(filters)

    ModalHelper.close(modalName)
  }

  function handleClearFilters(event) {
    event.preventDefault()

    setFilters({})

    if (onSubmit) onSubmit({})

    ModalHelper.close(modalName)
  }

  function handleBtnCloseClick(event) {
    ModalHelper.close(modalName)
  }

  return (
    <Modal name={modalName} onAfterOpen={onAfterOpen}>
      <div className="modal">
        <div className="modal-header">
          <div className="modal-header-left">
            <div className="modal-titles">
              <span className="modal-hat">Empresas</span>
              <span className="modal-title">Filtros</span>
            </div>
          </div>
          <div className="modal-header-right">
            <button className="btn btn-close" onClick={handleBtnCloseClick}>
              <div className="svg icon">
                <IcClose/>
              </div>
            </button>
          </div>
        </div>
        <div className="modal-content">
          <div className="filtros">
            <form onSubmit={handleSubmit}>
              <div className="inputs input-style-2">
                <div className="multi-input">
                  <SelectInput
                    size="w2-q2"
                    label="Segmento"
                    attribute="segmento"
                    options={segmentos}
                    isClearable={true}
                    onChange={handleSegmentoChange}
                    initialValue={filters.segmento}
                  />
                  <SelectInput
                    size="w3"
                    label="Setor"
                    attribute="setor"
                    options={setores}
                    isClearable={true}
                    onChange={handleSelectInputChange}
                    initialValue={filters.setor}
                  />
                  <SelectInput
                    size="w1-q2"
                    label="Porte"
                    attribute="porte"
                    options={portes}
                    isClearable={true}
                    onChange={handleSelectInputChange}
                    initialValue={filters.porte}
                  />
                </div>
              </div>

              <div className="form-actions">
                <button type="submit" className="btn btn-six btn-bright-blue">Aplicar</button>
                {(Object.keys(filters).length) ? (<button className="btn btn-six btn-bright-blue" onClick={handleClearFilters}>Limpar</button>) : null}
              </div>
            </form>
          </div>
        </div>
      </div>
    </Modal>
  )
}