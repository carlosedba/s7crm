export default {
  "definitions": {},
  "$schema": "http://json-schema.org/draft-07/schema#",
  "$id": "http://example.com/root.json",
  "type": "object",
  "title": "ProjetoS7Up",
  "required": [
    "consultorResponsavel",
    "cliente",
    "contatoCliente",
    "status",
    "categoria",
    "dataAbertura",
    "honorario",
  ],
  "properties": {
    "consultorResponsavel": {
      "$id": "#/properties/consultorResponsavel",
      "type": "string",
      "minLength": 1
    },
    "cliente": {
      "$id": "#/properties/cliente",
      "type": "string",
      "minLength": 1
    },
    "contatoCliente": {
      "$id": "#/properties/contatoCliente",
      "type": "string",
      "minLength": 1
    },
    "status": {
      "$id": "#/properties/status",
      "type": "object"
    },
    "categoria": {
      "$id": "#/properties/categoria",
      "type": "object"
    },
    "dataAbertura": {
      "$id": "#/properties/dataAbertura",
      "type": "object"
    },
    "dataFechamento": {
      "$id": "#/properties/dataFechamento",
      "type": "object"
    },
    "honorario": {
      "$id": "#/properties/honorario",
      "type": "object"
    },
    "objetivo": {
      "$id": "#/properties/honorario",
      "type": "string",
      "minLength": 1
    }
  }
}