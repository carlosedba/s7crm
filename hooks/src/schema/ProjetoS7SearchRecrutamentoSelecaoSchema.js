
export default {
  "definitions": {},
  "$schema": "http://json-schema.org/draft-07/schema#",
  "$id": "http://example.com/root.json",
  "type": "object",
  "title": "ProjetoS7SearchRecrutamentoSelecao",
  "required": [
    "consultorResponsavel",
    "cliente",
    "contatoCliente",
    "status",
    "tipo",
    "dataAbertura",
    "cargo",
    "area",
    "honorario",
    "salarioProposto",
    "equipeLiderada",
    "nivelEquipe",
    "missaoCargo",
    "responsabilidadesCargo",
  ],
  "properties": {
    "consultorResponsavel": {
      "$id": "#/properties/consultorResponsavel",
      "type": "string",
      "minLength": 1
    },
    "cliente": {
      "$id": "#/properties/cliente",
      "type": "string",
      "minLength": 1
    },
    "contatoCliente": {
      "$id": "#/properties/contatoCliente",
      "type": "string",
      "minLength": 1
    },
    "status": {
      "$id": "#/properties/status",
      "type": "object"
    },
    "tipo": {
      "$id": "#/properties/tipo",
      "type": "object"
    },
    "dataAbertura": {
      "$id": "#/properties/dataAbertura",
      "type": "object"
    },
    "dataFechamento": {
      "$id": "#/properties/dataFechamento",
      "type": "object"
    },
    "cargo": {
      "$id": "#/properties/cargo",
      "type": "object"
    },
    "area": {
      "$id": "#/properties/area",
      "type": "object"
    },
    "honorario": {
      "$id": "#/properties/honorario",
      "type": "object"
    },
    "salarioProposto": {
      "$id": "#/properties/salarioProposto",
      "type": "object"
    },
    "salarioFinal": {
      "$id": "#/properties/salarioFinal",
      "type": "object"
    },
    "equipeLiderada": {
      "$id": "#/properties/equipeLiderada",
      "type": "string",
      "minLength": 1
    },
    "nivelEquipe": {
      "$id": "#/properties/nivelEquipe",
      "type": "array",
      "items": {
        "$id": "#/properties/conselheiros/items",
        "type": "object"
      }
    },
    "missaoCargo": {
      "$id": "#/properties/missaoCargo",
      "type": "string",
      "minLength": 1
    },
    "responsabilidadesCargo": {
      "$id": "#/properties/responsabilidadesCargo",
      "type": "string",
      "minLength": 1
    }
  }
}