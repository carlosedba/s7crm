export default {
  "definitions": {},
  "$schema": "http://json-schema.org/draft-07/schema#",
  "$id": "http://example.com/root.json",
  "type": "object",
  "title": "S7Profissional",
  "required": [
    "instituicao",
    "curso",
    "tipo",
    "anoConclusao",
  ],
  "properties": {
    "profissional": {
      "$id": "#/properties/profissional",
      "type": "string",
      "minLength": 1
    },
    "instituicao": {
      "$id": "#/properties/instituicao",
      "type": "string",
      "minLength": 1
    },
    "curso": {
      "$id": "#/properties/curso",
      "type": "string",
      "minLength": 1
    },
    "tipo": {
      "$id": "#/properties/tipo",
      "type": "object"
    },
    "anoConclusao": {
      "$id": "#/properties/anoConclusao",
      "type": "string",
      "minLength": 1
    }
  }
}