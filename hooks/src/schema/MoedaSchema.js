export default {
  "definitions": {},
  "$schema": "http://json-schema.org/draft-07/schema#",
  "$id": "http://example.com/root.json",
  "type": "object",
  "title": "S7Moeda",
  "required": [
    "nome",
    "simbolo"
  ],
  "properties": {
    "nome": {
      "$id": "#/properties/nome",
      "type": "string",
      "minLength": 1
    },
    "simbolo": {
      "$id": "#/properties/simbolo",
      "type": "string",
      "minLength": 1
    }
  }
}