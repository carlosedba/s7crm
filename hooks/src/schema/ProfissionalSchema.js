export default {
  "definitions": {},
  "$schema": "http://json-schema.org/draft-07/schema#",
  "$id": "http://example.com/root.json",
  "type": "object",
  "title": "S7Profissional",
  "required": [
    "nome",
    "sobrenome",
    "sexo",
    "deficiente",
    "rg",
    "cpf",
    "estadoCivil",
    "email1",
    "naturalidade",
    "estadoOrigem",
    "cidadeOrigem",
    "paisAtual",
    "estadoAtual",
    "cidadeAtual",
  ],
  "properties": {
    "nome": {
      "$id": "#/properties/nome",
      "type": "string",
      "minLength": 1
    },
    "sobrenome": {
      "$id": "#/properties/sobrenome",
      "type": "string",
      "minLength": 1
    },
    "sexo": {
      "$id": "#/properties/sexo",
      "type": "object"
    },
    "deficiente": {
      "$id": "#/properties/deficiente",
      "type": "object"
    },
    "rg": {
      "$id": "#/properties/rg",
      "type": "string"
    },
    "cpf": {
      "$id": "#/properties/cpf",
      "type": "string"
    },
    "telefone": {
      "$id": "#/properties/telefone",
      "type": "string"
    },
    "celular1": {
      "$id": "#/properties/celular1",
      "type": "string",
      "minLength": 1
    },
    "celular2": {
      "$id": "#/properties/celular2",
      "type": "string"
    },
    "idade": {
      "$id": "#/properties/idade",
      "type": "string"
    },
    "dataNascimento": {
      "$id": "#/properties/dataNascimento",
      "type": "object"
    },
    "estadoCivil": {
      "$id": "#/properties/estadoCivil",
      "type": "object"
    },
    "empregoConjuge": {
      "$id": "#/properties/empregoConjuge",
      "type": "string"
    },
    "email1": {
      "$id": "#/properties/email1",
      "type": "string",
      "minLength": 1
    },
    "email2": {
      "$id": "#/properties/email2",
      "type": "string"
    },
    "emailComercial": {
      "$id": "#/properties/emailComercial",
      "type": "string"
    },
    "skype": {
      "$id": "#/properties/skype",
      "type": "string"
    },
    "linkedin": {
      "$id": "#/properties/linkedin",
      "type": "string"
    },
    "naturalidade": {
      "$id": "#/properties/naturalidade",
      "type": "object"
    },
    "estadoOrigem": {
      "$id": "#/properties/estadoOrigem",
      "type": "object"
    },
    "cidadeOrigem": {
      "$id": "#/properties/cidadeOrigem",
      "type": "object"
    },
    "paisAtual": {
      "$id": "#/properties/paisAtual",
      "type": "object"
    },
    "estadoAtual": {
      "$id": "#/properties/estadoAtual",
      "type": "object"
    },
    "cidadeAtual": {
      "$id": "#/properties/cidadeAtual",
      "type": "object"
    },
    "abordagem": {
      "$id": "#/properties/abordagem",
      "type": "string"
    },
    "comentarios": {
      "$id": "#/properties/comentarios",
      "type": "string"
    },
    "filhos": {
      "$id": "#/properties/filhos",
      "type": "array",
      "items": {
        "$id": "#/properties/filhos/items",
        "type": "string"
      }
    },
    "idiomas": {
      "$id": "#/properties/idiomas",
      "type": "array",
      "items": {
        "$id": "#/properties/idiomas/items",
        "type": "object"
      }
    }
  }
}