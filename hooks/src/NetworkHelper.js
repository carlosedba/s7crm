import to from 'await-to-js'

import { newRequest, updateRequest, deleteRequest, logResponse } from '@/actions/Network'

import IdGenerator from '@/utils/IdGenerator'

import store from '@/store'
import api from '@/api'

function request(payloads) {
  const dispatch = store.dispatch

  return new Promise(async (resolve, reject) => {
    try {
      const response = await api.post('/endpoint', {}, {
        data: payloads
      })
      const data = response.data

      if (data && Array.isArray(data)) {
        for (let i = 0; i < data.length; i++) {
          const response = data[i]

          if (response.status) {
            if (response.status === 'success') {
              dispatch(updateRequest(response.id, {
                status: 'success',
                data: null
              }))

              if (response.data) dispatch(logResponse(response))
            }

            if (response.status === 'error') {
              dispatch(updateRequest(response.id, {
                status: 'error'
              }))

              dispatch(logResponse(response))
            }
          }
        }
      }

      resolve()

    } catch (err) {
      console.error(err)
      reject(err)
    }
  })
}

export default {
  requestExists(id) {
    let state = store.getState()
    let requests = state.Network.requests

    for (let i = 0; i < requests.length; i++) {
      const request = requests[i]

      if (request.id === id) return true
    }

    return false
  },

  newRequest(payload) {
    const dispatch = store.dispatch

    if (!payload.id) {
      payload.id = IdGenerator.default()
    }

    dispatch(newRequest(payload))

    if (payload.params && payload.params.process) {
      return request([payload])
    }
  },

  updateRequest(id, payload) {
    const dispatch = store.dispatch

    dispatch(updateRequest(id, payload))
  },

  deleteRequest(id) {
    const dispatch = store.dispatch

    dispatch(deleteRequest(id))
  },

  processAllRequests() {
    const dispatch = store.dispatch
    let state = store.getState()
    let requests = state.Network.requests
    let idleRequests = []

    for (let i = 0; i < requests.length; i++) {
      let request = requests[i]

      console.log('request', request)

      if (request.status === 'idle') {
        idleRequests.push(request)

        dispatch(updateRequest(request.id, { status: 'sent' }))
      }
    }

    return request(idleRequests)
  }
}