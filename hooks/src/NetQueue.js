import {
  addItemToNetQueue,
  updateItemFromNetQueue,
  processNetQueue,
  netQueueProcessingFinished,
  clearNetQueue,
  logNetQueueError
} from '@/actions/NetQueue'

import store from '@/store'
import api from '@/api'

export default {
  exists(id) {
    let state = store.getState()

    for (const item of state.NetQueue.items) {
      if (item.id === id) return true
    }

    return false
  },

  getById(id) {
    let state = store.getState()

    for (const item of state.NetQueue.items) {
      if (item.id === id) return item
    }

    return null
  },

  add(netQueuePayload) {
    return store.dispatch(addItemToNetQueue(netQueuePayload))
  },

  update(id, netQueuePayload) {
    return store.dispatch(updateItemFromNetQueue(id, netQueuePayload))
  },

  process() {
    let state = store.getState()
    let items = state.NetQueue.items

    store.dispatch(processNetQueue())

    return new Promise((resolve, reject) => {
      api.post('/endpoint', {}, {
        data: items
      })
        .then((response) => {
          const data = response.data

          if (data && data.totalErrors) {
            store.dispatch(logNetQueueError(data.errors))
            reject(data)
          }

          store.dispatch(netQueueProcessingFinished())
          resolve(items)
        })
        .catch((err) => {
          store.dispatch(netQueueProcessingFinished())
          reject(err)
        })
    })
  },

  clear() {
    return store.dispatch(clearNetQueue())
  }
}
  