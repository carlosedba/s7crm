import * as types from '@/actionTypes'

export function addItemToNetQueue(item) { 
  return {
    type: types.ADD_ITEM_TO_NETQUEUE,
    payload: {
      item: item,
    }
  }
}

export function updateItemFromNetQueue(id, item) { 
  return {
    type: types.UPDATE_ITEM_FROM_NETQUEUE,
    payload: {
      id: id,
      item: item,
    }
  }
}

export function removeItemFromNetQueue(index) { 
  return {
    type: types.REMOVE_ITEM_FROM_NETQUEUE,
    payload: {
      index: index,
    }
  }
}

export function processNetQueue() {
  return {
    type: types.PROCESS_NETQUEUE,
  }
}

export function netQueueProcessingFinished() {
  return {
    type: types.NETQUEUE_PROCESSING_FINISHED,
  }
}

export function clearNetQueue() {
  return {
    type: types.CLEAR_NETQUEUE,
  }
}

export function logNetQueueError(errors = []) {
  return {
    type: types.LOG_NETQUEUE_ERROR,
    payload: {
      errors: errors
    }
  }
}
