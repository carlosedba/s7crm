import * as types from '@/actionTypes'

export function newRequest(payload) {
  return {
    type: types.NEW_REQUEST,
    payload: payload
  }
}

export function updateRequest(id, payload) {
  return {
    type: types.UPDATE_REQUEST,
    id: id,
    payload: payload,
  }
}

export function deleteRequest(id) {
  return {
    type: types.DELETE_REQUEST,
    id: id,
  }
}

export function clearAllRequests() {
  return {
    type: types.CLEAR_ALL_REQUESTS,
  }
}

export function logResponse(payload) {
  return {
    type: types.LOG_RESPONSE,
    payload: payload
  }
}

export function clearAllResponses() {
  return {
    type: types.CLEAR_ALL_RESPONSES,
  }
}