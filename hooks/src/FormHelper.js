import * as Form from '@/actions/Form'
import { newForm, updateForm, updateFormRawAttribute, updateFormComputedAttribute, removeForm } from '@/actions/Form'

import IdGenerator from '@/utils/IdGenerator'

import store from '@/store'
import NetQueue from '@/NetQueue'
import NetQueuePayload from '@/NetQueuePayload'
import NetworkHelper from '@/NetworkHelper'

export default {
  setObjectId(formId, objectId) {
    store.dispatch(Form.updateForm(formId, {
      $merge: {
        computed: {
          _id: objectId
        }
      }
    }))
  },

  getById(formId) {
    let state = store.getState()
    let Forms = state.Forms

    if (Forms[formId]) return Forms[formId]

    return null
  },

  getRawValue(formId, attribute) {
    let state = store.getState()
    let Forms = state.Forms
    let form = Forms[formId]

    if (form) {
      return form.raw[attribute] || ''
    }

    return ''
  },

  getComputedValue(formId, attribute) {
    let state = store.getState()
    let Forms = state.Forms
    let form = Forms[formId]

    if (form) {
      return form.computed[attribute] || ''
    }

    return ''
  },

  setRawAttribute(formId, attribute, value) {
    store.dispatch(updateFormRawAttribute(formId, attribute, value))
  },

  setComputedAttribute(formId, attribute, value) {
    store.dispatch(updateFormComputedAttribute(formId, attribute, value))
  },

  new({ formId, props } = {}) {
    //console.log('new form - formhelper', formId, props)

    if (!formId) formId = IdGenerator.default()

    store.dispatch(newForm(formId, props))

    return formId
  },

  update(formId, props) {
    return store.dispatch(updateForm(formId, props))
  },

  load(formId) {
    const payload = NetQueue.getById(formId)

    if (payload) {
      store.dispatch(Form.newForm(formId, {
        raw: payload.object.raw,
        computed: payload.object.computed
      }))
    }
  },

  remove(formId) {
    return store.dispatch(removeForm(formId))
  },

  clear(formId, formModel) {
    const model = forms[formModel]

    store.dispatch(Form.updateForm(formId, {
      $set: {
        raw: {},
        computed: {}
      }
    }))
  },

  async submit(props, processAllRequests) {
    //console.log('props', props)

    const id = props.id

    const params = {
      model: props.model,
      action: props.action,
      dependsOn: props.dependsOn || null,
      process: props.process
    }

    let state = store.getState()
    let forms = state.Forms
    let form = forms[id]

    let data = {
      formData: form.raw,
      formMeta: form.computed,
    }

    const payload = new NetQueuePayload(
      id,
      params,
      data
    )

    //console.log('requestExists', id, NetworkHelper.requestExists(id))

    if (!NetworkHelper.requestExists(id)) {
      NetworkHelper.newRequest(payload)
    } else {
      NetworkHelper.updateRequest(id, payload)
    }

    if (processAllRequests) {
      return NetworkHelper.processAllRequests()
    }
  }
}

