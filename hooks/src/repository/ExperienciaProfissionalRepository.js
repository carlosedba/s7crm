import api from '@/api'

export default {
  model: 'S7ExperienciaProfissional',

  find(queryParams = {}) {
    return new Promise((resolve, reject) => {
      api.get('/experiencias-profissionais', queryParams)
        .then((response) => {
          const data = response.data
          if (data) resolve(data)
          else reject(response)
        })
        .catch(reject)
    })
  },

  findById(id) {
    return new Promise((resolve, reject) => {
      api.get(`/experiencias-profissionais/${id}`)
        .then((response) => {
          const data = response.data
          if (data) resolve(data)
          else reject(response)
        })
        .catch(reject)
    })
  },
}