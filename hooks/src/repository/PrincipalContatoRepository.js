import api from '@/api'

export default {
  model: 'S7PrincipalContato',

  find(queryParams = {}) {
    return new Promise((resolve, reject) => {
      api.get('/principais-contatos', queryParams)
        .then((response) => {
          const data = response.data
          if (data) resolve(data)
          else reject(response)
        })
        .catch(reject)
    })
  },

  findById(id) {
    return new Promise((resolve, reject) => {
      api.get(`/principais-contatos/${id}`)
        .then((response) => {
          const data = response.data
          if (data) resolve(data)
          else reject(response)
        })
        .catch(reject)
    })
  },
}