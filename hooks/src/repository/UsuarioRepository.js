import api from '@/api'

export default {
  model: 'S7Usuario',

  suggestions(str, attributes) {
    return new Promise((resolve, reject) => {
      api.post('/usuarios/suggestions', {}, {
        str, attributes
      })
        .then((response) => {
          const data = response.data

          if (data) resolve(data)
          else reject(response)
        })
        .catch(reject)
    })
  },

  findById(id) {
    return new Promise((resolve, reject) => {
      api.get(`/usuarios/${id}`)
        .then((response) => {
          const data = response.data
          if (data) resolve(data)
          else reject(response)
        })
        .catch(reject)
    })
  },
}