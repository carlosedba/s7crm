import api from '@/api'

export default {
  model: 'S7Segmento',

  find(queryParams = {}) {
    return new Promise((resolve, reject) => {
      api.get('/segmentos', queryParams)
        .then((response) => {
          const data = response.data
          if (data) resolve(data)
          else reject(response)
        })
        .catch(reject)
    })
  },

  findById(id) {
    return new Promise((resolve, reject) => {
      api.get(`/segmentos/${id}`)
        .then((response) => {
          const data = response.data
          if (data) resolve(data)
          else reject(response)
        })
        .catch(reject)
    })
  },
}