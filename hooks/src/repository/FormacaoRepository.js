import api from '@/api'

export default {
  model: 'S7Formacao',

  find(queryParams = {}) {
    return new Promise((resolve, reject) => {
      api.get('/formacoes', queryParams)
        .then((response) => {
          const data = response.data
          if (data) resolve(data)
          else reject(response)
        })
        .catch(reject)
    })
  },

  findById(id) {
    return new Promise((resolve, reject) => {
      api.get(`/formacoes/${id}`)
        .then((response) => {
          const data = response.data
          if (data) resolve(data)
          else reject(response)
        })
        .catch(reject)
    })
  },

  remove(id) {
    return new Promise((resolve, reject) => {
      api.remove(`/formacoes/${id}`)
        .then((response) => {
          resolve(response)
        })
        .catch(reject)
    })
  },
}