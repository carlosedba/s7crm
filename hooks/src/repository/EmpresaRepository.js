import api from '@/api'

export default {
  model: 'S7Empresa',

  suggestions(str, attributes) {
    return new Promise((resolve, reject) => {
      api.post('/empresas/suggestions', {}, {
        str, attributes
      })
        .then((response) => {
          const data = response.data

          if (data) resolve(data)
          else reject(response)
        })
        .catch(reject)
    })
  },

  find(queryParams = {}) {
    return new Promise((resolve, reject) => {
      api.get('/empresas', queryParams)
        .then((response) => {
          const data = response.data
          if (data) resolve(data)
          else reject(response)
        })
        .catch(reject)
    })
  },

  findById(id) {
    return new Promise((resolve, reject) => {
      api.get(`/empresas/${id}`)
        .then((response) => {
          const data = response.data
          if (data) resolve(data)
          else reject(response)
        })
        .catch(reject)
    })
  },
}