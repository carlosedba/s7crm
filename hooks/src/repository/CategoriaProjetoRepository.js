import api from '@/api'

export default {
  model: 'S7CategoriaProjeto',

  find(queryParams = {}) {
    return new Promise((resolve, reject) => {
      api.get('/categorias-projeto', queryParams)
        .then((response) => {
          const data = response.data
          if (data) resolve(data)
          else reject(response)
        })
        .catch(reject)
    })
  },

  findById(id) {
    return new Promise((resolve, reject) => {
      api.get(`/categorias-projeto/${id}`)
        .then((response) => {
          const data = response.data
          if (data) resolve(data)
          else reject(response)
        })
        .catch(reject)
    })
  },
}