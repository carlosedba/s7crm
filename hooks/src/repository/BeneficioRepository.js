import api from '@/api'

export default {
  model: 'S7Beneficio',

  find(queryParams = {}) {
    return new Promise((resolve, reject) => {
      api.get('/beneficios', queryParams)
        .then((response) => {
          const data = response.data
          if (data) resolve(data)
          else reject(response)
        })
        .catch(reject)
    })
  },

  findById(id) {
    return new Promise((resolve, reject) => {
      api.get(`/beneficios/${id}`)
        .then((response) => {
          const data = response.data
          if (data) resolve(data)
          else reject(response)
        })
        .catch(reject)
    })
  },
}