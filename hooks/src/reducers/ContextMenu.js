import update from 'immutability-helper'

import {
  OPEN_CONTEXT_MENU, CLOSE_CONTEXT_MENU,
} from '@/actionTypes'

const INITIAL_STATE = {
  open: false,
  focusedElement: '',
}

export default function(state = INITIAL_STATE, action) {
  switch (action.type) {
    case OPEN_CONTEXT_MENU:
      return {
        ...state,
        open: true,
        focusedElement: action.payload.focusedElement
      }

    case CLOSE_CONTEXT_MENU:
      return {
        ...state,
        open: false,
        focusedElement: ''
      }

    default:
      return state
  }
}