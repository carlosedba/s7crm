import {
	ADD_ITEM_TO_NETQUEUE, UPDATE_ITEM_FROM_NETQUEUE, REMOVE_ITEM_FROM_NETQUEUE, CLEAR_NETQUEUE, PROCESS_NETQUEUE,  NETQUEUE_PROCESSING_FINISHED, LOG_NETQUEUE_ERROR
} from '@/actionTypes'

import update from 'immutability-helper'

const INITIAL_STATE = {
	items: [],
	processing: false,
	errors: []
}

function getItemIndexById(state, id) {
	for (let i = 0; i < state.items.length; i++) {
		if (state.items[i].id === id) return i 
	}
}

export default function(state = INITIAL_STATE, action) {
  let item, index, response, error

  switch (action.type) {
		case ADD_ITEM_TO_NETQUEUE:
			return update(state, {
				items: { $push: [action.payload.item] }
			})

		case UPDATE_ITEM_FROM_NETQUEUE:
			index = getItemIndexById(state, action.payload.id)

			return update(state, {
				items: {
					[index]: { $merge: action.payload.item }
				}
			})

		case REMOVE_ITEM_FROM_NETQUEUE:
			index = getItemIndexById(state, action.payload.id)

			return update(state, {
				items: { $splice: [[index, 1]] }
			})

		case PROCESS_NETQUEUE:
			return { ...state, processing: true }

		case NETQUEUE_PROCESSING_FINISHED:
			return { ...state, processing: false }

		case CLEAR_NETQUEUE:
			return update(state, {
				items: { $set: [] }
			})

		case LOG_NETQUEUE_ERROR:
			return update(state, {
				errors: { $push: action.payload.errors }
			})

		case `${PROCESS_NETQUEUE}_PENDING`:
			return { ...state, status: { pending: true, response: null, error: null } }

		case `${PROCESS_NETQUEUE}_FULFILLED`:
			response = action.payload.data

			return { ...state, status: { pending: false, response: response, error: null } }

		case `${PROCESS_NETQUEUE}_REJECTED`:
			error = action.payload.data || { message: action.payload.message }

			return { ...state, status: { pending: false, response: null, error: error } }

		default:
			return state
  }
}

