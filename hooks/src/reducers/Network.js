import update from 'immutability-helper'

import * as types from '@/actionTypes'

const INITIAL_STATE = {
	requests: [],
	responses: []
}

function getIndexById(arr, id) {
	for (let i = 0; i < arr.length; i++) {
		if (arr[i].id === id) return i
	}
}

export default function(state = INITIAL_STATE, action) {
  let index

  switch (action.type) {
		case types.NEW_REQUEST:
			return update(state, {
				requests: { $push: [action.payload] }
			})

		case types.UPDATE_REQUEST:
			index = getIndexById(state.requests, action.id)

			return update(state, {
				requests: {
					[index]: { $merge: action.payload }
				}
			})

		case types.DELETE_REQUEST:
			index = getIndexById(state.requests, action.id)

			return update(state, {
				requests: { $splice: [[index, 1]] }
			})

		case types.CLEAR_ALL_REQUESTS:
			return update(state, {
				requests: { $set: [] }
			})

		case types.LOG_RESPONSE:
			return update(state, {
				responses: { $push: [action.payload] }
			})

		case types.CLEAR_ALL_RESPONSES:
			return update(state, {
				responses: { $set: [] }
			})

		default:
			return state
  }
}

