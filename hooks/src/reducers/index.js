import { combineReducers } from 'redux'

import Auth from './Auth'
import ContextMenu from './ContextMenu'
import Users from './Users'
import Input from './Input'
import Modal from './Modal'
import Forms from './Forms'
import NetQueue from './NetQueue'
import Network from './Network'

const reducer = combineReducers({
  Auth,
  ContextMenu,
  Users,
  Input,
  Modal,
  Forms,
  NetQueue,
  Network,
})

export default reducer

