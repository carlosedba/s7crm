const Promise = require('bluebird')
const fs = require('fs')
const path = require('path')
const express = require('express')
const multer = require('multer')
const bcrypt = require('bcrypt')

const S7Areas = require('../api/S7Areas')
const S7Beneficios = require('../api/S7Beneficios')
const S7Cargos = require('../api/S7Cargos')
const S7CategoriasProjeto = require('../api/S7CategoriasProjeto')
const S7Cidades = require('../api/S7Cidades')
const S7Conselheiros = require('../api/S7Conselheiros')
const S7ConselhoAdministracao = require('../api/S7ConselhoAdministracao')
const S7Cursos = require('../api/S7Cursos')
const S7Empresas = require('../api/S7Empresas')
const S7EmpresasAlvo = require('../api/S7EmpresasAlvo')
const S7Estados = require('../api/S7Estados')
const S7ExperienciasProfissionais = require('../api/S7ExperienciasProfissionais')
const S7Ferramentas = require('../api/S7Ferramentas')
const S7Formacoes = require('../api/S7Formacoes')
const S7Formatos = require('../api/S7Formatos')
const S7Gestao = require('../api/S7Gestao')
const S7Idiomas = require('../api/S7Idiomas')
const S7Instituicoes = require('../api/S7Instituicoes')
const S7Moedas = require('../api/S7Moedas')
const S7Paises = require('../api/S7Paises')
const S7Portes = require('../api/S7Portes')
const S7PrincipaisContatos = require('../api/S7PrincipaisContatos')
const S7Profissionais = require('../api/S7Profissionais')
const S7ProfissionaisProjeto = require('../api/S7ProfissionaisProjeto')
const S7Projetos = require('../api/S7Projetos')
const S7Segmentos = require('../api/S7Segmentos')
const S7Setores = require('../api/S7Setores')
const S7Status = require('../api/S7Status')
const S7Usuarios = require('../api/S7Usuarios')
const Users = require('../api/Users')

const User = Promise.promisifyAll(require('../model/User'))
const S7Usuario = Promise.promisifyAll(require('../model/S7Usuario'))

const MainEndpoint = require('../MainEndpoint')
const FileSystemEndpoint = require('../FileSystemEndpoint')

const globals = require('../globals')

module.exports = (jwt) => {
  const router = express.Router()

  const parsers = [
    express.urlencoded({ extended: true }),
    express.json()
  ]

  const storage = multer.diskStorage({
    destination: function (req, file, cb) {
      const safePath = req.body.path

      if (safePath) {
        const fullPath = path.resolve(globals.DATA_DIR, safePath)

        fs.access(fullPath, (err) => {
          if (err) {
            fs.mkdir(fullPath, (err) => {
              if (!err) cb(null, path.resolve(globals.DATA_DIR, fullPath))
              else cb(null, null)
            })
          } else {
            cb(null, path.resolve(globals.DATA_DIR, fullPath))
          }
        })
      }
    },

    filename: function (req, file, cb) {
      cb(null, file.originalname)
    }
  })

  const upload = multer({ storage: storage })

  router.get('/s7/paises/:id', S7Paises.findOneById)
  router.get('/s7/paises', S7Paises.findAll)

  router.get('/s7/estados/:id', S7Estados.findOneById)
  router.get('/s7/estados', S7Estados.findAll)

  router.get('/s7/cidades/:id', S7Cidades.findOneById)
  router.get('/s7/cidades', S7Cidades.findAll)

  router.get('/s7/idiomas/:id', S7Idiomas.findOneById)
  router.get('/s7/idiomas', S7Idiomas.findAll)

  router.get('/s7/areas/:id', S7Areas.findOneById)
  router.get('/s7/areas', S7Areas.findAll)

  router.get('/s7/beneficios/:id', S7Beneficios.findOneById)
  router.get('/s7/beneficios', S7Beneficios.findAll)

  router.get('/s7/cargos/:id', S7Cargos.findOneById)
  router.get('/s7/cargos', S7Cargos.findAll)

  router.get('/s7/categorias-projeto/:id', S7CategoriasProjeto.findOneById)
  router.get('/s7/categorias-projeto', S7CategoriasProjeto.findAll)

  router.get('/s7/cursos/:id', S7Cursos.findOneById)
  router.post('/s7/cursos/suggestions', ...parsers, S7Cursos.suggestions)
  router.get('/s7/cursos', S7Cursos.findAll)

  router.get('/s7/ferramentas/:id', S7Ferramentas.findOneById)
  router.get('/s7/ferramentas', S7Ferramentas.findAll)

  router.get('/s7/instituicoes/:id', S7Instituicoes.findOneById)
  router.post('/s7/instituicoes/suggestions', ...parsers, S7Instituicoes.suggestions)
  router.get('/s7/instituicoes', S7Instituicoes.findAll)

  router.get('/s7/experiencias-profissionais/:id', S7ExperienciasProfissionais.findOneById)
  router.get('/s7/experiencias-profissionais', S7ExperienciasProfissionais.findAll)

  router.delete('/s7/formacoes/:id', S7Formacoes.delete)
  router.get('/s7/formacoes/:id', S7Formacoes.findOneById)
  router.get('/s7/formacoes', S7Formacoes.findAll)

  router.get('/s7/empresas-alvo/:id', S7EmpresasAlvo.findOneById)
  router.get('/s7/empresas-alvo', S7EmpresasAlvo.findAll)

  router.get('/s7/projetos/:id', S7Projetos.findOneById)
  router.get('/s7/projetos', S7Projetos.findAll)

  router.get('/s7/profissionais/:id', S7Profissionais.findOneById)
  router.post('/s7/profissionais/suggestions', ...parsers, S7Profissionais.suggestions)
  router.get('/s7/profissionais', S7Profissionais.findAll)

  router.get('/s7/principais-contatos/:id', S7PrincipaisContatos.findOneById)
  router.get('/s7/principais-contatos', S7PrincipaisContatos.findAll)

  router.get('/s7/conselheiros/:id', S7Conselheiros.findOneById)
  router.get('/s7/conselheiros', S7Conselheiros.findAll)

  router.get('/s7/profissionais-projeto/:id', S7ProfissionaisProjeto.findOneById)
  router.get('/s7/profissionais-projeto', S7ProfissionaisProjeto.findAll)

  router.get('/s7/conselho-administracao/:id', S7ConselhoAdministracao.findOneById)
  router.get('/s7/conselho-administracao', S7ConselhoAdministracao.findAll)

  router.get('/s7/gestao/:id', S7Gestao.findOneById)
  router.get('/s7/gestao', S7Gestao.findAll)

  router.get('/s7/formatos/:id', S7Formatos.findOneById)
  router.get('/s7/formatos', S7Formatos.findAll)

  router.get('/s7/portes/:id', S7Portes.findOneById)
  router.get('/s7/portes', S7Portes.findAll)

  router.get('/s7/moedas/:id', S7Moedas.findOneById)
  router.get('/s7/moedas', S7Moedas.findAll)

  router.get('/s7/setores/:id', S7Setores.findOneById)
  router.get('/s7/setores', S7Setores.findAll)

  router.get('/s7/status/:id', S7Status.findOneById)
  router.get('/s7/status', S7Status.findAll)

  router.get('/s7/segmentos/:id', S7Segmentos.findOneById)
  router.get('/s7/segmentos', S7Segmentos.findAll)

  router.get('/s7/empresas/:id', S7Empresas.findOneById)
  router.post('/s7/empresas/suggestions', ...parsers, S7Empresas.suggestions)
  router.get('/s7/empresas', S7Empresas.findAll)

  router.delete('/s7/usuarios/:id', S7Usuarios.delete)
  router.put('/s7/usuarios/:id', ...parsers, S7Usuarios.updateOneById)
  router.get('/s7/usuarios/:id', S7Usuarios.findOneById)
  router.post('/s7/usuarios/suggestions', ...parsers, S7Usuarios.suggestions)
  router.post('/s7/usuarios', ...parsers, S7Usuarios.create)
  router.get('/s7/usuarios', S7Usuarios.findAll)

  router.delete('/s7/users/:id', Users.delete)
  router.put('/s7/users/:id', ...parsers, Users.updateOneById)
  router.get('/s7/users/:id', Users.findOneById)
  router.post('/s7/users', ...parsers, Users.create)
  router.get('/s7/users', Users.findAll)

  router.post(
    '/s7/filesystem',
    ...parsers,
    upload.array('file[]', 10),
    FileSystemEndpoint.process
  )

  router.post('/s7/endpoint', ...parsers, MainEndpoint.process)

  router.post('/s7/search', ...parsers, MainEndpoint.search)

  router.post('/s7/token', ...parsers, async (req, res) => {
    const data = req.body

    if (data.email && typeof data.email === 'string') data.email = data.email.toLowerCase()

    if (data.email && data.senha) {
      let usuario = await S7Usuario.findOne({ email: data.email }).catch((err) => {
        res.json({
          token: null,
          error: {
            code: 1000,
            message: 'An error occurred.'
          }
        })
      })

      if (usuario) {
        usuario = usuario.toObject()
        const idUsuario = usuario._id.toString()

        bcrypt.compare(data.senha, usuario.senha, async (err, matches) => {
          if (matches) {
            const payload = {
              _id: idUsuario,
              nome: usuario.nome,
              sobrenome: usuario.sobrenome,
              email: usuario.email,
              foto: usuario.foto
            }

            const token = await jwt.generateToken(null, idUsuario, payload).catch((err) => {
              res.json({
                token: null,
                error: {
                  code: 1000,
                  message: 'An error occurred.'
                }
              })
            })

            res.json({ token: token })
          } else {
            res.json({
              token: null,
              error: {
                code: 1001,
                message: 'Wrong credentials.'
              }
            })
          }
        })
      } else {
        res.json({
          token: null,
          error: {
            code: 1001,
            message: 'Wrong credentials.'
          }
        })
      }
    } else {
      res.json({
        token: null,
        error: {
          code: 1001,
          message: 'Credentials missing.'
        }
      })
    }
  })

  router.post('/s7/token/validate', ...parsers, async (req, res) => {
    const data = req.body

    if (data.token) {
      const tokenIsValid = await jwt.verifyToken(data.token).catch((err) => {
        res.json({
          token: null,
          error: {
            message: 'Invalid token.'
          }
        })
      })

      if (tokenIsValid) res.json({ token: data.token })
    } else {
      res.json({
        token: null,
        error: {
          message: 'Token not provided.'
        }
      })
    }
  })

  router.post('/s7/token/renew', express.urlencoded({ extended: true }), async (req, res) => {
    const data = req.body

    if (data.token) {
      const decodedToken = await jwt.verifyToken(data.token).catch((err) => {
        res.json({
          token: null,
          error: {
            message: 'Invalid token.'
          }
        })
      })

      if (decodedToken) {
        const payload = {
          _id: decodedToken._id,
          nome: decodedToken.nome,
          sobrenome: decodedToken.sobrenome,
          email: decodedToken.email,
          foto: decodedToken.foto
        }

        const token = await jwt.generateToken(null, decodedToken._id, payload).catch((err) => {
          res.json({
            token: null,
            error: {
              message: 'An error occurred.'
            }
          })
        })

        res.json({ token: token })
      } else {
        res.json({
          token: null,
          error: {
            message: 'An error occurred.'
          }
        })
      }
    } else {
      res.json({
        token: null,
        error: {
          message: 'Token not provided.'
        }
      })
    }
  })

  return router
}