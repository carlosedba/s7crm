const Promise = require('bluebird')
const bcrypt = Promise.promisifyAll(require('bcrypt'))

const S7Pais = Promise.promisifyAll(require('./model/S7Pais'))
const S7Estado = Promise.promisifyAll(require('./model/S7Estado'))
const S7Cidade = Promise.promisifyAll(require('./model/S7Cidade'))
const S7Setor = Promise.promisifyAll(require('./model/S7Setor'))
const S7Segmento = Promise.promisifyAll(require('./model/S7Segmento'))
const S7Cargo = Promise.promisifyAll(require('./model/S7Cargo'))
const S7Area = Promise.promisifyAll(require('./model/S7Area'))
const S7Beneficio = Promise.promisifyAll(require('./model/S7Beneficio'))
const S7Ferramenta = Promise.promisifyAll(require('./model/S7Ferramenta'))
const S7Idioma = Promise.promisifyAll(require('./model/S7Idioma'))
const S7Instituicao = Promise.promisifyAll(require('./model/S7Instituicao'))
const S7Curso = Promise.promisifyAll(require('./model/S7Curso'))
const S7CategoriaProjeto = Promise.promisifyAll(require('./model/S7CategoriaProjeto'))
const S7Usuario = Promise.promisifyAll(require('./model/S7Usuario'))
const User = Promise.promisifyAll(require('./model/User'))

const paises = require('./initialData/paises')
const ufs = require('./initialData/ufs')
const municipios = require('./initialData/municipios')

function countCollectionDocuments(collection) {
  return new Promise((resolve, reject) => {
    collection.countDocuments({}, (err, count) => {
      if (err) reject(err)
      resolve(count)
    })
  })
}

function capitaisRefenciadas() {
  return new Promise((resolve, reject) => {
    S7Estado.countDocuments({capital: {$exists: true}}, (err, count) => {
      if (err) reject(err)
      resolve(count)
    })
  })
}

async function cadastrarPaises() {
  console.log('log > setup > Cadastrando países...')

  for (let i = 0; i < paises.length; i++) {
    const currentItem = paises[i]

    const pais = new S7Pais({
      nome: currentItem[0],
      gentilico: currentItem[1],
    })

    await pais.save()
  }
}

async function cadastrarEstados() {
  console.log('log > setup > Cadastrando estados...')

  const pais = await S7Pais.findOneAsync({nome: 'Brasil'}).catch(console.error)

  for (let i = 0; i < ufs.length; i++) {
    const currentItem = ufs[i]

    const estado = new S7Estado({
      pais: pais._id,
      nome: currentItem.nome,
      sigla: currentItem.sigla,
      slug: currentItem.slug,
    })

    await estado.save()
  }
}

async function cadastrarCidades() {
  console.log('log > setup > Cadastrando cidades...')

  const pais = await S7Pais.findOneAsync({nome: 'Brasil'}).catch(console.error)

  for (let i = 0; i < municipios.length; i++) {
    const currentItem = municipios[i]

    for (let j = 0; j < ufs.length; j++) {
      const uf = ufs[j]

      if (currentItem.codigoUf === uf.codigo) {
        const estado = await S7Estado.findOneAsync({slug: uf.slug}).catch(console.error)

        const cidade = new S7Cidade({
          pais: pais._id,
          estado: estado._id,
          nome: currentItem.nome,
          slug: currentItem.slug,
        })

        await cidade.save()
      }
    }
  }
}

async function adicionarReferenciaCapitais() {
  console.log('log > setup > Adicionando referências de capitais...')

  for (const uf of ufs) {
    for (const municipio of municipios) {
      if (municipio.codigo === uf.codigoCapital) {
        const cidade = await S7Cidade.findOneAsync({slug: municipio.slug}).catch(console.error)

        S7Estado.findOneAndUpdateAsync(
          {slug: uf.slug},
          {capital: cidade._id}
        ).catch(console.error)
      }
    }
  }
}

async function cadastrarSetores() {
  console.log('log > setup > Cadastrando setores...')

  const setores = [
    {nome: 'Alimentos e bebidas'},
    {nome: 'Madeira e moveleiro'},
    {nome: 'Florestal, celulose, papel e gráfica'},
    {nome: 'Construção cívil e mineral'},
    {nome: 'Vestuário, têxtil e couro'},
    {nome: 'Metalmecânico e eletroeletrônico'},
    {nome: 'Biotecnologia'},
    {nome: 'Energia'},
    {nome: 'TIC e audiovisual'},
    {nome: 'Reparação e automotivo'},
    {nome: 'Químico, farmacêutico e cosmético'},
    {nome: 'Plástico e borracha'},
    {nome: 'Logística e infraestrutura'}
  ]

  for (const setor of setores) {
    const s7Setor = new S7Setor(setor)

    await s7Setor.save()
  }
}

async function cadastrarSegmentosSetores() {
  console.log('log > setup > Cadastrando segmentos e setores...')

  let result

  const segmentos = [
    {
      nome: "Indústria",
      setores: [
        "Papel e Celulose",
        "Automotivo",
        "Auto Peças",
        "Bens de Capital",
        "Oléo e Gás",
        "Energia",
        "Construção Civil",
        "Infraestrutura",
        "Painéis, Madereira",
        "Químico",
        "Petroquímico",
        "Borracha",
        "Mineração ",
        "Máquinas e Equipamentos",
        "Embalagem",
        "Vidros",
        "Siderurgia",
        "Materiais de Construção",
        "Transporte e Logística",
        "Metalúrgica"
      ]
    },
    {
      nome: "Bens de Consumo",
      setores: [
        "Bebidas",
        "Alimentos",
        "Fumo",
        "Têxtil, Couro",
        "Calçados, Acessórios",
        "Vestuário",
        "Eletrodomésticos",
        "Eletroeletrônico",
        "Varejo",
        "Cosméticos",
        "Farmacêutico"
      ]
    },
    {
      nome: "High Tech",
      setores: [
        "Telecom",
        "Tecnologia Hardware",
        "Tecnologia Software",
        "Games e Aplicativos",
        "Tecnologias Emergentes",
        "E-commerce",
        "Media e Entretenimento",
        "Plataforma",
        "Ciência e Dados",
        "Segurança da Informação",
        "Digital Influencer",
        "Redes Sociais"
      ]
    },
    {
      nome: "Mercado Financeiro",
      setores: [
        "Banco Varejo",
        "Banco Atacado",
        "Asset",
        "Private Equity",
        "Seguradora",
        "Fundos de Investimentos",
        "Cobrança",
        "Fomento",
        "Crédito",
        "Meios de pagamento"
      ]
    },
    {
      nome: "Serviços Especializados",
      setores: [
        "Advocacia",
        "Auditoria",
        "Educação",
        "Consultoria Estratégica",
        "Consultoria Contábil e Tributária",
        "Serviços/Consultoria de Tecnologia",
        "ONG",
        "Associações de Classe",
        "Entidade Governamental",
        "Serviços de Logística",
        "Consultoria de RH",
        "Serviços de Recrutamento e Seleção",
        "Serviços Saúde, Diagnóstico",
        "Hospital",
        "Shopping",
        "Portos",
        "Concessionária",
        "Coworking",
        "Home center",
        "Órgaos Reguladores",
        "Hotelaria"
      ]
    },
    {
      nome: "Agronegócios",
      setores: [
        "Commodities Grãos",
        "Defensivos e Fertilizantes",
        "Pecuária",
        "Fumo",
        "Transformação Genética ",
        "Genética Animal",
        "Biosegurança",
        "Vacinas, Medicamentos",
        "Trading",
        "Sementes",
        "Frigorífico",
        "Máquinas e Equipamentos",
        "Horticultura",
        "Pesca",
        "Silos",
        "Cooperativas",
        "Solos, Georeferenciamento ",
        "Ervas e Fitoterápicos",
        "Alimentos saudáveis",
        "Veterinária e Diagnóstico",
        "Derivados de leite",
        "Floricultura",
        "Arvores, mudas, reflorestamento"
      ]
    },
  ]

  for (const segmento of segmentos) {
    const s7Segmento = new S7Segmento({nome: segmento.nome})

    result = await s7Segmento.save()

    for (const setor of segmento.setores) {
      const s7Setor = new S7Setor({
        segmento: result._id,
        nome: setor
      })

      await s7Setor.save()
    }
  }
}

async function cadastrarCargos() {
  console.log('log > setup > Cadastrando cargos...')

  const cargos = [
    {nome: "Menor Aprendiz"},
    {nome: "Estagiário"},
    {nome: "Auxiliar"},
    {nome: "Assistente"},
    {nome: "Analista Júnior"},
    {nome: "Analista Pleno"},
    {nome: "Analista Sênior"},
    {nome: "Especialista"},
    {nome: "Consultor"},
    {nome: "Business Partner"},
    {nome: "Auditor"},
    {nome: "Arquiteto"},
    {nome: "Psicologa"},
    {nome: "Cientista"},
    {nome: "Advogado"},
    {nome: "Engenheiro"},
    {nome: "Professor"},
    {nome: "Reitor"},
    {nome: "Vice Reitor"},
    {nome: "Supervisor"},
    {nome: "Coordenador"},
    {nome: "Gerente"},
    {nome: "Gerente Corporativo"},
    {nome: "Diretor"},
    {nome: "Superintendente"},
    {nome: "Vice-presidente"},
    {nome: "Presidente"},
    {nome: "Conselheiro"},
    {nome: "Board Member"},
    {nome: "Acionista"},
    {nome: "Socio"},
    {nome: "Socio Diretor"},
    {nome: "Socio Proprietário"},
    {nome: "PcD - Portador de Necessidades"},
    {nome: "CEO"},
    {nome: "CFO"},
    {nome: "COO"},
    {nome: "CTO"},
    {nome: "CIO"},
    {nome: "CMO"},
    {nome: "CPO"},
    {nome: "CAO (Chief Accounting Officer) - Diretor de Contabilidade"},
    {nome: "CAO (Chief Academic Officer) - Diretor Acadêmico"},
    {nome: "CBO (Chief Brand Officer) - Diretor de Marca"},
    {nome: "CBO (Chief Business Officer) - Diretor de Negócios"},
    {nome: "CCO (Chief Communications Officer) - Diretor de Comunicações"},
    {nome: "CCO (Chief Creative Officer) - Diretor Criativo"},
    {nome: "CCO (Chief Content Officer) - Diretor de Conteúdo"},
    {nome: "CDO (Chief Diversity Officer) - Diretor de Diversidade"},
    {nome: "CDO (Chief Design Officer) - Diretor de Design"},
    {nome: "CHRO (Chief Human Resources Officer) - Diretor de Recursos Humanos"},
    {nome: "CLO (Chief Legal Officer) - Diretor Jurídico"},
    {nome: "CSO (Chief Science Officer) – Diretor Científico"}
  ]

  for (const cargo of cargos) {
    const s7Cargo = new S7Cargo(cargo)

    await s7Cargo.save()
  }
}

async function cadastrarAreas() {
  console.log('log > setup > Cadastrando áreas...')

  const areas = [
    {nome: "Administrativo"},
    {nome: "Financeiro"},
    {nome: "Contas a pagar"},
    {nome: "Contas a receber"},
    {nome: "Crédito"},
    {nome: "Cobrança"},
    {nome: "Crédito e Cobrança"},
    {nome: "Contabilidade"},
    {nome: "Fiscal"},
    {nome: "Tributário"},
    {nome: "Contábil e Fiscal"},
    {nome: "Controladoria"},
    {nome: "Planejamento Estratégico"},
    {nome: "Novos Negócios"},
    {nome: "Comercial"},
    {nome: "Vendas"},
    {nome: "Comercial Nacional"},
    {nome: "Comercial Regional"},
    {nome: "Facilities"},
    {nome: "Obras"},
    {nome: "Responsabilidade Social"},
    {nome: "Comunicação"},
    {nome: "Comunicação Interna"},
    {nome: "Marketing"},
    {nome: "Comunicação e Marketing"},
    {nome: "Produtos"},
    {nome: "Desenvolvimento de Produtos"},
    {nome: "Pesquisa e Desenvolvimento"},
    {nome: "Laboratório"},
    {nome: "Expedição"},
    {nome: "Almoxarifado"},
    {nome: "Supply Chain"},
    {nome: "Logística"},
    {nome: "Frete"},
    {nome: "PCP"},
    {nome: "PCM"},
    {nome: "PCP e PCM"},
    {nome: "Engenharia"},
    {nome: "Expansão"},
    {nome: "Novos negócios"},
    {nome: "Inovação"},
    {nome: "Inteligência de Mercado"},
    {nome: "Recursos Humanos"},
    {nome: "Remuneração e Benefícios"},
    {nome: "Cargos e Salários"},
    {nome: "Cultura"},
    {nome: "Administração de Pessoal"},
    {nome: "Compliance"},
    {nome: "Governança"},
    {nome: "Governança e Compliance"},
    {nome: "Controles Internos"},
    {nome: "Central de Relacionamento"},
    {nome: "Central de Vendas"},
    {nome: "Central de Cobrança"},
    {nome: "Tecnologia"},
    {nome: "Infraestrutura"},
    {nome: "Banco de Dados"},
    {nome: "IA Inteligência Artificial"},
    {nome: "Sistemas"},
    {nome: "SAP"},
    {nome: "Arquitetura"},
    {nome: "Jurídico"},
    {nome: "Relações Sindicais"},
    {nome: "E-commerce"},
    {nome: "Mídias Sociais"},
    {nome: "Marketing Digital"},
    {nome: "Dados"},
    {nome: "Marketing e Produtos"},
    {nome: "Employer Branding"},
    {nome: "Marketplace"},
    {nome: "Compras"},
    {nome: "Suprimentos"},
    {nome: "Compras Diretas"},
    {nome: "Compras Indiretas"},
    {nome: "Comercio Exterior"},
    {nome: "Polos e Expansão"},
    {nome: "Vendas Multinacanal"},
    {nome: "Acadêmico"},
    {nome: "Operações"},
    {nome: "Vendas e Relacionamento"},
    {nome: "EAD"},
    {nome: "Polos EAD"},
    {nome: "Merchandising"},
    {nome: "Franquias"},
    {nome: "Qualidade"},
    {nome: "Importação"},
    {nome: "Exportação"},
    {nome: "Industrial"},
    {nome: "Produção"},
    {nome: "Estoque"},
    {nome: "Varejo"},
    {nome: "Private Equity"},
    {nome: "Fundo de Investimento"},
    {nome: "Atacado"},
    {nome: "Asset "},
    {nome: "Montagem"},
    {nome: "Teste"},
    {nome: "Engenharia de Produto"},
    {nome: "Manutenção"},
    {nome: "Suporte"},
    {nome: "Suporte Técnico"},
    {nome: "Relações Internacionais "},
    {nome: "Administrativo Financeiro"},
    {nome: "Planejamento de Demanda"},
    {nome: "Negócios Internacionais"},
  ]

  for (const area of areas) {
    const s7Area = new S7Area(area)

    await s7Area.save()
  }
}

async function cadastrarBeneficios() {
  console.log('log > setup > Cadastrando beneficios...')

  const beneficios = [
    {nome: 'Assistência Médica'},
    {nome: 'Assistência Odontológica'},
    {nome: 'Auxílio Farmácia'},
    {nome: 'Seguro de Vida'},
    {nome: 'Previdência Privada'},
    {nome: 'Auxílio Creche'},
    {nome: 'Bolsa de estudos'},
    {nome: 'Vale Refeição'},
    {nome: 'Vale Alimentação'},
    {nome: 'Refeitório Local'},
    {nome: 'Cesta Básica'},
    {nome: 'Cartão Corporativo'},
    {nome: 'Carro'},
    {nome: 'Combustível'},
  ]

  for (const beneficio of beneficios) {
    const s7Beneficio = new S7Beneficio(beneficio)

    await s7Beneficio.save()
  }
}

async function cadastrarFerramentas() {
  console.log('log > setup > Cadastrando ferramentas...')

  const ferramentas = [
    {nome: 'Carro'},
    {nome: 'Combustível'},
    {nome: 'Celular'},
    {nome: 'Notebook'},
  ]

  for (const ferramenta of ferramentas) {
    const s7Ferramenta = new S7Ferramenta(ferramenta)

    await s7Ferramenta.save()
  }
}

async function cadastrarIdiomas() {
  console.log('log > setup > Cadastrando idiomas...')

  const idiomas = [
    {nome: 'Africâner'},
    {nome: 'Akan'},
    {nome: 'Albanês'},
    {nome: 'Alemão'},
    {nome: 'Amárico'},
    {nome: 'Árabe'},
    {nome: 'Armênio'},
    {nome: 'Assamês'},
    {nome: 'Assírio'},
    {nome: 'Azeri'},
    {nome: 'Bambara'},
    {nome: 'Basco'},
    {nome: 'Bashkir'},
    {nome: 'Bengalês'},
    {nome: 'Bielo-russo'},
    {nome: 'Birmanês'},
    {nome: 'Bósnio'},
    {nome: 'Bravanês'},
    {nome: 'Búlgaro'},
    {nome: 'Butanês'},
    {nome: 'Cambojano'},
    {nome: 'Canarês'},
    {nome: 'Canjobal'},
    {nome: 'Cantonês'},
    {nome: 'Catalão'},
    {nome: 'Caxemira'},
    {nome: 'Cazaque'},
    {nome: 'Cebuano'},
    {nome: 'Chaldean'},
    {nome: 'Chamorro'},
    {nome: 'Chaozhou'},
    {nome: 'Chavacano'},
    {nome: 'Chin'},
    {nome: 'Chona'},
    {nome: 'Chuquês'},
    {nome: 'Cingalês'},
    {nome: 'Coreano'},
    {nome: 'Cree'},
    {nome: 'Croata'},
    {nome: 'Curdo'},
    {nome: 'Curmânji'},
    {nome: 'Dacota'},
    {nome: 'Dari'},
    {nome: 'Dinamarquês'},
    {nome: 'Dinka'},
    {nome: 'Dioula'},
    {nome: 'Eslovaco'},
    {nome: 'Esloveno'},
    {nome: 'Espanhol'},
    {nome: 'Estoniano'},
    {nome: 'Ewe'},
    {nome: 'Fanti'},
    {nome: 'Faroês'},
    {nome: 'Farsi'},
    {nome: 'Finlandês'},
    {nome: 'Flamengo'},
    {nome: 'Francês'},
    {nome: 'Franco-canadense'},
    {nome: 'Frísio'},
    {nome: 'Fujian'},
    {nome: 'Fujianês'},
    {nome: 'Fula'},
    {nome: 'Fulani'},
    {nome: 'Fuzhou'},
    {nome: 'Ga'},
    {nome: 'Gaélico'},
    {nome: 'Galego'},
    {nome: 'Galês'},
    {nome: 'Ganda'},
    {nome: 'Georgiano'},
    {nome: 'Gorani'},
    {nome: 'Grego'},
    {nome: 'Guanxi'},
    {nome: 'Gujarati'},
    {nome: 'Hacá'},
    {nome: 'Haitiano'},
    {nome: 'Hassaniya'},
    {nome: 'Hauçá'},
    {nome: 'Hebraico'},
    {nome: 'Hiligaynon'},
    {nome: 'Hindi'},
    {nome: 'Hindi fijiano'},
    {nome: 'Hmong'},
    {nome: 'Holandês'},
    {nome: 'Húngaro'},
    {nome: 'Ibanag'},
    {nome: 'Igbo'},
    {nome: 'Iídiche'},
    {nome: 'Ilocano'},
    {nome: 'Ilonggo'},
    {nome: 'Indiano'},
    {nome: 'Indonésio'},
    {nome: 'Inglês'},
    {nome: 'Inglês pidgin'},
    {nome: 'Inuktitut'},
    {nome: 'Iorubá'},
    {nome: 'Irlandês'},
    {nome: 'Islandês'},
    {nome: 'Italiano'},
    {nome: 'Jakartanese'},
    {nome: 'Japonês'},
    {nome: 'Javanês'},
    {nome: 'Karen'},
    {nome: 'Khalkha'},
    {nome: 'Khmer'},
    {nome: 'Kikuyu'},
    {nome: 'Kirundi'},
    {nome: 'Kosovo'},
    {nome: 'Kotokoli'},
    {nome: 'Krio'},
    {nome: 'Lakota'},
    {nome: 'Laociano'},
    {nome: 'Latim'},
    {nome: 'Letão'},
    {nome: 'Lingala'},
    {nome: 'Língua de sinais americana'},
    {nome: 'Lituano'},
    {nome: 'Luganda'},
    {nome: 'Luo'},
    {nome: 'Lusoga'},
    {nome: 'Luxemburguês'},
    {nome: 'Maay'},
    {nome: 'Macedônio'},
    {nome: 'Malaiala'},
    {nome: 'Malaio'},
    {nome: 'Maldivense'},
    {nome: 'Malgaxe'},
    {nome: 'Maltês'},
    {nome: 'Mandarim'},
    {nome: 'Mandinga'},
    {nome: 'Mandinka'},
    {nome: 'Maori'},
    {nome: 'Marata'},
    {nome: 'Marshalês'},
    {nome: 'Mien'},
    {nome: 'Mirpuri'},
    {nome: 'Mixteco'},
    {nome: 'Moldovan'},
    {nome: 'Mongol'},
    {nome: 'Napolitano'},
    {nome: 'Navajo'},
    {nome: 'Nepali'},
    {nome: 'Nianja'},
    {nome: 'Norueguês'},
    {nome: 'Nuer'},
    {nome: 'Ojíbua'},
    {nome: 'Oriá'},
    {nome: 'Oromo'},
    {nome: 'Osseta'},
    {nome: 'Pachto'},
    {nome: 'Pahari'},
    {nome: 'Pampangan'},
    {nome: 'Patoá'},
    {nome: 'Polonês'},
    {nome: 'Português'},
    {nome: 'Pothwari'},
    {nome: 'Pulaar'},
    {nome: 'Punjabi'},
    {nome: 'Putian'},
    {nome: 'Quechua'},
    {nome: 'Quiniaruanda'},
    {nome: 'Quirguiz'},
    {nome: 'Romanche'},
    {nome: 'Romani'},
    {nome: 'Romeno'},
    {nome: 'Rundi'},
    {nome: 'Russo'},
    {nome: 'Samoano'},
    {nome: 'Sango'},
    {nome: 'Sânscrito'},
    {nome: 'Sérvio'},
    {nome: 'Sichuan'},
    {nome: 'Siciliano'},
    {nome: 'Sindi'},
    {nome: 'Somali'},
    {nome: 'Soninquê'},
    {nome: 'Sorâni'},
    {nome: 'Soto do sul'},
    {nome: 'Suaíle'},
    {nome: 'Suázi/suazilandês'},
    {nome: 'Sueco'},
    {nome: 'Sundanês'},
    {nome: 'Susu'},
    {nome: 'Sylheti'},
    {nome: 'Tadjique'},
    {nome: 'Tagalog'},
    {nome: 'Tailandês'},
    {nome: 'Taiwanês'},
    {nome: 'Tâmil'},
    {nome: 'Tcheco'},
    {nome: 'Telugu'},
    {nome: 'Tibetano'},
    {nome: 'Tigrínia'},
    {nome: 'Tonga'},
    {nome: 'Tshiluba'},
    {nome: 'Tsonga'},
    {nome: 'Tswana'},
    {nome: 'Turco'},
    {nome: 'Turcomeno'},
    {nome: 'Ucraniano'},
    {nome: 'Uigur'},
    {nome: 'Uolofe'},
    {nome: 'Urdu'},
    {nome: 'Uzbeque'},
    {nome: 'Venda'},
    {nome: 'Vietnamita'},
    {nome: 'Visayan'},
    {nome: 'Xangainês'},
    {nome: 'Xhosa'},
    {nome: 'Yao'},
    {nome: 'Yupik'},
    {nome: 'Zulu'}
  ]

  for (const idioma of idiomas) {
    const s7Idioma = new S7Idioma(idioma)

    await s7Idioma.save()
  }
}

async function cadastrarInstituicoes() {
  console.log('log > setup > Cadastrando instituições...')

  const estado = await S7Estado.findOneAsync({slug: 'parana'}).catch(console.error)

  const instituicoes = [
    {
      estado: estado._id,
      nome: 'Pontifícia Universidade Católica do Paraná',
      sigla: 'PUCPR'
    },
    {
      estado: estado._id,
      nome: 'Universidade Federal do Paraná',
      sigla: 'UFPR'
    },
    {
      estado: estado._id,
      nome: 'Universidade Tecnológica Federal do Paraná',
      sigla: 'UTFPR'
    },
    {
      estado: estado._id,
      nome: 'Unicuritiba',
      sigla: 'UNICURITIBA'
    },
    {
      estado: estado._id,
      nome: 'UniBrasil Centro Universitário',
      sigla: 'UNIBRASIL'
    },
    {
      estado: estado._id,
      nome: 'Universidade Positivo',
      sigla: 'UP'
    }
  ]

  for (const instituicao of instituicoes) {
    const s7Instituicao = new S7Instituicao(instituicao)

    await s7Instituicao.save()
  }
}

async function cadastrarCursos() {
  console.log('log > setup > Cadastrando cursos...')

  const cursos = [
    {nome: 'Engenharia de Produção'},
    {nome: 'Engenharia Civíl'},
    {nome: 'Engenharia da Computação'},
    {nome: 'Engenharia de Software'},
    {nome: 'Administração'},
    {nome: 'Arquitetura'},
    {nome: 'Contabilidade'},
    {nome: 'Direito'},
    {nome: 'Sistemas de Informação'},
    {nome: 'Design'},
  ]

  for (const curso of cursos) {
    const s7Curso = new S7Curso(curso)

    await s7Curso.save()
  }
}

async function cadastrarCategoriasProjeto() {
  console.log('log > setup > Cadastrando categorias de projeto...')

  const categoriasProjeto = [
    {divisao: 'S7 Search', nome: 'Recrutamento e Seleção'},
    {divisao: 'S7 Search', nome: 'Mapeamento de Mercado'},
    {divisao: 'S7 Search', nome: 'Pesquisa Salarial'},
    {divisao: 'S7 Search', nome: 'Pesquisa Benefícios'},

    {divisao: 'S7 Up', nome: 'Assessment'},
    {divisao: 'S7 Up', nome: 'Coaching'},
    {divisao: 'S7 Up', nome: 'Mentoria Executiva'},
    {divisao: 'S7 Up', nome: 'Assessment e Coaching'},
    {divisao: 'S7 Up', nome: 'Assessment e Mentoria'},
    {divisao: 'S7 Up', nome: 'Desenvolvimento de Liderança'},
    {divisao: 'S7 Up', nome: 'Inovação RHTech'},
    {divisao: 'S7 Up', nome: 'Reengenharia da Estrutura organizacional,'},
    {divisao: 'S7 Up', nome: 'RH Estratégico'},
    {divisao: 'S7 Up', nome: 'Labor e Auditoria'},
    {divisao: 'S7 Up', nome: 'Estratégia de Recrutamento e Seleção'},
    {divisao: 'S7 Up', nome: 'Cargos e Salários'},
    {divisao: 'S7 Up', nome: 'Remuneração Variável'},
    {divisao: 'S7 Up', nome: 'Avaliação por Competências'},
    {divisao: 'S7 Up', nome: 'Clima Organizacional,'},
    {divisao: 'S7 Up', nome: 'Avaliação de Desempenho'},
    {divisao: 'S7 Up', nome: 'Plano de Sucessão'},
    {divisao: 'S7 Up', nome: 'Treinamento e Desenvolvimento'},
    {divisao: 'S7 Up', nome: 'Workshop'},
    {divisao: 'S7 Up', nome: 'Responsabilidade Social'},
    {divisao: 'S7 Up', nome: 'Meritocracia'},
    {divisao: 'S7 Up', nome: 'Cultura Organizacional'},
    {divisao: 'S7 Up', nome: 'Trilha de Carreira'},
    {divisao: 'S7 Up', nome: 'Dashboard e KPI\'s'},
    {divisao: 'S7 Up', nome: 'Ações de Governança em RH'},
    {divisao: 'S7 Up', nome: 'Modelagem de Processos'},
    {divisao: 'S7 Up', nome: 'Diversidade'},

    {divisao: 'S7 Brand', nome: 'Comunicação Interna'},
    {divisao: 'S7 Brand', nome: 'Employer Branding'},
    {divisao: 'S7 Brand', nome: 'EVP'},
    {divisao: 'S7 Brand', nome: 'Gestão de Crise'},
    {divisao: 'S7 Brand', nome: 'Porta Voz Imprensa'},
    {divisao: 'S7 Brand', nome: 'Assessoria CEO'},
    {divisao: 'S7 Brand', nome: 'Café com Presidente'},
    {divisao: 'S7 Brand', nome: 'Integração'},
    {divisao: 'S7 Brand', nome: 'Canais de Comunicação'},
    {divisao: 'S7 Brand', nome: 'Books e Materiais'},
    {divisao: 'S7 Brand', nome: 'Mídias Sociais'},
    {divisao: 'S7 Brand', nome: 'Manual de Ética e Conduta'},
    {divisao: 'S7 Brand', nome: 'Benefícios Emocionais'},
    {divisao: 'S7 Brand', nome: 'Ações de Engajamento'},
    {divisao: 'S7 Brand', nome: 'Desvinculo Humanizado'},
    {divisao: 'S7 Brand', nome: 'Mudança Cultural para Transformação Digital'},
    {divisao: 'S7 Brand', nome: 'Organograma Criativo'},
    {divisao: 'S7 Brand', nome: 'Campanha Atração de talentos'},
    {divisao: 'S7 Brand', nome: 'Jogos Corporativos'},
    {divisao: 'S7 Brand', nome: 'Processo Seletivo Diferenciado'},
    {divisao: 'S7 Brand', nome: 'Design Thinking e Inovação'},
    {divisao: 'S7 Brand', nome: 'Storytelling'},
    {divisao: 'S7 Brand', nome: 'Celebração de Ciclos'},
    {divisao: 'S7 Brand', nome: 'Ressignificação de Vínculos'},
    {divisao: 'S7 Brand', nome: 'Benchmarking Redes Sociais'},
    {divisao: 'S7 Brand', nome: 'Social Experience'},
    {divisao: 'S7 Brand', nome: 'Programas de RH'},
  ]

  for (const categoriaProjeto of categoriasProjeto) {
    const s7CategoriaProjeto = new S7CategoriaProjeto(categoriaProjeto)

    await s7CategoriaProjeto.save()
  }
}

async function createAdmin() {
  const plainTextPassword = 'AquelaSenhaMarota'
  const saltRounds = 10
  const hash = await bcrypt.hashAsync(plainTextPassword, saltRounds).catch(console.error)

  let usuario = new S7Usuario({
    nome: 'Carlos Eduardo',
    sobrenome: 'Barbosa de Almeida',
    email: 'carlosedba@outlook.com',
    senha: hash
  })

  await usuario.save()
}

async function createS7Users() {
  const plainTextPassword = 'Sistema@S7'
  const saltRounds = 10
  const hash = await bcrypt.hashAsync(plainTextPassword, saltRounds).catch(console.error)

  let usuario = null

  usuario = new S7Usuario({
    nome: 'Thaís',
    sobrenome: 'Cristiane Silva',
    email: 'thais@s7consulting.com.br',
    senha: hash
  })
  await usuario.save()

  usuario = new S7Usuario({
    nome: 'Mariciane',
    sobrenome: '',
    email: 'mariciane@s7consulting.com.br',
    senha: hash
  })
  await usuario.save()

  usuario = new S7Usuario({
    nome: 'Giovanna',
    sobrenome: '',
    email: 'giovanna@s7consulting.com.br',
    senha: hash
  })
  await usuario.save()

  usuario = new S7Usuario({
    nome: 'Catarina',
    sobrenome: '',
    email: 'catarina@s7consulting.com.br',
    senha: hash
  })
  await usuario.save()

  usuario = new S7Usuario({
    nome: 'Priscila',
    sobrenome: '',
    email: 'priscila@s7consulting.com.br',
    senha: hash
  })
  await usuario.save()

  usuario = new S7Usuario({
    nome: 'Sonia',
    sobrenome: '',
    email: 'sonia@s7consulting.com.br',
    senha: hash
  })
  await usuario.save()

  usuario = new S7Usuario({
    nome: 'Maria Antônia',
    sobrenome: '',
    email: 'mariaantonia@s7consulting.com.br',
    senha: hash
  })
  await usuario.save()

  usuario = new S7Usuario({
    nome: 'Maria Vitória',
    sobrenome: '',
    email: 'mariavitoria@s7consulting.com.br',
    senha: hash
  })
  await usuario.save()
}

module.exports = async () => {
  await countCollectionDocuments(S7Pais).then(async (count) => {
    if (count === 0) await cadastrarPaises()

    countCollectionDocuments(S7Pais).then(async (count) => {
      console.log('Países: ', count)
    }).catch(console.error)
  }).catch(console.error)

  await countCollectionDocuments(S7Estado).then(async (count) => {
    if (count === 0) await cadastrarEstados()

    countCollectionDocuments(S7Estado).then(async (count) => {
      console.log('Estados: ', count)
    }).catch(console.error)
  }).catch(console.error)

  await countCollectionDocuments(S7Cidade).then(async (count) => {
    if (count === 0) await cadastrarCidades()

    countCollectionDocuments(S7Cidade).then(async (count) => {
      console.log('Cidades: ', count)
    }).catch(console.error)
  }).catch(console.error)

  await countCollectionDocuments(S7Setor).then(async (count) => {
    if (count === 0) await cadastrarSegmentosSetores()

    countCollectionDocuments(S7Setor).then(async (count) => {
      console.log('Setores: ', count)
    }).catch(console.error)
  }).catch(console.error)

  await countCollectionDocuments(S7Cidade).then((count) => {
    if (count > 0) {
      capitaisRefenciadas().then(async (count) => {
        if (count === 0) await adicionarReferenciaCapitais()
      }).catch(console.error)
    }
  }).catch(console.error)

  await countCollectionDocuments(S7Area).then(async (count) => {
    if (count === 0) await cadastrarAreas()

    countCollectionDocuments(S7Area).then(async (count) => {
      console.log('Áreas: ', count)
    }).catch(console.error)
  }).catch(console.error)

  await countCollectionDocuments(S7Cargo).then(async (count) => {
    if (count === 0) await cadastrarCargos()

    countCollectionDocuments(S7Cargo).then(async (count) => {
      console.log('Cargos: ', count)
    }).catch(console.error)
  }).catch(console.error)

  await countCollectionDocuments(S7Beneficio).then(async (count) => {
    if (count === 0) await cadastrarBeneficios()

    countCollectionDocuments(S7Beneficio).then(async (count) => {
      console.log('Benefícios: ', count)
    }).catch(console.error)
  }).catch(console.error)

  await countCollectionDocuments(S7Ferramenta).then(async (count) => {
    if (count === 0) await cadastrarFerramentas()

    countCollectionDocuments(S7Ferramenta).then(async (count) => {
      console.log('Ferramentas: ', count)
    }).catch(console.error)
  }).catch(console.error)

  await countCollectionDocuments(S7Idioma).then(async (count) => {
    if (count === 0) await cadastrarIdiomas()

    countCollectionDocuments(S7Idioma).then(async (count) => {
      console.log('Idiomas: ', count)
    }).catch(console.error)
  }).catch(console.error)

  await countCollectionDocuments(S7Instituicao).then(async (count) => {
    if (count === 0) await cadastrarInstituicoes()

    countCollectionDocuments(S7Instituicao).then(async (count) => {
      console.log('Instituições: ', count)
    }).catch(console.error)
  }).catch(console.error)

  await countCollectionDocuments(S7Curso).then(async (count) => {
    if (count === 0) await cadastrarCursos()

    countCollectionDocuments(S7Curso).then(async (count) => {
      console.log('Cursos: ', count)
    }).catch(console.error)
  }).catch(console.error)

  await countCollectionDocuments(S7CategoriaProjeto).then(async (count) => {
    if (count === 0) await cadastrarCategoriasProjeto()

    countCollectionDocuments(S7CategoriaProjeto).then(async (count) => {
      console.log('Categorias de Projeto: ', count)
    }).catch(console.error)
  }).catch(console.error)

  await countCollectionDocuments(S7Usuario).then(async (count) => {
    if (count === 0) {
      await createAdmin()
      await createS7Users()
    }
  }).catch(console.error)
}