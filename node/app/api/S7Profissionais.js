'use strict';

const Promise = require('bluebird')
const mongoose = require('mongoose')

const S7Profissional = Promise.promisifyAll(require('../model/S7Profissional'))

const MongoHelper = require('../MongoHelper')

const ObjectId = mongoose.Types.ObjectId

function attrToMongoSyntax(attributes) {
  let arr = []

  for (let i = 0; i < attributes.length; i++) {
    let attribute = attributes[i]

    arr.push(`$${attribute}`)

    if (i !== attributes.length - 1) arr.push(' ')
  }

  return arr
}

class S7Profissionais {
  static async suggestions(req, res) {
    let data = req.body
    let str = data.str
    let attributes = data.attributes

    let pipeline = []

    if (attributes) {
      pipeline.push({
        $project: {
          label: { $concat: attrToMongoSyntax(attributes) }
        },
      })

      pipeline.push({
        $match: {
          label: { $regex: new RegExp(`${str}`, 'i') }
        }
      })
    }

    const result = await S7Profissional.aggregate(pipeline).catch(console.error)

    res.send(result)
  }

  static async findAll(req, res) {
    const count = req.query.count || 5
    const after = req.query.after
    const before = req.query.before
    const empregoAtual = req.query.empregoAtual

    const empresaAtual = req.query.empresaAtual
    const cargoAtual = req.query.cargoAtual
    const areaAtual = req.query.areaAtual

    let queryParams = {}
    let projection = {}
    let options = {
      limit: count
    }

    if (after) {
      let cursorObjectId = ObjectId(Buffer.from(after, 'base64').toString('utf8'))
      queryParams = { ...queryParams, _id: { $gt: cursorObjectId } }
      options = { ...options, sort: { _id: 1 } }
    }

    if (before) {
      let cursorObjectId = ObjectId(Buffer.from(before, 'base64').toString('utf8'))
      queryParams = { ...queryParams, _id: { $lt: cursorObjectId } }
      options = { ...options, sort: { _id: -1 } }
    }

    let result

    result = await S7Profissional.find(
      queryParams,
      projection,
      options
    )
      .populate('naturalidade')
      .populate('estadoOrigem')
      .populate('cidadeOrigem')
      .populate('paisAtual')
      .populate('estadoAtual')
      .populate('cidadeAtual')
      .catch(console.error)


    if (empregoAtual) {
      let query = [
        { $match: queryParams },
        { $limit : count },
        {
          $lookup: {
            from: 's7ExperienciasProfissionais',
            let: { profissional: '$_id' },
            pipeline: [
              {
                $match: {
                  $expr: {
                    $and: [
                      { $eq: ['$profissional', '$$profissional'] },
                      { empregoAtual: 'Sim' }
                    ]
                  }
                }
              }
            ],
            as: 'empregoAtual'
          }
        },
        {
          $unwind: {
            path: '$empregoAtual',
            preserveNullAndEmptyArrays: true
          }
        },
        {
          $lookup: {
            from: 's7Empresas',
            localField: 'empregoAtual.empresa',
            foreignField: '_id',
            as: 'empregoAtual.empresa'
          }
        },
        {
          $unwind: {
            path: '$empregoAtual.empresa',
            preserveNullAndEmptyArrays: true
          }
        },
        {
          $lookup: {
            from: 's7Cargos',
            localField: 'empregoAtual.cargo',
            foreignField: '_id',
            as: 'empregoAtual.cargo'
          }
        },
        {
          $unwind: {
            path: '$empregoAtual.cargo',
            preserveNullAndEmptyArrays: true
          }
        },
        {
          $lookup: {
            from: 's7Areas',
            localField: 'empregoAtual.area',
            foreignField: '_id',
            as: 'empregoAtual.area'
          }
        },
        {
          $unwind: {
            path: '$empregoAtual.area',
            preserveNullAndEmptyArrays: true
          }
        },
        {
          $lookup: {
            from: 's7Cidades',
            localField: 'cidadeAtual',
            foreignField: '_id',
            as: 'cidadeAtual'
          }
        },
        {
          $unwind: {
            path: '$cidadeAtual',
            preserveNullAndEmptyArrays: true
          }
        },
      ]

      if (empresaAtual) {
        query.push({
          $match: {
            'empregoAtual.empresa._id': ObjectId(empresaAtual)
          }
        })
      }

      if (cargoAtual) {
        query.push({
          $match: {
            'empregoAtual.cargo._id': ObjectId(cargoAtual)
          }
        })
      }

      if (areaAtual) {
        query.push({
          $match: {
            'empregoAtual.area._id': ObjectId(areaAtual)
          }
        })
      }

      result = await S7Profissional.aggregate(query)
    }


    if (result.length) {
      let cursors = {}

      let first = MongoHelper.getFirstItemObjectId(result)
      let last = MongoHelper.getLastItemObjectId(result)

      if (before) {
        first = MongoHelper.getLastItemObjectId(result)
        last = MongoHelper.getFirstItemObjectId(result)
      }

      if (await MongoHelper.hasNext(S7Profissional, queryParams, last)) {
        cursors = {
          ...cursors,
          after: Buffer.from(last.toString()).toString('base64'),
        }
      }

      if (await MongoHelper.hasPrev(S7Profissional, queryParams, first)) {
        cursors = {
          ...cursors,
          before: Buffer.from(first.toString()).toString('base64'),
        }
      }

      res.send({
        data: result,
        cursors
      })
    } else {
      res.send({
        data: result
      })
    }
  }

  static async findOneById(req, res) {
    const id = req.params.id

    let projection = {}

    const result = await S7Profissional.findById(id, projection)
      .populate('naturalidade')
      .populate('estadoOrigem')
      .populate('cidadeOrigem')
      .populate('paisAtual')
      .populate('estadoAtual')
      .populate('cidadeAtual')
      .catch(console.error)

    res.send(result)
  }

  static async create(req, res) {
    let data = req.body

    const profissional = new S7Profissional(data)

    profissional.save().then((document) => {
      res.sendStatus(201)
    }).catch((err) => {
      res.sendStatus(400)
    })
  }

  static async update(req, res) {
    const id = req.params.id
    let data = req.body

    const result = await S7Profissional.findByIdAndUpdateAsync(id, data).catch(console.error)

    if (result) res.sendStatus(200)
    else res.sendStatus(400)
  }

  static async delete(req, res) {
    const id = req.params.id

    const result = await S7Profissional.deleteOneAsync({ _id: id }).catch(console.error)

    if (result) res.sendStatus(200)
    else res.sendStatus(400)
  }
}

module.exports = S7Profissionais
