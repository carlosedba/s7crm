const Promise = require('bluebird')

const S7Usuario = Promise.promisifyAll(require('../model/S7Usuario'))

function attrToMongoSyntax(attributes) {
  let arr = []

  for (let i = 0; i < attributes.length; i++) {
    let attribute = attributes[i]

    arr.push(`$${attribute}`)

    if (i !== attributes.length - 1) arr.push(' ')
  }

  return arr
}

class S7Usuarios {
  static async suggestions(req, res) {
    let data = req.body
    let str = data.str
    let attributes = data.attributes

    let projection = S7Usuario.getHiddenFieldsProjection()

    let pipeline = []

    if (attributes) {
      pipeline.push({
        $project: {
          label: { $concat: attrToMongoSyntax(attributes) }
        },
      })

      pipeline.push({
        $project: {
          ...projection
        },
      })

      pipeline.push({
        $match: {
          label: { $regex: new RegExp(`${str}`, 'i') }
        }
      })
    }

    const result = await S7Usuario.aggregate(pipeline).catch(console.error)

    res.send(result)
  }

  static async findAll(req, res) {
    let queryParams = {}

    let projection = S7Usuario.getHiddenFieldsProjection()

    const result = await S7Usuario.find({ ...queryParams }, projection).catch(console.error)

    res.send(result)
  }

  static async findOneById(req, res) {
    const id = req.params.id

    let projection = S7Usuario.getHiddenFieldsProjection()

    const result = await S7Usuario.findById(id, projection).catch(console.error)

    res.send(result)
  }

  static async create(req, res) {
    let data = req.body

    if (data['senha']) {
      data = { ...data, password: await S7Usuario.hashPassword(data['senha']) }
    }

    const usuario = new S7Usuario(data)

    usuario.save().then((document) => {
      res.sendStatus(201)
    }).catch((err) => {
      res.sendStatus(400)
    })
  }

  static async updateOneById(req, res) {
    const id = req.params.id
    let data = req.body

    if (data['senha']) {
      data = { ...data, password: await S7Usuario.hashPassword(data['senha']) }
    }

    const result = await S7Usuario.findByIdAndUpdate(id, data).catch(console.error)

    if (result) res.sendStatus(200)
    else res.sendStatus(400)
  }

  static async delete(req, res) {
    const id = req.params.id

    const result = await S7Usuario.deleteOne({ _id: id }).catch(console.error)

    if (result) res.sendStatus(200)
    else res.sendStatus(400)
  }
}

module.exports = S7Usuarios