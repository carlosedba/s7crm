'use strict';

const Promise = require('bluebird')
const S7Moeda = Promise.promisifyAll(require('../model/S7Moeda'))

class S7Moedas {
  static async findAll(req, res) {
    const cursor = req.params.cursor

    let queryParams = {}
    if (cursor) queryParams = { ...queryParams, _id: { $gt: cursor } }

    let projection = {}

    const result = await S7Moeda.find({ ...queryParams }, projection, {
      sort: { simbolo: 1 }
    }).catch(console.error)

    res.send(result)
  }

  static async findOneById(req, res) {
    const id = req.params.id

    let projection = {}

    const result = await S7Moeda.findById(id, projection).catch(console.error)

    res.send(result)
  }

  static async create(req, res) {
    let data = req.body

    const moeda = new S7Moeda(data)

    moeda.save().then((document) => {
      res.sendStatus(201)
    }).catch((err) => {
      res.sendStatus(400)
    })
  }

  static async update(req, res) {
    const id = req.params.id
    let data = req.body

    const result = await S7Moeda.findByIdAndUpdate(id, data).catch(console.error)

    if (result) res.sendStatus(200)
    else res.sendStatus(400)
  }

  static async delete(req, res) {
    const id = req.params.id

    const result = await S7Moeda.deleteOne({ _id: id }).catch(console.error)

    if (result) res.sendStatus(200)
    else res.sendStatus(400)
  }
}

module.exports = S7Moedas