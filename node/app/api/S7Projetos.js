'use strict';

const Promise = require('bluebird')
const mongoose = require('mongoose')

const S7Projeto = Promise.promisifyAll(require('../model/S7Projeto'))

const MongoHelper = require('../MongoHelper')

const ObjectId = mongoose.Types.ObjectId

class S7Projetos {
  static async findAll(req, res) {
    const count = req.query.count || 5
    const after = req.query.after
    const before = req.query.before

    const divisao = req.query.divisao
    const consultorResponsavel = req.query.consultorResponsavel
    const cliente = req.query.cliente
    const contatoCliente = req.query.contatoCliente
    const status = req.query.status
    const categoria = req.query.categoria
    const tipo = req.query.tipo
    const cargo = req.query.cargo
    const area = req.query.area

    let queryParams = {}
    let projection = {}
    let options = {
      limit: count
    }

    if (after) {
      let cursorObjectId = ObjectId(Buffer.from(after, 'base64').toString('utf8'))
      queryParams = { ...queryParams, _id: { $gt: cursorObjectId } }
      options = { ...options, sort: { _id: 1 } }
    }

    if (before) {
      let cursorObjectId = ObjectId(Buffer.from(before, 'base64').toString('utf8'))
      queryParams = { ...queryParams, _id: { $lt: cursorObjectId } }
      options = { ...options, sort: { _id: -1 } }
    }

    if (divisao)
      queryParams = { ...queryParams, divisao: divisao }

    if (consultorResponsavel)
      queryParams = { ...queryParams, consultorResponsavel: consultorResponsavel }

    if (cliente)
      queryParams = { ...queryParams, cliente: cliente }

    if (contatoCliente)
      queryParams = { ...queryParams, contatoCliente: contatoCliente }

    if (status)
      queryParams = { ...queryParams, status: status }

    if (categoria)
      queryParams = { ...queryParams, categoria: categoria }

    if (tipo)
      queryParams = { ...queryParams, tipo: tipo }

    if (cargo)
      queryParams = { ...queryParams, cargo: cargo }

    if (area)
      queryParams = { ...queryParams, area: area }

    const result = await S7Projeto.find(
      queryParams,
      projection,
      options
    )
      .populate('consultorResponsavel')
      .populate('cliente')
      .populate('contatoCliente')
      .populate('cargo')
      .populate('area')
      .populate('categoria')
      .catch(console.error)


    if (result.length) {
      let cursors = {}

      let first = MongoHelper.getFirstItemObjectId(result)
      let last = MongoHelper.getLastItemObjectId(result)

      if (before) {
        first = MongoHelper.getLastItemObjectId(result)
        last = MongoHelper.getFirstItemObjectId(result)
      }

      if (await MongoHelper.hasNext(S7Projeto, queryParams, last)) {
        cursors = {
          ...cursors,
          after: Buffer.from(last.toString()).toString('base64'),
        }
      }

      if (await MongoHelper.hasPrev(S7Projeto, queryParams, first)) {
        cursors = {
          ...cursors,
          before: Buffer.from(first.toString()).toString('base64'),
        }
      }

      res.send({
        data: result,
        cursors
      })
    } else {
      res.send({
        data: result
      })
    }
  }

  static async findOneById(req, res) {
    const id = req.params.id

    let projection = {}

    const result = await S7Projeto.findById(id, projection)
      .populate('consultorResponsavel')
      .populate('cliente')
      .populate('contatoCliente')
      .populate('cargo')
      .populate('area')
      .populate('categoria')
      .catch(console.error)

    res.send(result)
  }

  static async create(req, res) {
    let data = req.body

    const projeto = new S7Projeto(data)

    projeto.save().then((document) => {
      res.sendStatus(201)
    }).catch((err) => {
      res.sendStatus(400)
    })
  }

  static async update(req, res) {
    const id = req.params.id
    let data = req.body

    const result = await S7Projeto.findByIdAndUpdateAsync(id, data).catch(console.error)

    if (result) res.sendStatus(200)
    else res.sendStatus(400)
  }

  static async delete(req, res) {
    const id = req.params.id

    const result = await S7Projeto.deleteOneAsync({ _id: id }).catch(console.error)

    if (result) res.sendStatus(200)
    else res.sendStatus(400)
  }
}

module.exports = S7Projetos
