'use strict';

const Promise = require('bluebird')
const S7Segmento = Promise.promisifyAll(require('../model/S7Segmento'))

class S7Segmentos {
  /*
  static async suggestions(req, res) {
    const str = req.query.str

    const query = await S7Segmento.query().where('nome', 'like', `%${str}%`)

    res.send(query)
  }
  */

  static async findAll(req, res) {
    const search = req.query.search

    let queryParams = {}
    if (search) queryParams = { ...queryParams, $text: { $search: search } }

    let projection = {}

    const result = await S7Segmento.find({ ...queryParams }, projection, {
      sort: { nome: 1 }
    }).catch(console.error)

    res.send(result)
  }

  static async findOneById(req, res) {
    const id = req.params.id

    let projection = {}

    const result = await S7Segmento.findById(id, projection).catch(console.error)

    res.send(result)
  }

  static async create(req, res) {
    let data = req.body

    const setor = new S7Segmento(data)

    setor.save().then((document) => {
      res.sendStatus(201)
    }).catch((err) => {
      res.sendStatus(400)
    })
  }

  static async update(req, res) {
    const id = req.params.id
    let data = req.body

    const result = await S7Segmento.findByIdAndUpdate(id, data).catch(console.error)

    if (result) res.sendStatus(200)
    else res.sendStatus(400)
  }

  static async delete(req, res) {
    const id = req.params.id

    const result = await S7Segmento.deleteOne({ _id: id }).catch(console.error)

    if (result) res.sendStatus(200)
    else res.sendStatus(400)
  }
}

module.exports = S7Segmentos
