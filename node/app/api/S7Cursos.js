'use strict';

const Promise = require('bluebird')
const S7Curso = Promise.promisifyAll(require('../model/S7Curso'))

function attrToMongoSyntax(attributes) {
  let arr = []

  for (let i = 0; i < attributes.length; i++) {
    let attribute = attributes[i]

    arr.push(`$${attribute}`)

    if (i !== attributes.length - 1) arr.push(' ')
  }

  return arr
}

class S7Cursos {
  static async suggestions(req, res) {
    let data = req.body
    let str = data.str
    let attributes = data.attributes

    let pipeline = []

    if (attributes) {
      pipeline.push({
        $project: {
          label: { $concat: attrToMongoSyntax(attributes) }
        },
      })

      pipeline.push({
        $match: {
          label: { $regex: new RegExp(`${str}`, 'i') }
        }
      })
    }

    const result = await S7Curso.aggregate(pipeline).catch(console.error)

    res.send(result)
  }
  
  static async findAll(req, res) {
    const count = req.query.count || 20
    const cursor = req.query.cursor
    const search = req.query.search

    let queryParams = {}
    if (cursor) queryParams = { ...queryParams, _id: { $gt: cursor } }
    if (search) queryParams = { ...queryParams, $text: { $search: search } }

    let projection = {}

    const result = await S7Curso.findAsync({ ...queryParams }, projection, {
      limit: count,
      sort: { nome: 1 }
    }).catch(console.error)

    res.send(result)
  }

  static async findOneById(req, res) {
    const id = req.params.id

    let projection = {}

    const result = await S7Curso.findByIdAsync(id, projection).catch(console.error)

    res.send(result)
  }

  static async create(req, res) {
    let data = req.body

    const curso = new S7Curso(data)

    curso.save().then((document) => {
      res.sendStatus(201)
    }).catch((err) => {
      res.sendStatus(400)
    })
  }

  static async update(req, res) {
    const id = req.params.id
    let data = req.body

    const result = await S7Curso.findByIdAndUpdateAsync(id, data).catch(console.error)

    if (result) res.sendStatus(200)
    else res.sendStatus(400)
  }

  static async delete(req, res) {
    const id = req.params.id

    const result = await S7Curso.deleteOneAsync({ _id: id }).catch(console.error)

    if (result) res.sendStatus(200)
    else res.sendStatus(400)
  }
}

module.exports = S7Cursos
