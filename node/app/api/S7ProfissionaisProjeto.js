'use strict';

const Promise = require('bluebird')
const mongoose = require('mongoose')
const ObjectId = mongoose.Types.ObjectId

const S7ProfissionalProjeto = Promise.promisifyAll(require('../model/S7ProfissionalProjeto'))

class S7ProfissionaisProjeto {
  static async findAll(req, res) {
    const count = req.query.count || 20
    const cursor = req.query.cursor
    const projeto = req.query.projeto
    const empregoAtual = req.query.empregoAtual

    let queryParams = {}
    if (cursor) queryParams = { ...queryParams, _id: { $gt: cursor } }
    if (projeto) queryParams = { ...queryParams, projeto: projeto }

    let projection = {}

    let result

    result = await S7ProfissionalProjeto.find({ ...queryParams }, projection, {
      limit: count
    })
      .populate('profissional')
      .catch(console.error)

    if (projeto && empregoAtual) {
      result = await S7ProfissionalProjeto.aggregate([
        { $match: { projeto: ObjectId(projeto) } },
        {
          $lookup: {
            from: 's7ExperienciasProfissionais',
            let: { profissional: '$profissional' },
            pipeline: [
              {
                $match: {
                  $expr: {
                    $and: [
                      { $eq: ['$profissional', '$$profissional'] },
                      { empregoAtual: 'Sim' }
                    ]
                  }
                }
              }
            ],
            as: 'empregoAtual'
          }
        },
        {
          $unwind: {
            path: '$empregoAtual',
            preserveNullAndEmptyArrays: true
          }
        },
        {
          $lookup: {
            from: 's7Empresas',
            localField: 'empregoAtual.empresa',
            foreignField: '_id',
            as: 'empregoAtual.empresa'
          }
        },
        {
          $unwind: {
            path: '$empregoAtual.empresa',
            preserveNullAndEmptyArrays: true
          }
        },
        {
          $lookup: {
            from: 's7Cargos',
            localField: 'empregoAtual.cargo',
            foreignField: '_id',
            as: 'empregoAtual.cargo'
          }
        },
        {
          $unwind: {
            path: '$empregoAtual.cargo',
            preserveNullAndEmptyArrays: true
          }
        },
        {
          $lookup: {
            from: 's7Areas',
            localField: 'empregoAtual.area',
            foreignField: '_id',
            as: 'empregoAtual.area'
          }
        },
        {
          $unwind: {
            path: '$empregoAtual.area',
            preserveNullAndEmptyArrays: true
          }
        },
        {
          $lookup: {
            from: 's7Profissionais',
            localField: 'profissional',
            foreignField: '_id',
            as: 'profissional'
          }
        },
        {
          $unwind: {
            path: '$profissional',
            preserveNullAndEmptyArrays: true
          }
        }
      ])
    }

    res.send(result)
  }

  static async findOneById(req, res) {
    const id = req.params.id

    let projection = {}

    const result = await S7ProfissionalProjeto.findById(id, projection)
      .populate('profissional')
      .catch(console.error)

    res.send(result)
  }

  static async create(req, res) {
    let data = req.body

    const profissionalSelecionado = new S7ProfissionalProjeto(data)

    profissionalSelecionado.save().then((document) => {
      res.sendStatus(201)
    }).catch((err) => {
      res.sendStatus(400)
    })
  }

  static async update(req, res) {
    const id = req.params.id
    let data = req.body

    const result = await S7ProfissionalProjeto.findByIdAndUpdate(id, data).catch(console.error)

    if (result) res.sendStatus(200)
    else res.sendStatus(400)
  }

  static async delete(req, res) {
    const id = req.params.id

    const result = await S7ProfissionalProjeto.deleteOne({ _id: id }).catch(console.error)

    if (result) res.sendStatus(200)
    else res.sendStatus(400)
  }
}

module.exports = S7ProfissionaisProjeto
