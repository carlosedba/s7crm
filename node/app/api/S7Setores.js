'use strict';

const Promise = require('bluebird')
const S7Setor = Promise.promisifyAll(require('../model/S7Setor'))

class S7Setores {
  /*
  static async suggestions(req, res) {
    const str = req.query.str

    const query = await S7Setor.query().where('nome', 'like', `%${str}%`)

    res.send(query)
  }
  */

  static async findAll(req, res) {
    const segmento = req.query.segmento
    const search = req.query.search

    let queryParams = {}
    if (segmento) queryParams = { ...queryParams, segmento: segmento }
    if (search) queryParams = { ...queryParams, $text: { $search: search } }

    let projection = {}

    const result = await S7Setor.find({ ...queryParams }, projection, {
      sort: { segmento: -1, nome: 1 }
    })
      .populate('segmento')
      .catch(console.error)

    res.send(result)
  }

  static async findOneById(req, res) {
    const id = req.params.id

    let projection = {}

    const result = await S7Setor.findById(id, projection)
      .populate('segmento')
      .catch(console.error)

    res.send(result)
  }

  static async create(req, res) {
    let data = req.body

    const setor = new S7Setor(data)

    setor.save().then((document) => {
      res.sendStatus(201)
    }).catch((err) => {
      res.sendStatus(400)
    })
  }

  static async update(req, res) {
    const id = req.params.id
    let data = req.body

    const result = await S7Setor.findByIdAndUpdate(id, data).catch(console.error)

    if (result) res.sendStatus(200)
    else res.sendStatus(400)
  }

  static async delete(req, res) {
    const id = req.params.id

    const result = await S7Setor.deleteOne({ _id: id }).catch(console.error)

    if (result) res.sendStatus(200)
    else res.sendStatus(400)
  }
}

module.exports = S7Setores
