'use strict';
                                                       
const Promise = require('bluebird')
const S7Pais = Promise.promisifyAll(require('../model/S7Pais'))

class S7Paises {
  static async findAll(req, res) {
    const search = req.query.search

    let queryParams = {}
    if (search) queryParams = { ...queryParams, $text: { $search: search } }

    let projection = {}

    const result = await S7Pais.find({ ...queryParams }, projection, {
      sort: { nome: 1 }
    }).catch(console.error)

    res.send(result)
  }

  static async findOneById(req, res) {
    const id = req.params.id

    let projection = {}

    const result = await S7Pais.findByIdAsync(id, projection).catch(console.error)

    res.send(result)
  }

  static async create(req, res) {
    let data = req.body

    const pais = new S7Pais(data)

    pais.save().then((document) => {
      res.sendStatus(201)
    }).catch((err) => {
      res.sendStatus(400)
    })
  }

  static async update(req, res) {
    const id = req.params.id
    let data = req.body

    const result = await S7Pais.findByIdAndUpdateAsync(id, data).catch(console.error)

    if (result) res.sendStatus(200)
    else res.sendStatus(400)
  }

  static async delete(req, res) {
    const id = req.params.id

    const result = await S7Pais.deleteOneAsync({ _id: id }).catch(console.error)

    if (result) res.sendStatus(200)
    else res.sendStatus(400)
  }
}

module.exports = S7Paises
