const Promise = require('bluebird')

const User = Promise.promisifyAll(require('../model/User'))

class Users {
  static async findAll(req, res) {
    const count = req.params.count || 20
    const cursor = req.params.cursor

    let queryParams = {}
    if (cursor) queryParams = { ...queryParams, _id: { $gt: cursor } }

    let projection = User.getHiddenFieldsProjection()

    const result = await User.findAsync({ ...queryParams }, projection, {
      limit: count
    }).catch(console.error)

    res.send(result)
  }

  static async findOneById(req, res) {
    const id = req.params.id

    let projection = User.getHiddenFieldsProjection()

    const result = await User.findByIdAsync(id, projection).catch(console.error)

    res.send(result)
  }

  static async create(req, res) {
    let data = req.body

    if (data['password']) {
      data = { ...data, password: await User.hashPassword(data['password']) }
    }

    const user = new User(data)

    user.save().then((document) => {
      res.sendStatus(201)
    }).catch((err) => {
      res.sendStatus(400)
    })
  }

  static async updateOneById(req, res) {
    const id = req.params.id
    let data = req.body

    if (data['password']) {
      data = { ...data, password: await User.hashPassword(data['password']) }
    }

    const result = await User.findByIdAndUpdateAsync(id, data).catch(console.error)

    if (result) res.sendStatus(200)
    else res.sendStatus(400)
  }

  static async delete(req, res) {
    const id = req.params.id

    const result = await User.deleteOneAsync({ _id: id }).catch(console.error)

    if (result) res.sendStatus(200)
    else res.sendStatus(400)
  }
}

module.exports = Users