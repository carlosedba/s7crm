'use strict';

const Promise = require('bluebird')
const mongoose = require('mongoose')

const S7PrincipalContato = Promise.promisifyAll(require('../model/S7PrincipalContato'))

const ObjectId = mongoose.Types.ObjectId

class S7PrincipaisContatos {
  static async findAll(req, res) {
    const count = req.query.count || 20
    const cursor = req.query.cursor
    const empresa = req.query.empresa

    let queryParams = {}
    if (cursor) queryParams = { ...queryParams, _id: { $gt: cursor } }
    if (empresa) queryParams = { ...queryParams, empresa: ObjectId(empresa) }

    let projection = {}

    const result = await S7PrincipalContato.find(
      { ...queryParams },
      projection,
      {
        limit: count
      },
    )
      .populate('profissional')
      .catch(console.error)

    res.send(result)
  }

  static async findOneById(req, res) {
    const id = req.params.id

    let projection = {}

    const result = await S7PrincipalContato.findById(id, projection)
      .populate('profissional')
      .catch(console.error)

    res.send(result)
  }

  static async create(req, res) {
    let data = req.body

    const principalContato = new S7PrincipalContato(data)

    principalContato.save().then((document) => {
      res.sendStatus(201)
    }).catch((err) => {
      res.sendStatus(400)
    })
  }

  static async update(req, res) {
    const id = req.params.id
    let data = req.body

    const result = await S7PrincipalContato.findByIdAndUpdate(id, data).catch(console.error)

    if (result) res.sendStatus(200)
    else res.sendStatus(400)
  }

  static async delete(req, res) {
    const id = req.params.id

    const result = await S7PrincipalContato.deleteOne({ _id: id }).catch(console.error)

    if (result) res.sendStatus(200)
    else res.sendStatus(400)
  }
}

module.exports = S7PrincipaisContatos
