'use strict';

const Promise = require('bluebird')
const S7Gestao = Promise.promisifyAll(require('../model/S7Gestao'))

class S7GestaoApi {
  static async findAll(req, res) {
    const cursor = req.params.cursor

    let queryParams = {}
    if (cursor) queryParams = { ...queryParams, _id: { $gt: cursor } }

    let projection = {}

    const result = await S7Gestao.find({ ...queryParams }, projection, {
      sort: { nome: 1 }
    }).catch(console.error)

    res.send(result)
  }

  static async findOneById(req, res) {
    const id = req.params.id

    let projection = {}

    const result = await S7Gestao.findById(id, projection).catch(console.error)

    res.send(result)
  }

  static async create(req, res) {
    let data = req.body

    const gestao = new S7Gestao(data)

    gestao.save().then((document) => {
      res.sendStatus(201)
    }).catch((err) => {
      res.sendStatus(400)
    })
  }

  static async update(req, res) {
    const id = req.params.id
    let data = req.body

    const result = await S7Gestao.findByIdAndUpdate(id, data).catch(console.error)

    if (result) res.sendStatus(200)
    else res.sendStatus(400)
  }

  static async delete(req, res) {
    const id = req.params.id

    const result = await S7Gestao.deleteOne({ _id: id }).catch(console.error)

    if (result) res.sendStatus(200)
    else res.sendStatus(400)
  }
}

module.exports = S7GestaoApi