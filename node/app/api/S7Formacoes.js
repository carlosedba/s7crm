'use strict';

const Promise = require('bluebird')
const S7Formacao = Promise.promisifyAll(require('../model/S7Formacao'))

class S7Formacaos {
  static async findAll(req, res) {
    const count = req.query.count || 20
    const cursor = req.query.cursor
    const profissional = req.query.profissional
    const instituicao = req.query.instituicao
    const curso = req.query.curso

    let queryParams = {}
    if (cursor) queryParams = { ...queryParams, _id: { $gt: cursor } }
    if (profissional) queryParams = { ...queryParams, profissional: profissional }
    if (instituicao) queryParams = { ...queryParams, instituicao: profissional }
    if (curso) queryParams = { ...queryParams, curso: profissional }

    let projection = {}

    const result = await S7Formacao.find({ ...queryParams }, projection, {
      limit: count
    })
      .populate('instituicao')
      .populate('curso')
      .catch(console.error)

    res.send(result)
  }

  static async findOneById(req, res) {
    const id = req.params.id

    let projection = {}

    const result = await S7Formacao.findById(id, projection)
      .populate('instituicao')
      .populate('curso')
      .catch(console.error)

    res.send(result)
  }

  static async create(req, res) {
    let data = req.body

    const formacao = new S7Formacao(data)

    formacao.save().then((document) => {
      res.sendStatus(201)
    }).catch((err) => {
      res.sendStatus(400)
    })
  }

  static async update(req, res) {
    const id = req.params.id
    let data = req.body

    const result = await S7Formacao.findByIdAndUpdate(id, data).catch(console.error)

    if (result) res.sendStatus(200)
    else res.sendStatus(400)
  }

  static async delete(req, res) {
    const id = req.params.id

    const result = await S7Formacao.deleteOneAsync({ _id: id }).catch(console.error)

    if (result) res.sendStatus(200)
    else res.sendStatus(400)
  }
}

module.exports = S7Formacaos
