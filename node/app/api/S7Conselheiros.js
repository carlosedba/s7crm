'use strict';

const Promise = require('bluebird')
const mongoose = require('mongoose')

const S7Conselheiro = Promise.promisifyAll(require('../model/S7Conselheiro'))

const ObjectId = mongoose.Types.ObjectId

class S7Conselheiros {
  static async findAll(req, res) {
    const count = req.query.count || 20
    const cursor = req.query.cursor

    const empresa = req.query.empresa

    let queryParams = {}
    if (cursor) queryParams = { ...queryParams, _id: { $gt: cursor } }
    if (empresa) queryParams = { ...queryParams, empresa: ObjectId(empresa) }

    let projection = {}

    const result = await S7Conselheiro.find(
      { ...queryParams },
      projection,
      {
        limit: count
      },
    )
      .populate('profissional')
      .catch(console.error)

    res.send(result)
  }

  static async findOneById(req, res) {
    const id = req.params.id

    let projection = {}

    const result = await S7Conselheiro.findById(id, projection)
      .populate('profissional')
      .catch(console.error)

    res.send(result)
  }

  static async create(req, res) {
    let data = req.body

    const principalContato = new S7Conselheiro(data)

    principalContato.save().then((document) => {
      res.sendStatus(201)
    }).catch((err) => {
      res.sendStatus(400)
    })
  }

  static async update(req, res) {
    const id = req.params.id
    let data = req.body

    const result = await S7Conselheiro.findByIdAndUpdate(id, data).catch(console.error)

    if (result) res.sendStatus(200)
    else res.sendStatus(400)
  }

  static async delete(req, res) {
    const id = req.params.id

    const result = await S7Conselheiro.deleteOne({ _id: id }).catch(console.error)

    if (result) res.sendStatus(200)
    else res.sendStatus(400)
  }
}

module.exports = S7Conselheiros
