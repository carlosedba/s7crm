'use strict';

const Promise = require('bluebird')
const S7CategoriaProjeto = Promise.promisifyAll(require('../model/S7CategoriaProjeto'))

class S7CategoriaProjetos {
  static async findAll(req, res) {
    const divisao = req.query.divisao

    let queryParams = {}
    if (divisao) queryParams = { ...queryParams, divisao: divisao }

    let projection = {}

    const result = await S7CategoriaProjeto.find({ ...queryParams }, projection, {
      sort: { divisao: 1, nome: 1 }
    }).catch(console.error)

    res.send(result)
  }

  static async findOneById(req, res) {
    const id = req.params.id

    let projection = {}

    const result = await S7CategoriaProjeto.findByIdAsync(id, projection).catch(console.error)

    res.send(result)
  }

  static async create(req, res) {
    let data = req.body

    const CategoriaProjeto = new S7CategoriaProjeto(data)

    CategoriaProjeto.save().then((document) => {
      res.sendStatus(201)
    }).catch((err) => {
      res.sendStatus(400)
    })
  }

  static async update(req, res) {
    const id = req.params.id
    let data = req.body

    const result = await S7CategoriaProjeto.findByIdAndUpdateAsync(id, data).catch(console.error)

    if (result) res.sendStatus(200)
    else res.sendStatus(400)
  }

  static async delete(req, res) {
    const id = req.params.id

    const result = await S7CategoriaProjeto.deleteOneAsync({ _id: id }).catch(console.error)

    if (result) res.sendStatus(200)
    else res.sendStatus(400)
  }
}

module.exports = S7CategoriaProjetos
