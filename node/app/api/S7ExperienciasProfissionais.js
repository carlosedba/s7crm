'use strict';

const Promise = require('bluebird')
const S7ExperienciaProfissional = Promise.promisifyAll(require('../model/S7ExperienciaProfissional'))

class S7ExperienciasProfissionais {
  static async findAll(req, res) {
    const count = req.query.count || 20
    const cursor = req.query.cursor
    const profissional = req.query.profissional
    const empresa = req.query.empresa
    const cargo = req.query.cargo

    let queryParams = {}
    if (cursor) queryParams = { ...queryParams, _id: { $gt: cursor } }
    if (profissional) queryParams = { ...queryParams, profissional: profissional }
    if (empresa) queryParams = { ...queryParams, empresa: empresa }
    if (cargo) queryParams = { ...queryParams, cargo: cargo }

    let projection = {}

    const result = await S7ExperienciaProfissional.find({ ...queryParams }, projection, {
      limit: count
    })
      .populate('empresa')
      .populate('cargo')
      .populate('area')
      .populate('moeda')
      .populate('beneficios')
      .populate('ferramentas')
      .catch(console.error)

    res.send(result)
  }

  static async findOneById(req, res) {
    const id = req.params.id

    let projection = {}

    const result = await S7ExperienciaProfissional.findById(id, projection)
      .populate('empresa')
      .populate('cargo')
      .populate('area')
      .populate('moeda')
      .populate('beneficios')
      .populate('ferramentas')
      .catch(console.error)

    res.send(result)
  }

  static async create(req, res) {
    let data = req.body

    const experienciaProfissional = new S7ExperienciaProfissional(data)

    experienciaProfissional.save().then((document) => {
      res.sendStatus(201)
    }).catch((err) => {
      res.sendStatus(400)
    })
  }

  static async update(req, res) {
    const id = req.params.id
    let data = req.body

    const result = await S7ExperienciaProfissional.findByIdAndUpdateAsync(id, data).catch(console.error)

    if (result) res.sendStatus(200)
    else res.sendStatus(400)
  }

  static async delete(req, res) {
    const id = req.params.id

    const result = await S7ExperienciaProfissional.deleteOneAsync({ _id: id }).catch(console.error)

    if (result) res.sendStatus(200)
    else res.sendStatus(400)
  }
}

module.exports = S7ExperienciasProfissionais
