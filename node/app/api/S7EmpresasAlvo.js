'use strict';

const Promise = require('bluebird')
const S7EmpresaAlvo = Promise.promisifyAll(require('../model/S7EmpresaAlvo'))

class S7EmpresasAlvo {
  static async findAll(req, res) {
    const count = req.query.count || 20
    const cursor = req.query.cursor
    const projeto = req.query.projeto

    let queryParams = {}
    if (cursor) queryParams = { ...queryParams, _id: { $gt: cursor } }
    if (projeto) queryParams = { ...queryParams, projeto: projeto }

    let projection = {}

    const result = await S7EmpresaAlvo.find({ ...queryParams }, projection, {
      limit: count
    })
      .populate('empresa')
      .populate({
        path: 'empresa',
        populate: ['segmento', 'setor']
      })
      .catch(console.error)

    res.send(result)
  }

  static async findOneById(req, res) {
    const id = req.params.id

    let projection = {}

    const result = await S7EmpresaAlvo.findById(id, projection)
      .populate('empresa')
      .catch(console.error)

    res.send(result)
  }

  static async create(req, res) {
    let data = req.body

    const empresaAlvo = new S7EmpresaAlvo(data)

    empresaAlvo.save().then((document) => {
      res.sendStatus(201)
    }).catch((err) => {
      res.sendStatus(400)
    })
  }

  static async update(req, res) {
    const id = req.params.id
    let data = req.body

    const result = await S7EmpresaAlvo.findByIdAndUpdate(id, data).catch(console.error)

    if (result) res.sendStatus(200)
    else res.sendStatus(400)
  }

  static async delete(req, res) {
    const id = req.params.id

    const result = await S7EmpresaAlvo.deleteOne({ _id: id }).catch(console.error)

    if (result) res.sendStatus(200)
    else res.sendStatus(400)
  }
}

module.exports = S7EmpresasAlvo
