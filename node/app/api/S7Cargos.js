'use strict';

const Promise = require('bluebird')
const S7Cargo = Promise.promisifyAll(require('../model/S7Cargo'))

class S7Cargos {
  /*
  static async suggestions(req, res) {
    const str = req.query.str

    const query = await S7Cargo.query().where('nome', 'like', `%${str}%`)

    res.send(query)
  }
  */
  
  static async findAll(req, res) {
    const count = req.params.count || 20
    const cursor = req.params.cursor
    const search = req.query.search

    let queryParams = {}
    if (cursor) queryParams = { ...queryParams, _id: { $gt: cursor } }
    if (search) queryParams = { ...queryParams, $text: { $search: search } }

    let projection = {}

    const result = await S7Cargo.find({ ...queryParams }, projection, {
      limit: count,
      sort: { nome: 1 }
    }).catch(console.error)

    res.send(result)
  }

  static async findOneById(req, res) {
    const id = req.params.id

    let projection = {}

    const result = await S7Cargo.findByIdAsync(id, projection).catch(console.error)

    res.send(result)
  }

  static async create(req, res) {
    let data = req.body

    const cargo = new S7Cargo(data)

    cargo.save().then((document) => {
      res.sendStatus(201)
    }).catch((err) => {
      res.sendStatus(400)
    })
  }

  static async update(req, res) {
    const id = req.params.id
    let data = req.body

    const result = await S7Cargo.findByIdAndUpdateAsync(id, data).catch(console.error)

    if (result) res.sendStatus(200)
    else res.sendStatus(400)
  }

  static async delete(req, res) {
    const id = req.params.id

    const result = await S7Cargo.deleteOneAsync({ _id: id }).catch(console.error)

    if (result) res.sendStatus(200)
    else res.sendStatus(400)
  }
}

module.exports = S7Cargos
