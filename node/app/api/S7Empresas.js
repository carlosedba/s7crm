'use strict';

const Promise = require('bluebird')
const mongoose = require('mongoose')

const S7Empresa = Promise.promisifyAll(require('../model/S7Empresa'))

const MongoHelper = require('../MongoHelper')

const ObjectId = mongoose.Types.ObjectId

function attrToMongoSyntax(attributes) {
  let arr = []

  for (let i = 0; i < attributes.length; i++) {
    let attribute = attributes[i]

    arr.push(`$${attribute}`)

    if (i !== attributes.length - 1) arr.push(' ')
  }

  return arr
}

class S7Empresas {
  static async suggestions(req, res) {
    let data = req.body
    let str = data.str
    let attributes = data.attributes

    let pipeline = []

    if (attributes) {
      pipeline.push({
        $project: {
          label: { $concat: attrToMongoSyntax(attributes) }
        },
      })

      pipeline.push({
        $match: {
          label: { $regex: new RegExp(`${str}`, 'i') }
        }
      })
    }

    const result = await S7Empresa.aggregate(pipeline).catch(console.error)

    res.send(result)
  }

  static async findAll(req, res) {
    const count = req.query.count || 5
    const after = req.query.after
    const before = req.query.before

    const segmento = req.query.segmento
    const setor = req.query.setor
    const porte = req.query.porte
    const projetos = req.query.projetos

    let queryParams = {}
    let projection = {}
    let options = {
      limit: count
    }

    if (after) {
      let cursorObjectId = ObjectId(Buffer.from(after, 'base64').toString('utf8'))
      queryParams = { ...queryParams, _id: { $gt: cursorObjectId } }
      options = { ...options, sort: { _id: 1 } }
    }

    if (before) {
      let cursorObjectId = ObjectId(Buffer.from(before, 'base64').toString('utf8'))
      queryParams = { ...queryParams, _id: { $lt: cursorObjectId } }
      options = { ...options, sort: { _id: -1 } }
    }

    if (segmento)
      queryParams = { ...queryParams, segmento: ObjectId(segmento) }

    if (setor)
      queryParams = { ...queryParams, setor: ObjectId(setor) }

    if (porte)
      queryParams = { ...queryParams, porte: ObjectId(porte) }

    let result

    result = await S7Empresa.find(
      queryParams,
      projection,
      options
    )
      .populate('segmento')
      .populate('setor')
      .populate('porte')
      .populate('origem')
      .populate('formato')
      .populate('gestao')
      .populate('conselhoAdministracao')
      .populate('conselheiros')
      .catch(console.error)


    if (projetos) {
      let query = [
        {$match: queryParams},
        {$limit: count},
        {
          $lookup: {
            from: 's7EmpresasAlvo',
            let: {empresa: '$_id'},
            pipeline: [
              {
                $match: {
                  $expr: {
                    $and: [
                      {$eq: ['$empresa', '$$empresa']}
                    ]
                  }
                }
              }
            ],
            as: 'projetos'
          }
        },
      ]

      result = await S7Empresa.aggregate(query)
    }

    if (result.length) {
      let cursors = {}

      let first = MongoHelper.getFirstItemObjectId(result)
      let last = MongoHelper.getLastItemObjectId(result)

      if (before) {
        first = MongoHelper.getLastItemObjectId(result)
        last = MongoHelper.getFirstItemObjectId(result)
      }

      if (await MongoHelper.hasNext(S7Empresa, queryParams, last)) {
        cursors = {
          ...cursors,
          after: Buffer.from(last.toString()).toString('base64'),
        }
      }

      if (await MongoHelper.hasPrev(S7Empresa, queryParams, first)) {
        cursors = {
          ...cursors,
          before: Buffer.from(first.toString()).toString('base64'),
        }
      }

      res.send({
        data: result,
        cursors
      })
    } else {
      res.send({
        data: result
      })
    }
  }

  static async findOneById(req, res) {
    const id = req.params.id

    const projetos = req.query.projetos

    let projection = {}

    let result = null

    if (projetos) {
      let query = [
        {
          $match: {
            _id: ObjectId(id)
          }
        },
        {
          $lookup: {
            from: 's7EmpresasAlvo',
            let: {empresa: '$_id'},
            pipeline: [
              {
                $match: {
                  $expr: {
                    $and: [
                      {$eq: ['$empresa', '$$empresa']}
                    ]
                  }
                }
              }
            ],
            as: 'projetos'
          }
        },
        {
          $lookup: {
            from: 's7Projetos',
            localField: 'empregoAtual.empresa',
            foreignField: '_id',
            as: 'empregoAtual.empresa'
          }
        },
      ]

      result = await S7Empresa.aggregate(query)
    } else {
      result = await S7Empresa.findById(id, projection)
        .populate('segmento')
        .populate('setor')
        .populate('porte')
        .populate('origem')
        .populate('formato')
        .populate('gestao')
        .populate('conselhoAdministracao')
        .populate('conselheiros')
        .catch(console.error)
    }

    res.send(result)
  }

  static async create(req, res) {
    let data = req.body

    const empresa = new S7Empresa(data)

    empresa.save().then((document) => {
      res.sendStatus(201)
    }).catch((err) => {
      res.sendStatus(400)
    })
  }

  static async update(req, res) {
    const id = req.params.id
    let data = req.body

    const result = await S7Empresa.findByIdAndUpdateAsync(id, data).catch(console.error)

    if (result) res.sendStatus(200)
    else res.sendStatus(400)
  }

  static async delete(req, res) {
    const id = req.params.id

    const result = await S7Empresa.deleteOneAsync({ _id: id }).catch(console.error)

    if (result) res.sendStatus(200)
    else res.sendStatus(400)
  }
}

module.exports = S7Empresas
