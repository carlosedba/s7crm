const path = require('path')

module.exports = {
    SERVER_PORT: 27200,

    PRIVATE_KEY: path.resolve(__dirname, '../keys/S7_PEM_RSA256_29062019'),
    PUBLIC_KEY: path.resolve(__dirname, '../keys/S7_PEM_RSA256_29062019.pub'),

    PUBLIC_DIR: path.resolve(__dirname, '../public'),
    PUBLIC_TEMP_DIR: path.resolve(__dirname, '../public/temp'),

    DATA_DIR: path.resolve(__dirname, '../data'),
};