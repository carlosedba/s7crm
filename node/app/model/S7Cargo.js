'use strict'

const mongoose = require('mongoose')

const S7CargoSchema = require('../schema/S7CargoSchema')

const S7Cargo = mongoose.model('S7Cargo', S7CargoSchema)

module.exports = S7Cargo