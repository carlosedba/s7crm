'use strict'

const mongoose = require('mongoose')

const S7StatusSchema = require('../schema/S7StatusSchema')

const S7Status = mongoose.model('S7Status', S7StatusSchema)

module.exports = S7Status