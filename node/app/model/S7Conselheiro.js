'use strict'

const mongoose = require('mongoose')

const S7ConselheiroSchema = require('../schema/S7ConselheiroSchema')

const S7Conselheiro = mongoose.model('S7Conselheiro', S7ConselheiroSchema)

module.exports = S7Conselheiro