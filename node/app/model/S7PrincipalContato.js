'use strict'

const mongoose = require('mongoose')

const S7PrincipalContatoSchema = require('../schema/S7PrincipalContatoSchema')

const S7PrincipalContato = mongoose.model('S7PrincipalContato', S7PrincipalContatoSchema)

module.exports = S7PrincipalContato