'use strict'

const mongoose = require('mongoose')

const S7ProfissionalSchema = require('../schema/S7ProfissionalSchema')

const S7Profissional = mongoose.model('S7Profissional', S7ProfissionalSchema)

module.exports = S7Profissional