'use strict'

const mongoose = require('mongoose')

const S7CidadeSchema = require('../schema/S7CidadeSchema')

const S7Cidade = mongoose.model('S7Cidade', S7CidadeSchema)

module.exports = S7Cidade