'use strict'

const mongoose = require('mongoose')

const S7EmpresaAlvoSchema = require('../schema/S7EmpresaAlvoSchema')

const S7EmpresaAlvo = mongoose.model('S7EmpresaAlvo', S7EmpresaAlvoSchema)

module.exports = S7EmpresaAlvo