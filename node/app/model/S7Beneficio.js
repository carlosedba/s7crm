'use strict'

const mongoose = require('mongoose')

const S7BeneficioSchema = require('../schema/S7BeneficioSchema')

const S7Beneficio = mongoose.model('S7Beneficio', S7BeneficioSchema)

module.exports = S7Beneficio