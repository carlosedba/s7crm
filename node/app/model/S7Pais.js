'use strict'

const mongoose = require('mongoose')

const S7PaisSchema = require('../schema/S7PaisSchema')

const S7Pais = mongoose.model('S7Pais', S7PaisSchema)

module.exports = S7Pais