'use strict'

const mongoose = require('mongoose')

const S7FormatoSchema = require('../schema/S7FormatoSchema')

const S7Formato = mongoose.model('S7Formato', S7FormatoSchema)

module.exports = S7Formato