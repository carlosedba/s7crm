'use strict'

const mongoose = require('mongoose')

const S7UsuarioSchema = require('../schema/S7UsuarioSchema')

const S7Usuario = mongoose.model('S7Usuario', S7UsuarioSchema)

module.exports = S7Usuario