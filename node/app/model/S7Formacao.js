'use strict'

const mongoose = require('mongoose')

const S7FormacaoSchema = require('../schema/S7FormacaoSchema')

const S7Formacao = mongoose.model('S7Formacao', S7FormacaoSchema)

module.exports = S7Formacao