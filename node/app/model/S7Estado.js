'use strict'

const mongoose = require('mongoose')

const S7EstadoSchema = require('../schema/S7EstadoSchema')

const S7Estado = mongoose.model('S7Estado', S7EstadoSchema)

module.exports = S7Estado