'use strict'

const mongoose = require('mongoose')

const S7IdiomaSchema = require('../schema/S7IdiomaSchema')

const S7Idioma = mongoose.model('S7Idioma', S7IdiomaSchema)

module.exports = S7Idioma