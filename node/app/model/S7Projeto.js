'use strict'

const mongoose = require('mongoose')

const S7ProjetoSchema = require('../schema/S7ProjetoSchema')

const S7Projeto = mongoose.model('S7Projeto', S7ProjetoSchema)

module.exports = S7Projeto