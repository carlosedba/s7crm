'use strict'

const mongoose = require('mongoose')

const S7CategoriaProjetoSchema = require('../schema/S7CategoriaProjetoSchema')

const S7CategoriaProjeto = mongoose.model('S7CategoriaProjeto', S7CategoriaProjetoSchema)

module.exports = S7CategoriaProjeto