'use strict'

const mongoose = require('mongoose')

const S7SetorSchema = require('../schema/S7SetorSchema')

const S7Setor = mongoose.model('S7Setor', S7SetorSchema)

module.exports = S7Setor