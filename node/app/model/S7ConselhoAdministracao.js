'use strict'

const mongoose = require('mongoose')

const S7ConselhoAdministracaoSchema = require('../schema/S7ConselhoAdministracaoSchema')

const S7ConselhoAdministracao = mongoose.model('S7ConselhoAdministracao', S7ConselhoAdministracaoSchema)

module.exports = S7ConselhoAdministracao