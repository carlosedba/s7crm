'use strict'

const mongoose = require('mongoose')

const S7EmpresaSchema = require('../schema/S7EmpresaSchema')

const S7Empresa = mongoose.model('S7Empresa', S7EmpresaSchema)

module.exports = S7Empresa
