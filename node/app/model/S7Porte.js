'use strict'

const mongoose = require('mongoose')

const S7PorteSchema = require('../schema/S7PorteSchema')

const S7Porte = mongoose.model('S7Porte', S7PorteSchema)

module.exports = S7Porte