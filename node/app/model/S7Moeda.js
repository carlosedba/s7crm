'use strict'

const mongoose = require('mongoose')

const S7MoedaSchema = require('../schema/S7MoedaSchema')

const S7Moeda = mongoose.model('S7Moeda', S7MoedaSchema)

module.exports = S7Moeda