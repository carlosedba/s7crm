'use strict'

const mongoose = require('mongoose')

const S7InstituicaoSchema = require('../schema/S7InstituicaoSchema')

const S7Instituicao = mongoose.model('S7Instituicao', S7InstituicaoSchema)

module.exports = S7Instituicao