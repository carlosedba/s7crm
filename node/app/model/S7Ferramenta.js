'use strict'

const mongoose = require('mongoose')

const S7FerramentaSchema = require('../schema/S7FerramentaSchema')

const S7Ferramenta = mongoose.model('S7Ferramenta', S7FerramentaSchema)

module.exports = S7Ferramenta