'use strict'

const mongoose = require('mongoose')

const S7SegmentoSchema = require('../schema/S7SegmentoSchema')

const S7Segmento = mongoose.model('S7Segmento', S7SegmentoSchema)

module.exports = S7Segmento