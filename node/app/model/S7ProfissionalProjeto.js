'use strict'

const mongoose = require('mongoose')

const S7ProfissionalProjetoSchema = require('../schema/S7ProfissionalProjetoSchema')

const S7ProfissionalProjeto = mongoose.model('S7ProfissionalProjeto', S7ProfissionalProjetoSchema)

module.exports = S7ProfissionalProjeto