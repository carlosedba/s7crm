'use strict'

const mongoose = require('mongoose')

const S7ComentarioProfissionalSchema = require('../schema/S7ComentarioProfissionalSchema')

const S7ComentarioProfissional = mongoose.model('S7ComentarioProfissional', S7ComentarioProfissionalSchema)

module.exports = S7ComentarioProfissional