'use strict'

const mongoose = require('mongoose')

const S7CursoSchema = require('../schema/S7CursoSchema')

const S7Curso = mongoose.model('S7Curso', S7CursoSchema)

module.exports = S7Curso