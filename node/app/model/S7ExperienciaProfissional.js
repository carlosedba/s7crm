'use strict'

const mongoose = require('mongoose')

const S7ExperienciaProfissionalSchema = require('../schema/S7ExperienciaProfissionalSchema')

const S7ExperienciaProfissional = mongoose.model('S7ExperienciaProfissional', S7ExperienciaProfissionalSchema)

module.exports = S7ExperienciaProfissional