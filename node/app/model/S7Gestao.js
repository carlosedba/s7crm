'use strict'

const mongoose = require('mongoose')

const S7GestaoSchema = require('../schema/S7GestaoSchema')

const S7Gestao = mongoose.model('S7Gestao', S7GestaoSchema)

module.exports = S7Gestao