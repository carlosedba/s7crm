'use strict'

const mongoose = require('mongoose')

const S7AreaSchema = require('../schema/S7AreaSchema')

const S7Area = mongoose.model('S7Area', S7AreaSchema)

module.exports = S7Area