const core = require('../core')
const globals = require('./globals')
const setup = require('./setup')
const WebRouter = require('./router/WebRouter')
const ApiRouter = require('./router/ApiRouter')

const JWT = require('../core/auth/jwt')
const JWTUnauthorizedMiddleware = require('../core/middleware/JWTUnauthorizedMiddleware')

const jwt = new JWT({
    privateKey: globals.PRIVATE_KEY,
    publicKey: globals.PUBLIC_KEY,
    audience: 's7',
    issuer: 's7'
})

jwt.loadKeys()
  .then(() => {
      const JWTMiddleware = jwt.getMiddleware(undefined, {
          path: [
              '/',
              '/login',
              '/api/s7/token'
          ]
      });

      core({
          port: globals.SERVER_PORT,

          express: {
              corsWhitelist: [
                  'http://localhost:4000',
                  'http://devsinparis.com',
                  'http://devsinparis.com:27200',
                  'http://carlosalmeida.co',
                  'http://carlosalmeida.co:27200',
              ],

              middlewares: [
                  JWTMiddleware,
                  JWTUnauthorizedMiddleware
              ],

              routers: [
                  { baseRoute: '/api', router: ApiRouter, arguments: [jwt] },
                  { baseRoute: '/', router: WebRouter },
              ]
          },

          mongoose: {
              uri: 'mongodb://root:AquelaSenhaMarota@127.0.0.1:27017',
              dbName: 's7crm'
          }
      });

      setup();
  })
  .catch(console.error)
