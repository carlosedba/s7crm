'use strict'

const mongoose = require('mongoose')
const mongooseFuzzySearching = require('mongoose-fuzzy-searching')

const Schema = mongoose.Schema
const ObjectId = Schema.Types.ObjectID

const schema = new Schema({
    segmento:        { type: ObjectId, ref: 'S7Segmento' },
    nome:            String
}, {
    collection: 's7Setores'
})

schema.plugin(mongooseFuzzySearching, { fields: ['nome'] })

module.exports = schema