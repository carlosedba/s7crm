'use strict'

const mongoose = require('mongoose')
const mongooseFuzzySearching = require('mongoose-fuzzy-searching')

const Schema = mongoose.Schema
const ObjectId = Schema.Types.ObjectID

const schema = new Schema({
    pais:                   { type: ObjectId, ref: 'S7Pais' },
    capital:                { type: ObjectId, ref: 'S7Cidade' },
    nome:                   String,
    sigla:                  String,
    slug:                   String
}, {
    collection: 's7Estados'
})

schema.plugin(mongooseFuzzySearching, { fields: ['nome'] })

module.exports = schema