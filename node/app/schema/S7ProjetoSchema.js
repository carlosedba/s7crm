'use strict'

const mongoose = require('mongoose')
const mongooseFuzzySearching = require('mongoose-fuzzy-searching')

const Schema = mongoose.Schema
const ObjectId = Schema.Types.ObjectID
const Mixed = Schema.Types.Mixed

const schema = new Schema({
    divisao:                      String,
    varianteDivisao:              String,

    // S7 Search - Recrutamento e Seleção
    consultorResponsavel:         { type: ObjectId, ref: 'S7Usuario' },
    cliente:                      { type: ObjectId, ref: 'S7Empresa' },
    contatoCliente:               { type: ObjectId, ref: 'S7Profissional' },
    status:                       String,
    tipo:                         String,
    dataAbertura:                 Date,
    dataFechamento:               Date,
    cargo:                        { type: ObjectId, ref: 'S7Cargo' },
    area:                         { type: ObjectId, ref: 'S7Area' },
    honorario:                    Mixed,
    salarioProposto:              Mixed,
    salarioFinal:                 Mixed,
    equipeLiderada:               String,
    nivelEquipe:                  [String],
    missaoCargo:                  String,
    responsabilidadesCargo:       String,

    // S7 Search - Mapeamento de Mercado
    // S7 Search - Pesquisa Salarial
    // S7 Brand
    // S7 Up
    categoria:                    { type: ObjectId, ref: 'S7CategoriaProjeto' },
    titulo:                       String,
    objetivo:                     String,
}, {
    collection: 's7Projetos'
})

schema.plugin(mongooseFuzzySearching, { fields: ['divisao', 'varianteDivisao', 'status', 'tipo', 'titulo'] })

module.exports = schema