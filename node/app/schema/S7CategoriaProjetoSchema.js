'use strict'

const mongoose = require('mongoose')
const mongooseFuzzySearching = require('mongoose-fuzzy-searching')

const Schema = mongoose.Schema

const schema = new Schema({
    divisao: String,
    nome: String,
}, {
    collection: 's7CategoriasProjeto'
})

schema.plugin(mongooseFuzzySearching, { fields: ['nome', 'divisao'] })

module.exports = schema