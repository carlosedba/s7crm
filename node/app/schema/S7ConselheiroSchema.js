'use strict'

const mongoose = require('mongoose')

const Schema = mongoose.Schema
const ObjectId = Schema.Types.ObjectID

module.exports = new Schema({
    empresa:                    { type: ObjectId, ref: 'S7Empresa' },
    profissional:               { type: ObjectId, ref: 'S7Profissional' },
}, {
    collection: 's7Conselheiros'
})