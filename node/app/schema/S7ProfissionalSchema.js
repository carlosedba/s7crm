'use strict'

const mongoose = require('mongoose')
const mongooseFuzzySearching = require('mongoose-fuzzy-searching')

const Schema = mongoose.Schema
const ObjectId = Schema.Types.ObjectID
const Mixed = Schema.Types.Mixed

const schema = new Schema({
    nome:                     String,
    sobrenome:                String,
    iniciais:                 String,
    sexo:                     String,
    deficiente:               String,
    rg:                       String,
    cpf:                      String,
    telefone:                 String,
    celular1:                 String,
    celular2:                 String,
    dataNascimento:           Date,
    estadoCivil:              String,
    empregoConjuge:           String,
    email1:                   String,
    email2:                   String,
    emailComercial:           String,
    skype:                    String,
    linkedin:                 String,
    naturalidade:             { type: ObjectId, ref: 'S7Pais' },
    estadoOrigem:             { type: ObjectId, ref: 'S7Estado' },
    cidadeOrigem:             { type: ObjectId, ref: 'S7Cidade' },
    paisAtual:                { type: ObjectId, ref: 'S7Pais' },
    estadoAtual:              { type: ObjectId, ref: 'S7Estado' },
    cidadeAtual:              { type: ObjectId, ref: 'S7Cidade' },
    filhos:                   [String],
    idiomas:                  [Mixed],
    abordagem:                String,
    comentarios:              String,
}, {
    collection: 's7Profissionais'
})

schema.plugin(mongooseFuzzySearching, { fields: ['nome', 'sobrenome', 'cpf', 'email1', 'email2', 'emailComercial'] })

module.exports = schema