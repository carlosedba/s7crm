'use strict'

const mongoose = require('mongoose')

const Schema = mongoose.Schema

const schema = new Schema({
    nome: String,
    simbolo: String
}, {
    collection: 's7Moedas'
})

module.exports = schema