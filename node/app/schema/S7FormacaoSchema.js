'use strict'

const mongoose = require('mongoose')

const Schema = mongoose.Schema
const ObjectId = Schema.Types.ObjectID

const schema = new Schema({
    profissional:               { type: ObjectId, ref: 'S7Profissional' },
    instituicao:                { type: ObjectId, ref: 'S7Instituicao' },
    curso:                      { type: ObjectId, ref: 'S7Curso' },
    tipo:                       String,
    anoConclusao:               Number
}, {
    collection: 's7Formacoes'
})

module.exports = schema