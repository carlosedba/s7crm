'use strict'

const mongoose = require('mongoose')

const Schema = mongoose.Schema
const ObjectId = Schema.Types.ObjectID

module.exports = new Schema({
    empresa:                    { type: ObjectId, ref: 'S7Empresa' },
    cargo:                      { type: ObjectId, ref: 'S7Cargo' },
    profissional:               { type: ObjectId, ref: 'S7Profissional' },
}, {
    collection: 's7PrincipaisContatos'
})