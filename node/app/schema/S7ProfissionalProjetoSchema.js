'use strict'

const mongoose = require('mongoose')

const Schema = mongoose.Schema
const ObjectId = Schema.Types.ObjectID

module.exports = new Schema({
    projeto:                    { type: ObjectId, ref: 'S7Projeto' },
    profissional:               { type: ObjectId, ref: 'S7Profissional' },
}, {
    collection: 's7ProfissionaisProjeto'
})