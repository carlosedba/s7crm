'use strict'

const mongoose = require('mongoose')

const Schema = mongoose.Schema
const ObjectId = Schema.Types.ObjectID
const Mixed = Schema.Types.Mixed

const schema = new Schema({
    profissional:               { type: ObjectId, ref: 'S7Profissional' },
    empresa:                    { type: ObjectId, ref: 'S7Empresa' },
    cargo:                      { type: ObjectId, ref: 'S7Cargo' },
    area:                       { type: ObjectId, ref: 'S7Area' },
    nivelExperiencia:           String,
    tipoContratacao:            String,
    empregoAtual:               String,
    dataAdmissao:               Date,
    dataDesligamento:           Date,
    beneficios:                 [{ type: ObjectId, ref: 'S7Beneficio' }],
    ferramentas:                [{ type: ObjectId, ref: 'S7Ferramenta' }],
    moeda:                      { type: ObjectId, ref: 'S7Moeda' },
    salario:                    String,
    quantidadeSalarios:         String,
    totalSalario:               String,
    comissao:                   String,
    quantidadeComissao:         String,
    totalComissao:              String,
    bonus:                      String,
    quantidadeBonus:            String,
    totalBonus:                 String,
    plr:                        String,
    quantidadePlr:              String,
    totalPlr:                   String,
}, {
    collection: 's7ExperienciasProfissionais'
})

module.exports = schema