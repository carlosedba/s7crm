'use strict'

const mongoose = require('mongoose')
const mongooseFuzzySearching = require('mongoose-fuzzy-searching')

const Schema = mongoose.Schema
const ObjectId = Schema.Types.ObjectID
const Mixed = Schema.Types.Mixed

const schema = new Schema({
  segmento:                 { type: ObjectId, ref: 'S7Segmento' },
  setor:                    { type: ObjectId, ref: 'S7Setor' },
  nomeFantasia:             String,
  razaoSocial:              String,
  cnpj:                     { type: String, required: true },
  cep:                      String,
  rua:                      String,
  numero:                   String,
  complemento:              String,
  bairro:                   String,
  cidade:                   String,
  estado:                   String,
  pais:                     String,
  telefone:                 String,
  website:                  String,
  faturamento:              Mixed,
  ebitda:                   String,
  porte:                    { type: ObjectId, ref: 'S7Porte' },
  origem:                   { type: ObjectId, ref: 'S7Pais' },
  formato:                  [{ type: ObjectId, ref: 'S7Formato' }],
  gestao:                   { type: ObjectId, ref: 'S7Gestao' },
  conselhoAdministracao:    { type: ObjectId, ref: 'S7ConselhoAdministracao' },
  headCount:                Number,
  descricao:                String,
  conselheiros:             [{ type: ObjectId, ref: 'S7Profissional' }],
  regionais:                [Mixed]
}, {
  collection: 's7Empresas'
})

schema.plugin(mongooseFuzzySearching, { fields: ['nomeFantasia', 'razaoSocial', 'cnpj'] })

module.exports = schema