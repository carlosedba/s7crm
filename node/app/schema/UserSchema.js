'use strict';

const Promise = require('bluebird')
const bcrypt = Promise.promisifyAll(require('bcrypt'))
const mongoose = require('mongoose')
const mongooseFuzzySearching = require('mongoose-fuzzy-searching')

const hidden = ['password']

const schema = new mongoose.Schema({
    firstName:      { type: String, required: true },
    lastName:       { type: String },
    email:          { type: String, required: true },
    password:       { type: String, required: true },
    picture:        { type: String },
}, {
    collection: 'users'
})

schema.plugin(mongooseFuzzySearching, { fields: ['firstName', 'lastName', 'email'] })

schema.static({
    getHiddenFieldsProjection: function () {
        let obj = {}

        for (const fieldName of hidden) {
            obj[fieldName] = 0
        }

        return obj
    },

    hashPassword: async function (plainText) {
        const saltRounds = 10;
        return await bcrypt.hashAsync(plainText, saltRounds).catch(console.error);
    }
})

module.exports = schema