'use strict'

const mongoose = require('mongoose')
const mongooseFuzzySearching = require('mongoose-fuzzy-searching')

const Schema = mongoose.Schema

const schema = new Schema({
    nome: String
}, {
    collection: 's7Areas'
})

schema.plugin(mongooseFuzzySearching, { fields: ['nome'] })

module.exports = schema