'use strict'

const mongoose = require('mongoose')
const mongooseFuzzySearching = require('mongoose-fuzzy-searching')

const Schema = mongoose.Schema
const ObjectId = Schema.Types.ObjectID

const schema = new Schema({
    nome:               String,
    sigla:              String,
    estado:             { type: ObjectId, ref: 'S7Estado' },
}, {
    collection: 's7Instituicoes'
})

schema.plugin(mongooseFuzzySearching, { fields: ['nome', 'sigla'] })

module.exports = schema