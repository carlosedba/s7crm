'use strict'

const mongoose = require('mongoose')

const Schema = mongoose.Schema
const ObjectId = Schema.Types.ObjectID

module.exports = new Schema({
    projeto:                    { type: ObjectId, ref: 'S7Projeto' },
    empresa:                    { type: ObjectId, ref: 'S7Empresa' },
}, {
    collection: 's7EmpresasAlvo'
})