const Promise = require('bluebird')
const bcrypt = Promise.promisifyAll(require('bcrypt'))

const extendObject = require('../../core/extendObject')
const BaseModel = require('./BaseModel')

const User = function () {
  console.log('log > User > constructor called!')

  this.fields = {}
  this.fillable = ['firstName', 'lastName', 'email', 'password', 'picture']
  this.hidden = ['password']
}

User.prototype.setPassword = async function (value) {
  console.log('log > User > setPassword called!')

  const saltRounds = 10;
  return await bcrypt.hashAsync(value, saltRounds).catch(console.error);
}

extendObject(BaseModel, User)

module.exports = User