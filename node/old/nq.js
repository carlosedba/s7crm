const Knex = require('knex')
const knexConfig = require('./knexfile')
const { Model } = require('objection')
const nanoid = require('nanoid')

const knex = Knex(knexConfig.development)

Model.knex(knex)

const S7Segmento = require('./core/model/s7Segmento')
const S7Setor = require('./model/S7Setor')
const S7Cargo = require('./model/S7Cargo')
const S7Beneficio = require('./model/S7Beneficio')
const S7Idioma = require('./model/S7Idioma')
const S7Instituicao = require('./model/S7Instituicao')
const S7Curso = require('./model/S7Curso')
const S7Empresa = require('./model/S7Empresa')
const S7PrincipalContato = require('./model/S7PrincipalContato')
const S7Profissional = require('./model/S7Profissional')
const S7ExperienciaProfissional = require('./model/S7ExperienciaProfissional')
const S7Formacao = require('./model/S7Formacao')
const S7ComentarioProfissional = require('./model/S7ComentarioProfissional')
const S7TipoProjeto = require('./core/model/s7TipoProjeto')
const S7Projeto = require('./model/S7Projeto')
const S7ProfissionalSelecionado = require('./model/S7ProfissionalSelecionado')

const models = {
  S7Segmento,
  S7Setor,
  S7Cargo,
  S7Beneficio,
  S7Idioma,
  S7Instituicao,
  S7Curso,
  S7Empresa,
  S7PrincipalContato,
  S7Profissional,
  S7ExperienciaProfissional,
  S7Formacao,
  S7ComentarioProfissional,
  S7TipoProjeto,
  S7Projeto,
  S7ProfissionalSelecionado,
}

let cache = {}

const payload = require('../payloads/teste3.json')

function wasProcessedSuccessfully(id) {
  for (let i = 0; i < cache.processed.length; i++) {
    if (cache.processed[i] === id) return i
  }

  return false
}

function getReferenceIdByIndex(index) {
  return cache.reference[index][1].id
}

function mergeObjects(item) {
  return Object.assign({}, item.object.raw, item.object.computed)
}

function generateId(item) {
  if (item.id === null || item.id === undefined) {
    return Object.assign({}, item, { id: nanoid(12) })
  }
}

function associate(item, data) {
  if (item.dependsOn) {
    item.dependsOn.forEach((dep) => {
      const index = wasProcessedSuccessfully(dep.id)

      if (index !== false) {
        data[dep.key] = getReferenceIdByIndex(index)
      }
    })
  }
}

function arrayFilter(obj, key) {
  if (Array.isArray(obj[key])) {
    obj[key] = JSON.stringify(obj[key])
  }
}

function labelValueFilter(obj, key) {
  if (typeof obj[key] === 'object') {
    if (obj[key].label) {
      obj[key] = obj[key].value
    }
  }
}

function currencyFilter(obj, key) {
  if (typeof obj[key] === 'object') {
    if (obj[key].currency) {
      obj[key] = JSON.stringify(obj[key])
    }
  }
}

async function executeAction(item, data) {
  let model = models[item.model]
  let query

  switch (item.action) {
    case 'create':
      query = await model.query().insert(data).catch(catchHandler.bind(null, item))
      break

    case 'update':
      break
      
    case 'delete':
      break
  }

  return query
}

function addToCache(item, result) {
  cache.processed.push(item.id)
  cache.reference.push([item.id, result])
}

function catchHandler(item, err) {
  addErrorToCache(item, err)
}

function addErrorToCache(item, err) {
  cache.error.push([item.id, err])
}

function initializeCache() {
  return {
    processed: [],
    reference: [],
    error: []
  }
}

;(async function () {
  cache = initializeCache()

  for (let item of payload) {
    if (!item.dependsOn) {
      let data = generateId(mergeObjects(item))

      for (let key of Object.keys(data)) {
        arrayFilter(data, key)
        labelValueFilter(data, key)
        currencyFilter(data, key)
      }

      const result = await executeAction(item, data)
      if (result) addToCache(item, result)
    }
  }

  for (let item of payload) {
    if (item.dependsOn) {
      let data = generateId(mergeObjects(item))

      for (let key of Object.keys(data)) {
        arrayFilter(data, key)
        labelValueFilter(data, key)
        currencyFilter(data, key)
      }

      associate(item, data)

      const result = await executeAction(item, data)
      if (result) addToCache(item, result)
    }
  }

  if (cache.error.length) {
    console.log('errors found', cache.error)
  } else {
    console.log('success')
  }
})()

