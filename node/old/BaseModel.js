const v = require('voca')

const AsyncFunction = (async () => {}).constructor

const BaseModel = function () {
  console.log('log > BaseModel > constructor called!')

  this.fields = {}
  this.fillable = []
  this.hidden = []
}

BaseModel.prototype.getField = function (fieldName) {
  return this.fields[fieldName]
}

BaseModel.prototype.getFields = function () {
  return this.fields
}

BaseModel.prototype.hasSetMutator = function (fieldName) {
  const mutator = this['set' + v.capitalize(fieldName)]

  if (mutator) return true

  return false
}

BaseModel.prototype.getSetMutator = function (fieldName) {
  const mutator = this['set' + v.capitalize(fieldName)]

  if (mutator) return mutator

  return null
}

BaseModel.prototype.setMutatedFieldValue = function (fieldName, fieldValue) {
  return new Promise(async (resolve, reject) => {
    const mutator = this.getSetMutator(fieldName)

    if (mutator) {
      if (mutator instanceof AsyncFunction) {
        ;(async () => {
          this.fields[fieldName] = await mutator(fieldValue)
          resolve()
        })();
      } else {
        this.fields[fieldName] = mutator(fieldValue)
        resolve()
      }
    } else {
      reject()
    }
  })
}

BaseModel.prototype.getFillable = function () {
  return this.fillable
}

BaseModel.prototype.getHidden = function () {
  return this.hidden
}

BaseModel.prototype.get = function () {}

BaseModel.prototype.fill = function (data) {
  console.log('log > BaseModel > create called!')

  return new Promise(async (resolve, reject) => {
    for (const fieldName of this.fillable) {
      if (data[fieldName]) {
        if (this.hasSetMutator(fieldName)) {
          await this.setMutatedFieldValue(fieldName, data[fieldName]).catch(console.error)
        } else {
          this.fields[fieldName] = data[fieldName]
        }
      }
    }

    resolve()
  })
}

module.exports = BaseModel