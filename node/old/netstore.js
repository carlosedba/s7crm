'use strict';

class NetStore {
  constructor(io) {
    this.setNamespace(io)
  }

  setNamespace(io) {
    this.io = io.of('/netstore')

    this.io.on('connection', (socket) => {
      this.onClientRequest(socket)
    })
  }

  onClientRequest(socket) {
    socket.on('cl_request', (payload) => {
      console.log(payload)
    })
  }
}

module.exports = NetStore
