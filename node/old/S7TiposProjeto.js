'use strict';
                                                       
const nanoid = require('nanoid')
const S7TipoProjeto = require('../model/S7TipoProjeto')

class S7TiposProjeto {
  static async findAll(req, res) {
    const pais = req.query.pais

    let query

    if (!query) query = await S7TipoProjeto.query()

    res.send(query)
  }

  static async findOneById(req, res) {
    const id = req.params.id

    const query = await S7TipoProjeto.query().findById(id)
    
    res.send(query)
  }

  static async create(req, res) {
    const data = req.body
    
    const query = await S7TipoProjeto.query().insert(Object.assign({}, data))
    
    if (query) res.sendStatus(201)
    else res.sendStatus(400)
  }

  static async update(req, res) {
    const id = req.params.id
    const data = req.body
    
    const query = await S7TipoProjeto.query().findById(id).patch(data)

    if (query) res.sendStatus(201)
    else res.sendStatus(400)
  }

  static async delete(req, res) {
    const id = req.params.id
    
    const query = await S7TipoProjeto.query().deleteById(id)

    if (query) res.sendStatus(200)
    else res.sendStatus(400)
  }
}

module.exports = S7TiposProjeto

