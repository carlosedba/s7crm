¼'use strict'

const fs = require('fs')
const expressJwt = require('express-jwt')
const jwt = require('jsonwebtoken')
const nanoid = require('nanoid')
const bcrypt = require('bcrypt')

const User = require('./model/User')

const S7Paises = require('./repository/S7Paises')
const S7Estados = require('./repository/S7Estados')
const S7Cidades = require('./repository/S7Cidades')
const S7Segmentos = require('./repository/S7Segmentos')
const S7Setores = require('./repository/S7Setores')
const S7Cargos = require('./repository/S7Cargos')
const S7Beneficios = require('./repository/S7Beneficios')
const S7Idiomas = require('./repository/S7Idiomas')
const S7Instituicoes = require('./repository/S7Instituicoes')
const S7Cursos = require('./repository/S7Cursos')
const S7Empresas = require('./repository/S7Empresas')
const S7PrincipaisContatos = require('./repository/S7PrincipaisContatos')
const S7Profissionais = require('./repository/S7Profissionais')
const S7ExperienciasProfissionais = require('./repository/S7ExperienciasProfissionais')
const S7Formacoes = require('./repository/S7Formacoes')
const S7ComentariosProfissional = require('./repository/S7ComentariosProfissional')
const S7TiposProjeto = require('./repository/S7TiposProjeto')
const S7Projetos = require('./repository/S7Projetos')
const S7ProfissionaisSelecionados = require('./repository/S7ProfissionaisSelecionados')
const Users = require('./repository/Users')

const netqueue = require('./netqueue')

module.exports = router => {
  const publicKey = fs.readFileSync('./keys/S7_PEM_RSA256_29062019.pub');

  router.use(expressJwt({
    secret: publicKey,
    audience: 'server/repository',
    issuer: 'server'
  }).unless({
    path: [
      '/',
      '/repository/s7/token'
    ]
  }))

  router.get('/', (req, res) => {
    res.send('leave')
  })

  router.post('/repository/s7/netqueue/proccess', netqueue.proccess)

  router.delete('/repository/s7/cidades/:id', S7Cidades.delete)
  router.put('/repository/s7/cidades/:id', S7Cidades.update)
  router.get('/repository/s7/cidades/:id', S7Cidades.findOneById)
  router.post('/repository/s7/cidades', S7Cidades.create)
  router.get('/repository/s7/cidades', S7Cidades.findAll)

  router.delete('/repository/s7/estados/:id', S7Estados.delete)
  router.put('/repository/s7/estados/:id', S7Estados.update)
  router.get('/repository/s7/estados/:id', S7Estados.findOneById)
  router.post('/repository/s7/estados', S7Estados.create)
  router.get('/repository/s7/estados', S7Estados.findAll)

  router.delete('/repository/s7/paises/:id', S7Paises.delete)
  router.put('/repository/s7/paises/:id', S7Paises.update)
  router.get('/repository/s7/paises/:id', S7Paises.findOneById)
  router.post('/repository/s7/paises', S7Paises.create)
  router.get('/repository/s7/paises', S7Paises.findAll)

  router.get('/repository/s7/segmentos/autocomplete', S7Segmentos.autocomplete)
  router.get('/repository/s7/segmentos/suggestions', S7Segmentos.suggestions)
  router.delete('/repository/s7/segmentos/:id', S7Segmentos.delete)
  router.put('/repository/s7/segmentos/:id', S7Segmentos.update)
  router.get('/repository/s7/segmentos/:id', S7Segmentos.findOneById)
  router.post('/repository/s7/segmentos', S7Segmentos.create)
  router.get('/repository/s7/segmentos', S7Segmentos.findAll)

  router.get('/repository/s7/setores/suggestions', S7Setores.suggestions)
  router.delete('/repository/s7/setores/:id', S7Setores.delete)
  router.put('/repository/s7/setores/:id', S7Setores.update)
  router.get('/repository/s7/setores/:id', S7Setores.findOneById)
  router.post('/repository/s7/setores', S7Setores.create)
  router.get('/repository/s7/setores', S7Setores.findAll)

  router.get('/repository/s7/cargos/suggestions', S7Cargos.suggestions)
  router.delete('/repository/s7/cargos/:id', S7Cargos.delete)
  router.put('/repository/s7/cargos/:id', S7Cargos.update)
  router.get('/repository/s7/cargos/:id', S7Cargos.findOneById)
  router.post('/repository/s7/cargos', S7Cargos.create)
  router.get('/repository/s7/cargos', S7Cargos.findAll)

  router.delete('/repository/s7/beneficios/:id', S7Beneficios.delete)
  router.put('/repository/s7/beneficios/:id', S7Beneficios.update)
  router.get('/repository/s7/beneficios/:id', S7Beneficios.findOneById)
  router.post('/repository/s7/beneficios', S7Beneficios.create)
  router.get('/repository/s7/beneficios', S7Beneficios.findAll)

  router.delete('/repository/s7/idiomas/:id', S7Idiomas.delete)
  router.put('/repository/s7/idiomas/:id', S7Idiomas.update)
  router.get('/repository/s7/idiomas/:id', S7Idiomas.findOneById)
  router.post('/repository/s7/idiomas', S7Idiomas.create)
  router.get('/repository/s7/idiomas', S7Idiomas.findAll)

  router.get('/repository/s7/instituicoes/suggestions', S7Instituicoes.suggestions)
  router.delete('/repository/s7/instituicoes/:id', S7Instituicoes.delete)
  router.put('/repository/s7/instituicoes/:id', S7Instituicoes.update)
  router.get('/repository/s7/instituicoes/:id', S7Instituicoes.findOneById)
  router.post('/repository/s7/instituicoes', S7Instituicoes.create)
  router.get('/repository/s7/instituicoes', S7Instituicoes.findAll)

  router.get('/repository/s7/cursos/suggestions', S7Cursos.suggestions)
  router.delete('/repository/s7/cursos/:id', S7Cursos.delete)
  router.put('/repository/s7/cursos/:id', S7Cursos.update)
  router.get('/repository/s7/cursos/:id', S7Cursos.findOneById)
  router.post('/repository/s7/cursos', S7Cursos.create)
  router.get('/repository/s7/cursos', S7Cursos.findAll)

  router.get('/repository/s7/empresas/suggestions', S7Empresas.suggestions)
  router.delete('/repository/s7/empresas/:id', S7Empresas.delete)
  router.put('/repository/s7/empresas/:id', S7Empresas.update)
  router.get('/repository/s7/empresas/:id', S7Empresas.findOneById)
  router.post('/repository/s7/empresas', S7Empresas.create)
  router.get('/repository/s7/empresas', S7Empresas.findAll)

  router.delete('/repository/s7/principais-contatos/:id', S7PrincipaisContatos.delete)
  router.put('/repository/s7/principais-contatos/:id', S7PrincipaisContatos.update)
  router.get('/repository/s7/principais-contatos/:id', S7PrincipaisContatos.findOneById)
  router.post('/repository/s7/principais-contatos', S7PrincipaisContatos.create)
  router.get('/repository/s7/principais-contatos', S7PrincipaisContatos.findAll)

  router.delete('/repository/s7/profissionais/:id', S7Profissionais.delete)
  router.put('/repository/s7/profissionais/:id', S7Profissionais.update)
  router.get('/repository/s7/profissionais/:id', S7Profissionais.findOneById)
  router.post('/repository/s7/profissionais', S7Profissionais.create)
  router.get('/repository/s7/profissionais', S7Profissionais.findAll)

  router.delete('/repository/s7/experiencias-profissionais/:id', S7ExperienciasProfissionais.delete)
  router.put('/repository/s7/experiencias-profissionais/:id', S7ExperienciasProfissionais.update)
  router.get('/repository/s7/experiencias-profissionais/:id', S7ExperienciasProfissionais.findOneById)
  router.post('/repository/s7/experiencias-profissionais', S7ExperienciasProfissionais.create)
  router.get('/repository/s7/experiencias-profissionais', S7ExperienciasProfissionais.findAll)

  router.delete('/repository/s7/formacoes/:id', S7Formacoes.delete)
  router.put('/repository/s7/formacoes/:id', S7Formacoes.update)
  router.get('/repository/s7/formacoes/:id', S7Formacoes.findOneById)
  router.post('/repository/s7/formacoes', S7Formacoes.create)
  router.get('/repository/s7/formacoes', S7Formacoes.findAll)

  router.delete('/repository/s7/comentarios-profissional/:id', S7ComentariosProfissional.delete)
  router.put('/repository/s7/comentarios-profissional/:id', S7ComentariosProfissional.update)
  router.get('/repository/s7/comentarios-profissional/:id', S7ComentariosProfissional.findOneById)
  router.post('/repository/s7/comentarios-profissional', S7ComentariosProfissional.create)
  router.get('/repository/s7/comentarios-profissional', S7ComentariosProfissional.findAll)

  router.delete('/repository/s7/tipos-projeto/:id', S7TiposProjeto.delete)
  router.put('/repository/s7/tipos-projeto/:id', S7TiposProjeto.update)
  router.get('/repository/s7/tipos-projeto/:id', S7TiposProjeto.findOneById)
  router.post('/repository/s7/tipos-projeto', S7TiposProjeto.create)
  router.get('/repository/s7/tipos-projeto', S7TiposProjeto.findAll)

  router.delete('/repository/s7/projetos/:id', S7Projetos.delete)
  router.put('/repository/s7/projetos/:id', S7Projetos.update)
  router.get('/repository/s7/projetos/:id', S7Projetos.findOneById)
  router.post('/repository/s7/projetos', S7Projetos.create)
  router.get('/repository/s7/projetos', S7Projetos.findAll)

  router.delete('/repository/s7/profissionais-selecionados/:id', S7ProfissionaisSelecionados.delete)
  router.put('/repository/s7/profissionais-selecionados/:id', S7ProfissionaisSelecionados.update)
  router.get('/repository/s7/profissionais-selecionados/:id', S7ProfissionaisSelecionados.findOneById)
  router.post('/repository/s7/profissionais-selecionados', S7ProfissionaisSelecionados.create)
  router.get('/repository/s7/profissionais-selecionados', S7ProfissionaisSelecionados.findAll)

  router.delete('/repository/s7/users/:id', Users.delete)
  router.put('/repository/s7/users/:id', Users.update)
  router.get('/repository/s7/users/:id', Users.findOneById)
  router.post('/repository/s7/users', Users.create)
  router.get('/repository/s7/users', Users.findAll)

  router.post('/repository/s7/token', async (req, res) => {
    const data = req.body

    if (data.email && data.password) {
      const query = await User.query().findOne('email', data.email)

      if (query) {
        bcrypt.compare(data.password, query.password, (err, matches) => {
          if (matches) {
            const privateKey = fs.readFileSync('./keys/S7_PEM_RSA256_29062019')
            const payload = query.toJSON()

            const token = jwt.sign(payload, privateKey, {
              algorithm: 'RS256',
              expiresIn: '5d',
              notBefore: '120ms',
              audience: 'server/repository',
              issuer: 'server',
              jwtid: nanoid(12),
              subject: query.id
            })

            res.json({ token: token })
          } else {
            res.json({
              token: null,
              error: {
                message: 'Wrong credentials.'
              }
            })
          }
        })
      } else {
        res.json({
          token: null,
          error: {
            message: 'Wrong credentials.'
          }
        })
      }
    }    
  })
    
  router.post('/repository/s7/token/validate', async (req, res) => {
    const data = req.body

    if (data.token) {
      const privateKey = fs.readFileSync('./keys/S7_PEM_RSA256_29062019')
      
      jwt.verify(data.token, publicKey, {
        algorithms: ['RS256'],
        audience: 'server/repository',
        issuer: 'server'
      }, (err, decoded) => {
        if (err) {
          res.json({
            token: null,
            error: {
              message: 'Invalid token.'
            }
          })
        } else {
          res.json({ token: data.token })
        }
      })
      
    } else {
      res.json({
        token: null,
        error: {
          message: 'Invalid token.'
        }
      })
    } 
  })
    
  router.post('/repository/s7/token/renew', async (req, res) => {
    const data = req.body

    if (data.token) {
      const privateKey = fs.readFileSync('./keys/S7_PEM_RSA256_29062019')
      const payload = jwt.verify(data.token, publicKey, {
        algorithms: ['RS256'],
        audience: 'server/repository',
        issuer: 'server'
      })

      delete payload.iat
      delete payload.exp
      delete payload.nbf
      delete payload.jti

      const token = jwt.sign(payload, privateKey, {
        algorithm: 'RS256',
        expiresIn: '5d',
        notBefore: '120ms',
        jwtid: nanoid(12),
      })
      
      res.json({ token: token })
    } else {
      res.json({
        token: null,
        error: {
          message: 'Invalid token.'
        }
      })
    } 
  })
}


// The error returned by this function is handled in the error handler middleware in app.js.
function createStatusCodeError(statusCode) {
  return Object.assign(new Error(), {
    statusCode
  })
}
