'use strict';

const nanoid = require('nanoid')
const S7Segmento = require('../model/S7Segmento')

class S7Segmentos {
  /*
  static async autocomplete(req, res) {
    const str = req.query.str

    const query = await S7Segmento.query().where('nome', 'like', `%${str}%`)

    res.send(query)
  }
  
  static async suggestions(req, res) {
    const str = req.query.str

    const query = await S7Segmento.query().where('nome', 'like', `%${str}%`)

    res.send(query)
  }
  */
  
  static async findAll(req, res) {
    const query = await S7Segmento.query()

    res.send(query)
  }

  static async findOneById(req, res) {
    const id = req.params.id

    const query = await S7Segmento.query().findById(id)
    
    res.send(query)
  }

  static async create(req, res) {
    const data = req.body
    
    const query = await S7Segmento.query().insert(Object.assign({}, data, {
      id: nanoid(12)
    }))
    
    if (query) res.sendStatus(201)
    else res.sendStatus(400)
  }

  static async update(req, res) {
    const id = req.params.id
    const data = req.body
    
    const query = await S7Segmento.query().findById(id).patch(data)

    if (query) res.sendStatus(201)
    else res.sendStatus(400)
  }

  static async delete(req, res) {
    const id = req.params.id
    
    const query = await S7Segmento.query().deleteById(id)

    if (query) res.sendStatus(200)
    else res.sendStatus(400)
  }
}

module.exports = S7Segmentos
