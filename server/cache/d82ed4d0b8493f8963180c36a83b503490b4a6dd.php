<?php /* /usr/share/nginx/html/hmi/server/public_html/themes/hmi/eventos.blade.php */ ?>
<?php $__env->startSection('title'); ?>
  Eventos | HMI - Honoris Mérito Institute
<?php $__env->stopSection(); ?>

<?php $__env->startSection('description'); ?>
  
<?php $__env->stopSection(); ?>

<?php $__env->startSection('vendor-css'); ?>
  ##parent-placeholder-97688f63ed1a87ba587e78933c42edf42ecae775##
<?php $__env->stopSection(); ?>

<?php $__env->startSection('application-css'); ?>
  ##parent-placeholder-b51d72c3ca446ab0f6f653f45ff8b7eb92a61211##
<?php $__env->stopSection(); ?>

<?php $__env->startSection('fonts'); ?>
  ##parent-placeholder-04d3b602cdc8d51e1a3bb4d03f7dab96a9ec37e5##
<?php $__env->stopSection(); ?>

<?php $__env->startSection('content'); ?>
  ##parent-placeholder-040f06fd774092478d450774f5ba30c5da78acc8##

  <section class="section section-eight">
    <div class="section-header" style="background-image: url('<?php echo GuzzleHttp\Psr7\UriNormalizer::normalize(new GuzzleHttp\Psr7\Uri('http://localhost:8080/hmi/server/public_html' . '/themes/' . 'hmi' . '/' . 'resources/img/helloquence-51716-unsplash-x1280.jpg')); ?>');">
      <p class="section-title">Eventos</p>
    </div>
    <div class="section-wrapper">
      <div class="content">
        <?php $__currentLoopData = $events; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $event): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
          <div class="component-event event-alpha" data-id="<?php echo e($event['id']); ?>">
            <div class="event-row">
              <div class="left">
                <div class="event-date"><?php echo $event['date']; ?></div>
                <div class="event-info">
                  <p class="event-title"><?php echo e($event['title']); ?></p>
                  <p class="event-text"><?php echo e($event['description']); ?></p>
                </div>
                <div class="event-info">
                  <p class="event-datetime">Local: <?php echo e($event['location']); ?></p>
                  <p class="event-datetime">Horário: <?php echo e($event['start_time']); ?> às <?php echo e($event['end_time']); ?></p>
                </div>
              </div>
              <div class="right">
                <a href="<?php echo e($event['url']); ?>" class="btn btn-event">Inscreva-se</a>
              </div>
            </div>
            <div class="event-row">
              <div class="event-content"><?php echo $event['content']; ?></div>
            </div>
          </div>
        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
      </div>
    </div>
  </section>

  <section id="patrocinadores" class="section white section-eleven">
    <div class="row no-pad">
      <div class="row-titles">
        <p class="row-title">Patrocinadores</p>
      </div>
      <div class="content">
        <?php $__currentLoopData = $patrocinadores; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $patrocinador): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
          <div class="component-patrocinador">
            <div class="patrocinador-logo" style="background-image: url('<?php echo GuzzleHttp\Psr7\UriNormalizer::normalize(new GuzzleHttp\Psr7\Uri('http://localhost:8080/hmi/server/public_html' . '/data/content_data/' . $patrocinador['data']['picture']->filename)); ?>');"></div>
          </div>
        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
      </div>
    </div>
  </section>

  <div class="modal micromodal-slide" id="event-subscription-modal" aria-hidden="true">
    <div class="modal__overlay" tabindex="-1" data-micromodal-close>
      <div class="modal__container" role="dialog" aria-modal="true" aria-labelledby="">
        <header class="modal__header">
          <div class="modal__titles">
            <h2 class="modal__title">
              Formulário de inscrição
            </h2>
            <p class="modal__description">Preencha os campos para inscrever-se no evento: <br><strong class="modal__event-name"></strong></p>
          </div>
          <button class="modal__close" aria-label="Close modal" data-micromodal-close></button>
        </header>
        <main class="modal__content">
          <form method="POST" action="/subscribe">
            <div class="inputs">
              <div class="input w3-q1">
                <label>Nome:</label>
                <input type="text" name="name" required>
              </div>
              <div class="input w3-q1">
                <label>E-mail:</label>
                <input type="email" name="email" required>
              </div>
            </div>
          </form>
        </main>
        <footer class="modal__footer">
          <button class="modal__btn modal__btn-primary">Enviar</button>
        </footer>
      </div>
    </div>
  </div>

<?php $__env->stopSection(); ?>

<?php $__env->startSection('footer-sections'); ?>
  ##parent-placeholder-2b652bef4ae9be2d3a3fe2f169e6a106e1d3faee##
<?php $__env->stopSection(); ?>

<?php $__env->startSection('scripts'); ?>
  ##parent-placeholder-16728d18790deb58b3b8c1df74f06e536b532695##
  <script type="text/javascript" src="<?php echo GuzzleHttp\Psr7\UriNormalizer::normalize(new GuzzleHttp\Psr7\Uri('http://localhost:8080/hmi/server/public_html' . '/themes/' . 'hmi' . '/' . 'vendor/flickity/flickity.pkgd.min.js')); ?>"></script>
  <script type="text/javascript" src="<?php echo GuzzleHttp\Psr7\UriNormalizer::normalize(new GuzzleHttp\Psr7\Uri('http://localhost:8080/hmi/server/public_html' . '/themes/' . 'hmi' . '/' . 'resources/js/EventSubscription.js')); ?>"></script>
  <script type="text/javascript">
    var secPatrocinadores = document.querySelector('#patrocinadores .content')
    var cases = new Flickity(secPatrocinadores, {
      cellAlign: 'center',
      wrapAround: false,
      freeScroll: true,
      contain: true,
      autoPlay: true,
      prevNextButtons: false,
    })
  </script>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.default', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>