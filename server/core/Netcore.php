<?php
namespace Rise;

use \ElephantIO\Client;
use \ElephantIO\Engine\SocketIO\Version2X;

class Netcore extends \Slim\App
{ 
  protected static $container;
  protected static $socket;

  public function __construct($container = [])
  {
    parent::__construct($container);
    self::$container = $container;
    $this->connectToWSServer($container);
    $this->setWSErrorHandler($container);
  }

  static public function socket()
  {
    if (self::$container->get('settings')->get('connectToWSServer')) {
      return self::$socket;
    } else {
      return false;
    }
  }

  public function connectToWSServer($container)
  {
    if ($container->get('settings')->get('connectToWSServer')) {
      $server = $container->get('settings')->get('webSocketsServer');
      self::$socket = new Client(new Version2X($server));
    }
  }

  public function setWSErrorHandler($container)
  {
    if ($container->get('settings')->get('sendErrorToWSServer') && self::socket()) {
      set_error_handler(array($this, 'errorHandler'));
      register_shutdown_function(array($this, 'fatalErrorHandler'));
    }
  }

  public function errorHandler($errno, $errstr, $errfile, $errline)
  {
    if ($socket = self::socket()) {
      $socket->initialize();
        $socket->emit('rm_log', [
            'type' => 'error',
            'data' => [
              'errno' => $errno,
              'errstr' => $errstr,
              'errfile' => $errfile,
              'errline' => $errline,
            ],
            'datetime' => date('d/m/Y H:i:s'),
        ]);
      $socket->close();

      return true; // cut off php default error handler
    }
  }

  public function fatalErrorHandler()
  {
    if ($socket = self::socket()) {
      $error = error_get_last();

      if ($error["type"] == E_ERROR) {
        $socket = self::socket();

        $socket->initialize();
          $socket->emit('rm_log', [
              'type' => 'error',
              'data' => [
                'errno' => $error["type"],
                'errstr' => $error["message"],
                'errfile' => $error["file"],
                'errline' => $error["line"],
              ],
              'datetime' => date('d/m/Y H:i:s'),
          ]);
        $socket->close();
      }
    }
  }
}
?>
