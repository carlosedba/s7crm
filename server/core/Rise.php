<?php
namespace Rise;

require_once('Bootstrap.php');

use \PHPMailer;
use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;
use Rise\Utils\IdGenerator;
use Rise\Auth\JWT;
use Rise\Models\User;

$container = new Container($config);
$net = new Netcore($container);

$net->add(new JWT([
  "secure" => false,
  "header" => "Authorization",
  "signature" => "RSA",
  "algorithm" => "SHA256",
  "key" => PUBLIC_KEY,
  "path" => ["/admin", "/api"],
  "ignore" => ["/api/v1/token", "/email/contato"],
  "callback" => function ($request, $response, $args) use ($container) {
    $container->set('token', $args['token']);
    return $response;
  },
  "error" => function ($request, $response, $args) {
    return $response;
  }
]));  


/* ****************************** */
/* ************ API ************* */
/* ****************************** */

$net->group('/api/s7', function () {
  $this->delete('/beneficios/delete/{id}[/]',      constant('NAMESPACE') . '\Api\S7Beneficios:delete');
  $this->post('/beneficios/update/{id}[/]',        constant('NAMESPACE') . '\Api\S7Beneficios:update');
  $this->post('/beneficios/create[/]',             constant('NAMESPACE') . '\Api\S7Beneficios:create');
  $this->get('/beneficios/email/{email}[/]',       constant('NAMESPACE') . '\Api\S7Beneficios:findOne');
  $this->get('/beneficios/{id}[/]',                constant('NAMESPACE') . '\Api\S7Beneficios:findOneById');
  $this->get('/beneficios[/]',                     constant('NAMESPACE') . '\Api\S7Beneficios:findAll');
});

$net->group('/api/v1', function () {
  $this->delete('/users/delete/{id}[/]',      constant('NAMESPACE') . '\Api\Users:delete');
  $this->post('/users/update/{id}[/]',        constant('NAMESPACE') . '\Api\Users:update');
  $this->post('/users/create[/]',             constant('NAMESPACE') . '\Api\Users:create');
  $this->get('/users/email/{email}[/]',       constant('NAMESPACE') . '\Api\Users:findOne');
  $this->get('/users/{id}[/]',                constant('NAMESPACE') . '\Api\Users:findOneById');
  $this->get('/users[/]',                     constant('NAMESPACE') . '\Api\Users:findAll');

  $this->delete('/config/delete/{id}[/]',     constant('NAMESPACE') . '\Api\Config:delete');
  $this->post('/config/update/{id}[/]',       constant('NAMESPACE') . '\Api\Config:update');
  $this->post('/config/create[/]',            constant('NAMESPACE') . '\Api\Config:create');  
  $this->get('/config/{id}[/]',               constant('NAMESPACE') . '\Api\Config:findOneById');
  $this->get('/config[/]',                    constant('NAMESPACE') . '\Api\Config:findAll');
});

$net->post('/api/v1/token', function (Request $request, Response $response, $args) {
  $data = $request->getParsedBody();
  $email = $data['email'];
  $password = $data['password'];
  $factory = Model::factory('User');

  if ($user = $factory->where('email', $email)->findOne()) {
    if ($user->password == hash('sha256', $password)) {
      $token = JWT::generate('RSA', 'SHA256', PRIVATE_KEY, [
        'id' => $user->id,
        'first_name' => $user->first_name,
        'last_name' => $user->last_name,
        'email' => $user->email,
        'picture' => $user->picture,
      ])->__toString();
      $json = json_encode(array('token' => $token));
      $response->getBody()->write($json);
    } else {
      $json = json_encode(array(
        "error" => [
          "code" => 1001,
          "message" => "Invalid password."
        ]
      ));

      $response = $response->withStatus(401);
      $response->getBody()->write($json);
    }
  } else {
    $json = json_encode(array(
      "error" => [
        "code" => 1002,
        "message" => "Email not found."
      ]
    ));

    $response = $response->withStatus(401);
    $response->getBody()->write($json);
  }

  return $response;
});

$net->post('/api/v1/token/validate', function (Request $request, Response $response, $args) {
  $data = $request->getParsedBody();
  
  $token = $data['token'];

  $isValid = JWT::verify('RSA', 'SHA256', PUBLIC_KEY, $token);
  $factory = Model::factory('User');

  if ($isValid) {
    $response = $response->withStatus(200);
  } else {
    $json = json_encode(array(
      "error" => [
        "code" => 1003,
        "message" => "Invalid token provided."
      ]
    ));

    $response = $response->withStatus(401);
    $response->getBody()->write($json);
  }

  return $response;
});

$net->post('/api/v1/token/renew', function (Request $request, Response $response, $args) {
  $data = $request->getParsedBody();
  $token = $data['token'];

  $isValid = JWT::verify('RSA', 'SHA256', PUBLIC_KEY, $token);
  $factory = Model::factory('User');

  if ($isValid) {
    if ($user = $factory->where('id', $isValid->getClaim('id'))->findOne()) {
      $newToken = JWT::generate('RSA', 'SHA256', PRIVATE_KEY, [
        'id' => $user->id,
        'first_name' => $user->first_name,
        'last_name' => $user->last_name,
        'email' => $user->email,
        'picture' => $user->picture,
      ])->__toString();
      $json = json_encode(array('token' => $token));
      $response->getBody()->write($json);
    } else {
      $json = json_encode(array(
        "error" => [
          "code" => 1003,
          "message" => "Invalid token provided."
        ]
      ));
    }
  } else {
    $json = json_encode(array(
      "error" => [
        "code" => 1003,
        "message" => "Invalid token provided."
      ]
    ));

    $response = $response->withStatus(401);
    $response->getBody()->write($json);
  }

  return $response;
});

$net->post('/api/v1/confirm/password/{id}', function (Request $request, Response $response, $args) {
  $id = $args['id'];

  $data = $request->getParsedBody();
  $password = $data['password'];

  $user = Model::factory('User')->where('id', $id)->findOne();

  if ($user->password == hash('sha256', $password)) {
    $response = $response->withStatus(200);
  } else {
    $json = json_encode(array(
      "error" => [
        "code" => 1001,
        "message" => "Invalid password."
      ]
    ));
    $response->getBody()->write($json);
  }

  return $response;
});


/* ****************************** */
/* ******* Control routes ********* */
/* ****************************** */

$net->get('/control[/{path:.*}]', function (Request $request, Response $response, $args) { 
  return $this->internal->render($response, 'index', [
    "config" => $this->store->get('config'),
  ]);
});


/* ****************************** */
/* ******* Theme routes ********* */
/* ****************************** */

$manifest = Theme::getManifest($container->store);

foreach ($manifest->routes as $route) {
  $net->get($route->pattern, function (Request $request, Response $response, $args) use ($route) {
    return $this->theme->render($response, $route->layout, ($route->data) ? $route->data : []);
  });
}

/*
$net->get('/', function (Request $request, Response $response, $args) {
  return $this->theme->render($response, 'index');
});
*/


/* ****************************** */
/* ********* DEV ROUTES ********* */
/* ****************************** */

$net->get('/dev/pwd', function (Request $request, Response $response, $args) {  
  $params = $request->getParams();
  $password = $params['password'];
  $hash = hash('sha256', $password);

  $response->getBody()->write($hash);
  return $response;
});

$net->get('/dev/id', function (Request $request, Response $response, $args) {
  $response->getBody()->write(IdGenerator::uniqueId(8));
  return $response;
});


/* ****************************** */
/* ******* CUSTOM ROUTES ******** */
/* ****************************** */

require_once('Routes.php');

$net->run();
?>
