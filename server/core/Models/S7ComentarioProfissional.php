<?php
namespace Rise\Models;

use Rise\Model;

class S7ComentarioProfissional extends Model
{
    /**
     * The table name.
     *
     * @var string
     */
    public static $_table = 's7_comentarios_profissional';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id', 'id_s7_profissional', 'comentario', 'created_at', 'updated_at',
    ];

    public function profissional()
    {
        return $this->belongsTo('S7Profissional', 'id_s7_profissional');
    }
}
?>