<?php
namespace Rise\Models;

use Rise\Model;

class S7Curso extends Model
{
    /**
     * The table name.
     *
     * @var string
     */
    public static $_table = 's7_cursos';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id', 'nome',
    ];
}
?>