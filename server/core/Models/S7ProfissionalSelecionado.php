<?php
namespace Rise\Models;

use Rise\Model;

class S7ProfissionalSelecionado extends Model
{
    /**
     * The table name.
     *
     * @var string
     */
    public static $_table = 's7_profissionais_selecionados';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id', 'id_s7_projeto', 'id_s7_profissional',
    ];

    public function projeto()
    {
        return $this->belongsTo('S7Projeto', 'id_s7_projeto');
    }

    public function profissional()
    {
        return $this->belongsTo('S7Profissional', 'id_s7_profissional');
    }
}
?>