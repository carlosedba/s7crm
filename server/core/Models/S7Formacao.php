<?php
namespace Rise\Models;

use Rise\Model;

class S7Formacao extends Model
{
    /**
     * The table name.
     *
     * @var string
     */
    public static $_table = 's7_formacoes';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id',
        'id_s7_profissional',
        'id_s7_instituicao',
        'id_s7_curso',
        'tipo',
        'ano_conclusao',
    ];

    public function profissional()
    {
        return $this->belongsTo('S7Profissional', 'id_s7_profissional');
    }

    public function instituicao()
    {
        return $this->belongsTo('S7Instituicao', 'id_s7_instituicao');
    }

    public function curso()
    {
        return $this->belongsTo('S7Curso', 'id_s7_curso');
    }
}
?>