<?php
namespace Rise\Models;

use Rise\Model;

class S7Projeto extends Model
{
    /**
     * The table name.
     *
     * @var string
     */
    public static $_table = 's7_projetos';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id',
        'id_s7_empresa',
        'id_s7_segmento',
        'id_s7_cargo',
        'nome',
        'descricao',
        'nivel',
        'missao',
        'responsabilidades',
        'competencias',
        'idiomas',
        'salario_inicial',
        'salario_final',
        'data_abertura',
        'data_encerramento',
        'honorario',
        'valor_total',
        'datas_pagamento',  
        'contato_cliente',
        'status',
    ];

    public function empresa()
    {
        return $this->belongsTo('S7Empresa', 'id_s7_empresa');
    }

    public function segmento()
    {
        return $this->belongsTo('S7Segmento', 'id_s7_segmento');
    }

    public function cargo()
    {
        return $this->belongsTo('S7Cargo', 'id_s7_cargo');
    }
}
?>