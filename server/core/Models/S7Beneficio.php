<?php
namespace Rise\Models;

use Rise\Model;

class S7Beneficio extends Model
{
    /**
     * The table name.
     *
     * @var string
     */
    public static $_table = 's7_beneficios';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id', 'nome',
    ];
}
?>