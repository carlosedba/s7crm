<?php
namespace Rise\Models;

use Rise\Model;

class S7Empresa extends Model
{
    /**
     * The table name.
     *
     * @var string
     */
    public static $_table = 's7_empresas';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id',
        'id_s7_segmento',
        'id_s7_setor',
        'nome_fantasia',
        'razao_social',
        'sobre',
        'cnpj',
        'website',
        'endereco_matriz',
        'cidade',
        'estado',
        'pais',
        'telefone',
        'regionais',
        'faturamento',
        'ebitda',
        'porte',
        'origem',
        'formato',
        'gestao',
        'conselho_administracao',
        'nome_conselheiros',
        'head_count',
    ];

    public function segmento()
    {
        return $this->belongsTo('S7Segmento', 'id_s7_segmento');
    }

    public function setor()
    {
        return $this->belongsTo('S7Setor', 'id_s7_setor');
    }
}
?>