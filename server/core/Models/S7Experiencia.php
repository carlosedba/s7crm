<?php
namespace Rise\Models;

use Rise\Model;

class S7Experiencia extends Model
{
    /**
     * The table name.
     *
     * @var string
     */
    public static $_table = 's7_experiencias';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id',
        'id_s7_profissional',
        'id_s7_segmento',
        'id_s7_cargo',
        'tipo_contratacao',
        'responsabilidades',
        'numero_salarios',
        'salario',
        'comissao',
        'bonus',
        'beneficios',
    ];

    public function profissional()
    {
        return $this->belongsTo('S7Profissional', 'id_s7_profissional');
    }

    public function segmento()
    {
        return $this->belongsTo('S7Segmento', 'id_s7_segmento');
    }

    public function cargo()
    {
        return $this->belongsTo('S7Cargo', 'id_s7_cargo');
    }
}
?>