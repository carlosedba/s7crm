<?php
namespace Rise\Models;

use Rise\Model;

class S7Profissional extends Model
{
    /**
     * The table name.
     *
     * @var string
     */
    public static $_table = 's7_profissionais';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id',
        'nome',
        'sobrenome',
        'iniciais',
        'rg',
        'cpf',
        'sexo',
        'deficiente',
        'telefone',
        'celular',
        'email',
        'skype',
        'linkedin',
        'whatsapp',
        'naturaliade',
        'estado_origem',
        'cidade_origem',
        'pais_atual',
        'estado_atual',
        'cidade_atual',
        'data_nascimento',
        'estado_civil',
        'filhos',
        'idiomas',
        'status',
    ];
}
?>