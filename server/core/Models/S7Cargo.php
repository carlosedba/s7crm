<?php
namespace Rise\Models;

use Rise\Model;

class S7Cargo extends Model
{
    /**
     * The table name.
     *
     * @var string
     */
    public static $_table = 's7_cargos';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id', 'nome',
    ];
}
?>