<?php
namespace Rise\Models;

use Rise\Model;

class S7PrincipalContato extends Model
{
    /**
     * The table name.
     *
     * @var string
     */
    public static $_table = 's7_principais_contatos';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id',
        'id_s7_segmento',
        'id_s7_empresa',
        'id_s7_cargo',
        'nome',
        'sobrenome',
        'telefone_comercial',
        'email_corporativo',
    ];

    public function segmento()
    {
        return $this->belongsTo('S7Segmento', 'id_s7_segmento');
    }

    public function empresa()
    {
        return $this->belongsTo('S7Empresa', 'id_s7_empresa');
    }

    public function cargo()
    {
        return $this->belongsTo('S7Cargo', 'id_s7_cargo');
    }
}
?>