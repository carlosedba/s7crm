<?php
namespace Rise\Api;

use \Psr\Container\ContainerInterface as Container;
use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;
use \Ausi\SlugGenerator\SlugGenerator;
use Rise\Model;
use Rise\Upload;
use Rise\Utils\IdGenerator;

class HMIPosts
{
  protected $container;

  public function __construct(Container $container) {
    $this->container = $container;
  }

	public function findAll(Request $request, Response $response, $args)
	{
		$posts = Model::factory('HMIPost')->findMany();
		$items = array();
		
		for ($i = 0; $i < count($posts); ++$i) {
			array_push($items, array_merge($posts[$i]->asArray(), [
				'cover' => json_decode($posts[$i]->cover),
				'user' => $posts[$i]->hmiUser()->findOne()->asArray()
			]));
		}

		$json = json_encode($items);
		$response->getBody()->write($json);
		$response = $response->withAddedHeader('Content-Type','application/json');

		return $response;
	}

	public function findOneById(Request $request, Response $response, $args)
	{
		$id = $args['id'];
		$post = Model::factory('HMIPost')->where('id', $id)->findOne();

		if ($post) {
			$post = array_merge($post->asArray(), [
				'cover' => json_decode($post->cover),
				'user' => $post->hmiUser()->findOne()->asArray()
			]);
			$json = json_encode($post);
			$response->getBody()->write($json);
			$response = $response->withAddedHeader('Content-Type','application/json');
		} else {
			$response = $response->withStatus(400);
		}

		return $response;
	}

	public function findLatestBySlug(Request $request, Response $response, $args)
	{
		$slug = $args['slug'];
		$post = Model::factory('HMIPost')->where('slug', $slug)->orderByDesc('updated_at')->findOne();

		if ($post) {
			$post = array_merge($post->asArray(), [
				'cover' => json_decode($post->cover),
				'user' => $post->hmiUser()->findOne()->asArray()
			]);
			$json = json_encode($post);
			$response->getBody()->write($json);
			$response = $response->withAddedHeader('Content-Type','application/json');
		} else {
			$response = $response->withStatus(400);
		}

		return $response;
	}
	
	public function create(Request $request, Response $response, $args)
	{
		$data = $request->getParsedBody();
		$files = $request->getUploadedFiles();
		$slug = new SlugGenerator;

		if (!empty($files['cover'])) {
			$data['cover'] = json_encode(Upload::saveTo(CONTENT_DATA_PATH, $files['cover']));
		}

		$post = Model::factory('HMIPost')->create(array(
			'id' 						=> IdGenerator::uniqueId(8),
			'hmi_user_id' 	=> $data['hmi_user_id'],
			'title' 				=> $data['title'],
			'description' 	=> $data['description'],
			'slug' 					=> $slug->generate($data['title']),
			'tags' 					=> $data['tags'],
			'categories' 		=> $data['categories'],
			'content' 			=> $data['content'],
			'cover' 				=> $data['cover'],
			'is_draft' 			=> $data['is_draft'],
			'created_at' 		=> date('Y-m-d H:i:s'),
			'updated_at' 		=> date('Y-m-d H:i:s'),
		));

		if ($post->save()) {
			$response = $response->withStatus(201);
		} else {
			$response = $response->withStatus(400);
		}

		return $response;
	}

	public function update(Request $request, Response $response, $args)
	{
		$id = $args['id'];
		$data = $request->getParsedBody();
		$files = $request->getUploadedFiles();
		$slug = new SlugGenerator;

		if (!empty($files['cover'])) {
			$data['cover'] = json_encode(Upload::saveTo(CONTENT_DATA_PATH, $files['cover']));
		}

		$post = Model::factory('HMIPost')->where('id', $id)->findOne();

		if ($post) {
			$post->fillAttributes(array_merge($data, [
				"slug" => $slug->generate($data['title']),
				"updated_at" => date('Y-m-d H:i:s')
			]));

			if ($post->save()) {
				$response = $response->withStatus(201);
			} else {
				$response = $response->withStatus(400);
			}
		} else {
			$response = $response->withStatus(400);
		}
				
		return $response;
	}

	public function delete(Request $request, Response $response, $args)
	{
		$data = $request->getParsedBody();
		$id = $args['id'];

		$post = Model::factory('HMIPost')->where('id', $id)->findOne();

		if ($post) {
			if ($post->delete()) {
				$response = $response->withStatus(201);
			} else {
				$response = $response->withStatus(400);
			}
		} else {
			$response = $response->withStatus(400);
		}

		return $response;
	}
}
?>
