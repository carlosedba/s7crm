<?php
namespace Rise\Api;

use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;
use Rise\Model;
use Rise\Utils\IdGenerator;

class S7Beneficios
{
    protected const MODEL = 'S7Beneficio';
    protected const FACTORY = Model::factory(self::MODEL);  

    public static function findAll(Request $request, Response $response, $args)
    {
        $query = self::FACTORY->findArray();
        
        $json = json_encode($query);
        $response->getBody()->write($json);
        $response = $response->withAddedHeader('Content-Type','application/json');

        return $response;
    }

    public static function findOneById(Request $request, Response $response, $args)
    {
        $id = $args['id'];

        $query = self::FACTORY->where('id', $id)->findOne()->asArray();

        $json = json_encode($query);
        $response->getBody()->write($json);
        $response = $response->withAddedHeader('Content-Type','application/json');

        return $response;
    }
    
    public static function create(Request $request, Response $response, $args)
    {
        $data = $request->getParsedBody();

        $query = self::FACTORY->create(array(
            'id'            => IdGenerator::uniqueId(8),
            'nome'          => $data['nome'],
        ));

        if ($query->save()) {
            $response = $response->withStatus(201);
        } else {
            $response = $response->withStatus(400);
        }

        return $response;
    }

    public static function update(Request $request, Response $response, $args)
    {
        $data = $request->getParsedBody();
        $id = $args['id'];
        
        $query = self::FACTORY->where('id', $id)->findOne()->fillAttributes($data);

        if ($query->save()) {
            $response = $response->withStatus(201);
        } else {
            $response = $response->withStatus(400);
        }

        return $response;
    }

    public static function delete(Request $request, Response $response, $args)
    {
        $data = $request->getParsedBody();
        $id = $args['id'];

        $query = self::FACTORY->where('id', $id)->findOne();

        if ($query->delete()) {
            $response = $response->withStatus(201);
        } else {
            $response = $response->withStatus(400);
        }

        return $response;
    }
}
?>
