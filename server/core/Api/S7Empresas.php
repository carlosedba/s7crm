<?php
namespace Rise\Api;

use \Psr\Container\ContainerInterface as Container;
use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;
use Rise\Model;
use Rise\Utils\IdGenerator;

class HMIEventSubscriptions
{
  protected $container;

  public function __construct(Container $container) {
    $this->container = $container;
  }

	public function findAll(Request $request, Response $response, $args)
	{
		$eventSubscriptions = Model::factory('HMIEventSubscription')->findArray();
		$json = json_encode($eventSubscriptions);
		$response->getBody()->write($json);
		$response = $response->withAddedHeader('Content-Type','application/json');

		return $response;
	}

	public function findByEventId(Request $request, Response $response, $args)
	{
		$id = $args['id'];
		$eventSubscriptions = Model::factory('HMIEventSubscription')->where('hmi_event_id', $id)->findMany();
		$items = array();
		
		for ($i = 0; $i < count($eventSubscriptions); ++$i) {
			array_push($items, array_merge($eventSubscriptions[$i]->asArray(), [
				'event' => $eventSubscriptions[$i]->hmiEvent()->findOne()->asArray()
			]));
		}

		$json = json_encode($items);
		$response->getBody()->write($json);
		$response = $response->withAddedHeader('Content-Type','application/json');

		return $response;
	}

	public function findOneById(Request $request, Response $response, $args)
	{
		$id = $args['id'];
		$eventSubscription = Model::factory('HMIEventSubscription')->where('id', $id)->findOne();

		if ($eventSubscription) {
			$eventSubscription = $eventSubscription->asArray();
			$json = json_encode($eventSubscription);
			$response->getBody()->write($json);
			$response = $response->withAddedHeader('Content-Type','application/json');
		} else {
			$response = $response->withStatus(400);
		}

		return $response;
	}
	
	public function create(Request $request, Response $response, $args)
	{
		$data = $request->getParsedBody();
		$eventSubscription = Model::factory('HMIEventSubscription')->create(array(
			'id' 						=> IdGenerator::uniqueId(8),
			'hmi_event_id' 	=> $data['hmi_event_id'],
			'name' 					=> $data['name'],
			'email' 				=> $data['email'],
		));

		if ($eventSubscription->save()) {
			$response = $response->withStatus(201);
		} else {
			$response = $response->withStatus(400);
		}

		return $response;
	}

	public function update(Request $request, Response $response, $args)
	{
		$data = $request->getParsedBody();
		$files = $request->getUploadedFiles();
		$id = $args['id'];

		$eventSubscription = Model::factory('HMIEventSubscription')->where('id', $id)->findOne();

		if ($eventSubscription) {
			$eventSubscription->fillAttributes($data);

			if ($eventSubscription->save()) {
				$response = $response->withStatus(201);
			} else {
				$response = $response->withStatus(400);
			}
		} else {
			$response = $response->withStatus(400);
		}
				
		return $response;
	}

	public function delete(Request $request, Response $response, $args)
	{
		$data = $request->getParsedBody();
		$id = $args['id'];

		$eventSubscription = Model::factory('HMIEventSubscription')->where('id', $id)->findOne();

		if ($eventSubscription) {
			if ($eventSubscription->delete()) {
				$response = $response->withStatus(201);
			} else {
				$response = $response->withStatus(400);
			}
		} else {
			$response = $response->withStatus(400);
		}

		return $response;
	}
}
?>
