<?php
namespace Rise\Api;

use \Psr\Container\ContainerInterface as Container;
use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;
use Rise\Model;
use Rise\Utils\IdGenerator;
use Rise\Upload;

class HMIUsers
{
  protected $container;

  public function __construct(Container $container) {
    $this->container = $container;
  }

	public function findAll(Request $request, Response $response, $args)
	{
		$users = Model::factory('HMIUser')->findArray();
		$json = json_encode($users);
		$response->getBody()->write($json);
		$response = $response->withAddedHeader('Content-Type','application/json');

		return $response;
	}

	public function findOneById(Request $request, Response $response, $args)
	{
		$id = $args['id'];
		$user = Model::factory('HMIUser')->where('id', $id)->findOne();

		if ($user) {
			$user = $user->asArray();
			$json = json_encode($user);
			$response->getBody()->write($json);
			$response = $response->withAddedHeader('Content-Type','application/json');
		} else {
			$response = $response->withStatus(400);
		}

		return $response;
	}

	public function findOne(Request $request, Response $response, $args)
	{
		$email = $args['email'];
		$user = Model::factory('HMIUser')->where('email', $email)->findOne();

		if ($user) {
			$user = $user->asArray();
			$json = json_encode($user);
			$response->getBody()->write($json);
			$response = $response->withAddedHeader('Content-Type','application/json');
		} else {
			$response = $response->withStatus(400);
		}

		return $response;
	}

	public function limit(Request $request, Response $response, $args)
	{
		$params = $request->getParams();
		$start = $params['start'];
		$limit = $params['limit'];

		$users = Model::factory('HMIUser')->limit($limit)->offset($start)->findArray();
		$json = json_encode($users);
		$response->getBody()->write($json);
		$response = $response->withAddedHeader('Content-Type','application/json');

		return $response;
	}

	public function create(Request $request, Response $response, $args)
	{
		$data = $request->getParsedBody();
		$files = $request->getUploadedFiles();

		if (!empty($files['picture'])) {
			$picture = Upload::saveTo(USER_DATA_PATH, $files['picture']);
			$data['picture'] = $picture;
		}

		$user = Model::factory('HMIUser')->create(array(
			'id' 						=> IdGenerator::uniqueId(8),
			'first_name' 		=> $data['first_name'],
			'last_name' 		=> $data['last_name'],
			'email' 				=> $data['email'],
			'password' 			=> hash('sha256', $data['password']),
			'picture' 			=> $data['picture'],
			'role' 					=> $data['role'],
		));

		if ($user->save()) {
			$response = $response->withStatus(201);
		} else {
			$response = $response->withStatus(400);
		}

		return $response;
	}

	public function update(Request $request, Response $response, $args)
	{
		$id = $args['id'];
		$data = $request->getParsedBody();
		$files = $request->getUploadedFiles();

		if (!empty($files['picture'])) {
			$picture = Upload::saveTo(USER_DATA_PATH, $files['picture']);
			$data['picture'] = $picture;
		}

		$user = Model::factory('HMIUser')->where('id', $id)->findOne();

		if ($user) {
			$user->fillAttributes($data);

			if ($user->save()) {
				$response = $response->withStatus(201);
			} else {
				$response = $response->withStatus(400);
			}
		} else {
			$response = $response->withStatus(400);
		}

		return $response;
	}

	public function delete(Request $request, Response $response, $args)
	{
		$data = $request->getParsedBody();
		$id = $args['id'];

		$user = Model::factory('HMIUser')->where('id', $id)->findOne();

		if ($user) {
			if ($user->delete()) {
				$response = $response->withStatus(201);
			} else {
				$response = $response->withStatus(400);
			}
		} else {
			$response = $response->withStatus(400);
		}

		return $response;
	}
}
?>
