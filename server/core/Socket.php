<?php
namespace Rise;

use \ElephantIO\Client;
use \ElephantIO\Engine\SocketIO\Version2X;

class Socket
{
  protected $config;
  protected $socket;

  public function __construct($config)
  {
    $this->config = $config;
    $this->connectToWSServer($config);
  }

  public function connectToWSServer($config)
  {
    if ($server = $config['webSocketsServer']) {
      $this->socket = new Client(new Version2X($server));
    }
  }

  public function send($type, $message)
  {
    if ($this->socket) {
      $this->socket->initialize();
      $this->socket->emit('rm_log', [
          'type' => $type,
          'data' => [
            'message' => $message,
          ],
          'datetime' => date('d/m/Y H:i:s'),
      ]);
      $this->socket->close();
    }
  }
}
?>
