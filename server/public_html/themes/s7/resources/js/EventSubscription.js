const EventSubscription = window.EventSubscription = function () {
  console.log('log > EventSubscription - initialized!')

  this.Event = new window.XEvent()
  this.Event.addHandler('handleSubmit', this.handleSubmit, this)
  this.Event.addHandler('handleSubscribeClick', this.handleSubscribeClick, this)

  document.addEventListener('DOMContentLoaded', this.render.bind(this))
  //window.addEventListener('resize', this.render.bind(this))
}

EventSubscription.prototype.countEntries = function (params) {
  let entries = params.entries()
  let i = 0

  while (entries.next().value !== undefined) {
    i++
  }

  return i
}

EventSubscription.prototype.validate = function (form, params) {
  let entriesNumber = this.countEntries(params)
  let entries = params.entries()

  for (let i = 0; i < entriesNumber; i++) {
    let pair = entries.next().value

    if (pair[0] !== 'hmi_event_id') {
      let input = form.querySelector('[name="' + pair[0] + '"]')
      let value = input.value

      if (input.dataset.type !== 'Estado') {
        if (input.value === '') return false
      }
  }
  }

  return true
}

EventSubscription.prototype.handleSubmit = function (event) {
  event.preventDefault()

  const eventSubscriptionModal = document.getElementById('event-subscription-modal')
  const form = eventSubscriptionModal.querySelector('form')
  let inputs = form.querySelectorAll('[name]')
  let params = new URLSearchParams()
  
  params.append('hmi_event_id', eventSubscriptionModal.dataset.id)

  ;[].forEach.call(inputs, function (el, i) {
    params.append(el.name, el.value)
  })

  if (this.validate(form, params)) {
    axios.post(form.action, params)
      .then(function (res) {
        form.reset()
        setTimeout(function () { alert('Formulário enviado com sucesso.') }, 240)
        MicroModal.close('event-subscription-modal');
      })
      .catch(function (err) {
        setTimeout(function () { alert('Ocorreu um erro. Tente novamente mais tarde')} )
        MicroModal.close('event-subscription-modal');
      })
  } else {
    alert('Preencha os campos obrigatórios')
  }
}

EventSubscription.prototype.handleSubscribeClick = function (event) {
  event.preventDefault()

  const path = event.path || (event.composedPath && event.composedPath()) || Utils.getElementPath(event.target)

  path.forEach(function (el, i) {
    if (el.classList && el.classList.contains('component-event')) {
      const eventSubscriptionModal = document.getElementById('event-subscription-modal')
      const form = eventSubscriptionModal.querySelector('form')
      const id = el.dataset.id
      const eventName = el.querySelector('.event-title').innerText
      const modalEventName = eventSubscriptionModal.querySelector('.modal__event-name')

      eventSubscriptionModal.dataset.id = id
      form.action = 'subscribe'
      modalEventName.innerText = eventName

      MicroModal.show('event-subscription-modal');
    }
  })
}

EventSubscription.prototype.render = function (event) {
  console.log('log > EventSubscription - render called!')

  if (Utils.shouldRender(this.windowWidth, event)) {
    console.log('log > EventSubscription - render approved!')

    let btnEventList = document.querySelectorAll('.component-event .btn-event')
    let form = document.getElementById('event-subscription-modal')
    let btnSubmit = form.querySelector('.modal__btn')

    if (btnSubmit) this.Event.addTo(btnSubmit, 'click', 'handleSubmit')
    if (btnEventList) this.Event.addTo(btnEventList, 'click', 'handleSubscribeClick')
  }
}

;(function (Application) { new Application() })(window.EventSubscription)

