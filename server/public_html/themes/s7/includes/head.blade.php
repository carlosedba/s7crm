<head>
	<meta charset="utf-8">
	<title>@yield('title')</title>
	<meta name="description" content="@yield('description')">
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0, user-scalable=no">
	<meta http-equiv="ClearType" content="true">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">

	@section('vendor-css')
		<!-- Vendor CSS -->
		<link rel="stylesheet" type="text/css" href="@asset('vendor/normalize/normalize.css')">
		<link rel="stylesheet" type="text/css" href="@asset('vendor/hamburgers/hamburgers.min.css')">
		<link rel="stylesheet" type="text/css" href="@asset('vendor/micromodal/micromodal.css')">
  	<link rel="stylesheet" type="text/css" href="@asset('vendor/css-loaders/css/load8.css')">
		@show

	@section('application-css')
		<!-- Application CSS -->
		<link rel="stylesheet" type="text/css" href="@asset('resources/css/navbar.css')">
		<link rel="stylesheet" type="text/css" href="@asset('resources/css/hero.css')">
		<link rel="stylesheet" type="text/css" href="@asset('resources/css/section.css')">
		<link rel="stylesheet" type="text/css" href="@asset('resources/css/inputs.css')">
		<link rel="stylesheet" type="text/css" href="@asset('resources/css/main.css')">
		@show

	@section('fonts')
		<!-- Fonts -->
		<link rel="stylesheet" type="text/css" href="@asset('resources/fonts/opensans/stylesheet.css')">
		<link rel="stylesheet" type="text/css" href="@asset('resources/fonts/vollkorn/stylesheet.css')">
		@show

	@section('js')
		<script src="@asset('vendor/modernizr/modernizr-custom.js')"></script>
		@show
</head>