@section('scripts')
  <!-- Scripts -->
  <script type="text/javascript" src="https://unpkg.com/axios/dist/axios.min.js"></script>
  <script type="text/javascript" src="https://unpkg.com/micromodal/dist/micromodal.min.js"></script>
  <script type="text/javascript" src="@asset('resources/js/Utils.js')"></script>
  <script type="text/javascript" src="@asset('resources/js/Event.js')"></script>
  <script type="text/javascript">
    MicroModal.init();
  </script>
  <script type="text/javascript">
    const hamburger = document.querySelector('.hamburger')
    const navbarMenu = document.querySelector('.navbar-menu')

    hamburger.addEventListener('click', function (event) {
      hamburger.classList.toggle('is-active')
      navbarMenu.classList.toggle('active')
    })
  </script>
  @show