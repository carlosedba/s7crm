@extends('layouts.default')

@section('title')
  O que oferecemos | @store(config.siteName)
@endsection

@section('description')
  @store(config.siteDescription)
@endsection

@section('vendor-css')
  @parent
@endsection

@section('application-css')
  @parent
@endsection

@section('fonts')
  @parent
@endsection

@section('content')
  @parent

  <section class="section white section-one wide">
    <div class="section-wrapper">
      <div class="row no-pad lr">
        <div class="row-titles">
          <p class="row-title">O que oferecemos</p>
        </div>
        <div class="content">{!! $conteudo['data']['content'] !!}</div>
      </div>
    </div>
  </section>
  
@endsection

@section('footer-sections')
  @parent
@endsection

@section('scripts')
  @parent
@endsection