@extends('layouts.default')

@section('title')
  Quem Somos | @store(config.siteName)
@endsection

@section('description')
  @store(config.siteDescription)
@endsection

@section('vendor-css')
  @parent
@endsection

@section('application-css')
  @parent
@endsection

@section('fonts')
  @parent
@endsection

@section('content')
  @parent

  <section class="section white section-one wide">
    <div class="section-wrapper">
      <div class="row no-pad lr">
        <div class="row-titles">
          <p class="row-title">Quem Somos</p>
          <div class="row-text">{!! $texto['data']['content'] !!}</div>
        </div>
        <div class="boxes">
          <div class="box box-one">
            <div class="box-content"><p>{!! $boxLaranja1['data']['text'] !!}</p></div>
          </div>
          <div class="box box-one">
            <div class="box-content"><p>{!! $boxLaranja2['data']['text'] !!}</p></div>
          </div>
        </div>
      </div>
    </div>
  </section>

  <section class="section white">
    <div class="section-wrapper">
      <div class="row no-pad">
        <div class="featured-wrapper">
          <div class="featured featured-two">
            <span class="featured-title">{!! $boxRoxo1['data']['title'] !!}</span>
            <div class="featured-content">{!! $boxRoxo1['data']['content'] !!}</div>
          </div>
          <div class="featured featured-two">
            <span class="featured-title">{!! $boxRoxo2['data']['title'] !!}</span>
            <div class="featured-content">{!! $boxRoxo2['data']['content'] !!}</div>
          </div>
          <div class="featured featured-two">
            <span class="featured-title">{!! $boxRoxo3['data']['title'] !!}</span>
            <div class="featured-content">{!! $boxRoxo3['data']['content'] !!}</div>
          </div>
        </div>
      </div>
    </div>
  </section>

  <section class="section white section-seven">
    <div class="section-wrapper">
      <div class="row no-pad lr">
        <div class="row-titles">
          <p class="row-title">Equipe</p>
        </div>
        <div class="content">
          @foreach ($equipe as $membro)
            <div class="component-team" data-modal="{{$membro['data']['name']}}">
              <div>
                <div class="member-picture" style="background-image: url('@contentData($membro['data']['picture']->filename)');">
                  <div class="svg icon">
                    <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 612 612">
                      <path d="M612 306.036C612 137.405 474.595 0 305.964 0S0 137.405 0 306.036c0 92.881 42.14 176.437 107.698 232.599.795.795 1.59 1.59 3.108 2.313C163.86 585.473 231.804 612 306.759 612c73.365 0 141.309-26.527 194.363-69.462 3.108-.795 5.493-3.108 7.011-5.493C571.451 480.088 612 398.122 612 306.036zm-583.883 0c0-153.018 124.901-277.919 277.919-277.919s277.919 124.901 277.919 277.919c0 74.955-29.635 142.826-78.063 192.845-7.806-36.719-31.225-99.169-103.072-139.718 16.408-20.311 25.732-46.838 25.732-74.955 0-67.149-54.644-121.793-121.793-121.793s-121.793 54.644-121.793 121.793c0 28.117 10.119 53.849 25.732 74.955-72.497 40.549-95.916 103-102.928 139.718-49.223-49.223-79.653-117.89-79.653-192.845zM212.36 284.93c0-51.536 42.14-93.676 93.676-93.676s93.676 42.14 93.676 93.676-42.14 93.676-93.676 93.676-93.676-42.14-93.676-93.676zm-79.653 238.093c1.59-22.624 14.022-99.169 98.374-142.104 21.106 16.408 46.838 25.732 74.955 25.732 28.117 0 54.644-10.119 75.75-26.527 83.556 42.935 96.784 117.89 99.169 142.104-47.633 38.237-108.493 61.655-174.052 61.655-66.425.072-126.563-22.552-174.196-60.86z"/>
                    </svg>
                  </div>
                </div>
                <p class="member-name">{{$membro['data']['name']}}</p>
                <p class="member-role">{{$membro['data']['role']}}</p>
              </div>
            </div>
          @endforeach
        </div>
      </div>
    </div>
  </section>
@endsection

@section('footer-sections')
  @parent
@endsection

@section('modal')
  @parent
  
  @foreach ($equipe as $membro)
    <div class="modal micromodal-slide modal-team" id="{{$membro['data']['name']}}" aria-hidden="true">
      <div class="modal__overlay" tabindex="-1" data-micromodal-close>
        <div class="modal__container" role="dialog" aria-modal="true" aria-labelledby="">
          <header class="modal__header">
            <div class="modal__titles">
              <h2 class="modal__title">
                {{$membro['data']['name']}}
              </h2>
            </div>
            <button class="modal__close" aria-label="Close modal" data-micromodal-close></button>
          </header>
          <main class="modal__content">
            {!! $membro['data']['content'] !!}
          </main>
          <footer class="modal__footer"></footer>
        </div>
      </div>
    </div>
  @endforeach
@endsection

@section('scripts')
  @parent
  <script type="text/javascript">
    const componentTeamList = document.querySelectorAll('.component-team')
    ;[].forEach.call(componentTeamList, function (el) {
      el.addEventListener('click', function (event) {
        MicroModal.show(el.dataset.modal)
      })
    })
  </script>
@endsection