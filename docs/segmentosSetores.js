[
  {
    nome: "Indústria",
    setores: [
      "Papel e Celulose",
      "Automotivo",
      "Auto Peças",
      "Bens de Capital",
      "Oléo e Gás",
      "Energia",
      "Construção Civil",
      "Infraestrutura",
      "Painéis, Madereira",
      "Químico",
      "Petroquímico",
      "Borracha",
      "Mineração ",
      "Máquinas e Equipamentos",
      "Embalagem",
      "Vidros",
      "Siderurgia",
      "Materiais de Construção",
      "Transporte e Logística",
      "Metalúrgica"
    ]
  },
  {
    nome: "Bens de Consumo",
    setores: [
      "Bebidas",
      "Alimentos",
      "Fumo",
      "Têxtil, Couro",
      "Calçados, Acessórios",
      "Vestuário",
      "Eletrodomésticos",
      "Eletroeletrônico",
      "Varejo",
      "Cosméticos",
      "Farmacêutico"
    ]
  },
  {
    nome: "High Tech",
    setores: [
      "Telecom",
      "Tecnologia Hardware",
      "Tecnologia Software",
      "Games e Aplicativos",
      "Tecnologias Emergentes",
      "E-commerce",
      "Media e Entretenimento",
      "Plataforma",
      "Ciência e Dados",
      "Segurança da Informação",
      "Digital Influencer",
      "Redes Sociais"
    ]
  },
  {
    nome: "Mercado Financeiro",
    setores: [
      "Banco Varejo",
      "Banco Atacado",
      "Asset",
      "Private Equity",
      "Seguradora",
      "Fundos de Investimentos",
      "Cobrança",
      "Fomento",
      "Crédito",
      "Meios de pagamento"
    ]
  },
  {
    nome: "Serviços Especializados",
    setores: [
      "Advocacia",
      "Auditoria",
      "Educação",
      "Consultoria Estratégica",
      "Consultoria Contábil e Tributária",
      "Serviços/Consultoria de Tecnologia",
      "ONG",
      "Associações de Classe",
      "Entidade Governamental",
      "Serviços de Logística",
      "Consultoria de RH",
      "Serviços de Recrutamento e Seleção",
      "Serviços Saúde, Diagnóstico",
      "Hospital",
      "Shopping",
      "Portos",
      "Concessionária",
      "Coworking",
      "Home center",
      "Órgaos Reguladores",
      "Hotelaria"
    ]
  },
  {
    nome: "Agronegócios",
    setores: [
      "Commodities Grãos",
      "Defensivos e Fertilizantes",
      "Pecuária",
      "Fumo",
      "Transformação Genética ",
      "Genética Animal",
      "Biosegurança",
      "Vacinas, Medicamentos",
      "Trading",
      "Sementes",
      "Frigorífico",
      "Máquinas e Equipamentos",
      "Horticultura",
      "Pesca",
      "Silos",
      "Cooperativas",
      "Solos, Georeferenciamento ",
      "Ervas e Fitoterápicos",
      "Alimentos saudáveis",
      "Veterinária e Diagnóstico",
      "Derivados de leite",
      "Floricultura",
      "Arvores, mudas, reflorestamento"
    ]
  },
]