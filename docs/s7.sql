  CREATE DATABASE IF NOT EXISTS `s7` CHARACTER SET utf8 COLLATE utf8_general_ci;
  USE `s7`;

  CREATE TABLE IF NOT EXISTS `s7_paises` ( 
    `id`              int  NOT NULL,
    `nome`            varchar(255),
    `sigla`           varchar(255),
    `gentilico`       varchar(255),
    PRIMARY KEY (`id`)
  ) DEFAULT CHARSET=utf8;

  CREATE TABLE IF NOT EXISTS `s7_estados` ( 
    `id`            int  NOT NULL,
    `id_s7_pais`    int,
    `id_s7_capital` int,
    `nome`          varchar(255),
    `sigla`         varchar(255),
    `slug`          varchar(255),
    PRIMARY KEY (`id`)
  ) DEFAULT CHARSET=utf8;

  CREATE TABLE IF NOT EXISTS `s7_cidades` ( 
    `id`            int  NOT NULL,
    `id_s7_pais`    int,
    `id_s7_estado`  int,
    `nome`          varchar(255),
    `slug`          varchar(255),
    PRIMARY KEY (`id`)
  ) DEFAULT CHARSET=utf8;

  ALTER TABLE `s7_estados` ADD FOREIGN KEY (`id_s7_pais`) REFERENCES `s7_paises` (`id`);
  ALTER TABLE `s7_estados` ADD FOREIGN KEY (`id_s7_capital`) REFERENCES `s7_cidades` (`id`);

  ALTER TABLE `s7_cidades` ADD FOREIGN KEY (`id_s7_pais`) REFERENCES `s7_paises` (`id`);
  ALTER TABLE `s7_cidades` ADD FOREIGN KEY (`id_s7_estado`) REFERENCES `s7_estados` (`id`);

  CREATE TABLE IF NOT EXISTS `s7_segmentos` ( 
    `id`            varchar(255)  NOT NULL,
    `nome`          varchar(255)  NOT NULL,
    PRIMARY KEY (`id`)
  ) DEFAULT CHARSET=utf8;
  
  CREATE TABLE IF NOT EXISTS `s7_setores` ( 
    `id`            varchar(255)  NOT NULL,
    `nome`          varchar(255)  NOT NULL,
    PRIMARY KEY (`id`)
  ) DEFAULT CHARSET=utf8;

  CREATE TABLE IF NOT EXISTS `s7_cargos` ( 
    `id`            varchar(255)  NOT NULL,
    `nome`          varchar(255)  NOT NULL,
    PRIMARY KEY (`id`)
  ) DEFAULT CHARSET=utf8;

  CREATE TABLE IF NOT EXISTS `s7_beneficios` (  
    `id`            varchar(255)  NOT NULL,
    `nome`          varchar(255)  NOT NULL,
    PRIMARY KEY (`id`)
  ) DEFAULT CHARSET=utf8;

  CREATE TABLE IF NOT EXISTS `s7_idiomas` (  
    `id`            varchar(255)  NOT NULL,
    `nome`          varchar(255)  NOT NULL,
    PRIMARY KEY (`id`)
  ) DEFAULT CHARSET=utf8;

  CREATE TABLE IF NOT EXISTS `s7_instituicoes` (  
    `id`            varchar(255)  NOT NULL,
    `nome`          varchar(255)  NOT NULL,
    `sigla`         varchar(255),
    `estado`        varchar(255),
    PRIMARY KEY (`id`)
  ) DEFAULT CHARSET=utf8;

  CREATE TABLE IF NOT EXISTS `s7_cursos` (  
    `id`            varchar(255)  NOT NULL,
    `nome`          varchar(255)  NOT NULL,
    PRIMARY KEY (`id`)
  ) DEFAULT CHARSET=utf8;

  CREATE TABLE IF NOT EXISTS `s7_empresas` (
    `id`                        varchar(255),
    `id_s7_segmento`            varchar(255),
    `id_s7_setor`               varchar(255),
    `nome_fantasia`             varchar(255),
    `razao_social`              varchar(255),
    `sobre`                     text,
    `website`                   varchar(255),
    `cnpj`                      varchar(255),
    `cep`                       varchar(255),
    `rua`                       varchar(255),
    `numero`                    varchar(255),
    `complemento`               varchar(255),
    `bairro`                    varchar(255),
    `cidade`                    varchar(255),
    `estado`                    varchar(255),
    `pais`                      varchar(255),
    `telefone`                  varchar(255),
    `regionais`                 json,
    `faturamento`               json,
    `ebitda`                    varchar(255),
    `porte`                     varchar(255),
    `origem`                    varchar(255),
    `formato`                   json,
    `gestao`                    varchar(255),
    `conselho_administracao`    varchar(255),
    `nome_conselheiros`         json,
    `head_count`                int,
    CHECK (JSON_VALID(`regionais`)),
    CHECK (JSON_VALID(`faturamento`)),
    CHECK (JSON_VALID(`formato`)),
    CHECK (JSON_VALID(`nome_conselheiros`)),
    PRIMARY KEY (`id`),
    FOREIGN KEY (`id_s7_segmento`) REFERENCES `s7_segmentos` (`id`),
    FOREIGN KEY (`id_s7_setor`) REFERENCES `s7_setores` (`id`)
  ) DEFAULT CHARSET=utf8;

  CREATE TABLE IF NOT EXISTS `s7_principais_contatos` (  
    `id`                    varchar(255)  NOT NULL,
    `id_s7_empresa`         varchar(255),
    `id_s7_cargo`           varchar(255),
    `nome`                  varchar(255)  NOT NULL,
    `sobrenome`             varchar(255),
    `telefone_comercial`    varchar(255),
    `email_corporativo`     varchar(255),
    PRIMARY KEY (`id`),
    FOREIGN KEY (`id_s7_empresa`) REFERENCES `s7_empresas` (`id`),
    FOREIGN KEY (`id_s7_cargo`) REFERENCES `s7_cargos` (`id`)
  ) DEFAULT CHARSET=utf8;

  CREATE TABLE IF NOT EXISTS `s7_profissionais` (  
    `id`                  varchar(255)  NOT NULL,
    `nome`                varchar(255),
    `sobrenome`           varchar(255),
    `iniciais`            varchar(6),
    `rg`                  varchar(255),
    `cpf`                 varchar(255),
    `sexo`                varchar(255),
    `deficiente`          varchar(255),
    `telefone`            varchar(255),
    `celular`             varchar(255),
    `email`               varchar(255),
    `skype`               varchar(255),
    `linkedin`            varchar(255),
    `whatsapp`            varchar(255),
    `naturalidade`        varchar(255),
    `estado_origem`       varchar(255),
    `cidade_origem`       varchar(255),
    `pais_atual`          varchar(255),
    `estado_atual`        varchar(255),
    `cidade_atual`        varchar(255),
    `data_nascimento`     varchar(255),
    `estado_civil`        varchar(255),
    `filhos`              json,
    `idiomas`             json,
    `status`              varchar(255),
    CHECK (JSON_VALID(`filhos`)),
    CHECK (JSON_VALID(`idiomas`)),
    PRIMARY KEY (`id`)
  ) DEFAULT CHARSET=utf8;

  CREATE TABLE IF NOT EXISTS `s7_experiencias_profissionais` ( 
    `id`                    varchar(255)  NOT NULL,
    `id_s7_profissional`    varchar(255),
    `id_s7_empresa`         varchar(255),
    `id_s7_cargo`           varchar(255),
    `tipo_contratacao`      varchar(255),
    `responsabilidades`     json,

    `moeda`                 varchar(255),

    `salario`               decimal(13,2),
    `quantidade_salarios`   decimal(13,2),
    `total_salario`         decimal(13,2),

    `comissao`              decimal(13,2),
    `quantidade_comissao`   decimal(13,2),
    `total_comissao`        decimal(13,2),

    `bonus`                 decimal(13,2),
    `quantidade_bonus`      decimal(13,2),
    `total_bonus`           decimal(13,2),

    `plr`                   decimal(13,2),
    `quantidade_plr`        decimal(13,2),
    `total_plr`             decimal(13,2),

    `beneficios`            json,
    `ferramentas`           json,
    CHECK (JSON_VALID(`responsabilidades`)),
    CHECK (JSON_VALID(`beneficios`)),
    CHECK (JSON_VALID(`ferramentas`)),
    PRIMARY KEY (`id`),
    FOREIGN KEY (`id_s7_profissional`) REFERENCES `s7_profissionais` (`id`),
    FOREIGN KEY (`id_s7_empresa`) REFERENCES `s7_empresas` (`id`),
    FOREIGN KEY (`id_s7_cargo`) REFERENCES `s7_cargos` (`id`)
  ) DEFAULT CHARSET=utf8;

  CREATE TABLE IF NOT EXISTS `s7_formacoes` ( 
    `id`                       varchar(255)  NOT NULL,
    `id_s7_profissional`       varchar(255),
    `id_s7_instituicao`        varchar(255),
    `id_s7_curso`              varchar(255),
    `tipo`                     varchar(255),
    `ano_conclusao`            int,
    PRIMARY KEY (`id`),
    FOREIGN KEY (`id_s7_profissional`) REFERENCES `s7_profissionais` (`id`),
    FOREIGN KEY (`id_s7_instituicao`) REFERENCES `s7_instituicoes` (`id`),
    FOREIGN KEY (`id_s7_curso`) REFERENCES `s7_cursos` (`id`)
  ) DEFAULT CHARSET=utf8;

  CREATE TABLE IF NOT EXISTS `s7_comentarios_profissional` ( 
    `id`                    varchar(255)  NOT NULL,
    `id_s7_profissional`    varchar(255)  NOT NULL,
    `comentario`            text,
    `created_at`            datetime      NOT NULL,
    `updated_at`            datetime      NOT NULL,
    PRIMARY KEY (`id`),
    FOREIGN KEY (`id_s7_profissional`) REFERENCES `s7_profissionais` (`id`)
  ) DEFAULT CHARSET=utf8;

  CREATE TABLE IF NOT EXISTS `s7_tipos_projeto` ( 
    `id`                    int  NOT NULL,
    `nome`                  varchar(255),
    PRIMARY KEY (`id`)
  ) DEFAULT CHARSET=utf8;

  CREATE TABLE IF NOT EXISTS `s7_projetos` (  
    `id`                        varchar(255)  NOT NULL,
    `id_s7_tipo_projeto`        int,
    `id_s7_empresa`             varchar(255),
    `id_s7_segmento`            varchar(255),
    `id_s7_cargo`               varchar(255),

    `nome`                      varchar(255),
    `tipo`                      text,
    `descricao`                 text,
    `nivel`                     varchar(255),
    `missao`                    varchar(255),
    `responsabilidades`         varchar(255),
    `competencias`              json,
    `idiomas`                   json,

    `salario_inicial`           varchar(255),
    `salario_final`             varchar(255),

    `data_abertura`             date,
    `data_encerramento`         date,

    `honorario`                 varchar(255),
    `valor_total`               varchar(255),
    `datas_pagamento`           json,  
    `contato_cliente`           varchar(255),

    `status`                    varchar(255),
    CHECK (JSON_VALID(`competencias`)),
    CHECK (JSON_VALID(`idiomas`)),
    CHECK (JSON_VALID(`datas_pagamento`)),
    PRIMARY KEY (`id`),
    FOREIGN KEY (`id_s7_tipo_projeto`) REFERENCES `s7_tipos_projeto` (`id`),
    FOREIGN KEY (`id_s7_empresa`) REFERENCES `s7_empresas` (`id`),
    FOREIGN KEY (`id_s7_segmento`) REFERENCES `s7_segmentos` (`id`),
    FOREIGN KEY (`id_s7_cargo`) REFERENCES `s7_cargos` (`id`),
    FOREIGN KEY (`contato_cliente`) REFERENCES `s7_profissionais` (`id`)
  ) DEFAULT CHARSET=utf8;

  CREATE TABLE IF NOT EXISTS `s7_profissionais_projeto` (  
    `id`                        varchar(255)  NOT NULL,
    `id_s7_projeto`             varchar(255),
    `id_s7_profissional`        varchar(255),
    `id_s7_cargo`               varchar(255),
    `honorario`                 varchar(255),
    `salario_inicial`           varchar(255),
    `salario_final`             varchar(255),
    `missao_cargo`              varchar(255),
    `responsabilidades_cargo`   varchar(255),
    `equipe_liderada`           varchar(255),
    `nivel`                     varchar(255),
    `competencias`              varchar(255),
    PRIMARY KEY (`id`),
    FOREIGN KEY (`id_s7_projeto`) REFERENCES `s7_projetos` (`id`),
    FOREIGN KEY (`id_s7_profissional`) REFERENCES `s7_profissionais` (`id`)
  ) DEFAULT CHARSET=utf8;

  CREATE TABLE IF NOT EXISTS `s7_profissionais_selecionados` (  
    `id`                        varchar(255)  NOT NULL,
    `id_s7_projeto`             varchar(255),
    `id_s7_profissional`        varchar(255),
    PRIMARY KEY (`id`),
    FOREIGN KEY (`id_s7_projeto`) REFERENCES `s7_projetos` (`id`),
    FOREIGN KEY (`id_s7_profissional`) REFERENCES `s7_profissionais` (`id`)
  ) DEFAULT CHARSET=utf8;

  CREATE TABLE IF NOT EXISTS `config` (
    `id`            int           NOT NULL  AUTO_INCREMENT,
    `name`          varchar(255)  NOT NULL,
    `value`         longtext      NOT NULL,
    `autoload`      bool          NOT NULL,
    PRIMARY KEY (`id`)
  ) DEFAULT CHARSET=utf8;

  CREATE TABLE IF NOT EXISTS `users` (
    `id`            varchar(255)  NOT NULL,
    `first_name`    varchar(255)  NOT NULL,
    `last_name`     varchar(255)  NOT NULL,
    `email`         varchar(255)  NOT NULL,
    `password`      varchar(255)  NOT NULL,
    `picture`       varchar(255),
    PRIMARY KEY (`id`)
  ) DEFAULT CHARSET=utf8;

  INSERT INTO `users` (`id`, `first_name`, `last_name`, `email`, `password`, `picture`) VALUES ('E0thkeSixKEL', 'Carlos Eduardo', 'Barbosa de Almeida', 'carlosedba@outlook.com', '$2b$10$ZXiB.fRQsiVZZSRCEbrtTun3xuKVJBwz9GMPVugOwUSt1f6JFInpy', 'eu.jpg');

  INSERT INTO `config` (`name`, `value`, `autoload`) VALUES ('site_url', 'http://localhost:8080/s7crm/server/public_html/', true);
  INSERT INTO `config` (`name`, `value`, `autoload`) VALUES ('site_home', 'http://localhost:8080/s7crm/server/public_html/', true);
  INSERT INTO `config` (`name`, `value`, `autoload`) VALUES ('site_name', 'S7CRM', true);
  INSERT INTO `config` (`name`, `value`, `autoload`) VALUES ('site_description', '', true);

  INSERT INTO `config` (`name`, `value`, `autoload`) VALUES ('theme', 's7', true);
  INSERT INTO `config` (`name`, `value`, `autoload`) VALUES ('admin_email', 'carlosedba@outlook.com', true);
  INSERT INTO `config` (`name`, `value`, `autoload`) VALUES ('timezone_string', 'America/Sao_Paulo', true);

  INSERT INTO `config` (`name`, `value`, `autoload`) VALUES ('mailserver_url', '', true);
  INSERT INTO `config` (`name`, `value`, `autoload`) VALUES ('mailserver_login', '', true);
  INSERT INTO `config` (`name`, `value`, `autoload`) VALUES ('mailserver_pass', '', true);
  INSERT INTO `config` (`name`, `value`, `autoload`) VALUES ('mailserver_port', '', true);

  INSERT INTO `s7_setores` (`id`, `nome`) VALUES
  ('1', 'Alimentos e bebidas'),
  ('10', 'Reparação e automotivo'),
  ('11', 'Químico, farmacêutico e cosmético'),
  ('12', 'Plástico e borracha'),
  ('13', 'Logística e infraestrutura'),
  ('2', 'Madeira e moveleiro'),
  ('3', 'Florestal, celulose, papel e gráfica'),
  ('4', 'Construção civil e mineral'),
  ('5', 'Vestuário, têxtil e couro'),
  ('6', 'Metalmecânico e eletroeletrônico'),
  ('7', 'Biotecnologia'),
  ('8', 'Energia'),
  ('9', 'TIC e audiovisual');

  INSERT INTO `s7_tipos_projeto` (`id`, `nome`) VALUES
  ('1', 'Assessment'),
  ('2', 'Avaliação de performance'),
  ('3', 'Cargos e salários'),
  ('4', 'Coaching'),
  ('5', 'Employer Branding'),
  ('6', 'Implantação de RH estratégico'),
  ('7', 'Mentoria'),
  ('8', 'Recrutamento e seleção'),
  ('9', 'Sucessão'),
  ('10', 'Workshop');

  INSERT INTO `s7_cargos` (`id`, `nome`) VALUES
  ('1', 'Analista de Marketing'),
  ('2', 'Analista de Sistemas');

  INSERT INTO `s7_empresas` (`id`, `id_s7_setor`, `nome_fantasia`) VALUES
  ('1', '9', 'Rise'),
  ('2', '9', 'Devs in Paris');

  INSERT INTO `s7_instituicoes` (`id`, `nome`, `sigla`, `estado`) VALUES
  ('1', 'Pontifícia Universidade Católica do Paraná', 'PUCPR', 'Paraná'),
  ('2', 'Universidade Tecnológica Federal do Paraná', 'UTFPR', 'Paraná'),
  ('3', 'Universidade Federal do Paraná', 'UFPR', 'Paraná');

  INSERT INTO `s7_cursos` (`id`, `nome`) VALUES
  ('1', 'Bacharelado em Sistemas de Informação'),
  ('2', 'Bacharelado em Publicidade e Propaganda');




