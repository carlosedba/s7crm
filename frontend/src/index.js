import React from 'react'
import ReactDOM from 'react-dom'
import { Provider } from 'react-redux'
import browserStore from 'store'
import moment from 'moment'
import Modal from 'react-modal'
import NetQueue from './NetQueue'

import { PRODUCTION, SERVER_ADDRESS } from '@/globals'

import MainRouter from './components/MainRouter'

import setStore from './store'

import './libs/store/store.min.js'
import './libs/normalize/normalize.css'
import './libs/jquery/jquery-3.1.1.min.js'
import './libs/tinymce/tinymce.min.js'

import './assets/css/boilerplate.global.css'
import './assets/css/common.global.css'
import './assets/css/inputs.global.css'
import './assets/css/daypicker.global.css'
import './assets/css/buttons.global.css'
import './assets/css/header.global.css'
import './assets/css/widget.global.css'
import './assets/css/table.global.css'
import './assets/css/ctable.global.css'
import './assets/css/card.global.css'
import './assets/css/list.global.css'
import './assets/css/collapse.global.css'
import './assets/css/window.global.css'

import 'moment/locale/pt-br'

window.NetQueue = new NetQueue()

moment.locale('pt-BR')

tinyMCE.baseURL = `${SERVER_ADDRESS}/themes/hmi/vendor/tinymce`
tinyMCE.suffix = '.min'

if (!PRODUCTION) tinyMCE.baseURL = `https://server.com/hmi/server/public_html/themes/hmi/vendor/tinymce`

const store = setStore()

const render = Component => {
  ReactDOM.render(
    <Provider store={store}>
      <Component/>
    </Provider>
    , document.getElementById('root')
  )
}

Modal.setAppElement('#root')

render(MainRouter)

// webpack Hot Module Replacement API
if (module.hot) {
  module.hot.accept('@/components/MainRouter', () => {
    render(MainRouter)
  })
}