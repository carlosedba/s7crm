import update from 'immutability-helper'

export default class State {
  static update(Component, state = {}) {  
    Component.setState(function (prevState, props) {
      return update(prevState, state)
    })
  }
}

