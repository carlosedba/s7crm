import React, { Component } from 'react'
import { connect } from 'react-redux'
import { Link } from 'react-router-dom'
import update from 'immutability-helper'
import axios from 'axios'

import { updateFormRaw, updateFormComputed } from '@/actions/Form'
import { addModal } from '@/actions/Modal'

import { trocarNomeSiglaEstado } from '@/utils/BrasilUtils'
import IdGenerator from '@/utils/IdGenerator'

import Form from '@/components/Form'
import TextInput from '@/components/Input/TextInput'
import SuggestionInput from '@/components/Input/SuggestionInput'
import SelectInput from '@/components/Select'

import FormSection from '@/components/FormSection'
import ArrayAttributeFormSection from '@/components/FormSection/ArrayAttributeFormSection'
import ObjectArrayAttributeFormSection from '@/components/FormSection/ObjectArrayAttributeFormSection'

import ModalConAdministrativoCreate from '@/components/Modal/ModalConAdministrativoCreate'
import ModalConAdministrativoUpdate from '@/components/Modal/ModalConAdministrativoUpdate'

import ModalPriContatosCreate from '@/components/Modal/ModalPriContatosCreate'
import ModalPriContatosUpdate from '@/components/Modal/ModalPriContatosUpdate'

import ModalRegionalCreate from '@/components/Modal/ModalRegionalCreate'
import ModalRegionalUpdate from '@/components/Modal/ModalRegionalUpdate'

import IcPlusSign from '@/icons/ic_plus_sign'
import IcEmpty from '@/icons/ic_empty'

import NQForm from '@/NQForm'

const NQF = new NQForm()

@connect((state) => {
  return NQF.mapStateToProps({
    forms: state.Forms,
    modal: state.Modal,
  })
}, NQF.mapDispatchToProps.bind(NQF, (dispatch, ownProps) => {
  return {
    updateFormRaw(model, action, props) {
      return dispatch(updateFormRaw(model, action, props))
    },

    addModal(name) {
      return dispatch(addModal(name))
    },
  }
}))
export default class Projetos extends Component {
  constructor(props) {
    super(props)
  }

  componentDidMount() {}

  updateState(state = {}) {  
    this.setState((prevState, props) => {
      return update(prevState, state)
    })
  }

  render() {
    return (
      <div className="page control-page">
        <div className="page-header-lr">
          <div className="page-header-left">
            <div className="page-titles">
              <p className="page-title">Projetos</p>
              <p className="page-text"></p>
            </div>
          </div>
          <div className="page-header-right">
            <Link to="/projetos/novo" className="btn btn-four">Novo projeto</Link>
          </div>
        </div>
        <div className="page-content">
          <div className="form-section">
            
            <div className="no-items">
              <div className="svg icon">
                <IcEmpty/>
              </div>
              <p>Nenhum projeto encontrado.</p>
            </div>
          
          </div>
        </div>
      </div>
    )
  }
}

