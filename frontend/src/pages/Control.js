import React, { Component } from 'react'
import { connect } from 'react-redux'
import { Link } from 'react-router-dom'
import update from 'immutability-helper'

import IcPlusSign from '@/icons/ic_plus_sign'

@connect((state) => {
  return {}
})
export default class Control extends Component {
  constructor(props) {
    super(props)

    this.previewPicture = React.createRef()
    this.inputPicture = React.createRef()

    this.state = {}

    this.handlePictureChange = this.handlePictureChange.bind(this)
  }

  componentDidMount() {}

  handlePictureChange(event) {
    event.preventDefault()

    const previewPicture = this.previewPicture.current
    const inputPicture = this.inputPicture.current

    for (let i = 0; i < inputPicture.files.length; i++) {
      if (i === 0) {
        const file = inputPicture.files[i]

        if (!file.type.startsWith('image/')){ continue }

        const reader = new FileReader()
        reader.onload = function (event) {
          previewPicture.style.backgroundImage = `url(${event.target.result})`
        }
        reader.readAsDataURL(file)
      }
    }
  }

  filterContentByAlias(alias = '') {
    if (!this.props.content.loading) {
      return this.props.content.items.filter(obj => {
        return obj.component.alias === alias
      })
    }
  }

  getContentByAlias(alias = '') {
    const content = this.filterContentByAlias(alias)

    if (content) return content[0]
  }

  render() {
    return (
      <div className="page control-page">
        <div className="page-header">
          <div className="page-titles">
            <span className="page-tie">Empresas</span>
            <p className="page-title">Cadastrar Empresa</p>
            <p className="page-subtitle">Informe os dados em seus respectivos campos:</p>
            <p className="page-text"></p>
          </div>
        </div>
        <div className="page-content">
          <form>
            <div className="inputs">
              <div className="multi-input">
                <div className="input w2-q1">
                  <label>Segmento:</label>
                  <input type="text"/>
                </div>
                <div className="input w2-q2">
                  <label>Setor:</label>
                  <input type="text"/>
                </div>
              </div>
              <div className="multi-input">
                <div className="input w3">
                  <label>Nome fantasia:</label>
                  <input type="text"/>
                </div>
                <div className="input w3">
                  <label>Razão Social:</label>
                  <input type="text"/>
                </div>
                <div className="input w2">
                  <label>CNPJ:</label>
                  <input type="text"/>
                </div>
              </div>
              <div className="multi-input">
                <div className="input w1-q2">
                  <label>País:</label>
                  <input type="text"/>
                </div>
                <div className="input w2">
                  <label>Estado:</label>
                  <input type="text"/>
                </div>
                <div className="input w2-q1">
                  <label>Cidade:</label>
                  <input type="text"/>
                </div>
                <div className="input w2">
                  <label>Bairro:</label>
                  <input type="text"/>
                </div>
              </div>
              <div className="multi-input">
                <div className="input w4">
                  <label>Rua:</label>
                  <input type="text"/>
                </div>
                <div className="input w1">
                  <label>Número:</label>
                  <input type="text"/>
                </div>
              </div>
              <div className="multi-input">
                <div className="input w2">
                  <label>Telefone:</label>
                  <input type="text"/>
                </div>
              </div>
              <div className="multi-input">
                <div className="input w1-q2">
                  <label>Faturamento:</label>
                  <input type="text"/>
                </div>
                <div className="input w1-q2">
                  <label>Ebitda:</label>
                  <input type="text"/>
                </div>
                <div className="input w1-q2">
                  <label>Porte:</label>
                  <input type="text"/>
                </div>
              </div>
              <div className="multi-input">
                <div className="input w1-q2">
                  <label>Origem:</label>
                  <input type="text"/>
                </div>
                <div className="input w2">
                  <label>Formato:</label>
                  <input type="text"/>
                </div>
                <div className="input w2">
                  <label>Gestão:</label>
                  <input type="text"/>
                </div>
                <div className="input w1">
                  <label>Head count:</label>
                  <input type="text"/>
                </div>
              </div>
            </div>
          </form>
          <div className="form-section">
            <p className="form-section-title">Conselho Administrativo</p>
            <div className="form-new-item">
              <div className="svg icon">
                <IcPlusSign/>
              </div>
              Adicionar item
            </div>
          </div>
          <div className="form-section">
            <p className="form-section-title">Principais contatos</p>
            <div className="form-new-item">
              <div className="svg icon">
                <IcPlusSign/>
              </div>
              Adicionar item
            </div>
          </div>
        </div>
      </div>
    )
  }
}

