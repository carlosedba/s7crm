import React, { Component } from 'react'
import { connect } from 'react-redux'
import { Link } from 'react-router-dom'
import update from 'immutability-helper'
import axios from 'axios'

import { updateFormRaw, updateFormComputed } from '@/actions/Form'
import { addModal } from '@/actions/Modal'

import NQForm from '@/NQForm'

const NQF = new NQForm()

@connect((state) => {
  return NQF.mapStateToProps({
    forms: state.Forms,
    modal: state.Modal,
  })
}, NQF.mapDispatchToProps.bind(NQF, (dispatch, ownProps) => {
  return {
    updateFormRaw(model, action, props) {
      return dispatch(updateFormRaw(model, action, props))
    },

    addModal(name) {
      return dispatch(addModal(name))
    },
  }
}))
export default class Dashboard extends Component {
  constructor(props) {
    super(props)
  }

  componentDidMount() {}

  updateState(state = {}) {  
    this.setState((prevState, props) => {
      return update(prevState, state)
    })
  }

  render() {
    return (
      <div className="page control-page">
        <div className="page-header">
          <div className="page-titles">
            <p className="page-title">Dashboard</p>
            <p className="page-text"></p>
          </div>
        </div>
        <div className="page-content">
          <div className="bottombar"></div>
        </div>
      </div>
    )
  }
}

