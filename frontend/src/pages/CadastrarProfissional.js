import React, { useState, useEffect } from 'react'
import { useSelector, useDispatch } from 'react-redux'
import { Link, useHistory } from 'react-router-dom'

import { addModal } from '@/actions/Modal'

import Form from '@/components/Form'
import TextInput from '@/components/Input/TextInput'
import SelectInput from '@/components/Select'
import FormSection from '@/components/FormSection'
import ArrayAttributeFormSection from '@/components/FormSection/ArrayAttributeFormSection'
import ObjectArrayAttributeFormSection from '@/components/FormSection/ObjectArrayAttributeFormSection'

import IcPlusSign from '@/icons/ic_plus_sign'

import IdGenerator from '@/utils/IdGenerator'

import * as textMasks from '@/textMasks'

const formId = IdGenerator.default()

@connect((state) => {
  return NQF.mapStateToProps({
    forms: state.Forms,

    modal: state.Modal,

    gentilicos: state.S7Paises.items.reduce((acc, currentValue) => {
      return acc.concat({ value: currentValue.id, label: currentValue.gentilico })
    }, []),

    paises: state.S7Paises.items.reduce((acc, currentValue) => {
      return acc.concat({ value: currentValue.id, label: currentValue.nome })
    }, []),

    estados: state.S7Estados.items.reduce((acc, currentValue) => {
      return acc.concat({ value: currentValue.id, label: currentValue.nome })
    }, []),

    cidades: state.S7Cidades.items.reduce((acc, currentValue) => {
      return acc.concat({ value: currentValue.id, label: currentValue.nome })
    }, []),
  })
}, NQF.mapDispatchToProps.bind(NQF, (dispatch, ownProps) => {
  return {
    S7Paises: {
      fetch(params) {
        return dispatch(S7Paises.fetch(params))
      }
    },

    S7Estados: {
      fetch(params) {
        return dispatch(S7Estados.fetch(params))
      }
    },

    S7Cidades: {
      fetch(params) {
        return dispatch(S7Cidades.fetch(params))
      }
    },

    addModal(name) {
      return dispatch(addModal(name))
    },
  }
}))
export default class CadastrarProfissional extends Component {
  constructor(props) {
    super(props)

    this.formId = IdGenerator.default()
    this.formSubmit = null

    this.model = 'S7Profissional'
    this.action = 'create'

    this.previewPicture = React.createRef()
    this.inputPicture = React.createRef()

    this.handleNaturalidadeChange = this.handleNaturalidadeChange.bind(this)
    this.handleEstadoOrigemChange = this.handleEstadoOrigemChange.bind(this)
    this.handlePaisAtualChange = this.handlePaisAtualChange.bind(this)
    this.handleEstadoAtualChange = this.handleEstadoAtualChange.bind(this)
    this.submit = this.submit.bind(this)
  }

  componentDidMount() {
    const { S7Paises } = this.props

    this.props.addModal('ModalFilhosCreate')
    this.props.addModal('ModalFilhosUpdate')

    this.props.addModal('ModalIdiomaCreate')
    this.props.addModal('ModalIdiomaUpdate')

    this.props.addModal('ModalExpProfissionalCreate')
    this.props.addModal('ModalExpProfissionalUpdate')
    this.props.addModal('ModalExpProfissionalSalario')

    this.props.addModal('ModalFormacaoCreate')
    this.props.addModal('ModalFormacaoUpdate')

    S7Paises.fetch()
  }

  updateState(state = {}) {
    this.setState((prevState, props) => {
      return update(prevState, state)
    })
  }

  handleNaturalidadeChange(event) {
    const { S7Estados } = this.props

    S7Estados.fetch({
      pais: event.value
    })
  }

  handleEstadoOrigemChange(event) {
    const { S7Cidades } = this.props

    S7Cidades.fetch({
      estado: event.value
    })
  }

  handlePaisAtualChange(event) {
    const { S7Estados } = this.props

    S7Estados.fetch({
      pais: event.value
    })
  }

  handleEstadoAtualChange(event) {
    const { S7Cidades } = this.props

    S7Cidades.fetch({
      estado: event.value
    })
  }

  submit() {
    this.formSubmit()
  }

  render() {
    const { gentilicos, paises, estados, cidades } = this.props

    const generalFormProps = {
      model: this.model,
      action: this.action
    }

    return (
      <div className="page control-page">
        <div className="page-header">
          <div className="page-titles">
            <Link to="/profissionais" className="page-tie">Profissionais</Link>
            <p className="page-title">Cadastrar Profissional</p>
            <p className="page-subtitle">Informe os dados em seus respectivos campos:</p>
            <p className="page-text"></p>
          </div>
        </div>
        <div className="page-content">

          <Form {...generalFormProps} id={this.formId} submit={(fn) => { this.formSubmit = fn }}>
            <div className="inputs">
              <div className="multi-input">
                <TextInput {...generalFormProps} classes="w2" label="Nome" attribute="nome"/>
                <TextInput {...generalFormProps} classes="w2-q2" label="Sobrenome" attribute="sobrenome"/>
                <SelectInput {...generalFormProps} size="w1-q3" label="Sexo" attribute="sexo"
                             options={[
                          { value: 'Masculino', label: 'Masculino' },
                          { value: 'Feminino', label: 'Feminino' },
                          { value: 'Outro', label: 'Outro' },
                        ]}/>
                <SelectInput {...generalFormProps} size="w1-q2" label="Deficiente" attribute="deficiente"
                             options={[
                          { value: 'Sim', label: 'Sim' },
                          { value: 'Não', label: 'Não' },
                        ]}/>
              </div>
              <div className="multi-input">
                <TextInput {...generalFormProps} classes="w1-q1" label="RG" attribute="rg" mask={textMasks.RG}/>
                <TextInput {...generalFormProps} classes="w1-q2" label="CPF" attribute="cpf" mask="cpf"/>
                <TextInput {...generalFormProps} classes="w1-q2" label="Telefone" attribute="telefone" mask="telefone"/>
                <TextInput {...generalFormProps} classes="w1-q2" label="Celular" attribute="celular" mask="celular"/>
              </div>
              <div className="multi-input">
                <TextInput {...generalFormProps} classes="w2" label="Data de nascimento" attribute="data_nascimento" mask={textMasks.DATE}/>
                <SelectInput {...generalFormProps} size="w2" label="Estado civil" attribute="estado_civil"
                             options={[
                          { value: 'Solteiro (a)', label: 'Solteiro (a)' },
                          { value: 'Casado (a)', label: 'Casado (a)' },
                          { value: 'Divorciado (a)', label: 'Divorciado (a)' },
                          { value: 'Viúvo (a)', label: 'Viúvo (a)' },
                          { value: 'União estável', label: 'União estável' },
                        ]}/>
              </div>
              <div className="multi-input">
                <TextInput {...generalFormProps} classes="w3-q1" label="E-mail" attribute="email"/>
                <TextInput {...generalFormProps} classes="w2" label="Skype" attribute="skype"/>
                <TextInput {...generalFormProps} classes="w2" label="LinkedIn" attribute="linkedin"/>
              </div>
              <div className="multi-input">
                <SelectInput {...generalFormProps} size="w2" label="Naturalidade" attribute="naturalidade" options={gentilicos} onChange={this.handleNaturalidadeChange}/>
                <SelectInput {...generalFormProps} size="w2-q1" label="Estado de origiem" attribute="estado_origem" options={estados} onChange={this.handleEstadoOrigemChange}/>
                <SelectInput {...generalFormProps} size="w2" label="Cidade de origiem" attribute="cidade_origem" options={cidades}/>
              </div>
              <div className="multi-input">
                <SelectInput {...generalFormProps} size="w2" label="País atual" attribute="pais_atual" options={paises} onChange={this.handlePaisAtualChange}/>
                <SelectInput {...generalFormProps} size="w2-q1" label="Estado atual" attribute="estado_atual" options={estados} onChange={this.handleEstadoAtualChange}/>
                <SelectInput {...generalFormProps} size="w2" label="Cidade atual" attribute="cidade_atual" options={cidades}/>
              </div>
            </div>
          </Form>

          <ArrayAttributeFormSection
            model="S7Profissional"
            action="create"
            attribute="filhos"
            title="Filhos"
            message="Adicionar filho"
            icon={IcPlusSign}
            onCreateModalName="ModalFilhosCreate"
            onUpdateModalName="ModalFilhosUpdate">
            <ModalFilhosCreate/>
            <ModalFilhosUpdate/>
          </ArrayAttributeFormSection>

          <ObjectArrayAttributeFormSection
            model="S7Profissional"
            action="create"
            attribute="idiomas"
            title="Idiomas"
            message="Adicionar idioma"
            icon={IcPlusSign}
            attributeIsSelect={true}
            attributeToDisplay="idioma"
            onCreateModalName="ModalIdiomaCreate"
            onUpdateModalName="ModalIdiomaUpdate">
            <ModalIdiomaCreate/>
            <ModalIdiomaUpdate/>
          </ObjectArrayAttributeFormSection>

          <FormSection
            model="S7ExperienciaProfissional"
            action="create"
            title="Experiências profissionais"
            message="Adicionar experiência"
            icon={IcPlusSign}
            attributeToDisplay="id_s7_cargo"
            onCreateModalName="ModalExpProfissionalCreate"
            onUpdateModalName="ModalExpProfissionalUpdate"
            customActions={[
              { icon: IcMoney, modal: 'ModalExpProfissionalSalario' }
            ]}>
            <ModalExpProfissionalCreate dependsOn={{ id: this.formId, key: 'id_s7_profissional' }}/>
            <ModalExpProfissionalUpdate dependsOn={{ id: this.formId, key: 'id_s7_profissional' }}/>
            <ModalExpProfissionalSalario dependsOn={{ id: this.formId, key: 'id_s7_profissional' }}/>
          </FormSection>

          <FormSection
            model="S7Formacao"
            action="create"
            title="Formações"
            message="Adicionar formação"
            icon={IcPlusSign}
            attributeToDisplay="id_s7_curso"
            onCreateModalName="ModalFormacaoCreate"
            onUpdateModalName="ModalFormacaoUpdate">
            <ModalFormacaoCreate dependsOn={{ id: this.formId, key: 'id_s7_profissional' }}/>
            <ModalFormacaoUpdate dependsOn={{ id: this.formId, key: 'id_s7_profissional' }}/>
          </FormSection>

          <button className="btn btn-four" type="submit" onClick={this.submit}>Salvar</button>

          <div className="bottombar"></div>
        </div>
      </div>
    )
  }
}

