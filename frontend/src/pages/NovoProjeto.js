import React, { Component } from 'react'
import { connect } from 'react-redux'
import { Link } from 'react-router-dom'
import update from 'immutability-helper'
import axios from 'axios'

import { updateFormRaw, updateFormComputed } from '@/actions/Form'
import { addModal } from '@/actions/Modal'
import { fetch } from '@/actions/S7TiposProjeto'

import { trocarNomeSiglaEstado } from '@/utils/BrasilUtils'
import IdGenerator from '@/utils/IdGenerator'

import * as textMasks from '@/textMasks'

import Form from '@/components/Form'
import TextInput from '@/components/Input/TextInput'
import CurrencyInput from '@/components/Input/CurrencyInput'
import SuggestionInput from '@/components/Input/SuggestionInput'
import DateInput from '@/components/Input/DateInput'
import SelectInput from '@/components/Select'

import FormSection from '@/components/FormSection'
import ArrayAttributeFormSection from '@/components/FormSection/ArrayAttributeFormSection'
import ObjectArrayAttributeFormSection from '@/components/FormSection/ObjectArrayAttributeFormSection'
import TextAttributeFormSection from '@/components/FormSection/TextAttributeFormSection'

import ModalDatPagamentoCreate from '@/components/Modal/ModalDatPagamentoCreate'
import ModalDatPagamentoUpdate from '@/components/Modal/ModalDatPagamentoUpdate'

import ModalDescricaoCreate from '@/components/Modal/ModalDescricaoCreate'
import ModalDescricaoUpdate from '@/components/Modal/ModalDescricaoUpdate'

import ModalProProjetoCreate from '@/components/Modal/ModalProProjetoCreate'
import ModalProProjetoUpdate from '@/components/Modal/ModalProProjetoUpdate'

import IcPlusSign from '@/icons/ic_plus_sign'
import IcPencil from '@/icons/ic_pencil'

import NQForm from '@/NQForm'

const NQF = new NQForm()

@connect((state) => {
  return NQF.mapStateToProps({
    forms: state.Forms,

    modal: state.Modal,

    tiposProjeto: state.S7TiposProjeto.items.reduce((acc, currentValue) => {
      return acc.concat({ value: currentValue.id, label: currentValue.nome })
    }, [])
  })
}, NQF.mapDispatchToProps.bind(NQF, (dispatch, ownProps) => {
  return {
    S7TiposProjeto: {
      fetch(params) {
        return dispatch(fetch(params))
      }
    },

    updateFormRaw(model, action, props) {
      return dispatch(updateFormRaw(model, action, props))
    },

    addModal(name) {
      return dispatch(addModal(name))
    },
  }
}))
export default class NovoProjeto extends Component {
  constructor(props) {
    super(props)

    this.formId = IdGenerator.default()
    this.formSubmit = null

    this.model = 'S7Projeto'
    this.action = 'create'

    this.previewPicture = React.createRef()
    this.inputPicture = React.createRef()

    this.handleTextInputChange = this.handleTextInputChange.bind(this)
    this.submit = this.submit.bind(this)
  }

  componentDidMount() {
    const { S7TiposProjeto } = this.props

    this.props.addModal('ModalDatPagamentoCreate')
    this.props.addModal('ModalDatPagamentoUpdate')

    this.props.addModal('ModalDescricaoCreate')
    this.props.addModal('ModalDescricaoUpdate')

    this.props.addModal('ModalProProjetoCreate')
    this.props.addModal('ModalProProjetoUpdate')

    S7TiposProjeto.fetch()
  }

  updateState(state = {}) {  
    this.setState((prevState, props) => {
      return update(prevState, state)
    })
  }

  fillAddress(cep) {
    if (cep.charAt(9) !== '' && cep.charAt(9) !== '_') {
      const cepNumber = parseInt(cep.replace('.', '').replace('-', ''))

      if (cepNumber) {
        axios({
          method: 'get',
          url: `https://viacep.com.br/ws/${cepNumber}/json/`
        }).then((response) => {
          console.log(response)
          this.props.updateFormRaw('S7Empresa', 'create', {
            $merge: {
              rua: response.data.logradouro,
              numero: response.data.complemento,
              bairro: response.data.bairro,
              cidade: response.data.localidade,
              estado: trocarNomeSiglaEstado(response.data.uf),
              pais: 'Brasil',
            }
          })
        }).catch((err) => console.error(err))
      }
    }
  }

  handleTextInputChange(event) {
    if (event.value) {
      if (event.attribute === 'cep') this.fillAddress(event.value)
    }
  }

  submit() {
    this.formSubmit()
  }

  render() {
    const { tiposProjeto } = this.props

    const generalFormProps = {
      model: this.model,
      action: this.action
    }

    return (
      <div className="page control-page">
        <div className="page-header">
          <div className="page-titles">
            <Link to="/projetos" className="page-tie">Projetos</Link>
            <p className="page-title">Novo Projeto</p>
            <p className="page-subtitle">Informe os dados em seus respectivos campos:</p>
            <p className="page-text"></p>
          </div>
        </div>
        <div className="page-content">

          <Form {...generalFormProps} id={this.formId} submit={(fn) => { this.formSubmit = fn }}>
            <div className="inputs">
              <div className="multi-input">
                <TextInput {...generalFormProps} classes="w3" label="Nome" attribute="nome"/>
                <SuggestionInput {...generalFormProps} classes="w2-q1" label="Empresa" attribute="id_s7_empresa" collection="empresas"/>
              </div>
              <SelectInput {...generalFormProps} size="w3" label="Tipo" attribute="tipo" options={tiposProjeto}/>
              <div className="multi-input">
                <DateInput {...generalFormProps} classes="w1-q3" label="Data de abertura" attribute="data_abertura"/>
                <DateInput {...generalFormProps} classes="w1-q3" label="Data de fechamento" attribute="data_fechamento"/>
              </div>
              <div className="multi-input">
                <SelectInput {...generalFormProps} size="w1-q3" label="Status" attribute="status"
                             options={[
                    { value: 'Open', label: 'Open' },
                    { value: 'Ativo 1', label: 'Ativo 1' },
                    { value: 'Ativo 2', label: 'Ativo 2' },
                    { value: 'Ativo 3', label: 'Ativo 3' },
                    { value: 'Lost', label: 'Lost' },
                    { value: 'Finalizado', label: 'Finalizado' },
                    { value: 'Stand-by', label: 'Stand-by' },
                    { value: 'On hold', label: 'On hold' },
                  ]}/>
                <CurrencyInput {...generalFormProps} classes="w2-q2" label="Valor total" attribute="valor_total" mask={textMasks.BRL}/>
                <SuggestionInput {...generalFormProps} classes="w2-q3" label="Contato no cliente" attribute="contato_cliente" collection="profissionais"/>
              </div>
            </div>
          </Form>

          <TextAttributeFormSection
            title="Descrição"
            message="Editar descrição"
            icon={IcPlusSign}
            onCreateModalName="ModalDescricaoCreate">
            <ModalDescricaoCreate/>
          </TextAttributeFormSection>

          <ObjectArrayAttributeFormSection
            model="S7Projeto"
            action="create"
            attribute="datas_pagamento"
            title="Datas de pagamento"
            message="Adicionar data"
            icon={IcPlusSign}
            attributeToDisplay="data"
            onCreateModalName="ModalDatPagamentoCreate"
            onUpdateModalName="ModalDatPagamentoUpdate">
            <ModalDatPagamentoCreate/>
            <ModalDatPagamentoUpdate/>
          </ObjectArrayAttributeFormSection>

          <FormSection 
            model="ProfissionalProjeto"
            action="create"
            title="Profissionais"
            message="Adicionar profissional"
            icon={IcPlusSign}
            attributeToDisplay="id_s7_profissional"
            onCreateModalName="ModalProProjetoCreate"
            onUpdateModalName="ModalProProjetoUpdate">
            <ModalProProjetoCreate dependsOn={this.formId}/>
            <ModalProProjetoUpdate dependsOn={this.formId}/>
          </FormSection>

          <button className="btn btn-four" type="submit" onClick={this.submit}>Salvar</button>

          <div className="bottombar"></div>
        </div>
      </div>
    )
  }
}

