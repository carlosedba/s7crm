import React, { Component } from 'react'
import { connect } from 'react-redux'

import { logout } from '@/actions/Session'

@connect((state) => {
  return {
    pending: state.Session.auth.pending,
    authenticated: state.Session.auth.authenticated,
  }
}, (dispatch, ownProps) => {
  return {
    logout() {
      return dispatch(logout())
    },
  }
})
export default class Logout extends Component {
  constructor(props) {
    super(props)
  }

  componentDidMount() {
    console.log('Logout > componentDidMount called!')

    this.props.logout()

    const { history, pending, authenticated } = this.props
    if (!authenticated) history.replace('/login')
  }

  componentDidUpdate(prevProps, prevState) {
    const { history, pending, authenticated } = this.props
    if (!authenticated) history.replace('/login')
  }

  render() {
    return (
      <div className="page">
        <div className="page-loading">
          <div className="lds-dual-ring"></div>
        </div>
      </div>
    )
  }
}

