import React, { Component } from 'react'
import { connect } from 'react-redux'
import { Link } from 'react-router-dom'
import update from 'immutability-helper'
import axios from 'axios'

import { updateFormRaw, updateFormComputed } from '@/actions/Form'
import { addModal } from '@/actions/Modal'
import * as S7Paises from '@/actions/S7Paises'

import { trocarNomeSiglaEstado } from '@/utils/BrasilUtils'
import IdGenerator from '@/utils/IdGenerator'

import Form from '@/components/Form'
import TextInput from '@/components/Input/TextInput'
import SuggestionInput from '@/components/Input/SuggestionInput'
import CurrencyInput from '@/components/Input/CurrencyInput'
import SelectInput from '@/components/Select'

import FormSection from '@/components/FormSection'
import ArrayAttributeFormSection from '@/components/FormSection/ArrayAttributeFormSection'
import ObjectArrayAttributeFormSection from '@/components/FormSection/ObjectArrayAttributeFormSection'

import ModalConAdministrativoCreate from '@/components/Modal/ModalConAdministrativoCreate'
import ModalConAdministrativoUpdate from '@/components/Modal/ModalConAdministrativoUpdate'

import ModalPriContatosCreate from '@/components/Modal/ModalPriContatosCreate'
import ModalPriContatosUpdate from '@/components/Modal/ModalPriContatosUpdate'

import ModalRegionalCreate from '@/components/Modal/ModalRegionalCreate'
import ModalRegionalUpdate from '@/components/Modal/ModalRegionalUpdate'

import IcPlusSign from '@/icons/ic_plus_sign'

import * as textMasks from '@/textMasks'
import NQForm from '@/NQForm'

const NQF = new NQForm()

@connect((state) => {
  return {
    paises: state.S7Paises.items.reduce((acc, currentValue) => {
      return acc.concat({ value: currentValue.id, label: currentValue.nome })
    }, []),
  }
}, NQF.mapDispatchToProps.bind(NQF, (dispatch, ownProps) => {
  return {
    S7Paises: {
      fetch(params) {
        return dispatch(S7Paises.fetch(params))
      }
    },

    updateFormRaw(model, action, props) {
      return dispatch(updateFormRaw(model, action, props))
    },

    addModal(name) {
      return dispatch(addModal(name))
    },
  }
}))
export default class CadastrarEmpresa extends Component {
  constructor(props) {
    super(props)

    this.formId = IdGenerator.default()
    this.formSubmit = null

    this.model = 'S7Empresa'
    this.action = 'create'

    this.handleTextInputChange = this.handleTextInputChange.bind(this)
    this.handleSubmit = this.handleSubmit.bind(this)
    this.submit = this.submit.bind(this)
  }

  componentDidMount() {
    const { S7Paises } = this.props

    this.props.addModal('ModalConAdministrativoCreate')
    this.props.addModal('ModalConAdministrativoUpdate')

    this.props.addModal('ModalPriContatosCreate')
    this.props.addModal('ModalPriContatosUpdate')

    this.props.addModal('ModalRegionalCreate')
    this.props.addModal('ModalRegionalUpdate')

    S7Paises.fetch()
  }

  fillAddress(cep) {
    if (cep.charAt(9) !== '' && cep.charAt(9) !== '_') {
      const cepNumber = parseInt(cep.replace('.', '').replace('-', ''))

      if (cepNumber) {
        axios({
          method: 'get',
          url: `https://viacep.com.br/ws/${cepNumber}/json/`
        }).then((response) => {
          if (!response.data.erro) {
            this.props.updateFormRaw(this.model, this.action, {
              $merge: {
                rua: response.data.logradouro,
                numero: response.data.complemento,
                bairro: response.data.bairro,
                cidade: response.data.localidade,
                estado: trocarNomeSiglaEstado(response.data.uf),
                pais: 'Brasil',
              }
            })
          }
        }).catch(console.error)
      }
    }
  }

  handleTextInputChange(event) {
    if (event.value) {
      if (event.attribute === 'cep') this.fillAddress(event.value)
    }
  }

  async handleSubmit(event) {
    const { history } = this.props

    const response = window.NetQueue.proccess().catch(console.error)

    if (response) {
      alert('Empresa cadastrada com sucesso!')
      history.push('/empresas')
    }
  }

  submit() {
    this.formSubmit()
  }

  render() {
    const { paises } = this.props

    const generalFormProps = {
      model: this.model,
      action: this.action
    }

    return (
      <div className="page control-page">
        <div className="page-header">
          <div className="page-titles">
            <Link to="/empresas" className="page-tie">Empresas</Link>
            <p className="page-title">Cadastrar Empresa</p>
            <p className="page-subtitle">Informe os dados em seus respectivos campos:</p>
            <p className="page-text"></p>
          </div>
        </div>
        <div className="page-content">

          <Form {...generalFormProps} id={this.formId} submit={(fn) => { this.formSubmit = fn }} onSubmit={this.handleSubmit}>
            <div className="inputs">
              <div className="multi-input">
                <SuggestionInput {...generalFormProps} classes="w2-q2" label="Setor" attribute="id_s7_setor" collection="setores"/>
              </div>
              <div className="multi-input">
                <TextInput {...generalFormProps} classes="w3" label="Nome fantasia" attribute="nome_fantasia"/>
                <TextInput {...generalFormProps} classes="w3" label="Razão Social" attribute="razao_social"/>
                <TextInput {...generalFormProps} classes="w2" label="CNPJ" attribute="cnpj" mask="cnpj"/>
              </div>
              <div className="multi-input">
                <TextInput {...generalFormProps} classes="w1-q1" label="CEP" attribute="cep" mask="cep" onChange={this.handleTextInputChange}/>
                <TextInput {...generalFormProps} classes="w4" label="Rua" attribute="rua"/>
                <TextInput {...generalFormProps} classes="w1" label="Número" attribute="numero"/>
                <TextInput {...generalFormProps} classes="w1" label="Complemento" attribute="complemento"/>
              </div>
              <div className="multi-input">
                <TextInput {...generalFormProps} classes="w2" label="Bairro" attribute="bairro"/>
                <TextInput {...generalFormProps} classes="w2-q1" label="Cidade" attribute="cidade"/>
                <TextInput {...generalFormProps} classes="w2" label="Estado" attribute="estado"/>
                <TextInput {...generalFormProps} classes="w1-q2" label="País" attribute="pais"/>
              </div>
              <div className="multi-input">
                <TextInput {...generalFormProps} classes="w2" label="Telefone" attribute="telefone" mask="telefone"/>
              </div>
              <div className="multi-input">
                <CurrencyInput {...generalFormProps} classes="w2-q2" label="Faturamento" attribute="faturamento" mask={textMasks.BRL}/>
                <TextInput {...generalFormProps} classes="w2-q1" label="Ebitda" attribute="ebitda"/>
                <SelectInput {...generalFormProps} size="w1-q2" label="Porte" attribute="porte"
                             options={[
                    { value: 'Pequeno', label: 'Pequeno' },
                    { value: 'Médio', label: 'Médio' },
                    { value: 'Grande', label: 'Grande' },
                  ]}/>
                <SelectInput {...generalFormProps} size="w1-q2" label="Origem" attribute="origem" options={paises}/>
              </div>
              <div className="multi-input">
                <SelectInput {...generalFormProps} size="w4" label="Formato" attribute="formato" isMulti={true}
                             options={[
                    { value: 'Nacional', label: 'Nacional' },
                    { value: 'Multinacional', label: 'Multinacional' },
                    { value: 'Mista', label: 'Mista' },
                    { value: 'Capital aberto', label: 'Capital aberto' },
                    { value: 'Capital fechado', label: 'Capital fechado' },
                    { value: 'Capital misto', label: 'Capital misto' },
                    { value: 'Listada em bolsa', label: 'Listada em bolsa' },
                    { value: 'Ltda', label: 'Ltda' },
                    { value: 'S.A.', label: 'S.A.' },
                  ]}/>
                <SelectInput {...generalFormProps} size="w2" label="Gestão" attribute="gestao"
                             options={[
                    { value: 'Acionista', label: 'Acionista' },
                    { value: 'Executivo', label: 'Executivo' },
                    { value: 'Híbrido', label: 'Híbrido' },
                  ]}/>
              </div>
              <div className="multi-input">
                <SelectInput {...generalFormProps} size="w4" label="Conselho de administração" attribute="conselho_administracao"
                             options={[
                    { value: 'Nenhum', label: 'Nenhum' },
                    { value: 'Acionistas', label: 'Acionistas' },
                    { value: 'Conselho Independente', label: 'Conselho Independente' },
                    { value: 'Acionistas e Conselho Independente', label: 'Acionistas e Conselho Independente' },
                  ]}/>
                <TextInput {...generalFormProps} classes="w1" label="Head count" attribute="head_count"/>
              </div>
            </div>
          </Form>

          <ArrayAttributeFormSection
            model="S7Empresa"
            action="create"
            attribute="nome_conselheiros"
            title="Conselho Administrativo"
            message="Adicionar pessoa"
            icon={IcPlusSign}
            onCreateModalName="ModalConAdministrativoCreate"
            onUpdateModalName="ModalConAdministrativoUpdate">
            <ModalConAdministrativoCreate/>
            <ModalConAdministrativoUpdate/>
          </ArrayAttributeFormSection>

          <FormSection 
            model="S7PrincipalContato"
            action="create"
            title="Principais contatos"
            message="Adicionar contato"
            icon={IcPlusSign}
            attributeToDisplay="nome"
            onCreateModalName="ModalPriContatosCreate"
            onUpdateModalName="ModalPriContatosUpdate">
            <ModalPriContatosCreate dependsOn={{ id: this.formId, key: 'id_s7_empresa' }}/>
            <ModalPriContatosUpdate dependsOn={{ id: this.formId, key: 'id_s7_empresa' }}/>
          </FormSection>

          <ObjectArrayAttributeFormSection
            model="S7Empresa"
            action="create"
            attribute="regionais"
            title="Regionais"
            message="Adicionar regional"
            icon={IcPlusSign}
            attributeToDisplay="nome"
            onCreateModalName="ModalRegionalCreate"
            onUpdateModalName="ModalRegionalUpdate">
            <ModalRegionalCreate/>
            <ModalRegionalUpdate/>
          </ObjectArrayAttributeFormSection>

          <button className="btn btn-four" type="submit" onClick={this.submit}>Salvar</button>

          <div className="bottombar"></div>
        </div>
      </div>
    )
  }
}

