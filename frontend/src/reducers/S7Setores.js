import {
	API_FIND_ALL_S7_SETORES,
	API_FIND_ONE_S7_SETOR,
	API_CREATE_S7_SETOR,
	API_UPDATE_S7_SETOR,
	API_DELETE_S7_SETOR,
	CHANGE_ACTIVE_S7_SETOR,
	CHANGE_NEW_S7_SETOR,
	CHANGE_UPDATED_S7_SETOR
} from '@/actionTypes'

const INITIAL_STATE = {
	list: 			{ items: [], error: null, loading: null },
	active: 		{ item: null, error: null, loading: null },
	new: 			{ item: null, error: null, loading: null },
	updated: 		{ item: null, error: null, loading: null },
	deleted: 		{ item: null, error: null, loading: null },
}

export default function(state = INITIAL_STATE, action) {
  let error

  switch (action.type) {
		case API_FIND_ALL_S7_SETORES:
			return { ...state, list: { items: [], error: null, loading: null } }

		case `${API_FIND_ALL_S7_SETORES}_PENDING`:
			return { ...state, list: { items: [], error: null, loading: true } }

		case `${API_FIND_ALL_S7_SETORES}_FULFILLED`:
			return { ...state, list: { items: action.payload.data, error: null, loading: false } }

		case `${API_FIND_ALL_S7_SETORES}_REJECTED`:
			error = action.payload.data || { message: action.payload.message }
			return { ...state, list: { items: [], error: error, loading: false } }


		case API_FIND_ONE_S7_SETOR:
			return { ...state, active: { item: null, error: null, loading: null } }

		case `${API_FIND_ONE_S7_SETOR}_PENDING`:
			return { ...state, active: { item: null, error: null, loading: true } }

		case `${API_FIND_ONE_S7_SETOR}_FULFILLED`:
			return { ...state, active: { item: action.payload.data, error: null, loading: false } }

		case `${API_FIND_ONE_S7_SETOR}_REJECTED`:
			error = action.payload.data || { message: action.payload.message }
			return { ...state, active: { item: null, error: error, loading: false } }


		case API_CREATE_S7_SETOR:
			return { ...state, new: { item: null, error: null, loading: null } }

		case `${API_CREATE_S7_SETOR}_PENDING`:
			return { ...state, new: { item: null, error: null, loading: true } }

		case `${API_CREATE_S7_SETOR}_FULFILLED`:
			return { ...state, new: { item: action.payload.data, error: null, loading: false } }

		case `${API_CREATE_S7_SETOR}_REJECTED`:
			error = action.payload.data || { message: action.payload.message }
			return { ...state, new: { item: null, error: error, loading: false } }


		case API_UPDATE_S7_SETOR:
			return { ...state, updated: { item: null, error: null, loading: null } }

		case `${API_UPDATE_S7_SETOR}_PENDING`:
			return { ...state, updated: { item: null, error: null, loading: true } }

		case `${API_UPDATE_S7_SETOR}_FULFILLED`:
			return { ...state, updated: { item: action.payload.data, error: null, loading: false } }

		case `${API_UPDATE_S7_SETOR}_REJECTED`:
			error = action.payload.data || { message: action.payload.message }
			return { ...state, updated: { item: null, error: error, loading: false } }


		case API_DELETE_S7_SETOR:
			return { ...state, deleted: { item: null, error: null, loading: null } }

		case `${API_DELETE_S7_SETOR}_PENDING`:
			return { ...state, deleted: { item: null, error: null, loading: true } }

		case `${API_DELETE_S7_SETOR}_FULFILLED`:
			return { ...state, deleted: { item: action.payload.data, error: null, loading: false } }

		case `${API_DELETE_S7_SETOR}_REJECTED`:
			error = action.payload.data || { message: action.payload.message }
			return { ...state, deleted: { item: null, error: error, loading: false } }


		case CHANGE_ACTIVE_S7_SETOR:
			return { ...state, active: { item: action.payload, error: null, loading: null } }


		case CHANGE_NEW_S7_SETOR:
			return { ...state, new: { item: action.payload, error: null, loading: null } }


		case CHANGE_UPDATED_S7_SETOR:
			return { ...state, updated: { item: action.payload, error: null, loading: null } }

		default:
			return state;
  }
}
