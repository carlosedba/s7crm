import {
	API_FIND_ALL_S7_PRINCIPAIS_CONTATOS,
	API_FIND_ONE_S7_PRINCIPAL_CONTATO, 
	API_CREATE_S7_PRINCIPAL_CONTATO,
	API_UPDATE_S7_PRINCIPAL_CONTATO,
	API_DELETE_S7_PRINCIPAL_CONTATO,
	CHANGE_ACTIVE_S7_PRINCIPAL_CONTATO,
	CHANGE_NEW_S7_PRINCIPAL_CONTATO,
	CHANGE_UPDATED_S7_PRINCIPAL_CONTATO
} from '@/actionTypes'
Ç
const INITIAL_STATE = {
	list: 			{ items: [], error: null, loading: null },
	active: 		{ item: null, error: null, loading: null },
	new: 				{ item: null, error: null, loading: null },
	updated: 		{ item: null, error: null, loading: null },
	deleted: 		{ item: null, error: null, loading: null },
}

export default function(state = INITIAL_STATE, action) {
  let error

  switch (action.type) {
		case API_FIND_ALL_S7_PRINCIPAIS_CONTATOS:
			return { ...state, list: { items: [], error: null, loading: null } }

		case `${API_FIND_ALL_S7_PRINCIPAIS_CONTATOS}_PENDING`:
			return { ...state, list: { items: [], error: null, loading: true } }

		case `${API_FIND_ALL_S7_PRINCIPAIS_CONTATOS}_FULFILLED`:
			return { ...state, list: { items: action.payload.data, error: null, loading: false } }

		case `${API_FIND_ALL_S7_PRINCIPAIS_CONTATOS}_REJECTED`:
			error = action.payload.data || { message: action.payload.message }
			return { ...state, list: { items: [], error: error, loading: false } }


		case API_FIND_ONE_S7_PRINCIPAL_CONTATO:
			return { ...state, active: { item: null, error: null, loading: null } }

		case `${API_FIND_ONE_S7_PRINCIPAL_CONTATO}_PENDING`:
			return { ...state, active: { item: null, error: null, loading: true } }

		case `${API_FIND_ONE_S7_PRINCIPAL_CONTATO}_FULFILLED`:
			return { ...state, active: { item: action.payload.data, error: null, loading: false } }

		case `${API_FIND_ONE_S7_PRINCIPAL_CONTATO}_REJECTED`:
			error = action.payload.data || { message: action.payload.message }
			return { ...state, active: { item: null, error: error, loading: false } }


		case API_CREATE_S7_PRINCIPAL_CONTATO:
			return { ...state, new: { item: null, error: null, loading: null } }

		case `${API_CREATE_S7_PRINCIPAL_CONTATO}_PENDING`:
			return { ...state, new: { item: null, error: null, loading: true } }

		case `${API_CREATE_S7_PRINCIPAL_CONTATO}_FULFILLED`:
			return { ...state, new: { item: action.payload.data, error: null, loading: false } }

		case `${API_CREATE_S7_PRINCIPAL_CONTATO}_REJECTED`:
			error = action.payload.data || { message: action.payload.message }
			return { ...state, new: { item: null, error: error, loading: false } }


		case API_UPDATE_S7_PRINCIPAL_CONTATO:
			return { ...state, updated: { item: null, error: null, loading: null } }

		case `${API_UPDATE_S7_PRINCIPAL_CONTATO}_PENDING`:
			return { ...state, updated: { item: null, error: null, loading: true } }

		case `${API_UPDATE_S7_PRINCIPAL_CONTATO}_FULFILLED`:
			return { ...state, updated: { item: action.payload.data, error: null, loading: false } }

		case `${API_UPDATE_S7_PRINCIPAL_CONTATO}_REJECTED`:
			error = action.payload.data || { message: action.payload.message }
			return { ...state, updated: { item: null, error: error, loading: false } }


		case API_DELETE_S7_PRINCIPAL_CONTATO:
			return { ...state, deleted: { item: null, error: null, loading: null } }

		case `${API_DELETE_S7_PRINCIPAL_CONTATO}_PENDING`:
			return { ...state, deleted: { item: null, error: null, loading: true } }

		case `${API_DELETE_S7_PRINCIPAL_CONTATO}_FULFILLED`:
			return { ...state, deleted: { item: action.payload.data, error: null, loading: false } }

		case `${API_DELETE_S7_PRINCIPAL_CONTATO}_REJECTED`:
			error = action.payload.data || { message: action.payload.message }
			return { ...state, deleted: { item: null, error: error, loading: false } }


		case CHANGE_ACTIVE_S7_PRINCIPAL_CONTATO:
			return { ...state, active: { item: action.payload, error: null, loading: null } }


		case CHANGE_NEW_S7_PRINCIPAL_CONTATO:
			return { ...state, new: { item: action.payload, error: null, loading: null } }


		case CHANGE_UPDATED_S7_PRINCIPAL_CONTATO:
			return { ...state, updated: { item: action.payload, error: null, loading: null } }

		default:
			return state
  }
}
