import {
  API_FETCH_S7_PAISES
} from '@/actionTypes'

const INITIAL_STATE = {
  items: [], loading: false, error: null
}

export default function(state = INITIAL_STATE, action) {
  let data, error

  switch (action.type) {
    case API_FETCH_S7_PAISES:
      return { ...state, items: [], loading: false, error: null }

    case `${API_FETCH_S7_PAISES}_PENDING`:
      return { ...state, loading: true }

    case `${API_FETCH_S7_PAISES}_FULFILLED`:
      data = action.payload.data

      return { ...state, items: data, loading: false }

    case `${API_FETCH_S7_PAISES}_REJECTED`:
      error = action.payload.data || { message: action.payload.message }

      return { ...state, error: error, loading: false }

    default:
      return state
  }
}

