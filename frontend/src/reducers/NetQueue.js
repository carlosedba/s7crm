import {
	ADD_ITEM_TO_NETQUEUE, UPDATE_ITEM_FROM_NETQUEUE, REMOVE_ITEM_FROM_NETQUEUE, CLEAR_NETQUEUE, PROCCESS_NETQUEUE,
} from '@/actionTypes'

import update from 'immutability-helper'

const INITIAL_STATE = {
	items: [],
	status: { pending: false, response: null, error: null }
}

export default function(state = INITIAL_STATE, action) {
  let response, error

  switch (action.type) {
		case ADD_ITEM_TO_NETQUEUE:
			return update(state, {
				items: { $push: [action.payload.item] }
			})

		case UPDATE_ITEM_FROM_NETQUEUE:
			return update(state, {
				items: {
					[action.payload.index]: { $merge: action.payload.item }
				}
			})

		case REMOVE_ITEM_FROM_NETQUEUE:
			return update(state, {
				items: { $splice: [[action.payload.index, 1]] }
			})

		case CLEAR_NETQUEUE:
			return update(state, {
				items: { $set: [] }
			})

		case 'TEST':
			console.log(state)
			return { ...state }

		case PROCCESS_NETQUEUE:
			return { ...state, status: { pending: false, response: null, error: null } }

		case `${PROCCESS_NETQUEUE}_PENDING`:
			return { ...state, status: { pending: true, response: null, error: null } }

		case `${PROCCESS_NETQUEUE}_FULFILLED`:
			response = action.payload.data

			return { ...state, status: { pending: false, response: response, error: null } }

		case `${PROCCESS_NETQUEUE}_REJECTED`:
			error = action.payload.data || { message: action.payload.message }

			return { ...state, status: { pending: false, response: null, error: error } }

		default:
			return state
  }
}

