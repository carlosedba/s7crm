import {
	FETCH_CONFIGS,
	FETCH_CONFIG,
	CREATE_CONFIG,
	UPDATE_CONFIG,
	DELETE_CONFIG,
} from '@/actionTypes'

const INITIAL_STATE = {
	list: 			{ items: [], error: null, loading: null },
	active: 		{ item: null, error: null, loading: null },
	new: 			{ item: null, error: null, loading: null },
	updated: 		{ item: null, error: null, loading: null },
	deleted: 		{ item: null, error: null, loading: null },
}

export default function(state = INITIAL_STATE, action) {
  let error

  switch (action.type) {
		case FETCH_CONFIGS:
			return { ...state, list: { items: [], error: null, loading: null } }

		case `${FETCH_CONFIGS}_PENDING`:
			return { ...state, list: { items: [], error: null, loading: true } }

		case `${FETCH_CONFIGS}_FULFILLED`:
			return { ...state, list: { items: action.payload.data, error: null, loading: false } }

		case `${FETCH_CONFIGS}_REJECTED`:
			error = action.payload.data || { message: action.payload.message }
			return { ...state, list: { items: [], error: error, loading: false } }


		case FETCH_CONFIG:
			return { ...state, active: { item: null, error: null, loading: null } }

		case `${FETCH_CONFIG}_PENDING`:
			return { ...state, active: { item: null, error: null, loading: true } }

		case `${FETCH_CONFIG}_FULFILLED`:
			return { ...state, active: { item: action.payload.data, error: null, loading: false } }

		case `${FETCH_CONFIG}_REJECTED`:
			error = action.payload.data || { message: action.payload.message }
			return { ...state, active: { item: null, error: error, loading: false } }


		case CREATE_CONFIG:
			return { ...state, new: { item: null, error: null, loading: null } }

		case `${CREATE_CONFIG}_PENDING`:
			return { ...state, new: { item: null, error: null, loading: true } }

		case `${CREATE_CONFIG}_FULFILLED`:
			return { ...state, new: { item: action.payload.data, error: null, loading: false } }

		case `${CREATE_CONFIG}_REJECTED`:
			error = action.payload.data || { message: action.payload.message }
			return { ...state, new: { item: null, error: error, loading: false } }


		case UPDATE_CONFIG:
			return { ...state, updated: { item: null, error: null, loading: null } }

		case `${UPDATE_CONFIG}_PENDING`:
			return { ...state, updated: { item: null, error: null, loading: true } }

		case `${UPDATE_CONFIG}_FULFILLED`:
			return { ...state, updated: { item: action.payload.data, error: null, loading: false } }

		case `${UPDATE_CONFIG}_REJECTED`:
			error = action.payload.data || { message: action.payload.message }
			return { ...state, updated: { item: null, error: error, loading: false } }


		case DELETE_CONFIG:
			return { ...state, deleted: { item: null, error: null, loading: null } }

		case `${DELETE_CONFIG}_PENDING`:
			return { ...state, deleted: { item: null, error: null, loading: true } }

		case `${DELETE_CONFIG}_FULFILLED`:
			return { ...state, deleted: { item: action.payload.data, error: null, loading: false } }

		case `${DELETE_CONFIG}_REJECTED`:
			error = action.payload.data || { message: action.payload.message }
			return { ...state, deleted: { item: null, error: error, loading: false } }

		default:
			return state
  }
}
