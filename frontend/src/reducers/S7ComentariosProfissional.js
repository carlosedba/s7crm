import {
	API_FIND_ALL_S7_COMENTARIOS_PROFISSIONAL,
	API_FIND_ONE_S7_COMENTARIO_PROFISSIONAL,
	API_CREATE_S7_COMENTARIO_PROFISSIONAL, 
	API_UPDATE_S7_COMENTARIO_PROFISSIONAL,
	API_DELETE_S7_COMENTARIO_PROFISSIONAL,
	CHANGE_ACTIVE_S7_COMENTARIO_PROFISSIONAL,
	CHANGE_NEW_S7_COMENTARIO_PROFISSIONAL,
	CHANGE_UPDATED_S7_COMENTARIO_PROFISSIONAL
} from '@/actionTypes'

const INITIAL_STATE = {
	list: 			{ items: [], error: null, loading: null },
	active: 		{ item: null, error: null, loading: null },
	new: 				{ item: null, error: null, loading: null },
	updated: 		{ item: null, error: null, loading: null },
	deleted: 		{ item: null, error: null, loading: null },
}

export default function(state = INITIAL_STATE, action) {
  let error

  switch (action.type) {
		case API_FIND_ALL_S7_COMENTARIOS_PROFISSIONAL:
			return { ...state, list: { items: [], error: null, loading: null } }

		case `${API_FIND_ALL_S7_COMENTARIOS_PROFISSIONAL}_PENDING`:
			return { ...state, list: { items: [], error: null, loading: true } }

		case `${API_FIND_ALL_S7_COMENTARIOS_PROFISSIONAL}_FULFILLED`:
			return { ...state, list: { items: action.payload.data, error: null, loading: false } }

		case `${API_FIND_ALL_S7_COMENTARIOS_PROFISSIONAL}_REJECTED`:
			error = action.payload.data || { message: action.payload.message }
			return { ...state, list: { items: [], error: error, loading: false } }


		case API_FIND_ONE_S7_COMENTARIO_PROFISSIONAL:
			return { ...state, active: { item: null, error: null, loading: null } }

		case `${API_FIND_ONE_S7_COMENTARIO_PROFISSIONAL}_PENDING`:
			return { ...state, active: { item: null, error: null, loading: true } }

		case `${API_FIND_ONE_S7_COMENTARIO_PROFISSIONAL}_FULFILLED`:
			return { ...state, active: { item: action.payload.data, error: null, loading: false } }

		case `${API_FIND_ONE_S7_COMENTARIO_PROFISSIONAL}_REJECTED`:
			error = action.payload.data || { message: action.payload.message }
			return { ...state, active: { item: null, error: error, loading: false } }


		case API_CREATE_S7_COMENTARIO_PROFISSIONAL:
			return { ...state, new: { item: null, error: null, loading: null } }

		case `${API_CREATE_S7_COMENTARIO_PROFISSIONAL}_PENDING`:
			return { ...state, new: { item: null, error: null, loading: true } }

		case `${API_CREATE_S7_COMENTARIO_PROFISSIONAL}_FULFILLED`:
			return { ...state, new: { item: action.payload.data, error: null, loading: false } }

		case `${API_CREATE_S7_COMENTARIO_PROFISSIONAL}_REJECTED`:
			error = action.payload.data || { message: action.payload.message }
			return { ...state, new: { item: null, error: error, loading: false } }


		case API_UPDATE_S7_COMENTARIO_PROFISSIONAL:
			return { ...state, updated: { item: null, error: null, loading: null } }

		case `${API_UPDATE_S7_COMENTARIO_PROFISSIONAL}_PENDING`:
			return { ...state, updated: { item: null, error: null, loading: true } }

		case `${API_UPDATE_S7_COMENTARIO_PROFISSIONAL}_FULFILLED`:
			return { ...state, updated: { item: action.payload.data, error: null, loading: false } }

		case `${API_UPDATE_S7_COMENTARIO_PROFISSIONAL}_REJECTED`:
			error = action.payload.data || { message: action.payload.message }
			return { ...state, updated: { item: null, error: error, loading: false } }


		case API_DELETE_S7_COMENTARIO_PROFISSIONAL:
			return { ...state, deleted: { item: null, error: null, loading: null } }

		case `${API_DELETE_S7_COMENTARIO_PROFISSIONAL}_PENDING`:
			return { ...state, deleted: { item: null, error: null, loading: true } }

		case `${API_DELETE_S7_COMENTARIO_PROFISSIONAL}_FULFILLED`:
			return { ...state, deleted: { item: action.payload.data, error: null, loading: false } }

		case `${API_DELETE_S7_COMENTARIO_PROFISSIONAL}_REJECTED`:
			error = action.payload.data || { message: action.payload.message }
			return { ...state, deleted: { item: null, error: error, loading: false } }


		case CHANGE_ACTIVE_S7_COMENTARIO_PROFISSIONAL:
			return { ...state, active: { item: action.payload, error: null, loading: null } }


		case CHANGE_NEW_S7_COMENTARIO_PROFISSIONAL:
			return { ...state, new: { item: action.payload, error: null, loading: null } }


		case CHANGE_UPDATED_S7_COMENTARIO_PROFISSIONAL:
			return { ...state, updated: { item: action.payload, error: null, loading: null } }

		default:
			return state
  }
}
