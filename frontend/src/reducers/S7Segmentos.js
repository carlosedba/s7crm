import {
	API_AUTOCOMPLETE_S7_SEGMENTOS,
	API_FIND_ALL_S7_SEGMENTOS,
	API_FIND_ONE_S7_SEGMENTO, 
	API_CREATE_S7_SEGMENTO, 
	API_UPDATE_S7_SEGMENTO, 
	API_DELETE_S7_SEGMENTO, 
	CHANGE_AUTOCOMPLETE_S7_SEGMENTOS,
	CHANGE_ACTIVE_S7_SEGMENTO,
	CHANGE_NEW_S7_SEGMENTO,
	CHANGE_UPDATED_S7_SEGMENTO
} from '@/actionTypes'

const INITIAL_STATE = {
	autocomplete: 	{ items: [], error: null, loading: null },
	list: 			  	{ items: [], error: null, loading: null },
	active: 				{ item: null, error: null, loading: null },
	new: 						{ item: null, error: null, loading: null },
	updated: 				{ item: null, error: null, loading: null },
	deleted: 				{ item: null, error: null, loading: null },
}

export default function(state = INITIAL_STATE, action) {
  let error

  switch (action.type) {
		case API_AUTOCOMPLETE_S7_SEGMENTOS:
			return { ...state, autocomplete: { items: [], error: null, loading: null } }

		case `${API_AUTOCOMPLETE_S7_SEGMENTOS}_PENDING`:
			return { ...state, autocomplete: { items: [], error: null, loading: true } }

		case `${API_AUTOCOMPLETE_S7_SEGMENTOS}_FULFILLED`:
			return { ...state, autocomplete: { items: action.payload.data, error: null, loading: false } }

		case `${API_AUTOCOMPLETE_S7_SEGMENTOS}_REJECTED`:
			error = action.payload.data || { message: action.payload.message }
			return { ...state, autocomplete: { items: [], error: error, loading: false } }


		case API_FIND_ONE_S7_SEGMENTO:
			return { ...state, active: { item: null, error: null, loading: null } }

		case `${API_FIND_ONE_S7_SEGMENTO}_PENDING`:
			return { ...state, active: { item: null, error: null, loading: true } }

		case `${API_FIND_ONE_S7_SEGMENTO}_FULFILLED`:
			return { ...state, active: { item: action.payload.data, error: null, loading: false } }

		case `${API_FIND_ONE_S7_SEGMENTO}_REJECTED`:
			error = action.payload.data || { message: action.payload.message }
			return { ...state, active: { item: null, error: error, loading: false } }


		case API_CREATE_S7_SEGMENTO:
			return { ...state, new: { item: null, error: null, loading: null } }

		case `${API_CREATE_S7_SEGMENTO}_PENDING`:
			return { ...state, new: { item: null, error: null, loading: true } }

		case `${API_CREATE_S7_SEGMENTO}_FULFILLED`:
			return { ...state, new: { item: action.payload.data, error: null, loading: false } }

		case `${API_CREATE_S7_SEGMENTO}_REJECTED`:
			error = action.payload.data || { message: action.payload.message }
			return { ...state, new: { item: null, error: error, loading: false } }


		case API_UPDATE_S7_SEGMENTO:
			return { ...state, updated: { item: null, error: null, loading: null } }

		case `${API_UPDATE_S7_SEGMENTO}_PENDING`:
			return { ...state, updated: { item: null, error: null, loading: true } }

		case `${API_UPDATE_S7_SEGMENTO}_FULFILLED`:
			return { ...state, updated: { item: action.payload.data, error: null, loading: false } }

		case `${API_UPDATE_S7_SEGMENTO}_REJECTED`:
			error = action.payload.data || { message: action.payload.message }
			return { ...state, updated: { item: null, error: error, loading: false } }


		case API_DELETE_S7_SEGMENTO:
			return { ...state, deleted: { item: null, error: null, loading: null } }

		case `${API_DELETE_S7_SEGMENTO}_PENDING`:
			return { ...state, deleted: { item: null, error: null, loading: true } }

		case `${API_DELETE_S7_SEGMENTO}_FULFILLED`:
			return { ...state, deleted: { item: action.payload.data, error: null, loading: false } }

		case `${API_DELETE_S7_SEGMENTO}_REJECTED`:
			error = action.payload.data || { message: action.payload.message }
			return { ...state, deleted: { item: null, error: error, loading: false } }


		case CHANGE_AUTOCOMPLETE_S7_SEGMENTOS:
			return { ...state, autocomplete: { items: action.payload, error: null, loading: null } }


		case CHANGE_ACTIVE_S7_SEGMENTO:
			return { ...state, active: { item: action.payload, error: null, loading: null } }


		case CHANGE_NEW_S7_SEGMENTO:
			return { ...state, new: { item: action.payload, error: null, loading: null } }


		case CHANGE_UPDATED_S7_SEGMENTO:
			return { ...state, updated: { item: action.payload, error: null, loading: null } }

		default:
			return state;
  }
}
