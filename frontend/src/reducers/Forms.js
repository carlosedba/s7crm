import {
  ADD_FORM, UPDATE_FORM, UPDATE_FORM_RAW, UPDATE_FORM_COMPUTED, RESET_FORM, REMOVE_FORM,
} from '@/actionTypes'

import update from 'immutability-helper'

const INITIAL_STATE = {
  S7Segmento: {
    create: { raw: {}, computed: {} },
    update: { raw: {}, computed: {} },
  },

  S7Setor: {
    create: { raw: {}, computed: {} },
    update: { raw: {}, computed: {} },
  },

  S7Cargo: {
    create: { raw: {}, computed: {} },
    update: { raw: {}, computed: {} },
  },

  S7Beneficio: {
    create: { raw: {}, computed: {} },
    update: { raw: {}, computed: {} },
  },

  S7Idioma: {
    create: { raw: {}, computed: {} },
    update: { raw: {}, computed: {} },
  },

  S7Instituicao: {
    create: { raw: {}, computed: {} },
    update: { raw: {}, computed: {} },
  },

  S7Curso: {
    create: { raw: {}, computed: {} },
    update: { raw: {}, computed: {} },
  },

  S7Empresa: {
    create: { raw: {}, computed: {} },
    update: { raw: {}, computed: {} },
  },

  Regional: {
    create: { raw: {}, computed: {} },
    update: { raw: {}, computed: {} },
  },

  NomeConselheiros: {
    create: { raw: {}, computed: {} },
    update: { raw: {}, computed: {} },
  },

  S7PrincipalContato: {
    create: { raw: {}, computed: {} },
    update: { raw: {}, computed: {} },
  },

  S7Profissional: {
    create: { raw: {}, computed: {} },
    update: { raw: {}, computed: {} },
  },

  S7ExperienciaProfissional: {
    create: { raw: {}, computed: {} },
    update: { raw: {}, computed: {} },
  },

  S7Formacao: {
    create: { raw: {}, computed: {} },
    update: { raw: {}, computed: {} },
  },

  S7ComentarioProfissional: {
    create: { raw: {}, computed: {} },
    update: { raw: {}, computed: {} },
  },

  S7Projeto: {
    create: { raw: {}, computed: {} },
    update: { raw: {}, computed: {} },
  },

  DataPagamento: {
    create: { raw: {}, computed: {} },
    update: { raw: {}, computed: {} },
  },

  ProfissionalProjeto: {
    create: { raw: {}, computed: {} },
    update: { raw: {}, computed: {} },
  },

  S7ProfissionalSelecionado: {
    create: { raw: {}, computed: {} },
    update: { raw: {}, computed: {} },
  },
}

export default function(state = INITIAL_STATE, action) {
  let error

  switch (action.type) {
    // separate between raw and computed
    case UPDATE_FORM:
      return update(state, {
        [action.payload.model]: {
          [action.payload.action]: action.payload.props
        }
      })

    case UPDATE_FORM_RAW:
      return update(state, {
        [action.payload.model]: {
          [action.payload.action]: {
            raw: action.payload.props
          }
        }
      })

    case UPDATE_FORM_COMPUTED:
      return update(state, {
        [action.payload.model]: {
          [action.payload.action]: {
            computed: action.payload.props
          }
        }
      })

    case RESET_FORM:
      return update(state, {
        [action.payload.model]: {
          [action.payload.action]: { $set: {} }
        }
      })

    default:
      return state
  }
}

