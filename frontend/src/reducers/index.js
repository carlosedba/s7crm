import { combineReducers } from 'redux'

import Session from './Session'
import Users from './Users'
import Input from './Input'
import Modal from './Modal'
import Forms from './Forms'
import NetQueue from './NetQueue'
import S7Paises from './S7Paises'
import S7Estados from './S7Estados'
import S7Cidades from './S7Cidades'
import S7Segmentos from './S7Segmentos'
import S7Setores from './S7Setores'
import S7Cargos from './S7Cargos'
import S7Beneficios from './S7Beneficios'
import S7Idiomas from './S7Idiomas'
import S7Instituicoes from './S7Instituicoes'
import S7Cursos from './S7Cursos'
import S7Empresas from './S7Empresas'
import S7TiposProjeto from './S7TiposProjeto'


const reducer = combineReducers({
  Session,
  Users,
  Input,
  Modal,
  Forms,
  NetQueue,
  S7Paises,
  S7Estados,
  S7Cidades,
  S7Segmentos,
  S7Setores,
  S7Cargos,
  S7Beneficios,
  S7Idiomas,
  S7Instituicoes,
  S7Cursos,
  S7Empresas,
  S7TiposProjeto,
})

export default reducer

