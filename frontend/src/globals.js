export const PRODUCTION = false
export const SERVER_PORT = location.port
export const SERVER_ADDRESS = (PRODUCTION) ? '' : `http://127.0.0.1:8641`
export const API_ENDPOINT = `${SERVER_ADDRESS}/api/v1`
export const S7_API_ENDPOINT = `${SERVER_ADDRESS}/api/s7`

