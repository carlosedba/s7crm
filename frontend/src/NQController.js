import update from 'immutability-helper'
import nanoid from 'nanoid'

export default class NQController {
  constructor(props) {
    this.dispatch = null
    this.props = {}
  }

  mapStateToProps(state) {
    this.props = Object.assign({}, this.props, state)

    return state
  }

  mapDispatchToProps(callback, dispatch, ownProps) {
    this.dispatch = dispatch
    this.props = Object.assign({}, this.props, ownProps)

    this.onReceiveProps()

    if (callback) {
      return callback(dispatch, ownProps)
    } else {
      return {}
    }
  }

  onReceiveProps() {}
}

