import React, { Component } from 'react'

export default () => (
<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 50 50"><path d="M50 22.4H27.6V0h-5.2v22.4H0v5.2h22.4V50h5.2V27.6H50z"/></svg>
)

