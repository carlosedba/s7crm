import axios from 'axios'
import { S7_API_ENDPOINT } from '@/globals'
import * as types from '@/actionTypes'

export function findAll() {
  const request = axios({
    method: 'get',
    url: `${S7_API_ENDPOINT}/idiomas`,
    headers: {
      'Authorization': `Bearer ${store.get('token')}`
    },
  })

  return {
    type: types.API_FIND_ALL_S7_IDIOMAS,
    payload: request
  }
}

export function findOneById(id) {
  const request = axios({
    method: 'get',
    url: `${S7_API_ENDPOINT}/idiomas/${id}`,
    headers: {
      'Authorization': `Bearer ${store.get('token')}`
    },
  })

  return {
    type: types.API_FIND_ONE_S7_IDIOMA,
    payload: request
  }
}

export function create(props) {
  const request = axios({
    method: 'post',
    data: props,
    url: `${S7_API_ENDPOINT}/idiomas`,
    headers: {
      'Authorization': `Bearer ${store.get('token')}`
    },
  })

  return {
    type: types.API_CREATE_S7_IDIOMA,
    payload: request
  }
}

export function update(id, props) {
  const request = axios({
    method: 'put',
    data: props,
    url: `${S7_API_ENDPOINT}/idiomas/${id}`,
    headers: {
      'Authorization': `Bearer ${store.get('token')}`
    },
  })

  return {
    type: types.API_UPDATE_S7_IDIOMA,
    payload: request
  }
}

export function remove(id) {
  const request = axios({
    method: 'delete',
    url: `${S7_API_ENDPOINT}/idiomas/${id}`,
    headers: {
      'Authorization': `Bearer ${store.get('token')}`
    },
  })

  return {
    type: types.API_DELETE_S7_IDIOMA,
    payload: request
  }
}

export function changeActive(props) {
  return {
    type: types.CHANGE_ACTIVE_S7_IDIOMA,
    payload: props
  }
}

export function changeNew(props) {
  return {
    type: types.CHANGE_NEW_S7_IDIOMA,
    payload: props
  }
}

export function changeUpdated(props) {
  return {
    type: types.CHANGE_UPDATED_S7_IDIOMA,
    payload: props
  }
}

