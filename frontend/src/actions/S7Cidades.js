import axios from 'axios'
import { S7_API_ENDPOINT } from '@/globals'
import * as types from '@/actionTypes'

export function fetch(params) {  
  const request = axios({
    method: 'get',
    url: `${S7_API_ENDPOINT}/cidades`,
    headers: {
      'Authorization': `Bearer ${store.get('token')}`
    },
    params: params
  })

  return {
    type: types.API_FETCH_S7_CIDADES,
    payload: request
  }
}

