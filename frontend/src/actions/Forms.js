import axios from 'axios'
import { S7_API_ENDPOINT } from '@/globals'
import * as types from '@/actionTypes'

export function updateForm(model, action, props) { 
  return {
    type: types.UPDATE_FORM,
    payload: {
      model: model,
      action: action,
      props: props,
    }
  }
}

export function updateFormRaw(model, action, props) { 
  return {
    type: types.UPDATE_FORM_RAW,
    payload: {
      model: model,
      action: action,
      props: props,
    }
  }
}

export function updateFormComputed(model, action, props) { 
  return {
    type: types.UPDATE_FORM_COMPUTED,
    payload: {
      model: model,
      action: action,
      props: props,
    }
  }
}

export function resetForm(model, action) { 
  return {
    type: types.RESET_FORM,
    payload: {
      model: model,
      action: action,
    }
  }
}