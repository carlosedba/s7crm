import axios from 'axios'
import { S7_API_ENDPOINT } from '@/globals'
import * as types from '@/actionTypes'

export function fetch() {  
  const request = axios({
    method: 'get',
    url: `${S7_API_ENDPOINT}/paises`,
    headers: {
      'Authorization': `Bearer ${store.get('token')}`
    }
  })

  return {
    type: types.API_FETCH_S7_PAISES,
    payload: request
  }
}

