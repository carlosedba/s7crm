import axios from 'axios'
import { S7_API_ENDPOINT } from '@/globals'
import * as types from '@/actionTypes'

export function autocomplete(str) {
  const request = axios({
    method: 'get',
    url: `${S7_API_ENDPOINT}/segmentos/autocomplete`,
    headers: {
      'Authorization': `Bearer ${store.get('token')}`
    },
    params: {
      str: str
    }
  })

  return {
    type: types.API_AUTOCOMPLETE_S7_SEGMENTOS,
    payload: request
  }
}

export function findAll() {
  const request = axios({
    method: 'get',
    url: `${S7_API_ENDPOINT}/segmentos`,
    headers: {
      'Authorization': `Bearer ${store.get('token')}`
    },
  })

  return {
    type: types.API_FIND_ALL_S7_SEGMENTOS,
    payload: request
  }
}

export function findOneById(id) {
  const request = axios({
    method: 'get',
    url: `${S7_API_ENDPOINT}/segmentos/${id}`,
    headers: {
      'Authorization': `Bearer ${store.get('token')}`
    },
  })

  return {
    type: types.API_FIND_ONE_S7_SEGMENTO,
    payload: request
  }
}

export function create(props) {
  const request = axios({
    method: 'post',
    data: props,
    url: `${S7_API_ENDPOINT}/segmentos`,
    headers: {
      'Authorization': `Bearer ${store.get('token')}`
    },
  })

  return {
    type: types.API_CREATE_S7_SEGMENTO,
    payload: request
  }
}

export function update(id, props) {
  const request = axios({
    method: 'put',
    data: props,
    url: `${S7_API_ENDPOINT}/segmentos/${id}`,
    headers: {
      'Authorization': `Bearer ${store.get('token')}`
    },
  })

  return {
    type: types.API_UPDATE_S7_SEGMENTO,
    payload: request
  }
}

export function remove(id) {
  const request = axios({
    method: 'delete',
    url: `${S7_API_ENDPOINT}/segmentos/${id}`,
    headers: {
      'Authorization': `Bearer ${store.get('token')}`
    },
  })

  return {
    type: types.API_DELETE_S7_SEGMENTO,
    payload: request
  }
}

export function changeAutocomplete(props) {
  return {
    type: types.CHANGE_AUTOCOMPLETE_S7_SEGMENTOS,
    payload: props
  }
}

export function changeActive(props) {
  return {
    type: types.CHANGE_ACTIVE_S7_SEGMENTO,
    payload: props
  }
}

export function changeNew(props) {
  return {
    type: types.CHANGE_NEW_S7_SEGMENTO,
    payload: props
  }
}

export function changeUpdated(props) {
  return {
    type: types.CHANGE_UPDATED_S7_SEGMENTO,
    payload: props
  }
}

