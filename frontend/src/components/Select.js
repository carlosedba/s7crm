import React, { Component } from 'react'
import { connect } from 'react-redux'
import { Link } from 'react-router-dom' 
import update from 'immutability-helper'
import classNames from 'classnames'
import ReactSelect from 'react-select'

import { updateForm, updateFormRaw, updateFormComputed } from '@/actions/Form'

@connect((state) => {
  return {
    forms: state.Forms,
  }
}, (dispatch, ownProps) => {
  return {
    updateForm(model, action, props) {
      return dispatch(updateForm(model, action, props))
    },

    updateFormRaw(model, action, props) {
      return dispatch(updateFormRaw(model, action, props))
    },

    updateFormComputed(model, action, props) {
      return dispatch(updateFormComputed(model, action, props))
    },
  }
})
export default class Select extends Component {
  constructor(props) {
    super(props)

    this.state = {
      selectedOption: null
    }

    this.handleChange = this.handleChange.bind(this)
  }

  componentDidMount() {
    const { attribute, initialValue, model, action } = this.props

    if (initialValue) {
      this.props.updateFormRaw(model, action, {
        $merge: { [attribute]: initialValue }
      })

      if (this.props.onChange) this.props.onChange({
        attribute: attribute,
        value: initialValue
      })
    }
  }

  updateState(state = {}) {  
    this.setState((prevState, props) => {
      return update(prevState, state)
    })
  }

  generateOptions(options) {
    const arr = []

    for (let option of options) {
      arr.push({ value: option, label: option })
    }

    return arr
  }

  handleChange(selectedOption) {
    const { attribute, model, action, onChange } = this.props

    if (selectedOption) {
      this.props.updateFormRaw(model, action, {
        $merge: { [attribute]: selectedOption }
      })

      if (this.props.onChange) this.props.onChange({
        attribute: attribute,
        value: selectedOption
      })
    }

    if (onChange) onChange(selectedOption)
  }

  getSize() {
    const { size } = this.props

    switch (size) {
      case 'w1':
        return 100
        break

      case 'w1-q1':
        return 125
        break
        
      case 'w1-q2':
        return 150
        break
        
      case 'w1-q3':
        return 175
        break


      case 'w2':
        return 200
        break

      case 'w2-q1':
        return 225
        break
        
      case 'w2-q2':
        return 250
        break
        
      case 'w2-q3':
        return 275
        break


      case 'w3':
        return 300
        break

      case 'w3-q1':
        return 325
        break
        
      case 'w3-q2':
        return 350
        break
        
      case 'w3-q3':
        return 375
        break


      case 'w4':
        return 400
        break

      case 'w4-q1':
        return 425
        break
        
      case 'w4-q2':
        return 450
        break
        
      case 'w4-q3':
        return 475
        break

      default:
        return 150
        break
    }
  }

  render() {
    const { label, classes, options, size, styles } = this.props
    const { attribute, model, action, forms } = this.props

    let value, reactSelectOptions, customStyles
    
    let controlStyles = {
      backgroundColor: "transparent"
    }

    if(forms[model][action]) value = forms[model][action]['raw'][attribute]
    
    if (size) {
      customStyles = Object.assign({}, {
        control: (provided, state) => ({
          ...provided,
          ...controlStyles,
          width: this.getSize(),
        })
      }, styles) 
    } else {
      customStyles = {
        control: (provided, state) => ({
          ...provided,
          ...controlStyles,
          width: this.getSize(),
        })
      }
    }

    if (typeof options[0] === 'string') {
      reactSelectOptions = this.generateOptions(options)
    } else {
      reactSelectOptions = options
    }

    return (
      <div className={classNames('input', classes)}>
        <label>{label}:</label>
        <ReactSelect
          {...this.props}
          styles={customStyles}
          placeholder="Selecione..."
          value={value}
          onChange={this.handleChange}
          options={reactSelectOptions}/>
      </div>
    )
  }
}

