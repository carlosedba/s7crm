import React, { Component } from 'react'
import { Link } from 'react-router-dom'
import { connect } from 'react-redux'
import update from 'immutability-helper'

import { updateFormRaw, updateFormComputed } from '@/actions/Form'

import { SERVER_ADDRESS } from '@/globals'

@connect((state) => {
  return {
    forms: state.Forms,
    modal: state.Modal,
  }
}, (dispatch, ownProps) => {
  return {
    updateFormRaw(model, action, props) {
      return dispatch(updateFormRaw(model, action, props))
    },

    updateFormComputed(model, action, props) {
      return dispatch(updateFormComputed(model, action, props))
    },
  }
})
export default class ArrayAttributeForm extends Component {
  constructor(props) {
    super(props)

    this.handleSubmit = this.handleSubmit.bind(this)
  }

  handleSubmit(event) {
    event.preventDefault()

    const { model, action, attribute, mode, modalName, onSubmit } = this.props
    const { forms, modal } = this.props

    const raw = forms[model][action]['raw']
    const computed = forms[model][action]['computed']

    if (!mode || mode === 'create') {
      if (!computed[attribute]) {
        this.props.updateFormComputed(model, action, {
          $merge: { [attribute]: [] }
        })
      }

      this.props.updateFormComputed(model, action, {
        [attribute]: { $push: [raw[attribute]] }
      })
    } else if (mode === 'update') {
      this.props.updateFormComputed(model, action, {
        [attribute]: {
          [modal[modalName].index]: { $set: raw[attribute] }
        }
      })
    }

    this.props.updateFormRaw(model, action, {
      [attribute]: { $set: '' }
    })
    
    onSubmit()
  }   

  render() {
    const { children } = this.props

    return (
      <form onSubmit={this.handleSubmit}>
        {children}
      </form>
    )
  }
}

