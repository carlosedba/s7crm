import React, { Component } from 'react'
import { Switch, Route } from 'react-router-dom'
import { connect } from 'react-redux'
import update from 'immutability-helper'

import { NetQueueContext } from '@/contexts/NetQueueContext'

import Sidebar from '@/components/Sidebar'

import Dashboard from '@/pages/Dashboard'

import Empresas from '@/pages/Empresa/Empresas'
import CadastrarEmpresa from '@/pages/Empresa/CadastrarEmpresa'

import Profissionais from '@/pages/Profissional/Profissionais'
import CadastrarProfissional from '@/pages/Profissional/CadastrarProfissional'

import Projetos from '@/pages/Projeto/Projetos'
import NovoProjeto from '@/pages/Projeto/NovoProjeto'

import Inputs from '@/pages/Inputs'

@connect((state) => {
  return {
    pending: state.Session.auth.pending,
    authenticated: state.Session.auth.authenticated,
  }
}, (dispatch, ownProps) => {
  return {}
})
export default class AuthorizedLayout extends Component {
  constructor(props) {
    super(props)

    this.state = {
      location: null,
    }
  }

  componentDidMount() {
    console.log('AuthorizedLayout > componentDidMount called!')

    const { location } = this.props

    this.setLocation(location)
  }

  componentWillReceiveProps(nextProps) {
    const { location } = nextProps

    this.setLocation(location)
  } 

  setLocation(location) {
    this.setState((prevState, props) => {
      return update(prevState, {
        $merge: { location: location }
      })
    })
  }

  render() {
    return (
      <div className="app">
        <NetQueueContext.Provider value={window.NetQueue}>
          <Sidebar location={this.state.location} user={this.state.user}/>

          <Switch>
            <Route path="/inputs" component={Inputs}/>

            <Route path="/projetos/novo" component={NovoProjeto}/>
            <Route path="/projetos" component={Projetos}/>

            <Route path="/profissionais/cadastrar" component={CadastrarProfissional}/>
            <Route path="/profissionais" component={Profissionais}/>

            <Route path="/empresas/cadastrar" component={CadastrarEmpresa}/>
            <Route path="/empresas" component={Empresas}/>

            <Route path="/" component={Dashboard}/>
          </Switch>
        </NetQueueContext.Provider>
      </div>
    )
  }
}
