import React, { Component } from 'react'
import { connect } from 'react-redux'
import { Link } from 'react-router-dom' 
import update from 'immutability-helper'

import { closeModal } from '@/actions/Modal'
import { updateFormRaw, updateFormComputed } from '@/actions/Form'

import Modal from '@/components/Modal'
import Form from '@/components/Form'
import TextInput from '@/components/Input/TextInput'
import SuggestionInput from '@/components/Input/SuggestionInput'

import IcClose from '@/icons/ic_close'

import NQForm from '@/NQForm'

const NQF = new NQForm()

@connect((state) => {
  return NQF.mapStateToProps({
    modal: state.Modal,
    forms: state.Forms,
    NetQueue: state.NetQueue,
  })
}, NQF.mapDispatchToProps.bind(NQF, (dispatch, ownProps) => {
  return {
    closeModal(name) {
      return dispatch(closeModal(name))
    },

    updateFormRaw(model, action, props) {
      return dispatch(updateFormRaw(model, action, props))
    },

    updateFormComputed(model, action, props) {
      return dispatch(updateFormComputed(model, action, props))
    },
  }
}))
export default class ModalPriContatosUpdate extends Component {
  constructor(props) {
    super(props)
    
    this.name = 'ModalPriContatosUpdate'
    
    this.title = 'Principais contatos'

    this.model = 'S7PrincipalContato'
    this.action = 'create'

    this.formId = null

    this.onAfterOpen = this.onAfterOpen.bind(this)
    this.onRequestClose = this.onRequestClose.bind(this)
    this.getInitialValue = this.getInitialValue.bind(this)
    this.handleBtnCloseClick = this.handleBtnCloseClick.bind(this)
    this.handleSubmit = this.handleSubmit.bind(this)
  }
  
  onAfterOpen() {
    const { modal } = this.props

    if (modal[this.name]) {
      const formId = modal[this.name].id

      if (formId) {
        this.formId = formId

        NQF.mapToForm(formId)
      }
    }
  }

  onRequestClose() {
    this.props.updateFormRaw(this.model, this.action, { $set: {} })
    this.props.updateFormComputed(this.model, this.action, { $set: {} })
  }

  getInitialValue(attribute) {
    console.log('log > ModalPriContatosUpdate > getInitialValue called!')

    const { forms } = this.props

    let value = ''

    value = forms[this.model][this.action]['computed'][attribute]

    return value
  }

  handleBtnCloseClick(event) {
    this.props.closeModal(this.name)
  }

  handleSubmit(event) {
    this.props.closeModal(this.name)
  }

  render() {
    const { modal } = this.props
  
    const generalFormProps = {
      model: this.model,
      action: this.action
    }

    return (
      <Modal name={this.name} contentLabel={this.title} onAfterOpen={this.onAfterOpen} onRequestClose={this.onRequestClose}>
        <div className="modal">
          <div className="modal-header">
            <div className="modal-header-left">
              <div className="modal-titles">
                <span className="modal-hat">{this.title}</span>
                <span className="modal-title">Editar contato</span>
              </div>
            </div>
            <div className="modal-header-right">
              <button className="btn btn-close" onClick={this.handleBtnCloseClick}>
                <div className="svg icon">
                  <IcClose/>
                </div>
              </button>
            </div>
          </div>
          <div className="modal-content">
            <Form {...generalFormProps} id={this.formId} update={true} onSubmit={this.handleSubmit}>
              <div className="inputs">
                <div className="multi-input">
                  <SuggestionInput {...generalFormProps} classes="w2-q2" label="Cargo" attribute="id_s7_cargo" collection="cargos" initialValue={this.getInitialValue('id_s7_cargo')}/>
                </div>
                <div className="multi-input">
                  <TextInput {...generalFormProps} classes="w2" label="Nome" attribute="nome"/>
                  <TextInput {...generalFormProps} classes="w2" label="Sobrenome" attribute="sobrenome"/>
                </div>
                <div className="multi-input"> 
                  <TextInput {...generalFormProps} classes="w3" label="E-mail corporativo" attribute="email_corporativo"/>
                  <TextInput {...generalFormProps} classes="w2" label="Telefone comercial" attribute="telefone_comercial"/>
                </div>
                <button type="submit" className="btn btn-three">Salvar</button>
              </div>
            </Form>
          </div>
        </div>
      </Modal>
    )
  }
}

