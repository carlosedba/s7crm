import React, { Component } from 'react'
import { Link } from 'react-router-dom'
import { connect } from 'react-redux'
import update from 'immutability-helper'
import classNames from 'classnames'
import axios from 'axios'

import { S7_API_ENDPOINT } from '@/globals'

import { findSuggestions, clearSuggestions } from '@/actions/Input'
import { updateFormRaw, updateFormComputed } from '@/actions/Form'

@connect((state) => {
  return {
    suggestions: state.Input.suggestions,
  }
}, (dispatch, ownProps) => {
  return {
    findSuggestions(collection, str) {
      return dispatch(findSuggestions(collection, str))
    },

    clearSuggestions() {
      return dispatch(clearSuggestions())
    },

    updateFormRaw(model, action, props) {
      return dispatch(updateFormRaw(model, action, props))
    },

    updateFormComputed(model, action, props) {
      return dispatch(updateFormComputed(model, action, props))
    },
  }
})
export default class SuggestionInput extends Component {
  constructor(props) {
    super(props)

    this.input = React.createRef()

    this.timeToWait = 1000
    this.timeToWaitTimeout = null

    this.state = {
      userInput: '',
      inputText: '',
      inputValue: '',
      focusedSuggestion: -1,
      suggestionSelected: false,
    }

    this.handleInputChange = this.handleInputChange.bind(this)
    this.handleInputKeyDown = this.handleInputKeyDown.bind(this)
    this.renderSuggestion = this.renderSuggestion.bind(this)
  }

  componentDidMount() {}

  componentDidUpdate(prevProps, prevState) {
    const { initialValue } = this.props
    const { inputValue } = this.state

    console.log(initialValue, inputValue)

    if (initialValue !== inputValue) this.getInitialData()
  }

  /*
  componentDidMount() {
    const { initialValue } = this.props

    if (initialValue) {
      this.updateState({
        inputValue: { $set: initialValue }
      })

      if (this.props.onChange) this.props.onChange({
        attribute: this.props.attribute,
        value: initialValue
      })
    }
  }
  */

  updateState(state = {}) {  
    this.setState((prevState, props) => {
      return update(prevState, state)
    })
  }

  getInitialData() {
    console.log('log > SuggestionInput > getInitialData called!')

    const { collection, attribute, initialValue } = this.props

    if (initialValue) {
      axios({
        method: 'get',
        url: `${S7_API_ENDPOINT}/${collection}/${initialValue}`,
        headers: {
          'Authorization': `Bearer ${store.get('token')}`
        },
      })
      .then((response) => {
        const data = response.data

        let inputValue = data.id
        let inputText = ''

        if (data.nome) inputText = data.nome

        if (data.nome) {
          this.updateState({
            inputValue: { $set: inputValue },
            inputText: { $set: inputText }
          })
        } 
      })
      .catch(console.error)
 
      /*
      this.updateState({
        inputValue: { $set: initialValue }
      })

      if (this.props.onChange) this.props.onChange({
        attribute: this.props.attribute,
        value: initialValue
      })
      */
    }
  }

  userIsTyping(str = '') {
    this.updateState({
      suggestionSelected: { $set: false }
    })
  }

  userFinishedTyping(str = '') {
    const { collection } = this.props

    if (str === '') {
      this.props.clearSuggestions()
    } else {
      this.props.findSuggestions(collection, str)
    }
  }

  handleInputChange(event) {
    if (event) {
      event.persist()

      const { attribute, model, action } = this.props
      const inputText = event.target.value

      if (inputText !== null) {
        this.userIsTyping(inputText)
  
        if (this.timeToWaitTimeout !== undefined && this.timeToWaitTimeout !== null) clearTimeout(this.timeToWaitTimeout)
        this.timeToWaitTimeout = setTimeout(this.userFinishedTyping.bind(this, inputText), this.timeToWait)
      }

      this.updateState({
        userInput: { $set: inputText },
        inputText: { $set: inputText },
        inputValue: { $set: inputText }
      })

      this.props.updateFormRaw(model, action, {
        $merge: { [attribute]: inputText }
      })

      if (this.props.onChange) this.props.onChange({
        attribute: this.props.attribute,
        text: inputText,
        value: inputText
      })

    } else {
      this.updateState({
        userInput: { $set: '' },
        inputText: { $set: '' },
        inputValue: { $set: '' }
      })

      if (this.props.onChange) this.props.onChange({
        attribute: this.props.attribute,
        text: '',
        value: ''
      })
    }
  }

  handleInputKeyDown(event) {
    const { attribute, model, action } = this.props
    const { suggestions } = this.props
    
    switch (event.keyCode) {
      case 13:
        if (this.state.focusedSuggestion > -1) {
          const focusedSuggestion = suggestions.items[this.state.focusedSuggestion]

          let suggestionText = ''

          if (item.nome) suggestionText = focusedSuggestion.nome
          if (item.nome_fantasia) suggestionText = focusedSuggestion.nome_fantasia
              
          this.updateState({
            inputText: { $set: suggestionText },
            inputValue: { $set: focusedSuggestion.id },
            suggestionSelected: { $set: true }
          })

          this.props.updateFormRaw(model, action, {
            $merge: { [attribute]: suggestionText }
          })

          this.props.updateFormComputed(model, action, {
            $merge: { [attribute]: focusedSuggestion.id }
          })

          if (this.props.onChange) this.props.onChange({
            attribute: this.props.attribute,
            text: suggestionText,
            value: focusedSuggestion.id
          })
        }
        break

      case 38:
        if (suggestions.items.length > 0) {
          if (this.state.focusedSuggestion > -1) {
            this.updateState({
              focusedSuggestion: { $set: this.state.focusedSuggestion - 1 }
            })
          }
        }
        break

      case 40:
        if (suggestions.items.length > 0) {
          if (this.state.focusedSuggestion < suggestions.items.length - 1) {
            this.updateState({
              focusedSuggestion: { $set: this.state.focusedSuggestion + 1 }
            })
          }
        }
        break
    }
  }

  handleSuggestionClick(item, event) {
    const { attribute, model, action } = this.props

    let suggestionText = ''

    if (item.nome) suggestionText = item.nome
    if (item.nome_fantasia) suggestionText = item.nome_fantasia

    this.updateState({
      inputText: { $set: suggestionText },
      inputValue: { $set: item.id },
      suggestionSelected: { $set: true }
    })

    this.props.updateFormRaw(model, action, {
      $merge: { [attribute]: suggestionText }
    })

    this.props.updateFormComputed(model, action, {
      $merge: { [attribute]: item.id }
    })

    if (this.props.onChange) this.props.onChange({
      attribute: this.props.attribute,
      text: suggestionText,
      value: item.id
    })
  }

  renderSuggestion(item, i) {
    const { inputText, focusedSuggestion } = this.state

    let suggestionText = ''

    if (item.nome) suggestionText = item.nome
    if (item.nome_fantasia) suggestionText = item.nome_fantasia

    suggestionText = suggestionText.toLowerCase()

    let inputTextLowerCase = inputText.toLowerCase()

    let textEnd = suggestionText.split(inputTextLowerCase)[1]
    let textStart = suggestionText.substring(0, suggestionText.indexOf(textEnd))

    let isHidden = false

    if (suggestionText.indexOf(textEnd) <= 0) isHidden = true

    return (
      <li key={item.id} tabIndex="0" className={classNames({
        'selected': focusedSuggestion == i,
        'hidden': isHidden
      })} onClick={this.handleSuggestionClick.bind(this, item)}>
        {textStart}
        <span className="bold">{textEnd}</span>
      </li>
    )
  }

  render() {
    const { label, classes = '', suggestions } = this.props

    return (
      <div className={classNames('input', classes)}>
        <label>{label}:</label>
        <input
          type="text"
          tabIndex="0"
          value={this.state.inputText}
          onChange={this.handleInputChange}
          onKeyDown={this.handleInputKeyDown}
          ref={this.input}
        />

        {this.state.inputText !== '' && !this.state.suggestionSelected && (suggestions.items.length > 0 || suggestions.loading) && (
          <ul className={classNames('suggestions', {
            'is-loading': suggestions.items.length <= 0
          })}>
            <div className="spinner"></div>
            {(suggestions.items.length > 0) && suggestions.items.map(this.renderSuggestion)}
          </ul>
        )}
      </div>
    )
  }
}




