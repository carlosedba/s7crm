import React, { Component } from 'react'
import { connect } from 'react-redux'
import { Link } from 'react-router-dom' 
import update from 'immutability-helper'

import { closeModal } from '@/actions/Modal'
import { updateFormRaw, updateFormComputed } from '@/actions/Form'

import Modal from '@/components/Modal'
import ObjectArrayAttributeForm from '@/components/Form/ObjectArrayAttributeForm'
import TextInput from '@/components/Input/TextInput'
import SelectInput from '@/components/Select'

import IcClose from '@/icons/ic_close'

@connect((state) => {
  return {
    modal: state.Modal,
    forms: state.Forms,
  }
}, (dispatch, ownProps) => {
  return {
    closeModal(name) {
      return dispatch(closeModal(name))
    },
    
    updateFormRaw(model, action, props) {
      return dispatch(updateFormRaw(model, action, props))
    },

    updateFormComputed(model, action, props) {
      return dispatch(updateFormComputed(model, action, props))
    },
  }
})
export default class ModalIdiomaCreate extends Component {
  constructor(props) {
    super(props)
    
    this.name = 'ModalIdiomaCreate'

    this.title = 'Idioma'

    this.formSource = {
      model: 'S7Idioma',
      action: 'create',
    }

    this.formTarget = {
      model: 'S7Profissional',
      action: 'create',
      attribute: 'idiomas'
    }

    this.onRequestClose = this.onRequestClose.bind(this)
    this.handleBtnCloseClick = this.handleBtnCloseClick.bind(this)
    this.handleSubmit = this.handleSubmit.bind(this)
  }

  onRequestClose() {
    this.props.updateFormRaw(this.formSource.model, this.formSource.action, { $set: {} })
    this.props.updateFormComputed(this.formSource.model, this.formSource.action, { $set: {} })
  }

  handleBtnCloseClick(event) {
    this.props.closeModal(this.name)
  }

  handleSubmit(event) {
    this.props.closeModal(this.name)
  }

  render() {
    return (
      <Modal name={this.name} contentLabel={this.title} onRequestClose={this.onRequestClose}>
        <div className="modal">
          <div className="modal-header">
            <div className="modal-header-left">
              <div className="modal-titles">
                <span className="modal-hat">{this.title}</span>
                <span className="modal-title">Adicionar idioma</span>
              </div>
            </div>
            <div className="modal-header-right">
              <button className="btn btn-close" onClick={this.handleBtnCloseClick}>
                <div className="svg icon">
                  <IcClose/>
                </div>
              </button>
            </div>
          </div>
          <div className="modal-content">
            <ObjectArrayAttributeForm source={this.formSource} target={this.formTarget} onSubmit={this.handleSubmit}>
              <div className="inputs">
                <SelectInput {...this.formSource} size="w2-q2" label="Idioma" attribute="idioma"
                             isSearchable={true}
                             options={[
                    { value: 'Africâner', label: 'Africâner' },
                    { value: 'Akan', label: 'Akan' },
                    { value: 'Albanês', label: 'Albanês' },
                    { value: 'Alemão', label: 'Alemão' },
                    { value: 'Amárico', label: 'Amárico' },
                    { value: 'Árabe', label: 'Árabe' },
                    { value: 'Armênio', label: 'Armênio' },
                    { value: 'Assamês', label: 'Assamês' },
                    { value: 'Assírio', label: 'Assírio' },
                    { value: 'Azeri', label: 'Azeri' },
                    { value: 'Bambara', label: 'Bambara' },
                    { value: 'Basco', label: 'Basco' },
                    { value: 'Bashkir', label: 'Bashkir' },
                    { value: 'Bengalês', label: 'Bengalês' },
                    { value: 'Bielo-russo', label: 'Bielo-russo' },
                    { value: 'Birmanês', label: 'Birmanês' },
                    { value: 'Bósnio', label: 'Bósnio' },
                    { value: 'Bravanês', label: 'Bravanês' },
                    { value: 'Búlgaro', label: 'Búlgaro' },
                    { value: 'Butanês', label: 'Butanês' },
                    { value: 'Cambojano', label: 'Cambojano' },
                    { value: 'Canarês', label: 'Canarês' },
                    { value: 'Canjobal', label: 'Canjobal' },
                    { value: 'Cantonês', label: 'Cantonês' },
                    { value: 'Catalão', label: 'Catalão' },
                    { value: 'Caxemira', label: 'Caxemira' },
                    { value: 'Cazaque', label: 'Cazaque' },
                    { value: 'Cebuano', label: 'Cebuano' },
                    { value: 'Chaldean', label: 'Chaldean' },
                    { value: 'Chamorro', label: 'Chamorro' },
                    { value: 'Chaozhou', label: 'Chaozhou' },
                    { value: 'Chavacano', label: 'Chavacano' },
                    { value: 'Chin', label: 'Chin' },
                    { value: 'Chona', label: 'Chona' },
                    { value: 'Chuquês', label: 'Chuquês' },
                    { value: 'Cingalês', label: 'Cingalês' },
                    { value: 'Coreano', label: 'Coreano' },
                    { value: 'Cree', label: 'Cree' },
                    { value: 'Croata', label: 'Croata' },
                    { value: 'Curdo', label: 'Curdo' },
                    { value: 'Curmânji', label: 'Curmânji' },
                    { value: 'Dacota', label: 'Dacota' },
                    { value: 'Dari', label: 'Dari' },
                    { value: 'Dinamarquês', label: 'Dinamarquês' },
                    { value: 'Dinka', label: 'Dinka' },
                    { value: 'Dioula', label: 'Dioula' },
                    { value: 'Eslovaco', label: 'Eslovaco' },
                    { value: 'Esloveno', label: 'Esloveno' },
                    { value: 'Espanhol', label: 'Espanhol' },
                    { value: 'Estoniano', label: 'Estoniano' },
                    { value: 'Ewe', label: 'Ewe' },
                    { value: 'Fanti', label: 'Fanti' },
                    { value: 'Faroês', label: 'Faroês' },
                    { value: 'Farsi', label: 'Farsi' },
                    { value: 'Finlandês', label: 'Finlandês' },
                    { value: 'Flamengo', label: 'Flamengo' },
                    { value: 'Francês', label: 'Francês' },
                    { value: 'Franco-canadense', label: 'Franco-canadense' },
                    { value: 'Frísio', label: 'Frísio' },
                    { value: 'Fujian', label: 'Fujian' },
                    { value: 'Fujianês', label: 'Fujianês' },
                    { value: 'Fula', label: 'Fula' },
                    { value: 'Fulani', label: 'Fulani' },
                    { value: 'Fuzhou', label: 'Fuzhou' },
                    { value: 'Ga', label: 'Ga' },
                    { value: 'Gaélico', label: 'Gaélico' },
                    { value: 'Galego', label: 'Galego' },
                    { value: 'Galês', label: 'Galês' },
                    { value: 'Ganda', label: 'Ganda' },
                    { value: 'Georgiano', label: 'Georgiano' },
                    { value: 'Gorani', label: 'Gorani' },
                    { value: 'Grego', label: 'Grego' },
                    { value: 'Guanxi', label: 'Guanxi' },
                    { value: 'Gujarati', label: 'Gujarati' },
                    { value: 'Hacá', label: 'Hacá' },
                    { value: 'Haitiano', label: 'Haitiano' },
                    { value: 'Hassaniya', label: 'Hassaniya' },
                    { value: 'Hauçá', label: 'Hauçá' },
                    { value: 'Hebraico', label: 'Hebraico' },
                    { value: 'Hiligaynon', label: 'Hiligaynon' },
                    { value: 'Hindi', label: 'Hindi' },
                    { value: 'Hindi fijiano', label: 'Hindi fijiano' },
                    { value: 'Hmong', label: 'Hmong' },
                    { value: 'Holandês', label: 'Holandês' },
                    { value: 'Húngaro', label: 'Húngaro' },
                    { value: 'Ibanag', label: 'Ibanag' },
                    { value: 'Igbo', label: 'Igbo' },
                    { value: 'Iídiche', label: 'Iídiche' },
                    { value: 'Ilocano', label: 'Ilocano' },
                    { value: 'Ilonggo', label: 'Ilonggo' },
                    { value: 'Indiano', label: 'Indiano' },
                    { value: 'Indonésio', label: 'Indonésio' },
                    { value: 'Inglês', label: 'Inglês' },
                    { value: 'Inglês pidgin', label: 'Inglês pidgin' },
                    { value: 'Inuktitut', label: 'Inuktitut' },
                    { value: 'Iorubá', label: 'Iorubá' },
                    { value: 'Irlandês', label: 'Irlandês' },
                    { value: 'Islandês', label: 'Islandês' },
                    { value: 'Italiano', label: 'Italiano' },
                    { value: 'Jakartanese', label: 'Jakartanese' },
                    { value: 'Japonês', label: 'Japonês' },
                    { value: 'Javanês', label: 'Javanês' },
                    { value: 'Karen', label: 'Karen' },
                    { value: 'Khalkha', label: 'Khalkha' },
                    { value: 'Khmer', label: 'Khmer' },
                    { value: 'Kikuyu', label: 'Kikuyu' },
                    { value: 'Kirundi', label: 'Kirundi' },
                    { value: 'Kosovo', label: 'Kosovo' },
                    { value: 'Kotokoli', label: 'Kotokoli' },
                    { value: 'Krio', label: 'Krio' },
                    { value: 'Lakota', label: 'Lakota' },
                    { value: 'Laociano', label: 'Laociano' },
                    { value: 'Latim', label: 'Latim' },
                    { value: 'Letão', label: 'Letão' },
                    { value: 'Lingala', label: 'Lingala' },
                    { value: 'Língua de sinais americana', label: 'Língua de sinais americana' },
                    { value: 'Lituano', label: 'Lituano' },
                    { value: 'Luganda', label: 'Luganda' },
                    { value: 'Luo', label: 'Luo' },
                    { value: 'Lusoga', label: 'Lusoga' },
                    { value: 'Luxemburguês', label: 'Luxemburguês' },
                    { value: 'Maay', label: 'Maay' },
                    { value: 'Macedônio', label: 'Macedônio' },
                    { value: 'Malaiala', label: 'Malaiala' },
                    { value: 'Malaio', label: 'Malaio' },
                    { value: 'Maldivense', label: 'Maldivense' },
                    { value: 'Malgaxe', label: 'Malgaxe' },
                    { value: 'Maltês', label: 'Maltês' },
                    { value: 'Mandarim', label: 'Mandarim' },
                    { value: 'Mandinga', label: 'Mandinga' },
                    { value: 'Mandinka', label: 'Mandinka' },
                    { value: 'Maori', label: 'Maori' },
                    { value: 'Marata', label: 'Marata' },
                    { value: 'Marshalês', label: 'Marshalês' },
                    { value: 'Mien', label: 'Mien' },
                    { value: 'Mirpuri', label: 'Mirpuri' },
                    { value: 'Mixteco', label: 'Mixteco' },
                    { value: 'Moldovan', label: 'Moldovan' },
                    { value: 'Mongol', label: 'Mongol' },
                    { value: 'Napolitano', label: 'Napolitano' },
                    { value: 'Navajo', label: 'Navajo' },
                    { value: 'Nepali', label: 'Nepali' },
                    { value: 'Nianja', label: 'Nianja' },
                    { value: 'Norueguês', label: 'Norueguês' },
                    { value: 'Nuer', label: 'Nuer' },
                    { value: 'Ojíbua', label: 'Ojíbua' },
                    { value: 'Oriá', label: 'Oriá' },
                    { value: 'Oromo', label: 'Oromo' },
                    { value: 'Osseta', label: 'Osseta' },
                    { value: 'Pachto', label: 'Pachto' },
                    { value: 'Pahari', label: 'Pahari' },
                    { value: 'Pampangan', label: 'Pampangan' },
                    { value: 'Patoá', label: 'Patoá' },
                    { value: 'Polonês', label: 'Polonês' },
                    { value: 'Português', label: 'Português' },
                    { value: 'Pothwari', label: 'Pothwari' },
                    { value: 'Pulaar', label: 'Pulaar' },
                    { value: 'Punjabi', label: 'Punjabi' },
                    { value: 'Putian', label: 'Putian' },
                    { value: 'Quechua', label: 'Quechua' },
                    { value: 'Quiniaruanda', label: 'Quiniaruanda' },
                    { value: 'Quirguiz', label: 'Quirguiz' },
                    { value: 'Romanche', label: 'Romanche' },
                    { value: 'Romani', label: 'Romani' },
                    { value: 'Romeno', label: 'Romeno' },
                    { value: 'Rundi', label: 'Rundi' },
                    { value: 'Russo', label: 'Russo' },
                    { value: 'Samoano', label: 'Samoano' },
                    { value: 'Sango', label: 'Sango' },
                    { value: 'Sânscrito', label: 'Sânscrito' },
                    { value: 'Sérvio', label: 'Sérvio' },
                    { value: 'Sichuan', label: 'Sichuan' },
                    { value: 'Siciliano', label: 'Siciliano' },
                    { value: 'Sindi', label: 'Sindi' },
                    { value: 'Somali', label: 'Somali' },
                    { value: 'Soninquê', label: 'Soninquê' },
                    { value: 'Sorâni', label: 'Sorâni' },
                    { value: 'Soto do sul', label: 'Soto do sul' },
                    { value: 'Suaíle', label: 'Suaíle' },
                    { value: 'Suázi/suazilandês', label: 'Suázi/suazilandês' },
                    { value: 'Sueco', label: 'Sueco' },
                    { value: 'Sundanês', label: 'Sundanês' },
                    { value: 'Susu', label: 'Susu' },
                    { value: 'Sylheti', label: 'Sylheti' },
                    { value: 'Tadjique', label: 'Tadjique' },
                    { value: 'Tagalog', label: 'Tagalog' },
                    { value: 'Tailandês', label: 'Tailandês' },
                    { value: 'Taiwanês', label: 'Taiwanês' },
                    { value: 'Tâmil', label: 'Tâmil' },
                    { value: 'Tcheco', label: 'Tcheco' },
                    { value: 'Telugu', label: 'Telugu' },
                    { value: 'Tibetano', label: 'Tibetano' },
                    { value: 'Tigrínia', label: 'Tigrínia' },
                    { value: 'Tonga', label: 'Tonga' },
                    { value: 'Tshiluba', label: 'Tshiluba' },
                    { value: 'Tsonga', label: 'Tsonga' },
                    { value: 'Tswana', label: 'Tswana' },
                    { value: 'Turco', label: 'Turco' },
                    { value: 'Turcomeno', label: 'Turcomeno' },
                    { value: 'Ucraniano', label: 'Ucraniano' },
                    { value: 'Uigur', label: 'Uigur' },
                    { value: 'Uolofe', label: 'Uolofe' },
                    { value: 'Urdu', label: 'Urdu' },
                    { value: 'Uzbeque', label: 'Uzbeque' },
                    { value: 'Venda', label: 'Venda' },
                    { value: 'Vietnamita', label: 'Vietnamita' },
                    { value: 'Visayan', label: 'Visayan' },
                    { value: 'Xangainês', label: 'Xangainês' },
                    { value: 'Xhosa', label: 'Xhosa' },
                    { value: 'Yao', label: 'Yao' },
                    { value: 'Yupik', label: 'Yupik' },
                    { value: 'Zulu', label: 'Zulu' }
                  ]}/>
                <SelectInput {...this.formSource} size="w2" label="Nível" attribute="nivel"
                             options={[
                    { value: 'Básico', label: 'Básico' },
                    { value: 'Intermediário', label: 'Intermediário' },
                    { value: 'Avançado', label: 'Avançado' },
                    { value: 'Fluente', label: 'Fluente' },
                    { value: 'Nativo', label: 'Nativo' }
                  ]}/>
                <button type="submit" className="btn btn-three">Salvar</button>
              </div>
            </ObjectArrayAttributeForm>
          </div>
        </div>
      </Modal>
    )
  }
}

