import React, { Component } from 'react'
import { connect } from 'react-redux'
import { Link } from 'react-router-dom' 
import update from 'immutability-helper'

import { closeModal } from '@/actions/Modal'
import { updateFormRaw, updateFormComputed } from '@/actions/Form'

import Modal from '@/components/Modal'
import ArrayAttributeForm from '@/components/Form/ArrayAttributeForm'
import TextInput from '@/components/Input/TextInput'

import IcClose from '@/icons/ic_close'

@connect((state) => {
  return {
    modal: state.Modal,
    forms: state.Forms,
  }
}, (dispatch, ownProps) => {
  return {
    closeModal(name) {
      return dispatch(closeModal(name))
    },

    updateFormRaw(model, action, props) {
      return dispatch(updateFormRaw(model, action, props))
    },

    updateFormComputed(model, action, props) {
      return dispatch(updateFormComputed(model, action, props))
    },
  }
})
export default class ModalConAdministrativoUpdate extends Component {
  constructor(props) {
    super(props)
    
    this.name = 'ModalConAdministrativoUpdate'
    
    this.title = 'Conselho Administrativo'

    this.model = 'S7Empresa'
    this.action = 'create'
    this.attribute = 'nome_conselheiros'

    this.onRequestClose = this.onRequestClose.bind(this)
    this.handleBtnCloseClick = this.handleBtnCloseClick.bind(this)
    this.handleSubmit = this.handleSubmit.bind(this)
  }

  getInitialValue() {
    const { modal, forms } = this.props

    const attributeValue = forms[this.model][this.action]['computed'][this.attribute]

    if (attributeValue) {
      return attributeValue[modal[this.name].index]
    }
  }

  onRequestClose() {
    this.props.updateFormRaw(this.model, this.action, {
      [this.attribute]: { $set: '' }
    })
  }

  handleBtnCloseClick(event) {
    this.props.closeModal(this.name)
  }

  handleSubmit(event) {
    this.props.closeModal(this.name)
  }

  render() {
    const generalFormProps = {
      model: this.model,
      action: this.action,
      attribute: this.attribute
    }

    return (
      <Modal name={this.name} contentLabel={this.title} onRequestClose={this.onRequestClose}>
        <div className="modal">
          <div className="modal-header">
            <div className="modal-header-left">
              <div className="modal-titles">
                <span className="modal-hat">{this.title}</span>
                <span className="modal-title">Editar pessoa</span>
              </div>
            </div>
            <div className="modal-header-right">
              <button className="btn btn-close" onClick={this.handleBtnCloseClick}>
                <div className="svg icon">
                  <IcClose/>
                </div>
              </button>
            </div>
          </div>
          <div className="modal-content">
            <ArrayAttributeForm {...generalFormProps} mode="update" modalName={this.name} onSubmit={this.handleSubmit}>
              <div className="inputs">
                <TextInput {...generalFormProps} classes="w3" label="Nome" initialValue={this.getInitialValue()}/>
                <button type="submit" className="btn btn-three">Salvar</button>
              </div>
            </ArrayAttributeForm>
          </div>
        </div>
      </Modal>
    )
  }
}

