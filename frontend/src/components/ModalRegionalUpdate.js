import React, { Component } from 'react'
import { connect } from 'react-redux'
import { Link } from 'react-router-dom' 
import update from 'immutability-helper'
import axios from 'axios'

import { trocarNomeSiglaEstado } from '@/utils/BrasilUtils'

import { closeModal } from '@/actions/Modal'
import { updateFormRaw, updateFormComputed } from '@/actions/Form'

import Modal from '@/components/Modal'
import ObjectArrayAttributeForm from '@/components/Form/ObjectArrayAttributeForm'
import TextInput from '@/components/Input/TextInput'

import IcClose from '@/icons/ic_close'

@connect((state) => {
  return {
    modal: state.Modal,
    forms: state.Forms,
  }
}, (dispatch, ownProps) => {
  return {
    closeModal(name) {
      return dispatch(closeModal(name))
    },

    updateFormRaw(model, action, props) {
      return dispatch(updateFormRaw(model, action, props))
    },

    updateFormComputed(model, action, props) {
      return dispatch(updateFormComputed(model, action, props))
    },
  }
})
export default class ModalRegionalUpdate extends Component {
  constructor(props) {
    super(props)
    
    this.name = 'ModalRegionalUpdate'

    this.title = 'Regional'

    this.formSource = {
      model: 'Regional',
      action: 'create',
    }

    this.formTarget = {
      model: 'S7Empresa',
      action: 'create',
      attribute: 'regionais'
    }

    this.onRequestClose = this.onRequestClose.bind(this)
    this.handleBtnCloseClick = this.handleBtnCloseClick.bind(this)
    this.handleTextInputChange = this.handleTextInputChange.bind(this)
    this.handleSubmit = this.handleSubmit.bind(this)
  }

  getInitialValue(attribute) {
    const { modal, forms } = this.props

    const attributeValue = forms[this.formTarget.model][this.formTarget.action]['computed'][this.formTarget.attribute]

    if (attributeValue) {
      const arrayItem = attributeValue[modal[this.name].index]

      if (arrayItem) {
        return arrayItem[attribute] || ''
      }
    }
  }

  onRequestClose() {
    this.props.updateFormRaw(this.formSource.model, this.formSource.action, { $set: {} })
    this.props.updateFormComputed(this.formSource.model, this.formSource.action, { $set: {} })
  }

  fillAddress(cep) {
    if (cep.charAt(9) !== '' && cep.charAt(9) !== '_') {
      const cepNumber = parseInt(cep.replace('.', '').replace('-', ''))

      if (cepNumber) {
        axios({
          method: 'get',
          url: `https://viacep.com.br/ws/${cepNumber}/json/`
        }).then((response) => {
          if (!response.data.erro) {
            this.props.updateFormRaw(this.formSource.model, this.formSource.action, {
              $merge: {
                rua: response.data.logradouro,
                numero: response.data.complemento,
                bairro: response.data.bairro,
                cidade: response.data.localidade,
                estado: trocarNomeSiglaEstado(response.data.uf),
                pais: 'Brasil',
              }
            })
          }
        }).catch(console.error)
      }
    }
  }

  handleBtnCloseClick(event) {
    this.props.closeModal(this.name)
  }

  handleTextInputChange(event) {
    if (event.value) {
      const initialCEP = this.getInitialValue('cep')

      if (event.attribute === 'cep' && event.value !== initialCEP) this.fillAddress(event.value)
    }
  }

  handleSubmit(event) {
    this.props.closeModal(this.name)
  }

  render() {
    return (
      <Modal name={this.name} contentLabel={this.title} onRequestClose={this.onRequestClose}>
        <div className="modal">
          <div className="modal-header">
            <div className="modal-header-left">
              <div className="modal-titles">
                <span className="modal-hat">{this.title}</span>
                <span className="modal-title">Editar regional</span>
              </div>
            </div>
            <div className="modal-header-right">
              <button className="btn btn-close" onClick={this.handleBtnCloseClick}>
                <div className="svg icon">
                  <IcClose/>
                </div>
              </button>
            </div>
          </div>
          <div className="modal-content">
            <ObjectArrayAttributeForm source={this.formSource} target={this.formTarget} mode="update" modalName={this.name} onSubmit={this.handleSubmit}>
              <div className="inputs">
                <div className="multi-input">
                  <TextInput {...this.formSource} classes="w4" label="Nome" attribute="nome" initialValue={this.getInitialValue('nome')}/>
                  <TextInput {...this.formSource} classes="w1-q1" label="CEP" attribute="cep" mask="cep" initialValue={this.getInitialValue('cep')} onChange={this.handleTextInputChange}/>
                </div>
                <div className="multi-input">
                  <TextInput {...this.formSource} classes="w4" label="Rua" attribute="rua" initialValue={this.getInitialValue('rua')}/>
                  <TextInput {...this.formSource} classes="w1" label="Número" attribute="numero" initialValue={this.getInitialValue('numero')}/>
                  <TextInput {...this.formSource} classes="w1" label="Complemento" attribute="complemento" initialValue={this.getInitialValue('complemento')}/>
                </div>
                <div className="multi-input">
                  <TextInput {...this.formSource} classes="w2" label="Bairro" attribute="bairro" initialValue={this.getInitialValue('bairro')}/>
                  <TextInput {...this.formSource} classes="w2-q1" label="Cidade" attribute="cidade" initialValue={this.getInitialValue('cidade')}/>
                </div>
                <div className="multi-input">
                  <TextInput {...this.formSource} classes="w2" label="Estado" attribute="estado" initialValue={this.getInitialValue('estado')}/>
                  <TextInput {...this.formSource} classes="w1-q2" label="País" attribute="pais" initialValue={this.getInitialValue('pais')}/>
                </div>
                <button type="submit" className="btn btn-three">Salvar</button>
              </div>
            </ObjectArrayAttributeForm>
          </div>
        </div>
      </Modal>
    )
  }
}

