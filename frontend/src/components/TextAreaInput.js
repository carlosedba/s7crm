import React, { Component } from 'react'
import { connect } from 'react-redux'
import { Link } from 'react-router-dom' 
import update from 'immutability-helper'
import classNames from 'classnames'
import { utilsBr } from 'js-brasil'
import MaskedInput from 'react-text-mask'

import { updateForm, updateFormRaw, updateFormComputed } from '@/actions/Form'

@connect((state) => {
  return {
    forms: state.Forms,
  }
}, (dispatch, ownProps) => {
  return {
    updateForm(model, action, props) {
      return dispatch(updateForm(model, action, props))
    },

    updateFormRaw(model, action, props) {
      return dispatch(updateFormRaw(model, action, props))
    },

    updateFormComputed(model, action, props) {
      return dispatch(updateFormComputed(model, action, props))
    },
  }
})
export default class TextAreaInput extends Component {
  constructor(props) {
    super(props)

    this.state = {
      inputValue: ''
    }

    this.handleInputChange = this.handleInputChange.bind(this)
  }

  componentDidMount() {
    const { attribute, initialValue, model, action } = this.props

    if (initialValue) {
      this.props.updateFormRaw(model, action, {
        $merge: { [attribute]: initialValue }
      })

      if (this.props.onChange) this.props.onChange({
        attribute: attribute,
        value: initialValue
      })
    }
  }

  updateState(state = {}) {  
    this.setState((prevState, props) => {
      return update(prevState, state)
    })
  }

  handleInputChange(event) {
    const { attribute, model, action } = this.props
    const inputText = event.target.value

    if (inputText !== undefined && inputText !== null) {
      this.props.updateFormRaw(model, action, {
        $merge: { [attribute]: inputText }
      })

      if (this.props.onChange) this.props.onChange({
        attribute: attribute,
        value: inputText
      })
    }
  }

  renderMaskedInput(value) {
    const { mask } = this.props
    let textMask

    if (typeof mask === "string") {
      textMask = utilsBr.MASKS[mask].textMask

      return (<MaskedInput mask={textMask} value={value || ''} onChange={this.handleInputChange}/>)
    } else {
      return (<MaskedInput mask={mask} value={value || ''} onChange={this.handleInputChange}/>)
    }
  }

  render() {
    const { label, classes, mask } = this.props
    const { attribute, model, action, forms } = this.props
      
    let textMask, value

    if (typeof mask === "string") {
      textMask = utilsBr.MASKS[mask].textMask
    }

    if(forms[model][action]) value = forms[model][action]['raw'][attribute]

    return (
      <div className={classNames('input', classes)}>
        {(label) && (<label>{label}:</label>)}
        {(mask)
          ? this.renderMaskedInput(value)
          : (<textarea value={value || ''} onChange={this.handleInputChange}/>)
        }
      </div>
    )
  }
}

