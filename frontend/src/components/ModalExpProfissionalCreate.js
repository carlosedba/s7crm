import React, { Component } from 'react'
import { connect } from 'react-redux'
import { Link } from 'react-router-dom' 
import update from 'immutability-helper'

import { closeModal } from '@/actions/Modal'

import Modal from '@/components/Modal'
import Form from '@/components/Form'
import TextInput from '@/components/Input/TextInput'
import SuggestionInput from '@/components/Input/SuggestionInput'
import SelectInput from '@/components/Select'

import IcClose from '@/icons/ic_close'

@connect((state) => {
  return {
    modal: state.Modal,
    forms: state.Forms,
  }
}, (dispatch, ownProps) => {
  return {
    closeModal(name) {
      return dispatch(closeModal(name))
    }
  }
})
export default class ModalExpProfissionalCreate extends Component {
  constructor(props) {
    super(props)
    
    this.name = 'ModalExpProfissionalCreate'
    
    this.title = 'Experiências profissionais'

    this.model = 'S7ExperienciaProfissional'
    this.action = 'create'

    this.handleBtnCloseClick = this.handleBtnCloseClick.bind(this)
    this.handleSubmit = this.handleSubmit.bind(this)
  }

  handleSubmit(event) {
    this.props.closeModal(this.name)
  }

  handleBtnCloseClick(event) {
    this.props.closeModal(this.name)
  }

  render() {
    const generalFormProps = {
      model: this.model,
      action: this.action
    }

    return (
      <Modal name={this.name} contentLabel={this.title}>
        <div className="modal">
          <div className="modal-header">
            <div className="modal-header-left">
              <div className="modal-titles">
                <span className="modal-hat">{this.title}</span>
                <span className="modal-title">Adicionar experiência</span>
              </div>
            </div>
            <div className="modal-header-right">
              <button className="btn btn-close" onClick={this.handleBtnCloseClick}>
                <div className="svg icon">
                  <IcClose/>
                </div>
              </button>
            </div>
          </div>
          <div className="modal-content">
            <Form {...generalFormProps} dependsOn={this.props.dependsOn} onSubmit={this.handleSubmit}>
              <div className="inputs">
                <div className="multi-input">
                  <SuggestionInput {...generalFormProps} classes="w3" label="Empresa" attribute="id_s7_empresa" collection="empresas"/>
                  <SuggestionInput {...generalFormProps} classes="w2-q2" label="Cargo" attribute="id_s7_cargo" collection="cargos"/>
                </div>
                <SelectInput {...generalFormProps} size="w2-q2" label="Tipo de contratação" attribute="tipo_contratacao"
                             options={[
                    { value: 'CLT', label: 'CLT' },
                    { value: 'PJ', label: 'PJ' },
                    { value: 'Estatutário', label: 'Estatutário' },
                    { value: 'Contrato', label: 'Contrato' },
                  ]}/>
                <SelectInput {...generalFormProps} size="w4-q3" label="Benefícios" attribute="beneficios" isMulti={true}
                             options={[
                    { value: 'Assistência médica', label: 'Assistência médica' },
                    { value: 'Assistência odontológica', label: 'Assistência odontológica' },
                    { value: 'Auxílio farmácia', label: 'Auxílio farmácia' },
                    { value: 'Seguro de vida', label: 'Seguro de vida' },
                    { value: 'Previdência privada', label: 'Previdência privada' },
                    { value: 'Auxílio creche', label: 'Auxílio creche' },
                    { value: 'Bolsa de estudos', label: 'Bolsa de estudos' },
                    { value: 'Vale refeição', label: 'Vale refeição' },
                    { value: 'Vale almentação', label: 'Vale almentação' },
                    { value: 'Refeitório local', label: 'Refeitório local' },
                    { value: 'Cesta básica', label: 'Cesta básica' },
                    { value: 'Cartão corporativo', label: 'Cartão corporativo' },
                    { value: 'Carro', label: 'Carro' },
                    { value: 'Combustível', label: 'Combustível' },
                  ]}/>
                <SelectInput {...generalFormProps} size="w4-q3" label="Ferramentas" attribute="ferramentas" isMulti={true}
                             options={[
                    { value: 'Carro', label: 'Carro' },
                    { value: 'Combustível', label: 'Combustível' },
                    { value: 'Celular', label: 'Celular' },
                    { value: 'Notebook', label: 'Notebook' },
                  ]}/>
                <button type="submit" className="btn btn-three">Salvar</button>
              </div>
            </Form>
          </div>
        </div>
      </Modal>
    )
  }
}

