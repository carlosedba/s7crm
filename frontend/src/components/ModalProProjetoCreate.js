import React, { Component } from 'react'
import { connect } from 'react-redux'
import { Link } from 'react-router-dom' 
import update from 'immutability-helper'

import { closeModal } from '@/actions/Modal'

import Modal from '@/components/Modal'
import Form from '@/components/Form'
import TextInput from '@/components/Input/TextInput'
import SuggestionInput from '@/components/Input/SuggestionInput'
import SelectInput from '@/components/Select'

import IcClose from '@/icons/ic_close'

@connect((state) => {
  return {
    modal: state.Modal,
    forms: state.Forms,
  }
}, (dispatch, ownProps) => {
  return {
    closeModal(name) {
      return dispatch(closeModal(name))
    }
  }
})
export default class ModalProProjetoCreate extends Component {
  constructor(props) {
    super(props)
    
    this.name = 'ModalProProjetoCreate'
    
    this.title = 'Profissionais'

    this.model = 'ProfissionalProjeto'
    this.action = 'create'

    this.handleBtnCloseClick = this.handleBtnCloseClick.bind(this)
    this.handleSubmit = this.handleSubmit.bind(this)
  }

  handleSubmit(event) {
    this.props.closeModal(this.name)
  }

  handleBtnCloseClick(event) {
    this.props.closeModal(this.name)
  }

  render() {
    const generalFormProps = {
      model: this.model,
      action: this.action
    }

    return (
      <Modal name={this.name} contentLabel={this.title}>
        <div className="modal">
          <div className="modal-header">
            <div className="modal-header-left">
              <div className="modal-titles">
                <span className="modal-hat">{this.title}</span>
                <span className="modal-title">Adicionar profissional</span>
              </div>
            </div>
            <div className="modal-header-right">
              <button className="btn btn-close" onClick={this.handleBtnCloseClick}>
                <div className="svg icon">
                  <IcClose/>
                </div>
              </button>
            </div>
          </div>
          <div className="modal-content">
            <Form {...generalFormProps} dependsOn={this.props.dependsOn} onSubmit={this.handleSubmit}>
              <div className="inputs">
                <div className="multi-input">
                  <SuggestionInput {...generalFormProps} classes="w3" label="Profissional" attribute="id_s7_profissional" collection="profissionais"/>
                  <SuggestionInput {...generalFormProps} classes="w2-q2" label="Cargo" attribute="id_s7_cargo" collection="cargos"/>
                </div>
                <div className="multi-input">
                  <TextInput {...generalFormProps} classes="w1-q1" label="Honorário" attribute="honorario"/>
                  <TextInput {...generalFormProps} classes="w1-q2" label="Salário inicial" attribute="salario_inicial"/>
                  <TextInput {...generalFormProps} classes="w1-q2" label="Salário final" attribute="salario_final"/>
                </div>
                <div className="multi-input"> 
                  <TextInput {...generalFormProps} classes="w3" label="Missão do cargo" attribute="missao_cargo"/>
                  <TextInput {...generalFormProps} classes="w3" label="Responsabilidades do cargo" attribute="responsabilidades_cargo"/>
                </div>
                <TextInput {...generalFormProps} classes="w4" label="Competências" attribute="competencias"/>
                <div className="multi-input"> 
                  <TextInput {...generalFormProps} classes="w2" label="Equipe liderada" attribute="equipe_liderada"/>
                  <SelectInput {...generalFormProps} size="w2" label="Nível" attribute="nivel"
                               options={[
                      { value: 'Executivo', label: 'Executivo' },
                      { value: 'Gestão', label: 'Gestão' },
                      { value: 'Especialista', label: 'Especialista' },
                    ]}/>
                </div>
                <button type="submit" className="btn btn-three">Salvar</button>
              </div>
            </Form>
          </div>
        </div>
      </Modal>
    )
  }
}

