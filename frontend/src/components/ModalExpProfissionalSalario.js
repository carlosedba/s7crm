import React, { Component } from 'react'
import { connect } from 'react-redux'
import { Link } from 'react-router-dom' 
import update from 'immutability-helper'
import createNumberMask from 'text-mask-addons/dist/createNumberMask'
import currency from 'currency.js'
import ReactSelect from 'react-select'

import { closeModal } from '@/actions/Modal'
import { updateFormRaw, updateFormComputed } from '@/actions/Form'

import Modal from '@/components/Modal'
import Form from '@/components/Form'
import TextInput from '@/components/Input/TextInput'
import SuggestionInput from '@/components/Input/SuggestionInput'
import SelectInput from '@/components/Select'

import IcClose from '@/icons/ic_close'

import State from '@/State'
import NQForm from '@/NQForm'

const NQF = new NQForm()

@connect((state) => {
  return NQF.mapStateToProps({
    modal: state.Modal,
    forms: state.Forms,
    NetQueue: state.NetQueue,
  })
}, NQF.mapDispatchToProps.bind(NQF, (dispatch, ownProps) => {
  return {
    closeModal(name) {
      return dispatch(closeModal(name))
    },

    updateFormRaw(model, action, props) {
      return dispatch(updateFormRaw(model, action, props))
    },

    updateFormComputed(model, action, props) {
      return dispatch(updateFormComputed(model, action, props))
    },
  }
}))
export default class ModalExpProfissionalSalario extends Component {
  constructor(props) {
    super(props)
    
    this.name = 'ModalExpProfissionalSalario'
    
    this.title = 'Experiências profissionais'

    this.model = 'S7ExperienciaProfissional'
    this.action = 'create'

    this.formId = null

    this.masks = {
      numero: createNumberMask({
        prefix: '',
        thousandsSeparatorSymbol: '.',
        decimalSymbol: ',',
        //requireDecimal: true,
        //allowLeadingZeroes: true
      })
    }
    
    this.state = {
      currency: ''
    }

    this.onAfterOpen = this.onAfterOpen.bind(this)
    this.onRequestClose = this.onRequestClose.bind(this)
    this.handleBtnCloseClick = this.handleBtnCloseClick.bind(this)
    this.handleTotalInputFocus = this.handleTotalInputFocus.bind(this)
    this.handleCurrencyChange = this.handleCurrencyChange.bind(this)
    this.handleSubmit = this.handleSubmit.bind(this)
  }
  
  onAfterOpen() {
    const { modal } = this.props

    if (modal[this.name]) {
      const formId = modal[this.name].id

      if (formId) {
        this.formId = formId

        NQF.mapToForm(formId)
      }
    }
  }

  onRequestClose() {
    this.props.updateFormRaw(this.model, this.action, { $set: {} })
    this.props.updateFormComputed(this.model, this.action, { $set: {} })
  }

  handleBtnCloseClick(event) {
    this.props.closeModal(this.name)
  }

  handleTotalInputFocus(event) {
    const { forms } = this.props
    const { model, action, attribute } = event

    let currentCurrency, base, multiplier, total

    currentCurrency = {
      separator: '.',
      decimal: ','
    }

    switch (attribute) {
      case 'total_salario':
        base = forms[model][action]['raw']['salario']
        multiplier = forms[model][action]['raw']['quantidade_salarios']
        break

      case 'total_comissao':
        base = forms[model][action]['raw']['comissao']
        multiplier = forms[model][action]['raw']['quantidade_comissao']
        break
        
      case 'total_bonus':
        base = forms[model][action]['raw']['bonus']
        multiplier = forms[model][action]['raw']['quantidade_bonus']
        break
        
      case 'total_plr':
        base = forms[model][action]['raw']['plr']
        multiplier = forms[model][action]['raw']['quantidade_plr']
        break
    }

    console.log(base)
    console.log(multiplier)

    base = currency(base, currentCurrency)
    multiplier = currency(multiplier, currentCurrency)
    total = base.multiply(multiplier.value)

    console.log(base)
    console.log(multiplier)
    console.log(total)
      
    this.props.updateFormRaw(this.model, this.action, {
      [attribute]: {
        $set: total.value
      }
    })
      
    this.props.updateFormComputed(this.model, this.action, {
      [attribute]: {
        $set: total.value
      }
    })
  }

  handleCurrencyChange(selectedOption) {
    const { model, action } = this
    const attribute = 'moeda'

    if (selectedOption) {
      State.update(this, {
        currency: { $set: selectedOption }
      })

      this.props.updateFormRaw(model, action, {
        [attribute]: { $set: selectedOption.value }
      })
    }
  }

  handleSubmit(event) {
    this.props.closeModal(this.name)
  }

  render() {
    const { modal } = this.props
  
    const generalFormProps = {
      model: this.model,
      action: this.action
    }

    const customStyles = Object.assign({}, {
      control: (provided, state) => ({
        ...provided,
        width: '100px',
        backgroundColor: 'transparent',
        border: 'none',
        fontSize: '15px'
      })
    })

    return (
      <Modal name={this.name} contentLabel={this.title} onAfterOpen={this.onAfterOpen} onRequestClose={this.onRequestClose}>
        <div className="modal">
          <div className="modal-header">
            <div className="modal-header-left">
              <div className="modal-titles">
                <span className="modal-hat">{this.title}</span>
                <span className="modal-title">Editar conteúdo</span>
              </div>
            </div>
            <div className="modal-header-right">
              <button className="btn btn-close" onClick={this.handleBtnCloseClick}>
                <div className="svg icon">
                  <IcClose/>
                </div>
              </button>
            </div>
          </div>
          <div className="modal-content">
            <Form {...generalFormProps} id={this.formId} update={true} onSubmit={this.handleSubmit}>
              <div className="ctable ctable-salario">
                <div className="ctable-row ctable-header">
                  <div className="ctable-col w1">
                    <ReactSelect
                      styles={customStyles}
                      placeholder="Moeda"
                      value={this.state.currency}
                      options={[
                        { value: 'BRL', label: 'BRL' },
                        { value: 'USD', label: 'USD' },
                      ]}
                      onChange={this.handleCurrencyChange}/>
                  </div>
                  <div className="ctable-col center w1-q2">Remuneração</div>
                  <div className="ctable-col center w1-q2">Quantidade</div>
                  <div className="ctable-col center w1-q2">Total</div>
                </div>
                <div className="ctable-row center">
                  <div className="ctable-col w1">Salário</div>
                  <div className="ctable-col w1-q2">
                    <TextInput {...generalFormProps} classes="ctable-input" attribute="salario" mask={this.masks.numero}/>
                  </div>
                  <div className="ctable-col w1-q2">
                    <TextInput {...generalFormProps} classes="ctable-input" attribute="quantidade_salarios"/>
                  </div>
                  <div className="ctable-col w1-q2">
                    <TextInput {...generalFormProps} classes="ctable-input" attribute="total_salario" mask={this.masks.numero} onFocus={this.handleTotalInputFocus}/>
                  </div>
                </div>
                <div className="ctable-row center">
                  <div className="ctable-col w1">Comissão</div>
                  <div className="ctable-col w1-q2">
                    <TextInput {...generalFormProps} classes="ctable-input" attribute="comissao" mask={this.masks.numero}/>
                  </div>
                  <div className="ctable-col w1-q2">
                    <TextInput {...generalFormProps} classes="ctable-input" attribute="quantidade_comissao"/>
                  </div>
                  <div className="ctable-col w1-q2">
                    <TextInput {...generalFormProps} classes="ctable-input" attribute="total_comissao" mask={this.masks.numero} onFocus={this.handleTotalInputFocus}/>
                  </div>
                </div>
                <div className="ctable-row center">
                  <div className="ctable-col w1">Bônus</div>
                  <div className="ctable-col w1-q2">
                    <TextInput {...generalFormProps} classes="ctable-input" attribute="bonus" mask={this.masks.numero}/>
                  </div>
                  <div className="ctable-col w1-q2">
                    <TextInput {...generalFormProps} classes="ctable-input" attribute="quantidade_bonus"/>
                  </div>
                  <div className="ctable-col w1-q2">
                    <TextInput {...generalFormProps} classes="ctable-input" attribute="total_bonus" mask={this.masks.numero} onFocus={this.handleTotalInputFocus}/>
                  </div>
                </div>
                <div className="ctable-row center">
                  <div className="ctable-col w1">PLR</div>
                  <div className="ctable-col w1-q2">
                    <TextInput {...generalFormProps} classes="ctable-input" attribute="plr" mask={this.masks.numero}/>
                  </div>
                  <div className="ctable-col w1-q2">
                    <TextInput {...generalFormProps} classes="ctable-input" attribute="quantidade_plr"/>
                  </div>
                  <div className="ctable-col w1-q2">
                    <TextInput {...generalFormProps} classes="ctable-input" attribute="total_plr" mask={this.masks.numero} onFocus={this.handleTotalInputFocus}/>
                  </div>
                </div>

                <button type="submit" className="btn btn-three">Salvar</button>
              </div>
            </Form>
          </div>
        </div>
      </Modal>
    )
  }
}

