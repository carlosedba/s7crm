import React, { Component } from 'react'
import { connect } from 'react-redux'
import { Link } from 'react-router-dom' 
import update from 'immutability-helper'

import { closeModal } from '@/actions/Modal'

import Modal from '@/components/Modal'
import Form from '@/components/Form'
import TextInput from '@/components/Input/TextInput'
import SuggestionInput from '@/components/Input/SuggestionInput'
import SelectInput from '@/components/Select'

import IcClose from '@/icons/ic_close'

@connect((state) => {
  return {
    modal: state.Modal,
    forms: state.Forms,
  }
}, (dispatch, ownProps) => {
  return {
    closeModal(name) {
      return dispatch(closeModal(name))
    }
  }
})
export default class ModalFormacaoCreate extends Component {
  constructor(props) {
    super(props)
    
    this.name = 'ModalFormacaoCreate'
    
    this.title = 'Formações'

    this.model = 'S7Formacao'
    this.action = 'create'

    this.handleBtnCloseClick = this.handleBtnCloseClick.bind(this)
    this.handleSubmit = this.handleSubmit.bind(this)
  }

  handleSubmit(event) {
    this.props.closeModal(this.name)
  }

  handleBtnCloseClick(event) {
    this.props.closeModal(this.name)
  }

  render() {
    const generalFormProps = {
      model: this.model,
      action: this.action
    }

    return (
      <Modal name={this.name} contentLabel={this.title}>
        <div className="modal">
          <div className="modal-header">
            <div className="modal-header-left">
              <div className="modal-titles">
                <span className="modal-hat">{this.title}</span>
                <span className="modal-title">Adicionar formação</span>
              </div>
            </div>
            <div className="modal-header-right">
              <button className="btn btn-close" onClick={this.handleBtnCloseClick}>
                <div className="svg icon">
                  <IcClose/>
                </div>
              </button>
            </div>
          </div>
          <div className="modal-content">
            <Form {...generalFormProps} dependsOn={this.props.dependsOn} onSubmit={this.handleSubmit}>
              <div className="inputs">
                <div className="multi-input">
                  <SuggestionInput {...generalFormProps} classes="w3" label="Instituição" attribute="id_s7_instituicao" collection="instituicoes"/>
                  <SuggestionInput {...generalFormProps} classes="w3" label="Curso" attribute="id_s7_curso" collection="cursos"/>
                </div>
                <div className="multi-input">
                  <SelectInput {...generalFormProps} size="w2-q2" label="Tipo" attribute="tipo"
                               options={[
                      { value: 'Técnico', label: 'Técnico' },
                      { value: 'Graduação', label: 'Graduação' },
                      { value: 'Pós-Gradução', label: 'Pós-Gradução' },
                      { value: 'Especialização', label: 'Especialização' },
                      { value: 'MBA', label: 'MBA' },
                      { value: 'Mestrado', label: 'Mestrado' },
                      { value: 'Doutorado', label: 'Doutorado' },
                    ]}/>
                  <TextInput {...generalFormProps} classes="w1-q2" label="Ano de conclusão" attribute="ano_conclusao"/>
                </div>
                <button type="submit" className="btn btn-three">Salvar</button>
              </div>
            </Form>
          </div>
        </div>
      </Modal>
    )
  }
}

