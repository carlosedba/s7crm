import React, { Component } from 'react'
import { Link } from 'react-router-dom'
import { connect } from 'react-redux'
import update from 'immutability-helper'

import { updateFormRaw, updateFormComputed } from '@/actions/Form'

import { SERVER_ADDRESS } from '@/globals'

@connect((state) => {
  return {
    forms: state.Forms,
    modal: state.Modal,
  }
}, (dispatch, ownProps) => {
  return {
    updateFormRaw(model, action, props) {
      return dispatch(updateFormRaw(model, action, props))
    },

    updateFormComputed(model, action, props) {
      return dispatch(updateFormComputed(model, action, props))
    },
  }
})
export default class ObjectAttributeForm extends Component {
  constructor(props) {
    super(props)

    this.handleSubmit = this.handleSubmit.bind(this)
  }

  handleSubmit(event) {
    event.preventDefault()

    const { source, target, mode, modalName, onSubmit } = this.props
    const { forms, modal } = this.props

    const raw = forms[source.model][source.action]['raw']
    const computed = forms[source.model][source.action]['computed']

    if (!mode || mode === 'create') {
      const targetAttribute = forms[target.model][target.action]['computed'][target.attribute]

      if (targetAttribute === null || targetAttribute === undefined) {
        this.props.updateFormComputed(target.model, target.action, {
          $merge: { [target.attribute]: [] }
        })
      }

      this.props.updateFormComputed(target.model, target.action, {
        [target.attribute]: { $push: [Object.assign({}, raw, computed)] }
      })
    } else if (mode === 'update') {
      this.props.updateFormComputed(target.model, target.action, {
        [target.attribute]: {
          [modal[modalName].index]: { $set: Object.assign({}, raw, computed) }
        }
      })
    }

    this.props.updateFormRaw(source.model, source.action, {
      $set: {}
    })
    
    if (onSubmit) onSubmit()
  }

  render() {
    const { children } = this.props

    return (
      <form onSubmit={this.handleSubmit}>
        {children}
      </form>
    )
  }
}

