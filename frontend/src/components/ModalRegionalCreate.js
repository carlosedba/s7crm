import React, { Component } from 'react'
import { connect } from 'react-redux'
import { Link } from 'react-router-dom' 
import update from 'immutability-helper'
import axios from 'axios'

import { trocarNomeSiglaEstado } from '@/utils/BrasilUtils'

import { closeModal } from '@/actions/Modal'
import { updateFormRaw, updateFormComputed } from '@/actions/Form'

import Modal from '@/components/Modal'
import ObjectArrayAttributeForm from '@/components/Form/ObjectArrayAttributeForm'
import TextInput from '@/components/Input/TextInput'

import IcClose from '@/icons/ic_close'

@connect((state) => {
  return {
    modal: state.Modal,
    forms: state.Forms,
  }
}, (dispatch, ownProps) => {
  return {
    closeModal(name) {
      return dispatch(closeModal(name))
    },
    
    updateFormRaw(model, action, props) {
      return dispatch(updateFormRaw(model, action, props))
    },

    updateFormComputed(model, action, props) {
      return dispatch(updateFormComputed(model, action, props))
    },
  }
})
export default class ModalRegionalCreate extends Component {
  constructor(props) {
    super(props)
    
    this.name = 'ModalRegionalCreate'

    this.title = 'Regional'

    this.formSource = {
      model: 'Regional',
      action: 'create',
    }

    this.formTarget = {
      model: 'S7Empresa',
      action: 'create',
      attribute: 'regionais'
    }

    this.onRequestClose = this.onRequestClose.bind(this)
    this.handleBtnCloseClick = this.handleBtnCloseClick.bind(this)
    this.handleTextInputChange = this.handleTextInputChange.bind(this)
    this.handleSubmit = this.handleSubmit.bind(this)
  }

  onRequestClose() {
    this.props.updateFormRaw(this.formSource.model, this.formSource.action, { $set: {} })
    this.props.updateFormComputed(this.formSource.model, this.formSource.action, { $set: {} })
  }

  fillAddress(cep) {
    if (cep.charAt(9) !== '' && cep.charAt(9) !== '_') {
      const cepNumber = parseInt(cep.replace('.', '').replace('-', ''))

      if (cepNumber) {
        axios({
          method: 'get',
          url: `https://viacep.com.br/ws/${cepNumber}/json/`
        }).then((response) => {
          if (!response.data.erro) {
            this.props.updateFormRaw(this.formSource.model, this.formSource.action, {
              $merge: {
                rua: response.data.logradouro,
                numero: response.data.complemento,
                bairro: response.data.bairro,
                cidade: response.data.localidade,
                estado: trocarNomeSiglaEstado(response.data.uf),
                pais: 'Brasil',
              }
            })
          }
        }).catch(console.error)
      }
    }
  }

  handleBtnCloseClick(event) {
    this.props.closeModal(this.name)
  }

  handleTextInputChange(event) {
    if (event.value) {
      if (event.attribute === 'cep') this.fillAddress(event.value)
    }
  }

  handleSubmit(event) {
    this.props.closeModal(this.name)
  }

  render() {
    return (
      <Modal name={this.name} contentLabel={this.title} onRequestClose={this.onRequestClose}>
        <div className="modal">
          <div className="modal-header">
            <div className="modal-header-left">
              <div className="modal-titles">
                <span className="modal-hat">{this.title}</span>
                <span className="modal-title">Adicionar regional</span>
              </div>
            </div>
            <div className="modal-header-right">
              <button className="btn btn-close" onClick={this.handleBtnCloseClick}>
                <div className="svg icon">
                  <IcClose/>
                </div>
              </button>
            </div>
          </div>
          <div className="modal-content">
            <ObjectArrayAttributeForm source={this.formSource} target={this.formTarget} onSubmit={this.handleSubmit}>
              <div className="inputs">
                <div className="multi-input">
                  <TextInput {...this.formSource} classes="w4" label="Nome" attribute="nome"/>
                  <TextInput {...this.formSource} classes="w1-q1" label="CEP" attribute="cep" mask="cep" onChange={this.handleTextInputChange}/>
                </div>
                <div className="multi-input">
                  <TextInput {...this.formSource} classes="w4" label="Rua" attribute="rua"/>
                  <TextInput {...this.formSource} classes="w1" label="Número" attribute="numero"/>
                  <TextInput {...this.formSource} classes="w1" label="Complemento" attribute="complemento"/>
                </div>
                <div className="multi-input">
                  <TextInput {...this.formSource} classes="w2" label="Bairro" attribute="bairro"/>
                  <TextInput {...this.formSource} classes="w2-q1" label="Cidade" attribute="cidade"/>
                </div>
                <div className="multi-input">
                  <TextInput {...this.formSource} classes="w2" label="Estado" attribute="estado"/>
                  <TextInput {...this.formSource} classes="w1-q2" label="País" attribute="pais"/>
                </div>
                <button type="submit" className="btn btn-three">Salvar</button>
              </div>
            </ObjectArrayAttributeForm>
          </div>
        </div>
      </Modal>
    )
  }
}

