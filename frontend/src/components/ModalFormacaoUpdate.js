import React, { Component } from 'react'
import { connect } from 'react-redux'
import { Link } from 'react-router-dom' 
import update from 'immutability-helper'

import { closeModal } from '@/actions/Modal'
import { updateFormRaw, updateFormComputed } from '@/actions/Form'

import Modal from '@/components/Modal'
import Form from '@/components/Form'
import TextInput from '@/components/Input/TextInput'
import SuggestionInput from '@/components/Input/SuggestionInput'
import SelectInput from '@/components/Select'

import IcClose from '@/icons/ic_close'

import NQForm from '@/NQForm'

const NQF = new NQForm()

@connect((state) => {
  return NQF.mapStateToProps({
    modal: state.Modal,
    forms: state.Forms,
    NetQueue: state.NetQueue,
  })
}, NQF.mapDispatchToProps.bind(NQF, (dispatch, ownProps) => {
  return {
    closeModal(name) {
      return dispatch(closeModal(name))
    },

    updateFormRaw(model, action, props) {
      return dispatch(updateFormRaw(model, action, props))
    },

    updateFormComputed(model, action, props) {
      return dispatch(updateFormComputed(model, action, props))
    },
  }
}))
export default class ModalFormacaoUpdate extends Component {
  constructor(props) {
    super(props)
    
    this.name = 'ModalFormacaoUpdate'
    
    this.title = 'Formações'

    this.model = 'S7Formacao'
    this.action = 'create'

    this.formId = null

    this.onAfterOpen = this.onAfterOpen.bind(this)
    this.onRequestClose = this.onRequestClose.bind(this)
    this.handleBtnCloseClick = this.handleBtnCloseClick.bind(this)
    this.handleSubmit = this.handleSubmit.bind(this)
  }
  
  onAfterOpen() {
    const { modal } = this.props

    if (modal[this.name]) {
      const formId = modal[this.name].id

      if (formId) {
        this.formId = formId

        NQF.mapToForm(formId)
      }
    }
  }

  onRequestClose() {
    this.props.updateFormRaw(this.model, this.action, { $set: {} })
    this.props.updateFormComputed(this.model, this.action, { $set: {} })
  }

  handleBtnCloseClick(event) {
    this.props.closeModal(this.name)
  }

  handleSubmit(event) {
    this.props.closeModal(this.name)
  }

  render() {
    const { modal } = this.props
  
    const generalFormProps = {
      model: this.model,
      action: this.action
    }

    return (
      <Modal name={this.name} contentLabel={this.title} onAfterOpen={this.onAfterOpen} onRequestClose={this.onRequestClose}>
        <div className="modal">
          <div className="modal-header">
            <div className="modal-header-left">
              <div className="modal-titles">
                <span className="modal-hat">{this.title}</span>
                <span className="modal-title">Editar formação</span>
              </div>
            </div>
            <div className="modal-header-right">
              <button className="btn btn-close" onClick={this.handleBtnCloseClick}>
                <div className="svg icon">
                  <IcClose/>
                </div>
              </button>
            </div>
          </div>
          <div className="modal-content">
            <Form {...generalFormProps} id={this.formId} update={true} onSubmit={this.handleSubmit}>
              <div className="inputs">
                <div className="multi-input">
                  <SuggestionInput {...generalFormProps} classes="w3" label="Instituição" attribute="id_s7_instituicao" collection="instituicoes"/>
                  <SuggestionInput {...generalFormProps} classes="w3" label="Curso" attribute="id_s7_curso" collection="cursos"/>
                </div>
                <div className="multi-input">
                  <SelectInput {...generalFormProps} size="w2-q2" label="Tipo" attribute="tipo"
                               options={[
                      { value: 'Técnico', label: 'Técnico' },
                      { value: 'Graduação', label: 'Graduação' },
                      { value: 'Pós-Gradução', label: 'Pós-Gradução' },
                      { value: 'Especialização', label: 'Especialização' },
                      { value: 'MBA', label: 'MBA' },
                      { value: 'Mestrado', label: 'Mestrado' },
                      { value: 'Doutorado', label: 'Doutorado' },
                    ]}/>
                  <TextInput {...generalFormProps} classes="w1-q2" label="Ano de conclusão" attribute="ano_conclusao"/>
                </div>
                <button type="submit" className="btn btn-three">Salvar</button>
              </div>
            </Form>
          </div>
        </div>
      </Modal>
    )
  }
}

