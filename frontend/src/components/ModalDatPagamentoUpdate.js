import React, { Component } from 'react'
import { connect } from 'react-redux'
import { Link } from 'react-router-dom' 
import update from 'immutability-helper'

import { closeModal } from '@/actions/Modal'
import { updateFormRaw, updateFormComputed } from '@/actions/Form'

import Modal from '@/components/Modal'
import ObjectArrayAttributeForm from '@/components/Form/ObjectArrayAttributeForm'
import TextInput from '@/components/Input/TextInput'

import IcClose from '@/icons/ic_close'

@connect((state) => {
  return {
    modal: state.Modal,
    forms: state.Forms,
  }
}, (dispatch, ownProps) => {
  return {
    closeModal(name) {
      return dispatch(closeModal(name))
    },

    updateFormRaw(model, action, props) {
      return dispatch(updateFormRaw(model, action, props))
    },

    updateFormComputed(model, action, props) {
      return dispatch(updateFormComputed(model, action, props))
    },
  }
})
export default class ModalDatPagamentoUpdate extends Component {
  constructor(props) {
    super(props)
    
    this.name = 'ModalDatPagamentoUpdate'

    this.title = 'Datas de pagamento'

    this.formSource = {
      model: 'DataPagamento',
      action: 'create',
    }

    this.formTarget = {
      model: 'S7Projeto',
      action: 'create',
      attribute: 'datas_pagamento'
    }

    this.onRequestClose = this.onRequestClose.bind(this)
    this.handleBtnCloseClick = this.handleBtnCloseClick.bind(this)
    this.handleSubmit = this.handleSubmit.bind(this)
  }

  getInitialValue(attribute) {
    const { modal, forms } = this.props

    const attributeValue = forms[this.formTarget.model][this.formTarget.action]['computed'][this.formTarget.attribute]

    if (attributeValue) {
      const arrayItem = attributeValue[modal[this.name].index]

      if (arrayItem) {
        return arrayItem[attribute] || ''
      }
    }
  }

  onRequestClose() {
    this.props.updateFormRaw(this.formSource.model, this.formSource.action, { $set: {} })
    this.props.updateFormComputed(this.formSource.model, this.formSource.action, { $set: {} })
  }

  handleBtnCloseClick(event) {
    this.props.closeModal(this.name)
  }

  handleSubmit(event) {
    this.props.closeModal(this.name)
  }

  render() {
    return (
      <Modal name={this.name} contentLabel={this.title} onRequestClose={this.onRequestClose}>
        <div className="modal">
          <div className="modal-header">
            <div className="modal-header-left">
              <div className="modal-titles">
                <span className="modal-hat">{this.title}</span>
                <span className="modal-title">Editar data</span>
              </div>
            </div>
            <div className="modal-header-right">
              <button className="btn btn-close" onClick={this.handleBtnCloseClick}>
                <div className="svg icon">
                  <IcClose/>
                </div>
              </button>
            </div>
          </div>
          <div className="modal-content">
            <ObjectArrayAttributeForm source={this.formSource} target={this.formTarget} mode="update" modalName={this.name} onSubmit={this.handleSubmit}>
              <div className="inputs">
                <TextInput {...this.formSource} classes="w3" label="Data" attribute="data" initialValue={this.getInitialValue('data')}/>
                <button type="submit" className="btn btn-three">Salvar</button>
              </div>
            </ObjectArrayAttributeForm>
          </div>
        </div>
      </Modal>
    )
  }
}

