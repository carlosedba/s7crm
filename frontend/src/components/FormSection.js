import React, { Component } from 'react'
import { connect } from 'react-redux'
import { Link } from 'react-router-dom' 
import update from 'immutability-helper'

import { NetQueueContext } from '@/contexts/NetQueueContext'

import { openModal, closeModal, updateModal } from '@/actions/Modal'
import { removeItemFromNetQueue } from '@/actions/NetQueue'

import IcPencil from '@/icons/ic_pencil'
import IcTrashcan from '@/icons/ic_trashcan'

@connect((state) => {
  return {
    forms: state.Forms,
    modal: state.Modal,
    NetQueue: state.NetQueue,
  }
}, (dispatch, ownProps) => {
  return {
    openModal(name) {
      return dispatch(openModal(name))
    },

    closeModal(name) {
      return dispatch(closeModal(name))
    },

    updateModal(name, props) {
      return dispatch(updateModal(name, props))
    },

    removeItemFromNetQueue(index) {
      return dispatch(removeItemFromNetQueue(index))
    },
  }
})
export default class FormSection extends Component {
  constructor(props) {
    super(props)

    this.handleNewItemClick = this.handleNewItemClick.bind(this)
  }

  componentDidMount() {}

  updateState(state = {}) {
    this.setState((prevState, props) => {
      return update(prevState, state)
    })
  }

  getNetQueueItems() {
    const { model, action } = this.props
    const { NetQueue } = this.props

    return NetQueue.items.filter((item, i) => {
      if (item.model === model && item.action == action) {
        return true
      }
    })
  }

  handleNewItemClick(event) {
    const { onCreateModalName } = this.props

    this.props.openModal(onCreateModalName)
  }

  handleUpdateItemClick(id, event) {
    const { onUpdateModalName } = this.props

    this.props.updateModal(onUpdateModalName, {
      $merge: { id: id }
    })
    
    this.props.openModal(onUpdateModalName)
  }

  handleRemoveItemClick(index, event) {
    const { onRemove } = this.props

    this.props.removeItemFromNetQueue(index)
  }

  handleCustomActionClick(id, modalName, event) {
    this.props.updateModal(modalName, {
      $merge: { id: id }
    })
    
    this.props.openModal(modalName)
  }

  renderSectionContent() {
    const { attributeToDisplay, customActions } = this.props
    const items = this.getNetQueueItems()

    if (items) {
      return (
        <div className="form-section-content">
          <NetQueueContext.Consumer>
            {(nq) => {
              console.log(nq)
              return nq.queue.map((payload, i) => {
                return (
                  <div key={i} className="form-item">
                    <div className="form-item-left">
                      <span className="form-item-name">{payload.object.raw[attributeToDisplay]}</span>
                    </div>
                    <div className="form-item-right">
                      <div className="actions">
                        {(customActions) && customActions.map((action, i) => {
                          const Icon = action.icon

                          return (
                            <button className="svg action" onClick={this.handleCustomActionClick.bind(this, payload.id, action.modal)}>
                              <Icon/>
                            </button>
                          )
                        })}
                        <button className="svg action" onClick={this.handleUpdateItemClick.bind(this, payload.id)}>
                          <IcPencil/>
                        </button>
                        <button className="svg action" onClick={this.handleRemoveItemClick.bind(this, i)}>
                          <IcTrashcan/>
                        </button>
                      </div>
                    </div>
                  </div>
                )
              })
            }}
          </NetQueueContext.Consumer>
        </div>
      )
    }
  }

  render() {
    const { title, message, onClick } = this.props
    const Icon = this.props.icon


    return (
      <div className="form-section">
        <p className="form-section-title">{title}</p>
        <button className="form-new-item" onClick={this.handleNewItemClick}>
          {(Icon) && (
            <div className="svg icon">
              <Icon/>
            </div>
          )}
          {message}
        </button>
        
        {this.renderSectionContent()}

        {this.props.children}
      </div>
    )
  }
}

