import React, { Component } from 'react'
import { Link } from 'react-router-dom'
import { connect } from 'react-redux'
import update from 'immutability-helper'
import classnames from 'classnames'

import { SERVER_ADDRESS } from '@/globals'

import '../assets/css/sidebar.css'

import logo from '@/assets/img/logo.png'

@connect((state) => {
  return {
    user: state.Session.user
  }
})
export default class Sidebar extends Component {
  constructor(props) {
    super(props)

    this.isCurrentPath = this.isCurrentPath.bind(this)
  }

  updateState(state = {}) {
    this.setState((prevState, props) => {
      return update(prevState, state)
    })
  }

  isCurrentPath(pathname) {
    const { location } = this.props

    if (location && location.pathname.includes(pathname)) {
      return true
    } else {
      return false
    }
  }

  getUserDataLink(filename) {
    return `${SERVER_ADDRESS}/data/user_data/${filename}`
  }

  getContentDataLink(filename) {
    return `${SERVER_ADDRESS}/data/content_data/${filename}`
  }

  render() {
    return (
      <nav styleName="sidebar">
        <div styleName="sidebar-top"></div>
        <div styleName="sidebar-row">
          <div styleName="sidebar-logo">
            <img src={logo}/>
          </div>
          <ul styleName="sidebar-menu">
            <li styleName={classnames({
              'active': false
            })}>
              <Link to="/">Dashboard</Link>
            </li>

            <li styleName={classnames({
              'active': this.isCurrentPath('/projetos')
            })}>
              <Link to="/projetos">Projetos</Link>
            </li>

            <li styleName={classnames({
              'active': this.isCurrentPath('/profissionais')
            })}>
              <Link to="/profissionais">Profissionais</Link>
            </li>

            <li styleName={classnames({
              'active': this.isCurrentPath('/empresas')
            })}>
              <Link to="/empresas">Empresas</Link>
            </li>
          </ul>
        </div>
        <button styleName="sidebar-action">
          <div className="svg" styleName="icon">
            <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 42 42"><path d="M37.059 16H26V4.941C26 2.224 23.718 0 21 0s-5 2.224-5 4.941V16H4.941C2.224 16 0 18.282 0 21s2.224 5 4.941 5H16v11.059C16 39.776 18.282 42 21 42s5-2.224 5-4.941V26h11.059C39.776 26 42 23.718 42 21s-2.224-5-4.941-5z"/></svg>
          </div>
        </button>
        <div styleName="sidebar-bottom"></div>
      </nav>
    )
  }
}

