import React, { Component } from 'react'
import { connect } from 'react-redux'
import { Link } from 'react-router-dom' 
import update from 'immutability-helper'

import { closeModal } from '@/actions/Modal'
import { updateFormRaw, updateFormComputed } from '@/actions/Form'

import Modal from '@/components/Modal'
import Form from '@/components/Form'
import TextInput from '@/components/Input/TextInput'
import SuggestionInput from '@/components/Input/SuggestionInput'
import SelectInput from '@/components/Select'

import IcClose from '@/icons/ic_close'

import NQForm from '@/NQForm'

const NQF = new NQForm()

@connect((state) => {
  return NQF.mapStateToProps({
    modal: state.Modal,
    forms: state.Forms,
    NetQueue: state.NetQueue,
  })
}, NQF.mapDispatchToProps.bind(NQF, (dispatch, ownProps) => {
  return {
    closeModal(name) {
      return dispatch(closeModal(name))
    },

    updateFormRaw(model, action, props) {
      return dispatch(updateFormRaw(model, action, props))
    },

    updateFormComputed(model, action, props) {
      return dispatch(updateFormComputed(model, action, props))
    },
  }
}))
export default class ModalExpProfissionalUpdate extends Component {
  constructor(props) {
    super(props)
    
    this.name = 'ModalProProjetoUpdate'
    
    this.title = 'Profissionais'

    this.model = 'ProfissionalProjeto'
    this.action = 'create'

    this.formId = null

    this.onAfterOpen = this.onAfterOpen.bind(this)
    this.onRequestClose = this.onRequestClose.bind(this)
    this.handleBtnCloseClick = this.handleBtnCloseClick.bind(this)
    this.handleSubmit = this.handleSubmit.bind(this)
  }
  
  onAfterOpen() {
    const { modal } = this.props

    if (modal[this.name]) {
      const formId = modal[this.name].id

      if (formId) {
        this.formId = formId

        NQF.mapToForm(formId)
      }
    }
  }

  onRequestClose() {
    this.props.updateFormRaw(this.model, this.action, { $set: {} })
    this.props.updateFormComputed(this.model, this.action, { $set: {} })
  }

  handleBtnCloseClick(event) {
    this.props.closeModal(this.name)
  }

  handleSubmit(event) {
    this.props.closeModal(this.name)
  }

  render() {
    const { modal } = this.props
  
    const generalFormProps = {
      model: this.model,
      action: this.action
    }

    return (
      <Modal name={this.name} contentLabel={this.title} onAfterOpen={this.onAfterOpen} onRequestClose={this.onRequestClose}>
        <div className="modal">
          <div className="modal-header">
            <div className="modal-header-left">
              <div className="modal-titles">
                <span className="modal-hat">{this.title}</span>
                <span className="modal-title">Editar profissional</span>
              </div>
            </div>
            <div className="modal-header-right">
              <button className="btn btn-close" onClick={this.handleBtnCloseClick}>
                <div className="svg icon">
                  <IcClose/>
                </div>
              </button>
            </div>
          </div>
          <div className="modal-content">
            <Form {...generalFormProps} id={this.formId} update={true} onSubmit={this.handleSubmit}>
              <div className="inputs">
                <div className="multi-input">
                  <SuggestionInput {...generalFormProps} classes="w3" label="Profissional" attribute="id_s7_profissional" collection="profissionais"/>
                  <SuggestionInput {...generalFormProps} classes="w2-q2" label="Cargo" attribute="id_s7_cargo" collection="cargos"/>
                </div>
                <div className="multi-input">
                  <TextInput {...generalFormProps} classes="w1-q1" label="Honorário" attribute="honorario"/>
                  <TextInput {...generalFormProps} classes="w1-q2" label="Salário inicial" attribute="salario_inicial"/>
                  <TextInput {...generalFormProps} classes="w1-q2" label="Salário final" attribute="salario_final"/>
                </div>
                <div className="multi-input"> 
                  <TextInput {...generalFormProps} classes="w1" label="Comissão" attribute="comissao"/>
                  <TextInput {...generalFormProps} classes="w1" label="Bonus" attribute="bonus"/>
                  <SelectInput {...generalFormProps} size="w3" label="Benefícios" attribute="beneficios" isMulti={true}
                               options={[
                      { value: 'Assistência médica', label: 'Assistência médica' },
                      { value: 'Assistência odontológica', label: 'Assistência odontológica' },
                      { value: 'Auxílio farmácia', label: 'Auxílio farmácia' },
                      { value: 'Seguro de vida', label: 'Seguro de vida' },
                      { value: 'Previdência privada', label: 'Previdência privada' },
                      { value: 'Auxílio creche', label: 'Auxílio creche' },
                      { value: 'Bolsa de estudos', label: 'Bolsa de estudos' },
                      { value: 'Vale refeição', label: 'Vale refeição' },
                      { value: 'Vale almentação', label: 'Vale almentação' },
                      { value: 'Refeitório local', label: 'Refeitório local' },
                      { value: 'Cesta básica', label: 'Cesta básica' },
                      { value: 'Cartão corporativo', label: 'Cartão corporativo' },
                      { value: 'Carro', label: 'Carro' },
                      { value: 'Combustível', label: 'Combustível' },
                      { value: 'Celular', label: 'Celular' },
                      { value: 'Notebook', label: 'Notebook' },
                    ]}/>
                </div>
                <button type="submit" className="btn btn-three">Salvar</button>
              </div>
            </Form>
          </div>
        </div>
      </Modal>
    )
  }
}

