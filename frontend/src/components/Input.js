import React, { Component } from 'react'
import { connect } from 'react-redux'
import { Link } from 'react-router-dom' 
import update from 'immutability-helper'

export default class Input extends Component {
  constructor(props) {
    super(props)

    this.state = {
      inputValue: ''
    }
  }

  componentDidMount() {}

  updateState(state = {}) {  
    this.setState((prevState, props) => {
      return update(prevState, state)
    })
  }

  handleInputChange(event) {
    const inputText = event.target.value

    if (value !== undefined && value !== null) {
      this.updateState({
        inputValue: { $set: inputText }
      })
    }
  }

  render() {
    const { label, classes } = this.props

    return (
      <div className={classNames('input', classes)}>
        <label>{label}:</label>
        <input type="text" value={this.state.inputValue} onChange={this.handleInputChange}/>
      </div>
    )
  }
}

