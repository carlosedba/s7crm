import React, { Component } from 'react'
import { connect } from 'react-redux'
import { Link } from 'react-router-dom' 
import update from 'immutability-helper'

import { openModal, closeModal, updateModal } from '@/actions/Modal'
import { updateFormRaw, updateFormComputed } from '@/actions/Form'

import IcPencil from '@/icons/ic_pencil'
import IcTrashcan from '@/icons/ic_trashcan'

@connect((state) => {
  return {
    forms: state.Forms,
    modal: state.Modal,
  }
}, (dispatch, ownProps) => {
  return {
    openModal(name) {
      return dispatch(openModal(name))
    },

    closeModal(name) {
      return dispatch(closeModal(name))
    },

    updateModal(name, props) {
      return dispatch(updateModal(name, props))
    },

    updateFormComputed(model, action, props) {
      return dispatch(updateFormComputed(model, action, props))
    },
  }
})
export default class TextAttributeFormSection extends Component {
  constructor(props) {
    super(props)

    this.handleNewItemClick = this.handleNewItemClick.bind(this)
  }

  componentDidMount() {}

  updateState(state = {}) {
    this.setState((prevState, props) => {
      return update(prevState, state)
    })
  }

  handleNewItemClick(event) {
    const { onCreateModalName } = this.props

    this.props.openModal(onCreateModalName)
  }

  handleUpdateItemClick(index, event) {
    const { onUpdateModalName } = this.props

    this.props.updateModal(onUpdateModalName, {
      $merge: { index: index }
    })
    
    this.props.openModal(onUpdateModalName)
  }

  handleRemoveItemClick(index, event) {
    const { model, action, attribute } = this.props
    const { forms } = this.props

    this.props.updateFormComputed(model, action, {
      [attribute]: { $splice: [[index, 1]] }
    })
  }

  render() {
    const { title, message, onClick } = this.props
    const Icon = this.props.icon


    return (
      <div className="form-section">
        <p className="form-section-title">{title}</p>
        <button className="form-new-item" onClick={this.handleNewItemClick}>
          {(Icon) && (
            <div className="svg icon">
              <Icon/>
            </div>
          )}
          {message}
        </button>
        
        {this.props.children}
      </div>
    )
  }
}

