import React, { Component } from 'react'
import { connect } from 'react-redux'
import { Link } from 'react-router-dom' 
import update from 'immutability-helper'
import ReactModal from 'react-modal'

import { addModal, openModal, closeModal, updateModal, resetModal } from '@/actions/Modal'

@connect((state) => {
  return {
    modal: state.Modal,
  }
}, (dispatch, ownProps) => {
  return {
    openModal(name) {
      return dispatch(openModal(name))
    },

    closeModal(name) {
      return dispatch(closeModal(name))
    },

    updateModal(name, props) {
      return dispatch(updateModal(name, props))
    },

    resetModal(name) {
      return dispatch(resetModal(name))
    },
  }
})
export default class Modal extends Component {
  constructor(props) {
    super(props)

    this.onAfterOpen = this.onAfterOpen.bind(this)
    this.onRequestClose = this.onRequestClose.bind(this)
  }

  componentDidMount() {}

  onAfterOpen(event) {
    const { onAfterOpen } = this.props

    if (onAfterOpen) onAfterOpen()
  }

  onRequestClose(event) {
    const { name, onRequestClose } = this.props

    this.props.closeModal(name)

    if (onRequestClose) onRequestClose()
  }

  render() {
    const { modal, name, children } = this.props

    return (
      <ReactModal {...this.props}
        isOpen={modal[name] && modal[name].isOpen}
        shouldCloseOnOverlayClick={true}
        onAfterOpen={this.onAfterOpen}
        onRequestClose={this.onRequestClose}

        className="react-modal"
        portalClassName="react-modal-portal"
        overlayClassName="react-modal-overlay"
        bodyOpenClassName="react-modal-body-open"

        shouldFocusAfterRender={true}>
        {children}
      </ReactModal>
    )
  }
}

