import React, { Component } from 'react'
import { connect } from 'react-redux'

import { NetQueueContext } from '@/contexts/NetQueueContext'
import { proccessNetQueue } from '@/actions/NetQueue'
import { updateFormRaw, updateFormComputed } from '@/actions/Form'

import IdGenerator from '@/utils/IdGenerator'

import NQForm from '@/NQForm'

const NQF = new NQForm()

@connect((state) => {
  return NQF.mapStateToProps({
    forms: state.Forms,
    NetQueue: state.NetQueue,
  })
}, NQF.mapDispatchToProps.bind(NQF, (dispatch, ownProps) => {
  return {
    proccessNetQueue(props) {
      return dispatch(proccessNetQueue(props))
    },

    updateFormRaw(model, action, props) {
      return dispatch(updateFormRaw(model, action, props))
    },

    updateFormComputed(model, action, props) {
      return dispatch(updateFormComputed(model, action, props))
    },
  }
}))
export default class Form extends Component {
  constructor(props) {
    super(props)

    this.handleSubmit = this.handleSubmit.bind(this)
  }

  componentDidMount() {
    const { submit } = this.props

    if (submit) submit(this.submit.bind(this))
  }

  submit() {
    const { model, action } = this.props
    const { id, update, dependsOn, onSubmit, NetQueue } = this.props
    
    NQF.mergeProps({ model, action })
    
    if (!id && !update) {
      NQF.add(IdGenerator.default(), dependsOn)
    } else if (id && !update) {
      NQF.add(id, dependsOn)
    } else if (id && update) {
      NQF.update(id, dependsOn)
    }
    
    if (onSubmit) onSubmit()

    this.props.updateFormRaw(model, action, { $set: {} })
    this.props.updateFormComputed(model, action, { $set: {} })
  }

  handleSubmit(event) {
    event.preventDefault()
    
    this.submit()
  }

  render() {
    const { children } = this.props

    return (
      <form onSubmit={this.handleSubmit}>
        {children}
      </form>
    )
  }
}

