import update from 'immutability-helper'
import nanoid from 'nanoid'

import { updateFormRaw, updateFormComputed } from '@/actions/Form'
import { addItemToNetQueue, updateItemFromNetQueue, proccessNetQueue, test } from '@/actions/NetQueue'

import IdGenerator from '@/utils/IdGenerator'

import NQController from '@/NQController'

export default class NQForm extends NQController {
  constructor(props) {
    super(props)
  }

  mapStateToProps(state) {
    return super.mapStateToProps(state)
  }

  mapDispatchToProps(callback, dispatch, ownProps) {
    return super.mapDispatchToProps(callback, dispatch, ownProps)
  }

  onReceiveProps() {
    super.onReceiveProps()
  }

  mergeProps(props) {
    this.props = update(this.props, {
      $merge: props
    })
  }

  add(id, dependsOn) {
    const { model, action } = this.props
    const { forms } = this.props

    const form = forms[model][action]

    let payload = {
      id: id,
      model: model,
      action: action,
      object: form,
      status: 'idle',
      timestamp: Date.now()
    }

    if (dependsOn) payload = Object.assign({}, payload, {
      dependsOn: [dependsOn]
    })

    window.NetQueue.add(payload)
  }

  update(id, dependsOn) {
    const { model, action } = this.props
    const { forms, NetQueue } = this.props

    const form = forms[model][action]

    let payload = {
      id: id,
      model: model,
      action: action,
      object: form,
      status: 'idle',
      timestamp: Date.now()
    }

    if (dependsOn) payload = Object.assign({}, payload, { dependsOn: dependsOn })

    window.NetQueue.update(id, payload)
  }

  proccess() {
    window.NetQueue.proccess()
  }

  mapToForm(id) {
    const payload = window.NetQueue.findPayloadById(id)

    if (payload) {
      this.dispatch(updateFormRaw(payload.model, payload.action, { $set: payload.object.raw }))
      this.dispatch(updateFormComputed(payload.model, payload.action, { $set: payload.object.computed }))
    }
  }
}

