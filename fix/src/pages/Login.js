import React, { useRef, useState, useEffect } from 'react'
import { useSelector, useDispatch } from 'react-redux'
import { Link, Redirect } from 'react-router-dom'
import { createSelector } from 'reselect'

import { login } from '@/actions/Session'

export default function Login(props) {
  const auth = useSelector(state => state.Session.auth)
  const authenticated = useSelector(state => state.Session.auth.authenticated)

  const [email, setEmail] = useState('')
  const [password, setPassword] = useState('')

  const dispatch = useDispatch()

  function handleEmailChange(event) {
    const value = event.target.value
    setEmail(value)
  }

  function handlePasswordChange(event) {
    const value = event.target.value
    setPassword(value)
  }

  function handleSubmit(event) {
    event.preventDefault()

    dispatch(login({ email, password }))
  }

  if (authenticated) {
    return (<Redirect to="/empresas"/>)
  }

  return (
    <div className="page page-login">
      <div className="wrapper">
        <div className="logo">
          <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 555 400"><path d="M550 394.9l-117.1-169L280.1 5.2l-135 195.6L4.3 395.2l287.3-113.5-25.2 64.7z"/></svg>
        </div>
        <form onSubmit={handleSubmit}>
          <div className="form">
            <div className="inputs">
              <div className="input">
                <label>E-mail</label>
                <input type="email"placeholder="exemplo@email.com" value={email} onChange={handleEmailChange}/>
                {(auth.error && auth.error.code === 1002) && (
                  <div className="input-alert error">
                    <div className="svg icon">
                      <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 510 510"><path d="M255 0C114.8 0 0 114.8 0 255s114.8 255 255 255 255-114.8 255-255S395.3 0 255 0zm30.4 406.8h-60.7v-60.7h60.7v60.7zm0-121.4h-60.7V103.2h60.7v182.2z"/></svg>
                    </div>
                    <span className="message">E-mail incorreto, tente novamente.</span>
                  </div>
                )}
              </div>
              <div className="input">
                <label>Senha</label>
                <input type="password" placeholder="●●●●●●●" value={password} onChange={handlePasswordChange}/>
                {(auth.error && auth.error.code === 1001) && (
                  <div className="input-alert error">
                    <div className="svg icon">
                      <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 510 510"><path d="M255 0C114.8 0 0 114.8 0 255s114.8 255 255 255 255-114.8 255-255S395.3 0 255 0zm30.4 406.8h-60.7v-60.7h60.7v60.7zm0-121.4h-60.7V103.2h60.7v182.2z"/></svg>
                    </div>
                    <span className="message">Senha incorreta, tente novamente.</span>
                  </div>
                )}
              </div>
            </div>
            <div className="action">
              <a href="#" className="forgot"></a>
              <button className="btn btn-secondary" type="submit">Entrar</button>
            </div>
          </div>
        </form>
      </div>
    </div>
  )
}

