import React, { useState, useEffect } from 'react'
import { useSelector, useDispatch } from 'react-redux'
import { Link } from 'react-router-dom'
import update from 'immutability-helper'

import IcPencil from '@/icons/ic_pencil'
import IcTrashcan from '@/icons/ic_trashcan'

export default function Empresas(props) {
  return (
    <div className="page control-page">
      <div className="page-header-lr">
        <div className="page-header-left">
          <div className="page-titles">
            <p className="page-title">Empresas</p>
            <p className="page-text"></p>
          </div>
        </div>
        <div className="page-header-right">
          <Link to="/empresas/cadastrar" className="btn btn-four">Cadastrar empresa</Link>
        </div>
      </div>
      <div className="page-content">
        <div className="form-section">

          <div className="form-item">
            <div className="form-item-left">
              <span className="form-item-name">Federação das Indústrias do Estado do Paraná</span>
            </div>
            <div className="form-item-right">
              <div className="actions">
                <button className="svg action">
                  <IcPencil/>
                </button>
                <button className="svg action">
                  <IcTrashcan/>
                </button>
              </div>
            </div>
          </div>

          <div className="form-item">
            <div className="form-item-left">
              <span className="form-item-name">Coca-Cola</span>
            </div>
            <div className="form-item-right">
              <div className="actions">
                <button className="svg action">
                  <IcPencil/>
                </button>
                <button className="svg action">
                  <IcTrashcan/>
                </button>
              </div>
            </div>
          </div>

          <div className="form-item">
            <div className="form-item-left">
              <span className="form-item-name">Rumo</span>
            </div>
            <div className="form-item-right">
              <div className="actions">
                <button className="svg action">
                  <IcPencil/>
                </button>
                <button className="svg action">
                  <IcTrashcan/>
                </button>
              </div>
            </div>
          </div>

        </div>
      </div>
    </div>
  )
}

