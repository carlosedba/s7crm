import React, { useState, useEffect } from 'react'
import { useSelector, useDispatch } from 'react-redux'
import { Link } from 'react-router-dom'
import update from 'immutability-helper'

export default function Projetos(props) {
  return (
    <div className="page control-page">
      <div className="page-header">
        <div className="page-titles">
          <p className="page-title">Projetos</p>
          <p className="page-text"></p>
        </div>
      </div>
      <div className="page-content">
        <Link to="/empresas">Empresas</Link>
        <br/>
        <Link to="/login">Login</Link>
        <div className="bottombar"></div>
      </div>
    </div>
  )
}

