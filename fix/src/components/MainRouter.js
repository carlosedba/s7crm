import React, { useState, useEffect } from 'react'
import { useSelector, useDispatch } from 'react-redux'
import {
  BrowserRouter as Router,
  Switch,
  Route,
  Link
} from 'react-router-dom'
import { TransitionGroup, CSSTransition } from 'react-transition-group'

import AuthorizedRoute from '@/components/AuthorizedRoute'
import AuthorizedLayout from '@/components/AuthorizedLayout'

import Sidebar from '@/components/Sidebar'

import Dashboard from '@/pages/Dashboard'
import Empresas from '@/pages/Empresa/Empresas'
import Profissionais from '@/pages/Profissional/Profissionais'
import Projetos from '@/pages/Projeto/Projetos'
import Inputs from '@/pages/Inputs'
import Login from '@/pages/Login'

import { NetQueueContext } from '@/contexts/NetQueueContext'

import { BASEPATH } from '@/globals'

export default function Main(props) {
  const auth = useSelector(state => state.Session.auth)

  return (
    <div className="app">
        <Router>
          <Switch>
            <Route path="/login" component={Login}/>
            <AuthorizedRoute path="/" component={AuthorizedLayout}/>
          </Switch>
        </Router>
    </div>
  )
}

