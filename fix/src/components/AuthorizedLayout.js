import React, { useState, useEffect } from 'react'
import { useSelector, useDispatch } from 'react-redux'
import {
  Switch,
  Route
} from 'react-router-dom'
import { TransitionGroup, CSSTransition } from 'react-transition-group'

import { NetQueueContext } from '@/contexts/NetQueueContext'

import AuthorizedRoute from '@/components/AuthorizedRoute'

import Sidebar from '@/components/Sidebar'

import Dashboard from '@/pages/Dashboard'
import Empresas from '@/pages/Empresa/Empresas'
import Profissionais from '@/pages/Profissional/Profissionais'
import Projetos from '@/pages/Projeto/Projetos'
import Inputs from '@/pages/Inputs'
import Logout from '@/pages/Logout'

export default function AuthorizedLayout() {
  const authenticated = useSelector(state => state.Session.auth.authenticated)

  useEffect(() => {
    console.log('log > AuthorizedLayout initialized!')
  })

  return (
    <NetQueueContext.Provider value={window.NetQueue}>
      <Sidebar/>

      <Switch>
        <Route path="/logout" component={Logout}/>
        <Route path="/inputs" component={Inputs}/>
        <Route path="/projetos" component={Projetos}/>
        <Route path="/profissionais" component={Profissionais}/>
        <Route path="/empresas" component={Empresas}/>
        <Route path="/" component={Dashboard}/>
      </Switch>
    </NetQueueContext.Provider>
  )
}

