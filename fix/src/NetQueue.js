import axios from 'axios'
import store from 'store'

import { S7_API_ENDPOINT } from '@/globals'

export default function NetQueue() {
  this.queue = []
}

NetQueue.prototype.findPayloadById = function (id) {
  for (let i = 0; i < this.queue.length; i++) {
    let payload = this.queue[i]

    if (payload.id === id) {
      return {
        index: i,
        payload: payload
      }
    }
  }
}

NetQueue.prototype.add = function (payload) {
  console.log('log > NetQueue > add called!')
  
  this.queue.push(payload)

  return payload
}

NetQueue.prototype.update = function (id, payload) {
  const query = this.findPayloadById(id)

  if (query) {
    this.queue[query.index] = payload
  }

  return this.queue[query.index]
}

NetQueue.prototype.remove = function (id) {
  const query = this.findPayloadById(id)

  if (query) {
    this.queue.splice(query.index, 1)
  }
}

NetQueue.prototype.clear = function () {
  this.queue = []
}


NetQueue.prototype.proccess = function () {
  console.log('log > NetQueue > proccess called!', this.queue)

  return new Promise((resolve, reject) => {
    axios({
      method: 'post',
      data: {
        payload: this.queue
      },
      url: `${S7_API_ENDPOINT}/netqueue/proccess`,
      headers: {
        'Authorization': `Bearer ${store.get('token')}`
      },
    })
    .then((response) => {
      if (!response.data.error) resolve(response)
      else reject(response)
    })
    .catch(reject)
  })
} 

  