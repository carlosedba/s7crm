import {
	SEARCH_USERS,
	FIND_ALL_USERS,
	FIND_ONE_USER,
	CREATE_USER,
	UPDATE_USER,
	DELETE_USER,
} from '@/actionTypes'

const INITIAL_STATE = {
	search: 		{ items: [], error: null, loading: null },
	list: 			{ items: [], error: null, loading: null },
	active: 		{ item: null, error: null, loading: null },
	new: 			{ item: null, error: null, loading: null },
	updated: 		{ item: null, error: null, loading: null },
	deleted: 		{ item: null, error: null, loading: null },
}

export default function(state = INITIAL_STATE, action) {
  let error

  switch (action.type) {
		case SEARCH_USERS:
			return { ...state, search: { items: [], error: null, loading: null } }

		case `${SEARCH_USERS}_PENDING`:
			return { ...state, search: { items: [], error: null, loading: true } }

		case `${SEARCH_USERS}_FULFILLED`:
			return { ...state, search: { items: action.payload.data, error: null, loading: false } }

		case `${SEARCH_USERS}_REJECTED`:
			error = action.payload.data || { message: action.payload.message }
			return { ...state, search: { items: [], error: error, loading: false } }


		case FIND_ALL_USERS:
			return { ...state, list: { items: [], error: null, loading: null } }

		case `${FIND_ALL_USERS}_PENDING`:
			return { ...state, list: { items: [], error: null, loading: true } }

		case `${FIND_ALL_USERS}_FULFILLED`:
			return { ...state, list: { items: action.payload.data, error: null, loading: false } }

		case `${FIND_ALL_USERS}_REJECTED`:
			error = action.payload.data || { message: action.payload.message }
			return { ...state, list: { items: [], error: error, loading: false } }


		case FIND_ONE_USER:
			return { ...state, active: { item: null, error: null, loading: null } }

		case `${FIND_ONE_USER}_PENDING`:
			return { ...state, active: { item: null, error: null, loading: true } }

		case `${FIND_ONE_USER}_FULFILLED`:
			return { ...state, active: { item: action.payload.data, error: null, loading: false } }

		case `${FIND_ONE_USER}_REJECTED`:
			error = action.payload.data || { message: action.payload.message }
			return { ...state, active: { item: null, error: error, loading: false } }


		case CREATE_USER:
			return { ...state, new: { item: null, error: null, loading: null } }

		case `${CREATE_USER}_PENDING`:
			return { ...state, new: { item: null, error: null, loading: true } }

		case `${CREATE_USER}_FULFILLED`:
			return { ...state, new: { item: action.payload.data, error: null, loading: false } }

		case `${CREATE_USER}_REJECTED`:
			error = action.payload.data || { message: action.payload.message }
			return { ...state, new: { item: null, error: error, loading: false } }


		case UPDATE_USER:
			return { ...state, updated: { item: null, error: null, loading: null } }

		case `${UPDATE_USER}_PENDING`:
			return { ...state, updated: { item: null, error: null, loading: true } }

		case `${UPDATE_USER}_FULFILLED`:
			return { ...state, updated: { item: action.payload.data, error: null, loading: false } }

		case `${UPDATE_USER}_REJECTED`:
			error = action.payload.data || { message: action.payload.message }
			return { ...state, updated: { item: null, error: error, loading: false } }


		case DELETE_USER:
			return { ...state, deleted: { item: null, error: null, loading: null } }

		case `${DELETE_USER}_PENDING`:
			return { ...state, deleted: { item: null, error: null, loading: true } }

		case `${DELETE_USER}_FULFILLED`:
			return { ...state, deleted: { item: action.payload.data, error: null, loading: false } }

		case `${DELETE_USER}_REJECTED`:
			error = action.payload.data || { message: action.payload.message }
			return { ...state, deleted: { item: null, error: error, loading: false } }

		default:
			return state;
  }
}
