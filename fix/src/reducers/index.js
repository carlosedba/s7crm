import { combineReducers } from 'redux'

import Session from './Session'
import Users from './Users'
import NetQueue from './NetQueue'

const reducer = combineReducers({
  Session,
  Users,
  NetQueue,
})

export default reducer

