import axios from 'axios'
import store from 'store'
import { S7_API_ENDPOINT } from '@/globals'
import * as types from '@/actionTypes'

export function addItemToNetQueue(item) { 
  return {
    type: types.ADD_ITEM_TO_NETQUEUE,
    payload: {
      item: item,
    }
  }
}

export function updateItemFromNetQueue(index, item) { 
  return {
    type: types.UPDATE_ITEM_FROM_NETQUEUE,
    payload: {
      index: index,
      item: item,
    }
  }
}

export function removeItemFromNetQueue(index) { 
  return {
    type: types.REMOVE_ITEM_FROM_NETQUEUE,
    payload: {
      index: index,
    }
  }
}

export function test() { 
  return {
    type: 'TEST',
    payload: null
  }
}

export function proccessNetQueue(props) { 
  const request = axios({
    method: 'post',
    data: props,
    url: `${S7_API_ENDPOINT}/netqueue/proccess`,
    headers: {
      'Authorization': `Bearer ${store.get('token')}`
    },
  })

  return {
    type: types.PROCCESS_NETQUEUE,
    payload: request
  }
}

