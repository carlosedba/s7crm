import nanoid from 'nanoid/generate'

const alphabet = '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz'

export default class IdGenerator {
  static default() {
    return nanoid(alphabet, 12)
  }
}