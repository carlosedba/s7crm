import nanoid from 'nanoid/generate'

import * as S7Segmentos from '@/actions/S7Segmentos'
import * as S7Setores from '@/actions/S7Setores'
import * as S7Cargos from '@/actions/S7Cargos'
import * as S7Beneficios from '@/actions/S7Beneficios'
import * as S7Idiomas from '@/actions/S7Idiomas'
import * as S7Instituicoes from '@/actions/S7Instituicoes'
import * as S7Cursos from '@/actions/S7Cursos'
import * as S7Empresas from '@/actions/S7Empresas'
import * as S7PrincipaisContatos from '@/actions/S7PrincipaisContatos'
import * as S7Profissionais from '@/actions/S7Profissionais'
import * as S7Experiencias from '@/actions/S7Experiencias'
import * as S7Formacoes from '@/actions/S7Formacoes'
import * as S7ComentariosProfissional from '@/actions/S7ComentariosProfissional'
import * as S7Projetos from '@/actions/S7Projetos'
import * as S7ProfissionaisSelecionados from '@/actions/S7ProfissionaisSelecionados'

export default class NetQueue {
  constructor(props) {
    this.collections = [
      'segmentos',
      'setores',
      'cargos',
      'beneficios',
      'idiomas',
      'instituicoes',
      'cursos',
      'empresas',
      'principais-contatos',
      'profissionais',
      'experiencias',
      'formacoes',
      'comentarios-profissional',
      'projetos',
      'profissionais-selecionados',
    ]

    this.handlers = [
      S7Segmentos,
      S7Setores,
      S7Cargos,
      S7Beneficios,
      S7Idiomas,
      S7Instituicoes,
      S7Cursos,
      S7Empresas,
      S7PrincipaisContatos,
      S7Profissionais,
      S7Experiencias,
      S7Formacoes,
      S7ComentariosProfissional,
      S7Projetos,
      S7ProfissionaisSelecionados,
    ]

    this.queue = []
  }

  generateId() {
    const alphabet = '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz'
    return nanoid(alphabet, 4)
  }

  isValidCollection(collection) {
    for (let i = 0; i < this.collections.length; i++) {
      if (this.collections[i] === collection) return i
    }
  }

  list() {
    return this.queue
  }

  add(action, collection, itemId, data) {
    const id = this.generateId()

    this.queue.push([id, action, collection, itemId, data])
  }

  remove(id) {
    //
  }

  proccess(id) {
    for (let item of this.queue) {
      if (item[0] === id) {
        if (collectionIndex = this.isValidCollection(item[2])) {
          switch (item[1]) {
            case 'create':
              this.handlers[collectionIndex].create(item[4])
              break
            case 'update':
              this.handlers[collectionIndex].update(item[3], item[4])
              break
            case 'remove':
              this.handlers[collectionIndex].remove(item[3])
              break
          }
        }
      }
    }
  }

  proccessAll() {
    for (let item of this.queue) {
      if (collectionIndex = this.isValidCollection(item[2])) {
        switch (item[1]) {
          case 'create':
            this.handlers[collectionIndex].create(item[4])
            break
          case 'update':
            this.handlers[collectionIndex].update(item[3], item[4])
            break
          case 'remove':
            this.handlers[collectionIndex].remove(item[3])
            break
        }
      }
    }
  }
}
























