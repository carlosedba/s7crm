export function getCollectionName(name, prefix) {
  if (prefix) name = name.split(prefix)[1]
  name = name.replace(/(?=[A-Z])/g, '-').toLowerCase().substr(1)
  return name
}
