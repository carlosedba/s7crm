const errors = [
	{ code: 1000, 	message: 'Planilha inválida.' },
	{ code: 2001, 	message: 'Preencha os campos obrigatórios.' },
	{ code: 2002, 	message: 'Selecione pelo menos uma linha antes de continuar.' },
	{ code: 2003, 	message: 'Erro ao salvar os dados da CI na planilha.\nA planilha está aberta em outro processo.' },
	{ code: 2004, 	message: 'Erro ao salvar o PDF.' },
]

module.exports = function (code) {
	let error = null

	errors.forEach(function (el, ind, arr) {
		if (el.code === code) {
			error = el
		}
	})

	return error
}