const path = require('path')
const webpack = require('webpack')
const MinifyPlugin = require("babel-minify-webpack-plugin")
const CompressionPlugin = require('compression-webpack-plugin')
const merge = require('webpack-merge')

const base = require('./webpack.config.base')

module.exports = function (env) {
	return merge(base(), {
		mode: 'production',

		devtool: false,

		output: {
			publicPath: 'fiep/'
		},

		optimization: {
			minimize: true,
			splitChunks: {
				chunks: 'all',
				automaticNameDelimiter: '.',
			},
		},

		plugins: [
			new webpack.DefinePlugin({
				PRODUCTION: true,
			}),

			new webpack.EnvironmentPlugin({
				NODE_ENV: 'production'
			}),

			new CompressionPlugin()
		]
	})
}

