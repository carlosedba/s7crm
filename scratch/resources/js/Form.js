const HMIForm = window.HMIForm = function () {
  console.log('log > HMIForm - initialized!')

  this.Event = new window.XEvent()
  this.Event.addHandler('handleSubmit', this.handleSubmit, this)

  document.addEventListener('DOMContentLoaded', this.render.bind(this))
  //window.addEventListener('resize', this.render.bind(this))
}

HMIForm.prototype.countEntries = function (params) {
  let entries = params.entries()
  let i = 0

  while (entries.next().value !== undefined) {
    i++
  }

  return i
}

HMIForm.prototype.validate = function (form, params) {
  let entriesNumber = this.countEntries(params)
  let entries = params.entries()

  for (let i = 0; i < entriesNumber; i++) {
    let pair = entries.next().value
    let input = form.querySelector('[name="' + pair[0] + '"]')
    let value = input.value

    if (input.dataset.type !== 'Estado') {
      if (input.value === '') return false
    }
  }

  return true
}

HMIForm.prototype.handleSubmit = function (event) {
  event.preventDefault()

  let form = document.querySelector('form')
  let inputs = form.querySelectorAll('[name]')
  let params = new URLSearchParams()

  ;[].forEach.call(inputs, function (el, i) {
    params.append(el.name, el.value)
  })

  if (this.validate(form, params)) {
    Loading.activate()
    axios.post(form.action, params)
      .then(function (res) {
        form.reset()
        Loading.deactivate()
        setTimeout(function () { alert('Formulário enviado com sucesso.') }, 240)
      })
      .catch(function (err) {
        Loading.deactivate()
        setTimeout(function () { alert('Ocorreu um erro. Tente novamente mais tarde')} )
      })
  } else {
    alert('Preencha os campos obrigatórios')
  }
}

HMIForm.prototype.render = function (event) {
  console.log('log > HMIForm - render called!')

  if (Utils.shouldRender(this.windowWidth, event)) {
    console.log('log > HMIForm - render approved!')

    let btnSubmit = document.querySelector('form .btn')

    if (btnSubmit) this.Event.addTo(btnSubmit, 'click', 'handleSubmit')
  }
}

;(function (Application) { new Application() })(window.HMIForm)

