const Utils = window.Utils = {
	isInt: function (num) {
		if (num % 1 === 0) {
			return true
		} else if (num % 1 !== 0) {
			return false
		}
	},

	extend: function (a, b) {
		for (var i in b) {
			a[i] = b[i]
		}
	},

	getQueryParam: function (param) {
		let query = window.location.search.substring(1)
		let vars = query.split('&')

		for (let i = 0; i < vars.length; i++) {
			let pair = vars[i].split('=')

			if (decodeURIComponent(pair[0]) == param) {
				return decodeURIComponent(pair[1])
			}
		}
	},

	shouldRender: function (storedWidth, event) {
		if (event.type === 'DOMContentLoaded' || (event.type === 'resize' && window.innerWidth !== storedWidth)) {
			return true
		} else {
			return false
		}
	},

	getElementPath: function (element) {
		if (element == null) return []

		let pathArr = [element]

		while(element.parentElement != null) {
			element = element.parentElement
			pathArr.unshift(element)
		}

		return pathArr
	},

	getMaxOfArray: function (numArray) {
	    return Math.max.apply(null, numArray)
	},

	getMinOfArray: function (numArray) {
	    return Math.min.apply(null, numArray)
	},
}